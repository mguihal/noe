// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

const editUrl = "https://gitlab.com/alternatiba/noe/-/tree/master/website/";

/** @type {import("@docusaurus/types").Config} */
const config = {
  title: "NOÉ",
  tagline: "Organisez votre événement participatif en un clic avec NOÉ.",
  url: process.env.REACT_APP_WEBSITE_URL || "https://get.noe-app.io",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "favicons/favicon.ico",

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "fr",
    locales: ["fr", "en"],
    localeConfigs: {
      en: {label: "English"},
      fr: {label: "Français"},
    },
  },

  customFields: {
    CONTACT_US_EMAIL: process.env.REACT_APP_CONTACT_US_EMAIL,
  },

  presets: [
    [
      "classic",
      /** @type {import("@docusaurus/preset-classic").Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebarsDocs.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl,
          editLocalizedFiles: true,
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl,
          editLocalizedFiles: true,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.less"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import("@docusaurus/preset-classic").ThemeConfig} */
    ({
      image: "img/common/logo-social-card-colors-on-blue.png", // Image for social cards sharing
      colorMode: {
        defaultMode: "light",
        disableSwitch: true,
        respectPrefersColorScheme: false,
      },
      navbar: {
        title: "NOÉ",
        hideOnScroll: true,
        logo: {
          alt: "NOÉ logo",
          src: "img/common/logo-colors.svg",
          width: 30,
          style: {paddingRight: 5},
        },
        items: [
          // LEFT
          {
            label: "Tutoriel",
            to: "/docs",
            position: "left",
          },

          {
            label: "Traduire NOÉ 🌍",
            to: "/community/translate",
            activeBaseRegex: `/community/`,
            position: "left",
          },
          {
            label: "Blog",
            to: "/blog",
            position: "left",
          },

          // RIGHT
          {
            type: "search",
            position: "right",
          },
          {
            label: "Faire un don",
            href: "https://opencollective.com/noeappio",
            position: "right",
          },
          {
            label: "GitLab",
            href: "https://gitlab.com/alternatiba/noe",
            position: "right",
            className: "header-gitlab-link",
          },
          {
            type: "localeDropdown",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        logo: {
          alt: "NOÉ logo",
          src: "img/common/logo-colors-with-white-text.svg",
          style: {paddingTop: 30, width: 200, opacity: 0.7},
        },
        links: [
          {
            title: "learn",
            items: [
              {
                label: "introduction",
                to: "/docs",
              },
            ],
          },
          {
            title: "community",
            items: [
              {
                label: "Traduire NOÉ 🌍",
                to: "/community/translate",
                activeBaseRegex: `/community/`,
              },
              {
                label: "Telegram",
                href: "https://t.me/noeappio",
              },
              {
                label: "Facebook",
                href: "https://facebook.com/noeappio",
              },
              {
                label: "Instagram",
                href: "https://instagram.com/noeappio",
              },
              {
                label: "Open Collective",
                href: "https://opencollective.com/noeappio",
              },
            ],
          },
          {
            title: "more info",
            items: [
              {
                label: "Blog",
                to: "/blog",
              },
              {
                label: "GitLab",
                href: "https://gitlab.com/alternatiba/noe",
              },
              {
                label: "contact us",
                href: "mailto:" + process.env.REACT_APP_CONTACT_US_EMAIL,
              },
            ],
          },
        ],
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),

  plugins: [
    "docusaurus-plugin-less",
    [
      "content-docs",
      /** @type {import('@docusaurus/plugin-content-docs').Options} */
      ({
        id: "community",
        path: "community",
        routeBasePath: "community",
        editUrl,
        editLocalizedFiles: true,
        editCurrentVersion: true,
        sidebarPath: require.resolve("./sidebarsOther.js"),
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      }),
    ],
    [
      "ideal-image",
      /** @type {import('@docusaurus/plugin-ideal-image').PluginOptions} */
      ({
        quality: 70,
        max: 1030,
        min: 640,
        steps: 2,
        disableInDev: true,
      }),
    ],
    [
      "pwa",
      {
        offlineModeActivationStrategies: ["appInstalled", "standalone", "queryString"],
        pwaHead: [
          {
            tagName: "link",
            rel: "icon",
            href: "img/favicons/android-chrome-192x192.png",
          },
          {
            tagName: "link",
            rel: "manifest",
            href: "manifest.json",
          },
          {
            tagName: "meta",
            name: "theme-color",
            content: "#000A2E",
          },
        ],
      },
    ],
    require.resolve("docusaurus-lunr-search"),
  ],
};

module.exports = config;
