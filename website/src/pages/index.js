import React, {useEffect} from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import Layout from "@theme/Layout";
import FeaturesGrid from "@site/src/components/FeaturesGrid";
import styles from "./index.module.css";
const Logo = require("@site/static/img/common/logo-white-with-white-text.svg").default;
import Translate, {translate} from "@docusaurus/Translate";
import SendEmailButton from "@site/src/components/SendEmailButton";

function HeroHeader() {
  useEffect(() => {
    const canTypewrite = window.screen.width >= 1443;
    const taglineElement = document.getElementById("tagline");

    if (canTypewrite) {
      const caret = "<strong>|</strong>";
      const typingSpeed = 40; // time delay of print out

      function typewriter(fullText, element, charPosition = 0) {
        const shouldDisplayCaret = charPosition + 1 < fullText.length;
        const displayText = fullText.slice(0, charPosition) + (shouldDisplayCaret ? caret : "");
        element.innerHTML = displayText.replace(/\n/g, "<br/>");
        charPosition++;
        if (charPosition <= fullText.length) {
          setTimeout(() => typewriter(fullText, element, charPosition), typingSpeed);
        }
      }

      typewriter(translate({id: "homepage.tagline"}), document.getElementById("tagline"));
    } else {
      taglineElement.innerHTML = translate({id: "homepage.tagline"});
    }
  }, []);

  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <div className={clsx("row", styles.reverseFlexOnMobile)}>
          <div
            className={clsx("col col--6", styles.centerVertically, styles.taglineContainer)}
            style={{width: "100%"}}>
            <h1 className={styles.tagline} id="tagline"></h1>
          </div>
          <div className={clsx("col col--6", styles.centerVertically)}>
            <Logo border="0" className={styles.logo} />
          </div>
        </div>
        <div className={styles.buttons} style={{flexDirection: "column", gap: 10}}>
          <Link
            className={clsx(
              styles.heroButton,
              "button button--secondary button--outline button--lg"
            )}
            to="/docs">
            <Translate id="homepage.heroButton" />
          </Link>
        </div>
      </div>
    </header>
  );
}

function CallToActionBanner({containerStyle, text, extra}) {
  return (
    <section style={{padding: "3rem 0.5rem", background: "#f5f5f5"}}>
      <div className="container" style={containerStyle}>
        <div className="row" style={{justifyContent: "center"}}>
          <div className={clsx("col", styles.callToActionText)}>{text}</div>
        </div>

        {extra}
      </div>
    </section>
  );
}

function SummaryAndLinks() {
  return (
    <CallToActionBanner
      containerStyle={{maxWidth: "45rem"}}
      text={
        <>
          <p>
            <Translate id="homepage.summary" />
          </p>
          <Translate id="homepage.summaryOpensource" />
        </>
      }
      extra={
        <div className={styles.buttons} style={{gap: "1rem 2rem", flexWrap: "wrap"}}>
          <Link className="button button--primary button--outline" href="https://orga.noe-app.io">
            <Translate id="orgaFront" /> 🎪
          </Link>
          <Link className="button button--primary button--outline" href="https://noe-app.io">
            <Translate id="inscriptionFront" /> 🙋
          </Link>
        </div>
      }
    />
  );
}

function ContactUsBanner() {
  return (
    <CallToActionBanner
      containerStyle={{maxWidth: "45rem"}}
      text={<Translate id="homepage.contactUs" />}
      extra={
        <div className={styles.buttons} style={{gap: "1.3rem 2rem", flexDirection: "column"}}>
          <SendEmailButton className="button--primary button--lg" />
          <Link className="button button--secondary" href="https://t.me/noeappio">
            <Translate id="homepage.contactUs.joinTelegramChannel" />
          </Link>
        </div>
      }
    />
  );
}

export default function Home() {
  return (
    <Layout
      title={translate({id: "homepage.tagline"})}
      description={translate({id: "homepage.summary"})}>
      <HeroHeader />
      <SummaryAndLinks />
      <FeaturesGrid />
      <ContactUsBanner />
    </Layout>
  );
}
