import React, {useState} from "react";
import clsx from "clsx";
import styles from "./styles.module.css";
import Translate, {translate} from "@docusaurus/Translate";

const FeatureList = [
  {
    title: translate({id: "homepage.features.1.title"}),
    Svg: require("@site/static/img/undraw_schedule_re_2vro.svg").default,
    description: <Translate id="homepage.features.1.description" />,
  },
  {
    title: translate({id: "homepage.features.2.title"}),
    Svg: require("@site/static/img/undraw_welcoming_re_x0qo.svg").default,
    description: <Translate id="homepage.features.2.description" />,
  },
  {
    title: translate({id: "homepage.features.3.title"}),
    Svg: require("@site/static/img/undraw_people_re_8spw.svg").default,
    description: <Translate id="homepage.features.3.description" />,
  },
  {
    title: translate({id: "homepage.features.4.title"}),
    Svg: require("@site/static/img/undraw_projections_re_ulc6.svg").default,
    description: (
      <>
        <Translate id="homepage.features.4.description.main" />
        <ExampleBox>
          <ul style={{marginBottom: 0}}>
            <li>
              <Translate id="homepage.features.6.description.examples.1" />
            </li>
            <li>
              <Translate id="homepage.features.6.description.examples.2" />
            </li>
            <li>
              <Translate id="homepage.features.6.description.examples.3" />
            </li>
            <li>
              <Translate id="homepage.features.6.description.examples.4" />
            </li>
            <li>
              <Translate id="homepage.features.6.description.examples.5" />
            </li>
          </ul>
        </ExampleBox>
      </>
    ),
  },
  {
    title: translate({id: "homepage.features.5.title"}),
    Svg: require("@site/static/img/undraw_going_offline_ihag.svg").default,
    description: <Translate id="homepage.features.5.description" />,
  },
  {
    title: translate({id: "homepage.features.6.title"}),
    Svg: require("@site/static/img/undraw_community_re_cyrm.svg").default,
    description: (
      <>
        <Translate id="homepage.features.6.description.main" />
        <ExampleBox>
          <ul style={{marginBottom: 0}}>
            <li>
              <Translate id="homepage.features.6.description.examples.1" />
            </li>
            <li>
              <Translate id="homepage.features.6.description.examples.2" />
            </li>
          </ul>
        </ExampleBox>
      </>
    ),
  },
];

function ExampleBox({children}) {
  const [open, setOpen] = useState(false);
  return (
    <>
      {" "}
      <span style={{color: "var(--noe-primary)", cursor: "pointer"}} onClick={() => setOpen(!open)}>
        {open ? (
          <Translate id="homepage.features.exampleBox.close" />
        ) : (
          <Translate id="homepage.features.exampleBox.examples" />
        )}
      </span>
      {open && (
        <div
          style={{
            border: "1px solid lightgray",
            borderRadius: 10,
            padding: 10,
            marginTop: 10,
            textAlign: "left",
            fontStyle: "italic",
            color: "gray",
          }}>
          {children}
        </div>
      )}
    </>
  );
}

function Feature({Svg, title, description}) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center" style={{marginTop: 20, marginBottom: 10}}>
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function FeaturesGrid() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
