---
sidebar_position: 1
---

# Présentation

**Découvrons NOÉ en 5 minutes ! 🥳**

## C'est quoi, NOÉ ?

NOÉ est le diminutif de "Nouvelle Organisation d'Événements". C'est un outil **open-source**, destiné aux
organisateur⋅ices d'événements qui souhaitent organizer des événements plus horizontaux, auto-organisés et
participatifs, grâce à un ensemble d'outils qui les aident à toutes les étapes de l'organisation :

- **Déclarez votre programme artistique, musical ou éducatif** directement dans NOÉ. L'application devient votre
  principale source d'information, où vous pouvez stocker le "quoi, où, quand et avec qui" de votre événement.

- **Invitez les participants à votre événement**. NOÉ permet à vos participant⋅es de s'inscrire aux activités, ateliers
  et sessions de volontariat qu'iels souhaitent, de manière totalement libre, même pour les très grands événements.
  Donnez les informations essentielles sur votre page d'accueil, entièrement modifiable avec un éditeur intégré dans
  NOÉ.

- **Intégrez le volontariat, de manière native**. Pour que votre événement se déroule bien, il se peut que vous ayez
  besoin que chaque participant⋅e passe un certain temps à aider en tant que bénévole. Définissez votre fourchette de
  temps de bénévolat acceptable par jour, afin que les participant⋅es soient encouragé⋅es à remplir leur jauge de
  bénévolat. Encouragez les personnes à se porter volontaires pour les bénévolats plus fatiguants, en fixant une
  majoration du temps de volontariat sur les postes qui ont besoin d'un coup de pouce.

- **Surveillez les inscriptions de vos participants et obtenez des informations clés pour votre événement**. Construisez
  votre formulaire d'inscription personnalisé directement dans NOÉ, demandez ce que vous avez besoin de savoir à vos
  participant⋅es. Compilez les résultats et prenez de meilleures décisions pour économiser du temps, de l'énergie et de
  l'argent. Exemples : "combien de personnes mangent sur place pour le déjeuner de demain, quelle quantité de nourriture
  dois-je préparer ?" / "Suis-je sûr d'avoir au moins 2 personnes formées aux premiers secours sur le site, à tout
  moment de l'événement ?" / "Combien de personnes doivent encore se présenter au point d'accueil ce soir ?" / "Combien
  de personnes participent à la formation XYZ à 18h ? Faut-il ajouter des bancs dans la salle ?"

- **Gardez le contact avec vos participant⋅es**. Vous êtes en mesure d'extraire les numéros de téléphone, les emails ou
  toute autre information utile très rapidement, ce qui vous permet de dialoguer facilement avec vos participants,
  d'envoyer des emails ciblés ou des SMS de groupe aux personnes qui en ont besoin. Exemple : envoyer des SMS à toutes
  les personnes qui se sont inscrites à un atelier particulier et dont l'horaire a changé.

## Comprendre NOÉ en vidéo

Cette vidéo montre **tout ce que l'on peut faire avec NOÉ en moins de 20 minutes** ! Elle date de mars 2022 mais elle
est encore d'actualité pour beaucoup de choses.

<iframe
  style={{width: "100%", aspectRatio: "16 / 9"}}
  sandbox="allow-same-origin allow-scripts allow-popups"
  frameborder="0"
  allowfullscreen
  src="https://peertube.virtual-assembly.org/videos/embed/37c55e47-428c-497f-b454-28a16de73041" />

### Quelles fonctionnalités principales ? (liste non-exhaustive !)

:::info

NOÉ est conçu dans une logique modulaire et s'adapte à de nombreuses typologies d'événements : festivals, 
résidences, universités d'été, etc. Pourquoi pas le vôtre ?

:::

Les organisateur⋅ices peuvent :
- Saisir tous les éléments du programme de l'événement (lieux, activités, intervenant⋅es) ainsi que toutes les contraintes associées (disponibilité des personnes, des salles, etc.),
- Réagencer facilement les sessions grâce à une vue agenda qui permet de déplacer, redimensionner, ou cloner les sessions facilement
- Consulter et modifier en deux clics les données d'inscription des participant⋅es
- Créer des équipes de participant⋅es
- Inscrire, ou désinscrire des participant⋅es à des sessions, ou des équipes.
- Et plein d'autres choses...

Les participant⋅es peuvent :
- S'inscrire à l'événement,
- Choisir les activités auxquelles iels souhaitent prendre part, en recherchant, filtrant, les sessions disponibles,
- Consulter leur emploi du temps facilement, sur PC ou sur mobile.
- Exporter leur planning en PDF
- Et d'autres choses encore...

## Comment le projet NOÉ s'est lancé ?

Le projet NOÉ a été lancé par Alternatiba en 2019. Alternatiba est une association qui milite pour la justice sociale et
climatique. À l'origine, NOÉ a été créé pour aider à organiser les Camps Climat (qui sont des rassemblements de
militant⋅es dans toute la France pour se former sur les questions de climat et de justice sociale, se rencontrer,
partager et fédérer le mouvement climat).

Puis nous avons vu que NOÉ attirait également d'autres acteurs et associations, qui avaient également besoin d'un outil
nativement horizontal pour l'organisation de leurs événements... C'est ainsi que NOÉ a commencé à être utilisé dans des
festivals, et autres séminaires.
