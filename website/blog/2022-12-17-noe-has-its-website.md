---
slug: noe-a-son-site-web
title: NOÉ a son site web !
authors: jules
tags: [hello, site web, docs, documentation]
---

C'est bon ! On l'a fait !

**NOÉ a enfin son site web.** Il permettra d'y héberger une documentation fournie pour les organisateur·ices d'événements
qui souhaitent se lancer avec NOÉ.

Plus d'informations à venir très bientôt !