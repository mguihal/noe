---
sidebar_position: 1
---

# Presentation

**Let's discover NOÉ in 5 minutes! 🥳**

## What is NOÉ ?

NOÉ is the short name for "Nouvelle Organization d'Événements" (which could translate to "New Ways to Organize
Events"). It is an **open-source** tool for event organizators that wish to organize more horizontal, auto-organized and
participative events, through a set of tools that helps during all the steps of the organization:

- **Declare your artistic, musical or educational schedule** directly in NOÉ. It becomes your main source of
  information, where you can store the "what, where, when and with who" information of your event.

- **Invite participants to your event**. NOÉ provides a way for your participants to register to activities, workshops
  and volunteering sessions that they want, totally freely, even in very big events. Give essential information in
  your welcome page, fully editable with a built-in editor, inside NOÉ.

- **Integrate volunteering, natively**. For your event to run perfectly, you may need each participant to spend some
  time helping as a volunteer. Set your desired range of acceptable volunteering time per day, so participants get
  encouraged to fill their volunteering gauge. Encourage people to volunteer in more exhausting volunteerings by
  setting a volunteering majoration on the shifts that need a boost.

- **Monitor your participants' registrations and get key insights from them**. Build your custom registration form
  directly inside NOÉ, ask what you need to know to your participants. Compile results and make better decisions
  overall to spare time, effort and money. Examples: "how many people are eating on site for tomorrow's lunch, how
  much food should I prepare ?" / "Am I sure that I have at least 2 people with first aid training on site, anytime in
  the event ?" / "How many people are still to come at the welcome point this evening ?" / “How many people are
  attending the formation XYZ at 6pm ? Should we add more benches ?”

- **Keep in touch with your participants**. You’re able to extract phone numbers, emails or any other useful
  information really quickly, so you can talk easily with your participants, send targeted group emails or group text
  messages to people who need it. Ex: send text messages to all people who registered a particular workshop whose
  schedule has changed.

## Understanding NOÉ in video

This video shows **everything you can do with NOE in less than 20 minutes**! It dates from March 2022 but it is still relevant for many things.

<iframe
  style={{width: "100%", aspectRatio: "16 / 9"}}
  sandbox="allow-same-origin allow-scripts allow-popups"
  frameborder="0"
  allowfullscreen
  src="https://peertube.virtual-assembly.org/videos/embed/37c55e47-428c-497f-b454-28a16de73041" />

### What are the main features? (a non-exhaustive list!)

:::info

NOÉ is designed in a modular way and can be adapted to many types of events: festivals
residencies, summer universities, etc. Why not yours?

:::

Organizers can :
- Enter all elements of the event program (venues, activities, speakers) as well as all associated constraints (availability of people, rooms, etc.),
- Easily rearrange sessions thanks to an agenda view that allows you to move, resize, or clone sessions easily
- Consult and modify in two clicks the registration data of participants
- Create teams of participants
- Register, or unregister participants to sessions, or teams.
- And many other things...

Participants can:
- Register for the event,
- Choose the activities they want to take part in, by searching, filtering, the available sessions,
- Consult their schedule easily, on PC or mobile.
- Export their schedule in PDF
- And much more...

## How did the NOÉ project get started?

The NOÉ project was launched by Alternatiba in 2019. Alternatiba is a social and climate justice association.
climate justice. Originally, NOÉ was created to help organise Climate Camps (which are gatherings of
activists throughout France to learn about climate and social justice issues, meet each other
share and federate the climate movement).

Then we saw that NOÉ was also attracting other actors and associations, who also needed a natively horizontal tool for organising
natively horizontal tool for the organisation of their events... This is how NOÉ started to be used in
festivals, and other seminars.
