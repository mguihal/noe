import {ConfigProvider} from "antd";

export default {
  token: {
    colorPrimary: "#3775e1",
    colorTextBase: "#000a2e",
    wireframe: false,
    borderRadius: 10,
    colorBorderSecondary: "#e7e7e7",
  },
  components: {
    Dropdown: {
      borderRadiusLG: 14,
    },
    Button: {
      borderRadius: 10,
    },
    Tag: {
      borderRadiusSM: 8,
    },
    Pagination: {
      borderRadiusLG: 12,
      borderRadius: 10,
    },
    Card: {
      fontWeightStrong: 500,
    },
    Table: {
      borderRadiusLG: 0,
    },
    Modal: {
      fontWeightStrong: 500,
      wireframe: true,
    },
  },
};

export const setAppTheme = ({bg, accent1, accent2} = {}) => {
  const theme = {
    "--noe-bg": bg || null,
    "--noe-accent-1": accent1 || null,
    "--noe-accent-1-90": accent1 || null,
    "--noe-accent-1-85": accent1 || null,
    "--noe-accent-2": accent2 || null,
    "--noe-accent-2-85": accent2 || null,
  };
  const setVariables = (vars) => {
    const bodyStyle = document.getElementsByTagName("html")[0].style;
    Object.entries(vars).forEach((v) =>
      v[1] !== null ? bodyStyle.setProperty(v[0], v[1]) : bodyStyle.removeProperty(v[0])
    );
  };
  setVariables(theme);
};

export const DynamicThemeProvider = ({currentProject, children}) => {
  if (currentProject.theme) setAppTheme(currentProject.theme);
  const primaryColor = currentProject?.theme?.primary || currentProject?.theme?.bg;

  return (
    <ConfigProvider
      theme={
        primaryColor && {
          token: {
            colorPrimary: primaryColor,
            colorLink: primaryColor,
          },
        }
      }>
      {children}
    </ConfigProvider>
  );
};
