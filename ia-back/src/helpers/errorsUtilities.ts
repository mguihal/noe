import * as Sentry from "@sentry/node";
import {NextFunction, Request, Response, RequestHandler} from "express";

export function initSentry(app: any) {
  Sentry.init({
    debug: true,
    environment: process.env.REACT_APP_MODE,
    dsn: process.env.SENTRY_DSN,
  });

  app.use(Sentry.Handlers.requestHandler() as RequestHandler);

  // TracingHandler creates a trace for every incoming request
  // app.use(Sentry.Handlers.tracingHandler());
}

// Router function wrapper. We encapsulate them into that so the errors created can be redirected in the
// middleware pipeline, and be captured by Sentry
export function handleErrors(func: any) {
  return async function (req: Request, res: Response, next: NextFunction) {
    try {
      await func(req, res);
    } catch (e) {
      next(e);
    }
  };
}

export function globalErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): void {
  // Check if it is a known error, and return if that's the case
  let status;
  if (err.name === "VersionError") status = 409; // Mongoose version conflict

  status = status || 500; // If status hasn't been set, make it 500

  console.error(`=============== ${status} ERROR ===============`);
  let eventId;
  try {
    // Send the error to Sentry with all the necessary information
    eventId = Sentry.captureException(err, {
      extra: {
        "a - URL": `${req.method} ${req.url}`,
        "b - Origin": req.headers?.origin,
        "c - User": "-",
        "d - authenticatedUser": "-",
        "e - Request params": req.params,
        "f - Request query": req.query,
        "g - Request body": req.body,
        "h - Full request": req,
      },
    });
    // Display the error ID in the logs
    // console.log("Event reported in Sentry:", eventId);
  } catch (e) {
    console.log("Problem with Sentry. Error not reported.");
  }

  console.error(err); // Display the trace in the logs, just in case
  console.error(`=========================================`);
  res.status(status).end(eventId); // Return status with the Sentry eventId
}
