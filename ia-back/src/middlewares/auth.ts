const fetch = require("node-fetch");

const authMiddlware = async (req: any, res: any, next: any) => {
  try {
    const headers = {authorization: req.headers.authorization};
    const response = await fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/auth/me`, {
      headers: headers,
    });

    const user = await response.json();
    req.user = user;
    next();
  } catch (e) {
    console.log("ERROR", e);
    return next(e);
  }
};

export const isUserSetBySelf = authMiddlware;
