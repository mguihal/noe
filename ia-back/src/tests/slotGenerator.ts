const SltsGenerator = require("../computing/slotsGenerator");
const assert = require("assert");
const {existingSessions, candidate, config, resources} = require("./slotGeneratorData");
const fs = require("fs");
const {parseData} = require("./utils");

const addDays = (date: any, days: any) => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

describe("Basic test", () => {
  it("Should create as many slots as requested", function () {
    const slotHandler = new SltsGenerator();
    const result = slotHandler.generate(candidate, resources, config);
    // console.log('Res:', result)
    assert.strictEqual(result.length, candidate.occurrence);
  });
});

describe("Frozen time", () => {
  it("Should keep frozen sessions not generate slotsAvaible", () => {
    existingSessions[0].isScheduleFrozen = true;
    const newStart = addDays(existingSessions[0].start, 1);
    const newEnd = addDays(existingSessions[0].end, 1);
    existingSessions[0].start = newStart;
    existingSessions[0].end = newEnd;
    const slotHandler = new SltsGenerator();
    candidate.existingSessions = existingSessions;
    resources.existingSessions = existingSessions;
    const result = slotHandler.generate(candidate, resources, config);
    assert.strictEqual(result.length, candidate.occurrence);
    assert(result.some((res: any) => res.slotsAvaible === undefined));
  });
});

describe("Slots should respect daily opening", () => {
  const availabilitySlots = [
    {
      _id: "6040bed0d047160035cec57e",
      start: new Date("2021-01-08T07:00:05.000Z"),
      end: new Date("2021-01-08T15:00:05.000Z"),
    },
    {
      _id: "6040bed0d047160035cec57d",
      start: new Date("2021-01-09T09:00:50.000Z"),
      end: new Date("2021-01-09T15:00:00.000Z"),
    },
    {
      _id: "6040bed0d047160035cec57c",
      start: new Date("2021-01-10T07:00:19.000Z"),
      end: new Date("2021-01-10T15:00:00.000Z"),
    },
  ];
  const slotHandler = new SltsGenerator();
  const result = slotHandler.filterOutBadRooms(
    resources.roomAvailability,
    candidate,
    availabilitySlots
  );

  it("Should not open before daily opening", () => {
    const firstRoomDay1 = result[0];
    const firstRoomDay2 = result[1];
    const secondRoomDay2 = result[4];
    assert.strictEqual(
      firstRoomDay1.startAvailability.getHours(),
      availabilitySlots[0].start.getHours()
    );
    assert.strictEqual(
      firstRoomDay2.startAvailability.getHours(),
      availabilitySlots[1].start.getHours()
    );
    assert.strictEqual(
      secondRoomDay2.startAvailability.getHours(),
      availabilitySlots[1].start.getHours()
    );
    assert.strictEqual(
      firstRoomDay2.endAvailability.getHours(),
      availabilitySlots[1].end.getHours()
    );
    assert.strictEqual(
      secondRoomDay2.endAvailability.getHours(),
      availabilitySlots[1].end.getHours()
    );
    // console.log(result)
  });

  // TODO update the tests and understand what they stand for.
  //  Fuckin comment your fuckin tests please <3
  // it("check for slot handling", () => {
  //   config.project.availabilitySlots = [
  //     {
  //       _id: "6040bed0d047160035cec57e",
  //       start: new Date("2021-01-08T07:00:00.000Z"),
  //       end: new Date("2021-01-08T17:00:00.000Z"),
  //     },
  //     {
  //       _id: "6040bed0d047160035cec57d",
  //       start: new Date("2021-01-09T09:30:00.000Z"),
  //       end: new Date("2021-01-09T17:00:00.000Z"),
  //     },
  //     {
  //       _id: "6040bed0d047160035cec57c",
  //       start: new Date("2021-01-10T07:00:00.000Z"),
  //       end: new Date("2021-01-10T17:00:00.000Z"),
  //     },
  //   ];
  //   const results = slotHandler.generate(candidate, resources, config);
  //   const dayResult = results
  //     .filter((result: any) => result.slotsAvaible)
  //     .map((result: any) =>
  //       result.slotsAvaible.filter((elem: any) => elem.startStepTime.getDate() == 9)
  //     );
  //   const startTimes = dayResult
  //     .map((room: any) => room.map((result: any) => result.startStepTime))
  //     .reduce((acc: any, val: any) => acc.concat(val), []);
  //   const endTimes = dayResult
  //     .map((room: any) => room.map((result: any) => result.endStepTime))
  //     .reduce((acc: any, val: any) => acc.concat(val), []);
  //
  //   assert.strictEqual(
  //     new Date(Math.min(...startTimes)).getTime(),
  //     config.project.availabilitySlots[1].start.getTime()
  //   );
  //   assert(
  //     new Date(Math.max(...startTimes)).getTime() <
  //       config.project.availabilitySlots[1].end.getTime()
  //   );
  // });

  it("Should not keep unavailable rooms", () => {
    const availabilitySlots = [
      {
        _id: "6040bed0d047160035cec57e",
        start: new Date("2021-01-08T07:00:05.000Z"),
        end: new Date("2021-01-08T15:00:05.000Z"),
      },
      {
        _id: "6040bed0d047160035cec57d",
        start: new Date("2021-01-09T18:00:50.000Z"),
        end: new Date("2021-01-09T21:00:00.000Z"),
      },
      {
        _id: "6040bed0d047160035cec57c",
        start: new Date("2021-01-10T07:00:19.000Z"),
        end: new Date("2021-01-10T15:00:00.000Z"),
      },
    ];
    const slotHandler = new SltsGenerator();
    const results = slotHandler.filterOutBadRooms(
      resources.roomAvailability,
      candidate,
      availabilitySlots
    );
    assert.strictEqual(
      results.filter((result: any) => result.startAvailability.getDate() == 9).length,
      0
    );
  });

  it("Two availabilities per day", () => {
    const availabilitySlots = [
      {start: new Date("2021-01-08T06:00:00.000Z"), end: new Date("2021-01-08T10:00:00.000Z")},
      {start: new Date("2021-01-08T11:30:00.000Z"), end: new Date("2021-01-08T17:00:00.000Z")},
    ];
    const roomAvailability = [
      {
        startAvailability: new Date("2021-01-08T07:00:00.000Z"),
        endAvailability: new Date("2021-01-08T12:00:00.000Z"),
      },
      {
        startAvailability: new Date("2021-01-08T14:00:00.000Z"),
        endAvailability: new Date("2021-01-08T17:00:00.000Z"),
      },
    ];
    const slotHandler = new SltsGenerator();
    const results = slotHandler.filterOutBadRooms(roomAvailability, candidate, availabilitySlots);
    assert.strictEqual(results.length, roomAvailability.length);
  });
});

describe("test", function () {
  const candidates = parseData(__dirname + "/candidate.json");
  const resources = parseData(__dirname + "/resources.json");
  const config = parseData(__dirname + "/config.json");

  it("Should output 2 possibilities", () => {
    const slotHandler = new SltsGenerator();
    const result = slotHandler.generate(candidates, resources, config);
    console.log(result);
    result.map((r: any) => {
      console.log(r.slotsAvaible);
    });
  });
});
