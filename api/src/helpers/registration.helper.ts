export const flatFormInputs = (components: Array<any>) => {
  if (!components) return [];

  const tree: Array<any> = components.map((comp) => {
    if (comp.input) {
      // If the component is a simple one, return it
      return comp;
    } else {
      let branchs: Array<any> = [];
      // Put together all child components present in the "components" and "columns" arguments, and then apply recursively the flatFormInputs function on it
      if (comp.components) branchs = branchs.concat(comp.components);
      if (comp.columns) branchs = branchs.concat(comp.columns);
      return flatFormInputs(branchs);
    }
  });
  // Flatten the return value
  return tree.flat();
};

export const cleanAnswer = (
  answer: any,
  separator = ",\n",
  flatFormComponents?: any,
  formField?: any
) => {
  const formComp =
    formField &&
    flatFormComponents &&
    flatFormComponents.find((comp: any) => comp.key === formField);
  const options = formComp?.values || formComp?.data?.values || {};
  console.log(formComp?.type, options, answer);

  const findLabelForOption = (optValue: any) =>
    (options?.find && options.find((opt: any) => opt.value === optValue)?.label) || optValue;

  if (formComp?.type === "selectboxes") {
    const formattedAnswer = Array.isArray(answer)
      ? // New form
        answer
      : // Legacy Formio
        answer &&
        Object.entries(answer)
          .filter((entry) => entry[1])
          .map((entry) => entry[0]);
    return formattedAnswer?.map(findLabelForOption).join(separator);
  }

  if (formComp?.type === "select" || formComp?.type === "radio") {
    return findLabelForOption(answer);
  }

  return answer;
};
