import {Document, model, ObjectId, Schema, SchemaTypes, Types} from "mongoose";
import {UserD} from "./user.model";
import {Project, ProjectD} from "./project.model";
import {StewardD} from "./steward.model";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import {sessionMethods, Session, SessionD} from "./session.model";
import moment from "moment";
import {addDiffHistory, Lean, preventDuplicatesInProject, softDelete} from "../config/mongoose";
import {TeamD} from "./team.model";
import {HelloAssoTicket, Ticket} from "./ticketing.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export interface FormMapping {
  formField: string;
  columnName: string;
  inPdfExport?: boolean;
  isPhone?: boolean;
  defaultTextMessage?: string;
}

interface DaysOfPresence {
  start: string;
  end: string;
  breakfast: boolean;
  lunch: boolean;
  dinner: boolean;
  projectDay: boolean;
}

export type SessionSubscriptionD = Document & {
  subscribedBy: UserD;
  session: SessionD;
  hasNotShownUp?: boolean | UserD;
  team?: TeamD; // If filled, it means that the session subscription has been created because of the team registration
};
const SessionSubscriptionSchema = new Schema<SessionSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    session: {type: SchemaTypes.ObjectId, ref: "Session", required: true},
    hasNotShownUp: {type: SchemaTypes.ObjectId, ref: "User"}, // Save the ref of the user who checked the person as checkedIn
    // Redundant info to tell that the session subscription was subscribed
    // either on its own (no team), or subscribed via a team subscription.
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
  },
  {timestamps: true}
);

type TeamSubscriptionD = Document & {
  subscribedBy: UserD;
  team: TeamD;
};
const TeamSubscriptionSchema = new Schema<TeamSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    team: {type: SchemaTypes.ObjectId, ref: "Team", required: true},
  },
  {timestamps: true}
);

/**
 * DEFINITION
 */
export type RegistrationD = Document & {
  // Entities
  user: UserD;
  project: ProjectD;
  steward: StewardD;

  // Project role
  role: string;

  // Invitation data
  invitationToken: string;

  // Registration
  booked: boolean;
  specific: Record<string, unknown>;
  sessionsSubscriptions: Array<SessionSubscriptionD>;
  teamsSubscriptions: Array<TeamSubscriptionD>;
  availabilitySlots: Array<AvailabilitySlotD>;
  hasCheckedIn: {type: boolean; default: false};

  // Fields for the orga team
  notes: string;
  tags: Array<string>;

  // Ticketing
  helloAssoTickets: Array<HelloAssoTicket>;
  customTicketingTickets: Array<Ticket>;

  // Tells if the registration is hidden from public, and is only synced to the project
  hidden: boolean;

  // Computed stuff
  numberOfDaysOfPresence: number;
  daysOfPresence: Array<DaysOfPresence>;

  deletedAt: Date;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const RegistrationSchema = new Schema<RegistrationD>(
  {
    user: {type: SchemaTypes.ObjectId, ref: "User", required: true},
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    steward: {type: SchemaTypes.ObjectId, ref: "Steward"},

    role: String,

    invitationToken: String,

    booked: Boolean,
    specific: Object,
    sessionsSubscriptions: [SessionSubscriptionSchema],
    teamsSubscriptions: [TeamSubscriptionSchema],
    availabilitySlots: [AvailabilitySlotSchema],
    hasCheckedIn: {type: Boolean, default: false},

    notes: String,
    tags: [String],

    helloAssoTickets: [Object],
    customTicketingTickets: [Object],

    hidden: Boolean,

    numberOfDaysOfPresence: Number,
    daysOfPresence: [Object],

    deletedAt: Date,
  },
  {
    timestamps: true,
  }
);

/**
 * HOOKS
 */
RegistrationSchema.pre(["save", "updateOne"], async function (next) {
  const registration = this as RegistrationD;
  await registrationMethods.computeDaysOfPresence(registration);
  next();
});

/**
 * INSTANCE METHODS
 */
export const registrationMethods = {
  async withVoluntaryCounter(
    registration: RegistrationD
  ): Promise<Lean<RegistrationD> & {voluntaryCounter: number}> {
    // The volunteeringCounter is an async calculus that cannot be done in a
    // Mongoose getter function (cause getters can't be async).

    // The steward can be undefined
    const {steward} = registration;

    // TODO use the slots instead cause you can be steward only on one slot of a session
    const sessionsSubscriptions = await Session.find({
      $or: [
        {_id: {$in: registration.sessionsSubscriptions.map((ss) => ss.session)}},
        {stewards: steward?._id},
      ],
    }).populate([{path: "activity"}, {path: "slots"}]);

    // TODO this doesn't take in account the case of un-synchronized slot stewards
    // Compute the weighted volunteering time in all session for the current user
    const weightedTime = sessionsSubscriptions
      .map((session) => {
        const isStewardForThisSession = session.stewards.toString().includes(steward?._id);
        const volunteeringCoefficient = isStewardForThisSession
          ? session.activity.stewardVolunteeringCoefficient || 0
          : sessionMethods.getSessionVolunteeringCoefficient(session) || 0;
        const duration =
          session.slots.reduce((durationSum, slot) => durationSum + slot.duration, 0) || 0;
        return volunteeringCoefficient * duration;
      })
      .reduce(
        (totalWeightedTime, sessionWeightedTime) => totalWeightedTime + sessionWeightedTime,
        0
      );

    // Compute numberOfDaysOfPresence if not computed already
    if (
      (!registration.daysOfPresence || registration.daysOfPresence.length === 0) &&
      registration.availabilitySlots?.length > 0
    ) {
      await registrationMethods.computeDaysOfPresence(registration);
    }

    // To avoid division by 0, make the check before
    return {
      ...registration.toObject(),
      voluntaryCounter:
        registration.numberOfDaysOfPresence === 0
          ? 0
          : weightedTime / registration.numberOfDaysOfPresence,
    };
  },

  async computeDaysOfPresence(registration: RegistrationD): Promise<void> {
    // If there are no days of presence, everything is equal to zero
    if (!registration.availabilitySlots || registration.availabilitySlots.length === 0) {
      registration.numberOfDaysOfPresence = 0;
      registration.daysOfPresence = [];
      return;
    }

    // Get the project
    const project = await Project.findByIdOrSlug(registration.project?._id || registration.project);

    // Get meals times from project
    const {breakfastTime, lunchTime, dinnerTime} = project;

    // Convert project dates to Moment dates
    const projectStart = moment(project.start);
    const projectEnd = moment(project.end);
    const projectSlots = project.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));

    // Convert the registration slots to moment dates
    const registrationSlots = registration.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));

    // Get the earliest and latest presence time
    const registrationStart: moment.Moment = registrationSlots.reduce(
      (acc: moment.Moment, slot) => (acc !== undefined ? moment.min(acc, slot.start) : slot.start),
      undefined
    );
    const registrationEnd: moment.Moment = registrationSlots.reduce(
      (acc: moment.Moment, slot) => (acc !== undefined ? moment.max(acc, slot.end) : slot.end),
      undefined
    );

    // Total days of presence of the user, no matter the project dates
    const totalDaysOfPresenceForRegistration =
      registrationEnd.clone().startOf("day").diff(registrationStart.clone().startOf("day"), "day") +
      1;

    // Compute the real days of presence on site: iterate through each day and get data
    const daysOfPresenceOnSite = [];
    for (let dayIndex = 0; dayIndex < totalDaysOfPresenceForRegistration; dayIndex++) {
      // Create the current day object
      const currentDayStart = registrationStart.clone().startOf("day");
      currentDayStart.add(dayIndex, "day");
      const currentDayEnd = currentDayStart.clone().endOf("day");

      // Get user availabilitySlots for this day (even if they are on multiple days, if the current days is inside, get it)
      let registrationSlotsThisDay = registrationSlots.filter(
        (slot) =>
          currentDayStart.diff(slot.start.clone().startOf("day"), "day") >= 0 &&
          currentDayStart.diff(slot.end.clone().startOf("day"), "day") <= 0
      );

      // If no slots on this day, don't add this day to the days of presence, and skip it
      // If there are slots on this day, compute the breakfastTime, lunchTime and dinnerTime and all the rest
      if (registrationSlotsThisDay.length > 0) {
        // Truncate multi-days slots with the begging and the end of the current day
        registrationSlotsThisDay = registrationSlotsThisDay.map((slot) => ({
          start: moment.max(currentDayStart.clone(), slot.start),
          end: moment.min(currentDayEnd.clone(), slot.end),
        }));

        const [userEatsForBreakfastThisDay, userEatsForLunchThisDay, userEatsForDinnerThisDay] = [
          breakfastTime,
          lunchTime,
          dinnerTime,
        ].map((mealTime) => {
          // Build the real lunchTime and dinnerTime dates for this current day
          const mealTimeThisDay = moment(mealTime);
          mealTimeThisDay.year(currentDayStart.year());
          mealTimeThisDay.month(currentDayStart.month());
          mealTimeThisDay.date(currentDayStart.date());

          // If the project ends before or start after the meal times, then it means the user will not eat this meal
          const projectEndsBeforeOrStartsAfterMeal =
            projectEnd <= mealTimeThisDay || projectStart >= mealTimeThisDay;

          // Tell if the user eats for lunch or dinner this current day
          return (
            !projectEndsBeforeOrStartsAfterMeal &&
            !!registrationSlotsThisDay.find(
              (slot) => slot.start <= mealTimeThisDay && slot.end > mealTimeThisDay
            )
          );
        });

        const thereIsAProjectSlotOnThisDay = !!projectSlots.find(
          (slot) =>
            currentDayStart.diff(slot.start.startOf("day"), "day") >= 0 &&
            currentDayStart.diff(slot.end.startOf("day"), "day") <= 0
        );

        daysOfPresenceOnSite.push({
          start: registrationSlotsThisDay.reduce(
            (acc: moment.Moment, slot) => (acc ? moment.min(acc, slot.start) : slot.start),
            undefined
          ),
          end: registrationSlotsThisDay.reduce(
            (acc: moment.Moment, slot) => (acc ? moment.max(acc, slot.end) : slot.end),
            undefined
          ),
          breakfast: userEatsForBreakfastThisDay,
          lunch: userEatsForLunchThisDay,
          dinner: userEatsForDinnerThisDay,
          projectDay: thereIsAProjectSlotOnThisDay,
        });
      }
    }

    // A day of presence is a day when the participant eats at least for lunch or for dinner during a project day.
    // A day of presence out of the project days is not counted
    registration.numberOfDaysOfPresence = daysOfPresenceOnSite.filter(
      (p) => (p.lunch || p.dinner) && p.projectDay
    ).length;
    registration.daysOfPresence = daysOfPresenceOnSite.map((d) => ({
      ...d,
      start: d.start?.toISOString(),
      end: d.end?.toISOString(),
    }));
  },
};

/**
 * PLUGINS
 */
RegistrationSchema.plugin(softDelete);
RegistrationSchema.plugin(addDiffHistory, ["user"]);
RegistrationSchema.plugin(preventDuplicatesInProject, [{user: 1}]);

/**
 * MODEL
 */
export const Registration = model<RegistrationD>("Registration", RegistrationSchema);

/**
 * SCHEMA DEFINITIONS
 */
export const allowedBodyArgs = [
  "user",
  "project",
  "steward",
  "role",
  "booked",
  "specific",
  "sessionsSubscriptions",
  "teamsSubscriptions",
  "availabilitySlots",
  "hasCheckedIn",
  "notes",
  "tags",
  "hidden",
  "helloAssoTickets",
  "customTicketingTickets",
];
