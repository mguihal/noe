import {Schema} from "mongoose";
import {MongoEncrypted} from "../utilities/crypto";

export type Ticket = {
  id: string;
  name: string;
  amount: number;
};
export type HelloAssoTicket = Ticket & {
  user: {firstName: string; lastName: string};
  priceCategory: string;
  ticketUrl: string;
};

export type HelloAssoConfig = {
  clientId: string;
  clientSecret: string;
  selectedEvent?: string;
  token?: string;
  tokenExpirationDate?: Date;
  refreshToken?: string;
  organizationSlug?: string;
  selectedEventCandidates?: Array<{
    title: string;
    formSlug: string;
    organizationSlug: string;
  }>;
};
export const HelloAssoConfigSchema = new Schema<HelloAssoConfig>({
  clientId: String,
  clientSecret: String,
  selectedEvent: String,
  token: String,
  tokenExpirationDate: Date,
  refreshToken: String,
  organizationSlug: String,
  selectedEventCandidates: [Object],
});

export type TiBilletConfig = {
  serverUrl: string;
  eventSlug: string;
  apiKey: string | unknown;
};
export const TiBilletConfigSchema = new Schema<TiBilletConfig>(
  {
    serverUrl: String,
    eventSlug: String,
    apiKey: MongoEncrypted,
  },
  {toObject: {getters: true}, toJSON: {getters: true}}
);
