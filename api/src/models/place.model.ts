import {Document, model, Schema, SchemaTypes} from "mongoose";
import {ProjectD} from "./project.model";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongoose";
import {lightSelection} from "../utilities/controllersUtilities";
import {SchemaDescription} from "../utilities/routeUtilities";

/**************************************************************
 *            TYPES
 **************************************************************/

export type PlaceD = Document & {
  name: string;
  project: ProjectD;
  summary: string;
  notes: string;
  availabilitySlots: Array<AvailabilitySlotD>;
  maxNumberOfParticipants: number;

  deletedAt: Date;
  importedId: string;
};

export type LightPlaceD = Pick<PlaceD, keyof typeof lightSelection.place>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const PlaceSchema = new Schema<PlaceD>(
  {
    name: {type: String, required: true},
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    summary: String,
    notes: String,
    availabilitySlots: [AvailabilitySlotSchema],
    maxNumberOfParticipants: Number,

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
PlaceSchema.plugin(softDelete);
PlaceSchema.plugin(addDiffHistory);
PlaceSchema.plugin(indexByIdAndProject);
PlaceSchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Place = model<PlaceD>("Place", PlaceSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription = [
  {key: "name", label: "Nom de l'espace", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Informations"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de personnes"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "availabilitySlots", label: "Disponibilités"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
