import {Document, HydratedDocument, Model, model, QueryWithHelpers, Schema, Types} from "mongoose";
import {FormMapping, RegistrationD} from "./registration.model";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import fetch from "node-fetch";
import AbortController from "abort-controller";
import {addDiffHistory, softDelete} from "../config/mongoose";
import defaultWelcomePageContent from "../config/defaultWelcomePageContent";
import momentTz from "moment-timezone";
import {logger} from "../services/logger";
import {lightSelection} from "../utilities/controllersUtilities";
import {isValidObjectId} from "../config/mongoose";
import {
  HelloAssoConfig,
  HelloAssoConfigSchema,
  TiBilletConfig,
  TiBilletConfigSchema,
} from "./ticketing.model";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const mongooseSlugPlugin = require("mongoose-slug-plugin");

// For reference: https://jasonching2005.medium.com/complete-guide-for-using-typescript-in-mongoose-with-lean-function-e55adf1189dc

/**************************************************************
 *            TYPES
 **************************************************************/

/**
 * DEFINITION
 */
export type ProjectD = Document & {
  name: string;
  slug: string;

  // Form data
  formComponents: Array<unknown>;
  // Columns we want to map, so they are displayed in the participant list
  formMapping: Array<FormMapping>;
  // Welcome page content
  content: unknown;

  // Project opening times
  start: string;
  end: string;
  availabilitySlots: Types.DocumentArray<AvailabilitySlotD>;

  // Project opening state management
  openingState: string;
  isPublic: boolean;
  blockSubscriptions: boolean;
  full: boolean;
  secretSchedule: boolean;

  // Enable features in project
  usePlaces: boolean;
  useTeams: boolean;
  useAI: boolean;

  // Ticketing
  // Mode chosen for the ticketing. If no mode is chosen, it means that
  // the ticketing is not mandatory to register, and is deactivated
  ticketingMode?: string;
  customTicketing?: string; // Validation URL to validate ticket numbers
  helloAsso?: HelloAssoConfig; // Hello Asso credentials
  tiBillet?: TiBilletConfig;

  // Volunteering
  minMaxVolunteering: Array<number>;
  blockVolunteeringUnsubscribeIfBeginsSoon: boolean;

  // Orga notes
  notes: string;

  // Allow time overlap when subscribing to sessions
  notAllowOverlap: boolean;

  // Meal times
  breakfastTime: Date;
  lunchTime: Date;
  dinnerTime: Date;

  // Customization
  theme: {
    bg: string;
    main: string;
    accent1: string;
    accent2: string;
    imageUrl: string;
  };

  deletedAt: Date;
};

export type LightProjectD = Pick<ProjectD, keyof typeof lightSelection.project>;

/**
 * QUERY HELPERS
 */
type ProjectQueryHelpers = {
  findByIdOrSlug<ReturnType = ProjectD>(
    id: string | number | Types.ObjectId,
    ...args: any[]
  ): QueryWithHelpers<HydratedDocument<ReturnType>, HydratedDocument<ReturnType>>;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

const defaultHour = (hour: string) => momentTz.tz(`2000-01-01T${hour}:00`, "Europe/Paris");

/**
 * SCHEMA
 */
const ProjectSchema = new Schema<ProjectD, Model<ProjectD> & ProjectQueryHelpers>(
  {
    name: {type: String, required: true},
    slug: {type: String, required: true},

    formComponents: Object,
    formMapping: [Object],
    content: {type: Object, default: defaultWelcomePageContent, required: true},

    start: String,
    end: String,
    availabilitySlots: [AvailabilitySlotSchema],

    openingState: {
      type: String,
      enum: ["notOpened", "preRegisterOnly", "registerForStewardsOnly", "registerForAll"],
      default: "notOpened",
      required: true,
    },
    isPublic: {type: Boolean, default: false, required: true},
    blockSubscriptions: {type: Boolean, default: false, required: true},
    full: {type: Boolean, default: false, required: true},
    secretSchedule: {type: Boolean, default: false, required: true},

    usePlaces: {type: Boolean, default: true, required: true},
    useTeams: {type: Boolean, default: false, required: true},
    useAI: {type: Boolean, default: false, required: true},

    ticketingMode: {
      type: String,
      enum: [null, "helloAsso", "customTicketing", "tiBillet"],
      default: null,
    },
    helloAsso: HelloAssoConfigSchema,
    tiBillet: TiBilletConfigSchema,
    customTicketing: String,

    minMaxVolunteering: {type: [Number, Number], default: [60, 120]},

    blockVolunteeringUnsubscribeIfBeginsSoon: {type: Boolean, default: false},

    notes: String,

    notAllowOverlap: {type: Boolean, default: false},

    breakfastTime: {type: Date, default: defaultHour("08:30"), required: true},
    lunchTime: {type: Date, default: defaultHour("12:00"), required: true},
    dinnerTime: {type: Date, default: defaultHour("19:30"), required: true},

    // Customization
    theme: {
      type: new Schema({
        bg: {type: String, default: "#000a2e"},
        primary: {type: String, default: "#3775e1"},
        accent1: {type: String, default: "#000bff"},
        accent2: {type: String, default: "#7fffbb"},
        imageUrl: String,
      }),
      default: {bg: "#000a2e", primary: "#3775e1", accent1: "#000bff", accent2: "#7fffbb"},
    },

    deletedAt: Date,
  },
  {
    timestamps: true,
    toJSON: {virtuals: true, getters: true},
    toObject: {virtuals: true, getters: true},
  }
);

/**
 * POPULATED VIRTUALS
 */
// All the registrations linked to the project
ProjectSchema.virtual("registrations", {
  ref: "Registration",
  localField: "_id",
  foreignField: "project",
});

/**
 * QUERY HELPERS
 */
ProjectSchema.statics = {
  findByIdOrSlug(idOrSlug, ...args) {
    const conditions = isValidObjectId(idOrSlug)
      ? {$or: [{slug: idOrSlug}, {_id: idOrSlug}, {slug_history: idOrSlug}]}
      : {$or: [{slug: idOrSlug}, {slug_history: idOrSlug}]};
    return this.findOne(conditions, ...args);
  },
} as ProjectQueryHelpers;

/**
 * INSTANCE METHODS
 */
export const projectMethods = {
  async getHelloAssoAuthTokens(project: ProjectD): Promise<void> {
    if (project.helloAsso?.clientId && project.helloAsso?.clientSecret) {
      try {
        const response = await fetch("https://api.helloasso.com/oauth2/token", {
          method: "POST",
          body: new URLSearchParams({
            grant_type: "client_credentials",
            client_id: project.helloAsso?.clientId,
            client_secret: project.helloAsso?.clientSecret,
          }).toString(),
          headers: {"content-type": "application/x-www-form-urlencoded"},
        });
        const authResult = await response.json();

        if (authResult.error) return;

        project.helloAsso.token = authResult.access_token;
        project.helloAsso.refreshToken = authResult.refresh_token;
        project.helloAsso.tokenExpirationDate = authResult.expires_in
          ? new Date(new Date().getTime() + authResult.expires_in * 1000)
          : undefined; // Set date at expiration time

        const organisationResponse = await fetch(
          "https://api.helloasso.com/v5/users/me/organizations",
          {headers: {Authorization: `Bearer ${authResult.access_token}`}}
        );
        if (organisationResponse.status === 200) {
          const organisationObject = await organisationResponse.json();
          project.helloAsso.organizationSlug = organisationObject[0].organizationSlug;
        } else {
          // If error, reset the Hello Asso stuff
          project.helloAsso.organizationSlug = undefined;
          project.helloAsso.selectedEvent = undefined;
          project.helloAsso.selectedEventCandidates = undefined;
        }
      } catch (error) {
        logger.error(error);
      }
    }
  },

  async refreshHelloAssoAuthToken(project: ProjectD): Promise<void> {
    if (
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret &&
      project.helloAsso?.refreshToken &&
      (!project.helloAsso?.tokenExpirationDate ||
        project.helloAsso?.tokenExpirationDate?.getTime() < new Date().getTime()) // if token has expired only, refresh the token, or if the expiration date doesn't exist
    ) {
      try {
        const response = await fetch("https://api.helloasso.com/oauth2/token", {
          method: "POST",
          body: new URLSearchParams({
            grant_type: "refresh_token",
            client_id: project.helloAsso.clientId,
            refresh_token: project.helloAsso.refreshToken,
          }).toString(),
          headers: {"content-type": "application/x-www-form-urlencoded"},
        });
        const authResult = await response.json();

        project.helloAsso.token = authResult.access_token;
        project.helloAsso.refreshToken = authResult.refresh_token;
        project.helloAsso.tokenExpirationDate = authResult.expires_in
          ? new Date(new Date().getTime() + authResult.expires_in * 1000)
          : undefined; // Set date at expiration time

        await project.save({timestamps: false});
      } catch (error) {
        logger.error(error);
      }
    }
  },

  // TODO gets called even for the inscription front requests, not normal
  async populateHelloAssoEventCandidates(project: ProjectD): Promise<void> {
    if (project.helloAsso?.token && project.helloAsso?.organizationSlug) {
      try {
        const controller = new AbortController();
        const timeout = setTimeout(() => {
          controller.abort();
        }, 10000);

        const response = await fetch(
          `https://api.helloasso.com/v5/organizations/${project.helloAsso.organizationSlug}/forms?formTypes=Event`,
          {
            headers: {Authorization: `Bearer ${project.helloAsso.token}`},
            signal: controller.signal,
          }
        );
        const events = await response.json();

        clearTimeout(timeout);
        project.helloAsso.selectedEventCandidates = events.data;
      } catch (error) {
        logger.error(error);
      }
    }
  },

  async hideSensitiveFields(project: ProjectD, userRegistration: RegistrationD): Promise<void> {
    if (!userRegistration?.role) project.customTicketing = undefined;

    if (project.helloAsso) {
      if (!userRegistration?.role) {
        project.helloAsso.clientSecret = undefined;
      }
      project.helloAsso.token = undefined;
      project.helloAsso.tokenExpirationDate = undefined;
      project.helloAsso.refreshToken = undefined;
    }
  },
};

/**
 * PLUGINS
 */
ProjectSchema.plugin(softDelete);
ProjectSchema.plugin(addDiffHistory, ["helloAsso", "registrations"]);
ProjectSchema.plugin(mongooseSlugPlugin, {tmpl: "<%=name%>"}); // Add autogenerated project slug
ProjectSchema.index({name: 1, deletedAt: 1}, {unique: true}); // Prevent projects with the same name

/**
 * MODEL
 */
export const Project = model<ProjectD, Model<ProjectD> & ProjectQueryHelpers>(
  "Project",
  ProjectSchema
);
