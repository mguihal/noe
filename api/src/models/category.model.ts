import {Document, model, Schema, SchemaTypes} from "mongoose";
import {ProjectD} from "./project.model";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongoose";
import {lightSelection} from "../utilities/controllersUtilities";
import {SchemaDescription} from "../utilities/routeUtilities";

/**************************************************************
 *            TYPES
 **************************************************************/

export type CategoryD = Document & {
  name: string;
  project: ProjectD;
  color: string;
  summary: string;

  deletedAt: Date;
  importedId: string;
};

export type LightCategoryD = Pick<CategoryD, keyof typeof lightSelection.category>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const CategorySchema = new Schema<CategoryD>(
  {
    name: {type: String, required: true},
    color: String,
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    summary: String,

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
CategorySchema.plugin(softDelete);
CategorySchema.plugin(addDiffHistory);
CategorySchema.plugin(indexByIdAndProject);
CategorySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Category = model<CategoryD>("Category", CategorySchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription = [
  {key: "name", label: "Nom de la catégorie", noGroupEditing: true},
  {key: "color", label: "Couleur"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Résumé"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
