import {Document, model, Schema, SchemaTypes} from "mongoose";
import {Registration, RegistrationD} from "./registration.model";
import {StewardD} from "./steward.model";
import {ActivityD} from "./activity.model";
import {PlaceD} from "./place.model";
import {ProjectD} from "./project.model";
import {SlotD} from "./slot.model";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongoose";
import {TeamD} from "./team.model";
import {SchemaDescription} from "../utilities/routeUtilities";

/**************************************************************
 *            TYPES
 **************************************************************/

/**
 * DEFINITION
 */
export type SessionD = Document & {
  name: string;
  start: Date;
  end: Date;

  notes: string;
  tags: Array<string>;
  maxNumberOfParticipants: number;
  volunteeringCoefficient: number;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;
  activity: ActivityD;
  team: TeamD;
  project: ProjectD;
  slots: Array<SlotD>;

  isScheduleFrozen: boolean;

  busyVolume: number;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  importedId: string;

  //*** Fields calculated on demand: they do not always exist and are not stored in database ***//

  participants: Array<RegistrationD>;
  numberParticipants: number;

  computedMaxNumberOfParticipants: number;

  sameTimeSessions: Array<SessionD>;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const SessionSchema = new Schema<SessionD>(
  {
    name: String,
    start: {type: Date, required: true},
    end: {type: Date, required: true},
    notes: String,
    tags: [String],
    maxNumberOfParticipants: Number,
    volunteeringCoefficient: {type: Number, required: false},
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    activity: {type: SchemaTypes.ObjectId, ref: "Activity", required: true},
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
    slots: {type: [{type: SchemaTypes.ObjectId, ref: "Slot"}], required: true},
    isScheduleFrozen: {type: Boolean, default: false},

    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true, toObject: {virtuals: true}, toJSON: {virtuals: true}}
);

/**
 * POPULATED VIRTUALS
 */
// The max number of participants computed based on the places, the activity and the session data
SessionSchema.virtual("computedMaxNumberOfParticipants").get(function (): number {
  return sessionMethods.getMaxParticipants(this);
});

/**
 * INSTANCE METHODS
 */
export const sessionMethods = {
  // This function is duplicated in orga-front/src/helpers/sessionUtilities.js
  getMaxParticipantsBasedOnPlaces(session: SessionD): number {
    return session.places?.length > 0
      ? session?.places?.reduce(
          (acc, place) => acc + (place.maxNumberOfParticipants ?? Infinity),
          0
        )
      : Infinity;
  },

  // Warning: Infinity is transmitted as "null" in JSON, so if it's "null" in the frontends, it's like infinity
  getMaxParticipants(session: SessionD): number | undefined {
    // Compute the jauge based on th session places
    const sessionMax = session.maxNumberOfParticipants;
    const activityMax = session.activity?.maxNumberOfParticipants;

    const maxParticipantsBasedOnPlaces = sessionMethods.getMaxParticipantsBasedOnPlaces(session);
    if (sessionMax !== undefined && sessionMax !== null) {
      return maxParticipantsBasedOnPlaces !== undefined
        ? Math.min(sessionMax, maxParticipantsBasedOnPlaces)
        : sessionMax;
    } else if (activityMax !== undefined && activityMax !== null) {
      return maxParticipantsBasedOnPlaces !== undefined
        ? Math.min(activityMax, maxParticipantsBasedOnPlaces)
        : activityMax;
    } else {
      return maxParticipantsBasedOnPlaces;
    }
  },

  getSessionVolunteeringCoefficient(session: SessionD): number {
    return session.volunteeringCoefficient || session.volunteeringCoefficient === 0
      ? session.volunteeringCoefficient
      : session.activity.volunteeringCoefficient || 0;
  },

  async getAllSessionsSlotsInProject(projectId: string): Promise<Array<SlotD>> {
    const allSessions = await Session.find({project: {_id: projectId}})
      .select({_id: 1})
      .populate([
        {path: "slots", select: {start: 1, end: 1, stewards: 1}},
        {path: "activity", select: {name: 1, allowSameTime: 1}},
      ]);
    return allSessions
      .map((session) =>
        session.slots.map((slot) => ({
          session: {
            _id: session._id,
            activity: session.activity,
          },
          start: slot.start,
          end: slot.end,
          stewards: slot.stewards,
          _id: slot._id,
        }))
      )
      .flat() as Array<SlotD>;
  },

  /**
   * Populates the session with its subscribed users, and adds also the number of participants
   * @param session the session to work on
   * @param includeParticipants if true, include the participants, if false don't, and just count the
   * number of participants without fetching the data.
   */
  async populateParticipants(session: SessionD, includeParticipants = true): Promise<void> {
    if (includeParticipants) {
      // If we include participants, fetch them entirely
      session.participants = await Registration.find(
        {sessionsSubscriptions: {$elemMatch: {session: session._id}}},
        "tags teamsSubscriptions sessionsSubscriptions specific"
      ).populate([
        {path: "user", select: "firstName lastName email"},
        {path: "teamsSubscriptions.team", select: "name"},
      ]);
      session.numberParticipants = session.participants.length;
    } else {
      // Else, just count the project registrations, that's enough
      session.numberParticipants = await Registration.countDocuments({
        sessionsSubscriptions: {$elemMatch: {session: session._id}},
      });
    }
  },

  /**
   * Computes the sessions that occur in the same time asa the current session. It will be useful, so we can then
   * calculate in the frontends which session is potentially in conflict with another
   * @param session the session to work on
   * @param registration the current registration, containing a list of subscribed sessions by a user
   * @param allSessionsSlots the project slots. They are given for performance improvement reasons, but can
   * also be calculated if not given.
   */
  computeSameTimeSessions: async function (
    session: SessionD,
    registration?: RegistrationD,
    allSessionsSlots?: Array<SlotD>
  ): Promise<void> {
    // If not given, calculate the project slots
    if (!allSessionsSlots) {
      const projectId = session.project?._id || session.project;
      allSessionsSlots = await sessionMethods.getAllSessionsSlotsInProject(projectId.toString());
    }

    //enlever les slots de la session
    const sessionsSlots = allSessionsSlots.filter(
      (s) => s.session._id.toString() !== session._id.toString()
    );

    const sameTimeSlots = session.slots
      .map((currentSlot) => {
        const conflictSlots = sessionsSlots.filter(
          (slot) =>
            slot.session.activity.allowSameTime !== true &&
            slot.start < currentSlot.end &&
            slot.end > currentSlot.start &&
            slot._id != currentSlot._id
        );
        return conflictSlots.map((conflictSlot) => ({
          sameTimeSlot: conflictSlot,
          slot: currentSlot,
        }));
      })
      .flat();

    session.sameTimeSessions = sameTimeSlots.reduce((acc, currentValue) => {
      const sameTimeSession = currentValue.sameTimeSlot.session;
      const sameTimeSlot = {
        _id: currentValue.sameTimeSlot._id,
        start: currentValue.sameTimeSlot.start,
        end: currentValue.sameTimeSlot.end,
        stewards: currentValue.sameTimeSlot.stewards,
      };
      const sessionSlot = {
        _id: currentValue.slot._id,
        start: currentValue.slot.start,
        end: currentValue.slot.end,
      };

      const existingSession = acc.find(
        (session) => session.sameTimeSession._id == sameTimeSession._id
      );
      if (existingSession) {
        existingSession.sameTimeSlots.push({
          sameTimeSlot: sameTimeSlot,
          sessionSlot: sessionSlot,
        });
      } else {
        acc.push({
          sameTimeSession: sameTimeSession,
          sameTimeSessionRegistered: registration
            ? registration.sessionsSubscriptions
                .map((ss) => ss.session.toString())
                .includes(sameTimeSession._id.toString())
            : undefined,
          sameTimeSessionSteward: registration?.steward
            ? sameTimeSlot.stewards.includes(registration.steward._id.toString())
            : undefined,
          sameTimeSlots: [{sameTimeSlot: sameTimeSlot, sessionSlot: sessionSlot}],
        });
      }
      return acc;
    }, []);
  },
};

/**
 * PLUGINS
 */
SessionSchema.plugin(softDelete);
SessionSchema.plugin(addDiffHistory);
SessionSchema.plugin(indexByIdAndProject);
SessionSchema.plugin(preventDuplicatesInProject);

/**
 * MODEL
 */
export const Session = model<SessionD>("Session", SessionSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription = [
  {key: "name", label: "Nom de la session"},
  {key: "start", label: "Début", noGroupEditing: true},
  {key: "end", label: "Fin", noGroupEditing: true},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewards", label: "Encadrant⋅es"},
  {key: "places", label: "Espaces", usePlaces: true},
  {key: "activity", label: "Activité"},
  {key: "team", label: "Équipe", useTeams: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "slots", label: "Plages"},
  {key: "registrations", label: "Participant⋅es"},
  {key: "isScheduleFrozen", label: "Session figée", useAI: true},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
