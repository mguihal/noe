import {Document, model, Schema, SchemaTypes} from "mongoose";
import {StewardD} from "./steward.model";
import {PlaceD} from "./place.model";
import {ProjectD} from "./project.model";
import {CategoryD} from "./category.model";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongoose";
import {lightSelection} from "../utilities/controllersUtilities";
import {SchemaDescription} from "../utilities/routeUtilities";

/**************************************************************
 *            TYPES
 **************************************************************/

export type ActivityD = Document & {
  name: string;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;
  category: CategoryD;
  secondaryCategories: Array<string>;
  summary: string;
  description: string;
  notes: string;
  tags: Array<string>;
  project: ProjectD;
  slots: Array<{duration: number}>;
  numberOfSessions: number;
  nonBlockingActivity: boolean;
  allowSameTime: boolean;
  maxNumberOfParticipants: number;
  volunteeringCoefficient: number;
  stewardVolunteeringCoefficient: number;

  minNumberOfStewards: number;
  deletedAt: Date;
  importedId: string;
};

export type LightActivityD = Pick<ActivityD, keyof typeof lightSelection.activity>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const ActivitySchema = new Schema<ActivityD>(
  {
    name: String,
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    category: {type: SchemaTypes.ObjectId, ref: "Category", required: true},
    secondaryCategories: [String],
    summary: String,
    description: String,
    notes: String,
    tags: [String],
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    slots: [{duration: Number}],
    numberOfSessions: Number,
    nonBlockingActivity: {type: Boolean, default: false},
    allowSameTime: {type: Boolean, default: false},
    maxNumberOfParticipants: Number,
    minNumberOfStewards: Number,
    volunteeringCoefficient: Number,
    stewardVolunteeringCoefficient: {type: Number, required: false, default: 1.5},

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
ActivitySchema.plugin(softDelete);
ActivitySchema.plugin(addDiffHistory);
ActivitySchema.plugin(indexByIdAndProject);
ActivitySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Activity = model<ActivityD>("Activity", ActivitySchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription = [
  {key: "name", label: "Nom de l'activité", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "stewards", label: "Encadrant⋅es éligibles"},
  {key: "places", label: "Espaces éligibles", usePlaces: true},
  {key: "slots", label: "Plages par défaut"},
  {key: "category", label: "Catégorie"},
  {key: "secondaryCategories", label: "Catégories secondaires"},
  {key: "summary", label: "Résumé"},
  {key: "description", label: "Description détaillée"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewardVolunteeringCoefficient", label: "Coefficient de bénévolat encadrant⋅es"},
  {key: "allowSameTime", label: "Autoriser les inscriptions en même temps"},
  {key: "numberOfSessions", label: "Nombre de sessions voulues", useAI: true},
  {key: "minNumberOfStewards", label: "Nombre minimum d'encadrant⋅es", useAI: true},
  {key: "nonBlockingActivity", label: "Ne pas bloquer les espaces", useAI: true},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
