import * as Sentry from "@sentry/node";
import {ProfilingIntegration} from "@sentry/profiling-node";

import {Express, NextFunction, Request, RequestHandler, Response} from "express";
import * as Tracing from "@sentry/tracing";
import config from "../config/config";
import {logger} from "./logger";

export function initSentry(app: Express): void {
  Sentry.init({
    environment: config.appMode,
    dsn: config.sentryDsn,
    tracesSampleRate: 0.05,
    profilesSampleRate: 1, // Relative to tracesSampleRate
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({tracing: true}),
      // enable Express.js middleware tracing
      new Tracing.Integrations.Express({
        // to trace all requests to the default router
        app,
        // alternatively, you can specify the routes you want to trace:
        // router: someRouter,
      }),
      new ProfilingIntegration(),
    ],
  });

  app.use(Sentry.Handlers.requestHandler() as RequestHandler);

  // TracingHandler creates a trace for every incoming request
  app.use(Sentry.Handlers.tracingHandler());
}

export function globalErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): Response {
  // Check if it is a known error, and return if that's the case
  let status, message;

  // Dirty stuff to extract keys that are in conflict
  const getConflictingResourcesFromError = (err: Error): string =>
    /dup key:\s\{(.*)\}/
      .exec(err.message)[1] // Get the JSON-like string
      .match(/(,?\s.*?: )/g) // Match all the fields
      .filter((el) => !/(project|deletedAt)/.exec(el)) // Remove project and deletedAt attributes
      .map((el) => el.replace(/([:, ])/g, "")) // Clean
      .join(", "); // Join

  // Mongoose version conflict
  if (err.name === "VersionError") {
    status = 409;
  }
  // Resource already exists
  if (/duplicate key error/.exec(err.message)) {
    status = 403;
    message = `Un autre objet a déjà les mêmes champs suivants : ${getConflictingResourcesFromError(
      err
    )}. Deux objets ne peuvent pas avoir ces champs identiques.`;
  }

  status = status || 500; // If status hasn't been set, make it 500

  logger.error(err); // Display the trace in the logs

  try {
    // Send the error to Sentry with all the necessary information
    Sentry.captureException(err, {
      extra: {
        "a - URL": `${req.method} ${req.url}`,
        "b - Origin": req.headers?.origin,
        "c - User": req.user,
        "d - authenticatedUser":
          req.user?._id !== req.authenticatedUser?._id ? req.authenticatedUser : "same as user",
        "e - Request params": JSON.stringify(req.params, null, 2),
        "f - Request query": JSON.stringify(req.query, null, 2),
        "g - Request body": JSON.stringify(req.body, null, 2),
        "h - Full request": req,
      },
    });
  } catch (e) {
    logger.error("Problem with Sentry. Error not reported.", e);
  }
  return res.status(status).send(message); // Return status with the Sentry eventId
}
