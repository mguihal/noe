import Joi from "joi";
import path from "path";
import {logger} from "../services/logger";

const getConfig = () => {
  // VALIDATE ENV VARIABLES
  const {value: envVars, error} = Joi.object()
    .keys({
      NODE_ENV: Joi.string().valid("production", "development", "debug", "test").required(),
      PORT: Joi.number().default(4000),
      WS_PORT: Joi.number().default(4001),
      OPENSSL_CONF: Joi.string().valid("/dev/null").description("OpensslConf, must be /dev/null"),

      REACT_APP_MODE: Joi.string().valid("DEMO", "PRODUCTION"),
      REACT_APP_INSTANCE_NAME: Joi.string()
        .default("NOÉ")
        .description("Give a custom name to your NOÉ instance"),
      REACT_APP_BLOCK_PROJECT_CREATION: Joi.boolean().description(
        "blocks the creation of new projects for everyone, except super-admins"
      ),

      SENTRY_DSN: Joi.string().description(
        "the Sentry DSN url so errors are ent to Sentry properly"
      ),

      JWT_SECRET: Joi.string().required().description("JWT secret key"),
      JWT_TOKEN_EXPIRATION_DAYS: Joi.number()
        .default(15)
        .description("days after which refresh tokens expire"),
      ENCRYPTION_KEY: Joi.string()
        .default("v8y/B?E(H+MbQeThVmYq3t6w9z$C&F)J")
        .length(32)
        .description(
          "If you're storing sensitive data like API keys, provide a good encryption key !"
        ),

      MONGODB_URI: Joi.string().required().description("Mongo DB url"),

      SMTP_SERVER: Joi.string().description("server that will send the emails (<HOST>:<PORT>"),
      SMTP_ADDRESS: Joi.string().description("username for email server"),
      SMTP_PASSWD: Joi.string().description("password for email server"),

      REACT_APP_ORGA_FRONT_URL: Joi.string().description("URL of the orga-front frontend"),
      REACT_APP_INSCRIPTION_FRONT_URL: Joi.string().description(
        "URL of the inscription-front frontend"
      ),
      REACT_APP_IA_BACK_INTERNAL_URL: Joi.string().description("URL of the ia-back backend"),
      REACT_APP_API_URL: Joi.string().description("URL of the current api server"),
    })
    .unknown()
    .prefs({errors: {label: "key"}})
    .validate(process.env);

  if (error) throw new Error(`Config validation error: ${error.message}`);

  return {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
    wsPort: envVars.WS_PORT,
    appMode: envVars.REACT_APP_MODE,
    instanceName: envVars.REACT_APP_INSTANCE_NAME,
    blockProjectsCreation: envVars.REACT_APP_BLOCK_PROJECT_CREATION,
    sentryDsn: envVars.SENTRY_DSN,
    jwt: {
      secret: envVars.JWT_SECRET,
      expiresIn: envVars.JWT_TOKEN_EXPIRATION_DAYS,
    },
    encryptionKey: envVars.ENCRYPTION_KEY,
    mongoose: {
      uri: envVars.MONGODB_URI + (envVars.NODE_ENV === "test" ? "-test" : ""),
    },
    urls: {
      orgaFront: envVars.REACT_APP_ORGA_FRONT_URL,
      inscriptionFront: envVars.REACT_APP_INSCRIPTION_FRONT_URL,
      iaBack: envVars.REACT_APP_IA_BACK_INTERNAL_URL,
      api: envVars.REACT_APP_API_URL,
    },
    urlsNames: {
      [envVars.REACT_APP_ORGA_FRONT_URL]: "orga-front",
      [envVars.REACT_APP_INSCRIPTION_FRONT_URL]: "inscription-front",
      [envVars.REACT_APP_IA_BACK_INTERNAL_URL]: "ia-back",
      [envVars.REACT_APP_API_URL]: "api",
    },
    smtp: {
      server: envVars.SMTP_SERVER,
      address: envVars.SMTP_ADDRESS,
      pass: envVars.SMTP_PASSWD,
    },
    localesPath: "locales",
  };
};

let config: Record<string, any>;
if (process.env.NODE_ENV === "debug") {
  /**
   * DEBUG CONFIG
   */
  logger.info("==== DEBUG MODE ====");
  logger.info(
    "DEBUG: Fetching environment variables from ./../.env.local instead of Docker Compose environment."
  );
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require("dotenv").config({path: path.join(__dirname, "../../../.env.local")});
  process.env["JWT_SECRET"] = "FxrR8Hg9TMY5ob6@N9MF@TZeF4KQ?@LdQ6GPoT4k@ot#TgDsnJjnRHGDLDoF&7A";
  process.env["REACT_APP_IA_BACK_INTERNAL_URL"] = "http://localhost:4010";
  config = getConfig();

  // Making last modifications so this runs smoothly outside Docker:
  // - change local paths so we can link the folders we need
  config.localesPath = path.join("..", "common", "locales");
  // - Change the database path from @db (container name) to @localhost (which is the way to access the container outside Docker)
  config.mongoose.uri = config.mongoose.uri.replace("@db", "@localhost");
} else {
  /**
   * CLASSIC CONFIG
   */
  config = getConfig();
}

export default config;
