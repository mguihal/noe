import passport from "passport";
import passportJwt, {VerifiedCallback} from "passport-jwt";
import * as Sentry from "@sentry/node";

import {User, UserD} from "../models/user.model";
import {Request, Response, NextFunction} from "express";
import {isValidSlug} from "./mongoose";
import config from "./config";
import {Project} from "../models/project.model";
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

const jwtParams = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("JWT"),
  passReqToCallback: true,
};

const cookieExtractor = (req: Request) => {
  let token = null;
  if (req && req.cookies) {
    token = req.cookies["JWT"];
  }
  return token;
};

const jwtCookieParams = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: cookieExtractor,
  passReqToCallback: true,
};

// Fetch the user. If there is a project id, also fetch the registration associated to the project and the user, else, don't
const findUserAndRegistrationForProject = async (
  userId: string,
  projectId: string
): Promise<UserD | void> => {
  try {
    if (isValidSlug(projectId)) {
      // If projectId present, get the user and the (unique) registration associated to the current project.
      // It can later be accessed with user.registration when available.
      const project = await Project.findByIdOrSlug(projectId, "_id").lean();
      return await User.findById(userId).populate({
        path: "registrations",
        match: {project: project?._id},
      });
    } else {
      // Else, only get the user
      return await User.findById(userId);
    }
  } catch (e) {
    console.error(e);
    Sentry.captureException(e);
  }
};

/**
 * Sign in using JWT token
 */
passport.use(
  "user-jwt",
  new JwtStrategy(
    jwtParams,
    async (req: Request, payload: {id: string}, done: VerifiedCallback) => {
      try {
        const user = await findUserAndRegistrationForProject(payload.id, req.params.projectId);
        if (!user) return done(undefined, false, {message: `User ${payload.id} not found.`});
        return done(null, user);
      } catch (e) {
        done(e);
      }
    }
  )
);

/**
 * Sign in using JWT token in cookies
 */
passport.use(
  "user-jwt-cookie",
  new JwtStrategy(
    jwtCookieParams,
    async (req: Request, payload: {id: string}, done: VerifiedCallback) => {
      try {
        const user = await findUserAndRegistrationForProject(payload.id, req.params.projectId);
        if (!user) return done(undefined, false, {message: `User ${payload.id} not found.`});
        if (user.xsrfToken != req.headers["x-xsrf-token"]) {
          return done(undefined, false, {message: `X-XSRF-TOKEN header not found.`});
        }
        return done(null, user);
      } catch (e) {
        done(e);
      }
    }
  )
);

export const authenticateUser = passport.authenticate(["user-jwt", "user-jwt-cookie"], {
  session: false,
});

export const enableUserImpersonation = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  if (req.isAuthenticated()) {
    // Set the authenticated real user
    req.authenticatedUser = req.user;

    // Enable impersonation if there is a requested impersonation user id present
    const impersonatedUserId = req.headers["x-connected-as-user"] as string;
    const impersonatedUser =
      impersonatedUserId &&
      (await findUserAndRegistrationForProject(impersonatedUserId, req.params.projectId as string));

    // If we found the user to impersonate in DB, then make the swap
    if (impersonatedUser) req.user = impersonatedUser;
  }

  next();
};

export const withAuthentication = [authenticateUser, enableUserImpersonation];
