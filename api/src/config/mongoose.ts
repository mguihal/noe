import {
  DocumentOrQueryMiddleware,
  LeanDocument,
  MongooseQueryMiddleware,
  Schema,
  Types,
} from "mongoose";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const diffHistory = require("mongoose-diff-history/diffHistory");

/**
 * ID Utils
 */
export const isValidSlug = (id: string): boolean =>
  !/(new|clone|groupedit|projects|auth|users)/.exec(id) && id?.length > 1;

export const isValidObjectId = (id: string | number | Types.ObjectId): boolean =>
  Types.ObjectId.isValid(id);

const queryMiddlewares: Array<MongooseQueryMiddleware> = [
  "count",
  "estimatedDocumentCount",
  "countDocuments",
  "deleteMany",
  "deleteOne",
  "distinct",
  "find",
  "findOne",
  "findOneAndDelete",
  "findOneAndRemove",
  "findOneAndReplace",
  "findOneAndUpdate",
  "remove",
  "replaceOne",
  "update",
  "updateOne",
  "updateMany",
];

const documentOrQueryMiddlewares: Array<DocumentOrQueryMiddleware | MongooseQueryMiddleware> = [
  "updateOne",
  "deleteOne",
  "remove",
];

const queryOnlyMiddlewares = queryMiddlewares.filter(
  (middleware) => !documentOrQueryMiddlewares.includes(middleware)
);

/**
 * SOFT DELETE: add a deletedAt date argument to objects instead of deleting them for real.
 */

export function excludeInFindQueriesDeletedAt(): void {
  this._conditions?.searchAlsoInDeletedElements
    ? delete this._conditions?.searchAlsoInDeletedElements // Don't do anything, just remove the fake condition
    : this.where({deletedAt: {$exists: false}}); // Filter the deleted elements
}

export function softDelete<T>(schema: Schema<T>): void {
  queryOnlyMiddlewares.forEach((type) => {
    schema.pre(type, excludeInFindQueriesDeletedAt);
  });
}

/**
 * PREVENT DUPLICATES: Index creator helper to prevent objects with same key
 * properties to be crz ted in the DB
 */

export function preventDuplicatesInProject(
  schema: Schema,
  additionalUniqueKeys?: Record<string, number>[] // Give any primary key combination we want to be set
): void {
  [{_id: 1}, ...(additionalUniqueKeys || [])].forEach((fields) =>
    schema.index({...fields, project: 1, deletedAt: 1}, {unique: true})
  );
}

/**
 * CREATE DEFAULT INDEX BY ID AND PROJECT
 */

export function indexByIdAndProject(schema: Schema): void {
  schema.index({_id: 1, project: 1}, {unique: true});
}

// Diff history:
export function addDiffHistory<T>(schema: Schema<T>, omit: string[] = []): void {
  schema.plugin(diffHistory.plugin, {omit: ["createdAt", "updatedAt", ...omit]});
}
// Add the diff history additional fields for typing
declare module "mongoose" {
  interface SaveOptions {
    __reason?: string;
    __user?: string;
  }
  interface QueryOptions {
    __reason?: string;
    __user?: string;
  }
}

// Object references in Schemas
export type ObjectIdRef<T = Document> = Types.ObjectId | T;

// Lean documents
export type Lean<T = Document> = LeanDocument<T>;

// Populated document (can be null)
export type Populated<T = Document> = T | null;
