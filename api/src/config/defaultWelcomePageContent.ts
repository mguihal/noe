export default {
  id: "1",
  version: 1,
  rows: [
    {
      id: "tdp85x",
      cells: [
        {
          id: "g78klb",
          size: 12,
          plugin: {
            id: "ory/editor/core/layout/background",
            version: 1,
          },
          dataI18n: {
            fr: {
              background:
                "https://media.istockphoto.com/photos/are-you-ready-to-party-picture-id1181169462?k=20&m=1181169462&s=612x612&w=0&h=Bl6aor50n-318v4vfwHRcg_w3OgMHmXWM6g0kUrSftk=",
              isParallax: false,
              modeFlag: 7,
              backgroundColor: {
                r: 55,
                g: 117,
                b: 225,
                a: 0.5,
              },
              gradients: [
                {
                  deg: 45,
                  opacity: 1,
                  colors: [
                    {
                      color: {
                        r: 55,
                        g: 117,
                        b: 225,
                        a: 0.37,
                      },
                    },
                  ],
                },
              ],
              hasPadding: true,
            },
          },
          rows: [
            {
              id: "os69nf",
              cells: [
                {
                  id: "fpatk5",
                  size: 12,
                  plugin: {
                    id: "ory/editor/core/content/slate",
                    version: 1,
                  },
                  dataI18n: {
                    fr: {
                      slate: [
                        {
                          type: "HEADINGS/HEADING-ONE",
                          children: [
                            {
                              text: "Bienvenue sur NOÉ !",
                              SetColor: {
                                color: "rgba(255, 255, 255, 1)",
                              },
                            },
                          ],
                          data: {
                            align: "center",
                          },
                        },
                      ],
                      marginTop: 200,
                      marginBottom: 180,
                      marginLeft: 20,
                      marginRight: 20,
                      paddingTop: 20,
                      paddingBottom: 20,
                      paddingLeft: 20,
                      paddingRight: 20,
                      cellSpacingX: 0,
                      cellSpacingY: 0,
                      borderWidth: 0,
                      borderRadius: 0,
                      borderColor: "#000000",
                      textColor: "#000000",
                      backgroundColor: "#ffffff",
                    },
                  },
                  rows: [],
                  inline: null,
                },
              ],
            },
          ],
          inline: null,
        },
      ],
    },
    {
      id: "jem7g7",
      cells: [
        {
          id: "1q1b96",
          size: 5,
          plugin: {
            id: "ory/editor/core/content/slate",
            version: 1,
          },
          dataI18n: {
            fr: {
              slate: [
                {
                  type: "HEADINGS/HEADING-THREE",
                  children: [
                    {
                      text: "Sur cette page, vous pouvez un peu tout faire. ",
                    },
                  ],
                },
                {
                  children: [
                    {
                      text: "Libre à ",
                    },
                    {
                      text: "vous",
                      "EMPHASIZE/STRONG": true,
                    },
                    {
                      text: " de ",
                    },
                    {
                      text: "faire ce qu'il vous plait",
                      "EMPHASIZE/EM": true,
                    },
                    {
                      text: "!",
                    },
                  ],
                },
                {
                  type: "LISTS/UNORDERED-LIST",
                  children: [
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "Changez le style des éléments",
                        },
                      ],
                    },
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "Ajoutez des marges",
                        },
                      ],
                    },
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "repositionnez les éléments",
                        },
                      ],
                    },
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "Mettez des espaces",
                        },
                      ],
                    },
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "Incorporez des vidéos, et des images !",
                        },
                      ],
                    },
                    {
                      type: "LISTS/LIST-ITEM",
                      children: [
                        {
                          text: "Rédigez du texte à destination de vos participant⋅es",
                        },
                      ],
                    },
                  ],
                },
                {
                  children: [
                    {
                      text: "",
                    },
                  ],
                },
              ],
              borderWidth: 0,
              borderRadius: 0,
              borderColor: "#000000",
              textColor: "#000000",
              backgroundColor: "#ffffff",
              marginTop: 0,
              marginBottom: 0,
              marginLeft: 0,
              marginRight: 0,
              paddingTop: 20,
              paddingBottom: 20,
              paddingLeft: 20,
              paddingRight: 20,
              cellSpacingX: 0,
              cellSpacingY: 0,
            },
          },
          rows: [],
          inline: null,
        },
        {
          id: "sksoyx",
          size: 7,
          plugin: {
            id: "ory/editor/core/content/image",
            version: 1,
          },
          dataI18n: {
            fr: {
              src: "https://media.istockphoto.com/photos/concert-stage-people-are-visible-waving-and-clapping-silhouettes-are-picture-id1194060639?k=20&m=1194060639&s=612x612&w=0&h=x-0MqkUFwRVvf52GtrM23XrD02r-MB2y_hlOKaQVvvs=",
              borderWidth: 0,
              borderRadius: 0,
              borderColor: "#000000",
              textColor: "#000000",
              backgroundColor: "#ffffff",
              marginTop: 0,
              marginBottom: 0,
              marginLeft: 0,
              marginRight: 0,
              paddingTop: 20,
              paddingBottom: 20,
              paddingLeft: 20,
              paddingRight: 20,
              cellSpacingX: 0,
              cellSpacingY: 0,
            },
          },
          rows: [],
          inline: null,
        },
      ],
    },
    {
      id: "rfhapp",
      cells: [
        {
          id: "55p6du",
          size: 12,
          plugin: {
            id: "ory/editor/core/layout/background",
            version: 1,
          },
          dataI18n: {
            fr: {
              modeFlag: 5,
              gradients: [
                {
                  deg: 45,
                  opacity: 1,
                  colors: [
                    {
                      color: {
                        r: 0,
                        g: 11,
                        b: 255,
                        a: 1,
                      },
                    },
                    {
                      color: {
                        r: 127,
                        g: 255,
                        b: 187,
                        a: 1,
                      },
                    },
                  ],
                },
                {
                  deg: 45,
                  opacity: 1,
                },
              ],
            },
          },
          rows: [
            {
              id: "a018l9",
              cells: [
                {
                  id: "j4eywt",
                  size: 12,
                  plugin: {
                    id: "ory/editor/core/content/slate",
                    version: 1,
                  },
                  dataI18n: {
                    fr: {
                      slate: [
                        {
                          children: [
                            {
                              text: "Elle est pas belle la vie ?",
                              SetColor: {
                                color: "rgba(255, 255, 255, 1)",
                              },
                            },
                          ],
                          type: "HEADINGS/HEADING-THREE",
                          data: {
                            align: "center",
                          },
                        },
                      ],
                      backgroundColor: "rgba(255, 255, 255, 1)",
                      borderWidth: 0,
                      borderRadius: 0,
                      borderColor: "#000000",
                      textColor: "rgba(255, 255, 255, 1)",
                      marginTop: 40,
                      marginBottom: 20,
                      marginLeft: 20,
                      marginRight: 20,
                      paddingTop: 20,
                      paddingBottom: 20,
                      paddingLeft: 20,
                      paddingRight: 20,
                      cellSpacingX: 0,
                      cellSpacingY: 0,
                    },
                  },
                  rows: [],
                  inline: null,
                },
              ],
            },
          ],
          inline: null,
        },
      ],
    },
  ],
};
