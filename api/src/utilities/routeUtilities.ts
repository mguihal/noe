import {RequestHandler} from "express";
import {ParamsDictionary, RequestHandlerParams} from "express-serve-static-core";
import {ParsedQs} from "qs";
import {HistoryFormatted, readEntityHistory} from "./controllersUtilities";
import {permit, projectGuests} from "./permissionsUtilities";
import {schemaDescription} from "../models/session.model";

type TReqBody = Record<string, unknown>;
type TResBody = Record<string, unknown>;
type TQueryString = ParsedQs;

export type ProjectIdAndId = {projectId: string; id: string};
export type ProjectId = {projectId: string};
export type New<T> = Partial<T & {_id: "new" | undefined}>;
export type Modified<T> = Partial<T>;
export type ErrorString = string;

export type TypedRequestHandler<
  Params = ParamsDictionary,
  ReqBody = TReqBody,
  ResBody = TResBody,
  QueryString = TQueryString
> = RequestHandler<Params, Partial<ResBody>, Partial<ReqBody>, Partial<QueryString>>;

export type TypedRequestHandlerParams<
  Params = ParamsDictionary,
  ReqBody = TReqBody,
  ResBody = TResBody,
  QueryString = TQueryString
> = RequestHandlerParams<Params, Partial<ResBody>, Partial<ReqBody>, Partial<QueryString>>;

export type Endpoint<
  Params = ParamsDictionary,
  ReqBody = TReqBody,
  ResBody = TResBody,
  QueryString = TQueryString
> = {
  path: string;
  noAuthenticationNeeded?: boolean;
  middlewares: TypedRequestHandlerParams<Params, ReqBody, ResBody, QueryString>;
  handler: TypedRequestHandler<Params, ReqBody, ResBody, QueryString>;
};

// Router function wrapper. We encapsulate them into that so the errors created can be redirected in the
// middleware pipeline, and be captured by Sentry
export function handleErrors<
  Params = ParamsDictionary,
  ReqBody = TReqBody,
  ResBody = TResBody,
  QueryString = TQueryString
>(
  func: TypedRequestHandler<Params, ReqBody, ResBody, QueryString>
): TypedRequestHandler<Params, ReqBody, ResBody, QueryString> {
  return async function (req, res, next) {
    try {
      await func(req, res, next);
    } catch (e) {
      next(e);
    }
  };
}

export const endpoint = <
  Params = ParamsDictionary,
  ReqBody = TReqBody,
  ResBody = TResBody,
  QueryString = TQueryString
>(
  path: string,
  middlewares: TypedRequestHandlerParams<Params, ReqBody, ResBody, QueryString>[],
  handler: TypedRequestHandler<Params, ReqBody, ResBody, QueryString>
): [
  path: string,
  ...middlewares: TypedRequestHandlerParams<Params, ReqBody, ResBody, QueryString>[],
  handler: TypedRequestHandler<Params, ReqBody, ResBody, QueryString>
] => {
  return [path, ...middlewares, handleErrors(handler)];
};

/**
 * GENERIC ROUTES
 */

/**
 * Get an element's modification history
 * GET /:id/history
 */
export const MODIFICATIONS_HISTORY = (entityClassName: string) =>
  endpoint<ProjectIdAndId, undefined, Array<HistoryFormatted>>(
    "/:id/history",
    [permit(projectGuests)],
    async function (req, res) {
      return await readEntityHistory(req.params.id, req.params.projectId, entityClassName, res);
    }
  );

export type SchemaDescription = Array<{
  key: string;
  label: string;
  noGroupEditing?: boolean;
  usePlaces?: boolean;
  useTeams?: boolean;
  useAI?: boolean;
}>;

/**
 * Get an element's schema definition
 * GET /schema
 */
export const SCHEMA_DESCRIPTION = (schemaDescription: SchemaDescription) =>
  endpoint<ProjectId, undefined, SchemaDescription>("/schema", [], async function (req, res) {
    return res.send(schemaDescription);
  });

export const ss = SCHEMA_DESCRIPTION(schemaDescription);
