import {Router} from "express";
import {withAuthentication} from "../config/passport";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "../utilities/routeUtilities";
import {
  createEntity,
  deleteEntity,
  GenericQueryParams,
  getDependenciesList,
  lightSelection,
  listAllEntities,
  parseQueryParams,
  readEntity,
  removeNestedObjectsContent,
  updateEntity,
} from "../utilities/controllersUtilities";
import {
  filterBody,
  permit,
  projectAdmins,
  projectContributors,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activity.model";
import {Session} from "../models/session.model";
import {
  allowedBodyArgs,
  LightPlaceD,
  Place,
  PlaceD,
  schemaDescription,
} from "../models/place.model";
import {Slot} from "../models/slot.model";

export const PlaceRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
PlaceRouter.get(...MODIFICATIONS_HISTORY("Place"));
PlaceRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the places in the project
 * GET /
 */
const LIST_PLACES = endpoint<ProjectId, undefined, Array<LightPlaceD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Place.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.place
      ),
      {name: "asc"},
      res
    );
  }
);
PlaceRouter.get(...LIST_PLACES);

/**
 * Get a place
 * GET /:id
 */
const GET_PLACE = endpoint<ProjectIdAndId, undefined, PlaceD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Place.findOne({_id: id, project: projectId}).lean()
    );
  }
);
PlaceRouter.get(...GET_PLACE);

/**
 * Create a place
 * POST /
 */
const CREATE_PLACE = endpoint<ProjectId, New<PlaceD>, PlaceD>(
  "/",
  [permit(projectContributors), filterBody(allowedBodyArgs), removeNestedObjectsContent("project")],
  async function (req, res) {
    return await createEntity(Place, req.body, res);
  }
);
PlaceRouter.post(...CREATE_PLACE);

/**
 * Modify a place
 * PATCH /:id
 */
const MODIFY_PLACE = endpoint<ProjectIdAndId, Modified<PlaceD>, PlaceD>(
  "/:id",
  [
    permit(projectContributors),
    filterBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Place,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
PlaceRouter.patch(...MODIFY_PLACE);

/**
 * Delete a place
 * DELETE /:id
 */
const DELETE_PLACE = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const placeId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {places: placeId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    const dependentSessionsList = await getDependenciesList(Session, {places: placeId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    const dependentSlotsList = await getDependenciesList(Slot, {places: placeId}, async (e) => {
      await e.populate("session");
      return e.session._id;
    });
    if (dependentSlotsList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des plages de sessions (${dependentSlotsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(placeId, Place, req.authenticatedUser, res);
  }
);
PlaceRouter.delete(...DELETE_PLACE);
