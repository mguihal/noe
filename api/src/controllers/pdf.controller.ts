import {Request, Response, Router} from "express";
import {withAuthentication} from "../config/passport";
import {handleErrors} from "../utilities/routeUtilities";
import {sessionMethods, Session} from "../models/session.model";
import {Slot, SlotD} from "../models/slot.model";
import {UserD} from "../models/user.model";
import {FormMapping, Registration, RegistrationD} from "../models/registration.model";
import {permit, projectGuests} from "../utilities/permissionsUtilities";
import {Steward, StewardD} from "../models/steward.model";
import {Project, ProjectD} from "../models/project.model";
import {Team, TeamD} from "../models/team.model";
import momentTz = require("moment-timezone");
import {personName} from "../utilities/utilities";
import {compileHtmlTemplate, printPdf} from "../services/html-and-pdf";
import {getSessionName, slotNumberString} from "../helpers/session.helper";
import {cleanAnswer, flatFormInputs} from "../helpers/registration.helper";

export const PdfRouter = Router({mergeParams: true});

/**
 * Date formatting helpers
 */
const longDateFormat = (date: unknown) =>
  momentTz.tz(date, "Europe/Paris").locale("fr").format("dddd D MMMM");
const timeFormat = (date: unknown) =>
  momentTz.tz(date, "Europe/Paris").locale("fr").format("HH:mm");

/**
 * Base planning generation function
 */
const createHtmlUserPlanning = async (
  registration: RegistrationD,
  currentSteward: StewardD,
  project: ProjectD,
  user: UserD,
  allRegistrations: Array<RegistrationD>,
  formFieldsToDisplay: Array<FormMapping>
) => {
  // This doesn't select session where only a slot has the user as a steward
  const sessionsSubscribedOrAsSteward = await Session.find({
    $or: [
      {_id: registration?.sessionsSubscriptions?.map((ss) => ss.session)}, // Find the user's subscribed sessions
      {stewards: currentSteward}, // Or that the user animates
    ],
  }).lean();

  const sessionsSubscribedOrAsStewardSlotIds = sessionsSubscribedOrAsSteward
    .map((session) => session.slots)
    .reduce((acc, slots) => acc.concat(slots), []);

  const slots: SlotD[] = await Slot.find({
    $or: [
      {_id: sessionsSubscribedOrAsStewardSlotIds}, // Select the slots corresponding to the sessions
      {stewards: currentSteward}, // And also the slots where the steward is assigned on the slot itself
    ],
  })
    .sort({start: 1})
    .populate([
      "stewards",
      "places",
      {
        path: "session",
        populate: ["stewards", "places", {path: "activity", populate: ["category"]}],
      },
    ])
    .lean();

  const teams: TeamD[] = await Team.find({project}, "_id name").lean();

  const slotsGroupedByDate = slots
    .filter((slot) => slot.session) // Be sure that the slot is assigned to a session (this can happen that we loose slots in the nature...)
    .map((slot) => {
      const slotPlaces = slot.placesSessionSynchro ? slot.session.places : slot.places;
      const placesString = slotPlaces.map((el) => el.name).join(", ");

      const slotStewards = slot.stewardsSessionSynchro ? slot.session.stewards : slot.stewards;

      const isStewardOnThisSession =
        slotStewards?.findIndex(
          (steward) => steward._id.toString() === currentSteward?._id.toString()
        ) !== -1;

      const stewardsString = slotStewards
        .filter((el) => el._id.toString() !== currentSteward?._id.toString())
        .map(personName)
        .join(", ");

      const hasMultipleStewards = slotStewards.length > 1;

      const isVolunteering = sessionMethods.getSessionVolunteeringCoefficient(slot.session) > 0;

      const participants =
        (isStewardOnThisSession || registration?.role) &&
        allRegistrations
          .filter((r) =>
            r.sessionsSubscriptions?.find(
              (sessionSubscription) =>
                sessionSubscription.session.toString() === slot.session._id.toString()
            )
          )
          .map((r) => {
            const additionalFields =
              formFieldsToDisplay?.length > 0
                ? [
                    r.user.email,
                    ...formFieldsToDisplay.map(
                      ({columnName, formField}) =>
                        `${columnName}: ${
                          cleanAnswer(
                            r.specific?.[formField],
                            ", ",
                            flatFormInputs(project.formComponents),
                            formField
                          ) || "/"
                        }`
                    ),
                  ].join(" - ")
                : r.user.email;
            return `${personName(r.user)} (${additionalFields})`;
          });

      return {
        ...slot,
        title: slotNumberString(slot) + getSessionName(slot.session, teams),
        startFormatted: timeFormat(slot.start),
        endFormatted: timeFormat(slot.end),
        places: slotPlaces,
        stewards: slotStewards,
        placesString,
        stewardsString,
        hasMultipleStewards,
        participants,
        isStewardOnThisSession,
        isVolunteering,
      };
    })
    .reduce((groupingAcc, slot) => {
      // Group slots by day
      const dateString = longDateFormat(slot.start);
      return {
        ...groupingAcc,
        [dateString]: [...(groupingAcc[dateString] || []), slot],
      };
    }, {});

  // Format in a way that Handlebar can manage
  const slotsForHandlebars = Object.entries(slotsGroupedByDate).map(([day, slots]) => ({
    dayName: day,
    slot: slots,
  }));

  const pdfPlanningData = {
    currentExportDate: `${longDateFormat(momentTz.tz())} ${timeFormat(momentTz.tz())}`,
    day: slotsForHandlebars,
    user: user,
    hasRoleOrSteward: registration?.role || currentSteward,
    steward: currentSteward,
    projectName: project.name,
  };

  return await compileHtmlTemplate("pdf-user-planning", pdfPlanningData);
};

/**
 * Generate and download the PDF planning for a particular registration
 * GET /registrationPlanning/:id
 */
PdfRouter.get(
  "/registrationPlanning/:id",
  withAuthentication,
  handleErrors(async function registrationPdfPlanning(req: Request, res: Response) {
    const projectId = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    const formFieldsToDisplay = project.formMapping?.filter((mapping) => mapping.inPdfExport);

    if (!req.user.registration.role && req.user.registration._id.toString() !== req.params.id) {
      return res.sendStatus(403); // If a non-orga user wants something else than its own registration, forbid it
    }

    const allRegistrations = await Registration.find(
      {project},
      "user project _id sessionsSubscriptions specific"
    )
      .populate("user")
      .lean();

    const registration = await Registration.findById(req.params.id)
      .populate(["user", "steward"])
      .lean();

    const compiledHtml = await createHtmlUserPlanning(
      registration as RegistrationD,
      registration.steward as StewardD,
      project as ProjectD,
      registration.user as UserD,
      allRegistrations as Array<RegistrationD>,
      formFieldsToDisplay
    );
    const pdf = await printPdf(compiledHtml);
    return res.contentType("application/pdf").send(pdf);
  })
);

/**
 * Generate and download the PDF planning for a particular steward
 * GET /stewardPlanning/:id
 *
 * GET /stewardsPlanning/all gets all plannings from project
 * GET /stewardsPlanning/xxxxxxx gets planning for id xxxxxxx
 * GET /stewardsPlanning/xxxxxxx,yyyyyyy,zzzzzzz gets planning for id xxxxxxx, yyyyyyy and zzzzzzz
 */
PdfRouter.get(
  "/stewardPlanning/:id",
  permit(projectGuests),
  handleErrors(async function registrationPdfPlanning(req: Request, res: Response) {
    const stewardId = req.params.id;
    const projectId = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    // If "all" the stewards are wanted, select all from project. Else, take the id and split it if there are some multiple ids there
    const researchConditions =
      stewardId === "all" ? {project: projectId} : {project: projectId, _id: stewardId.split(",")};

    const formFieldsToDisplay = project.formMapping?.filter((mapping) => mapping.inPdfExport);

    const allRegistrations = await Registration.find(
      {project},
      "user project _id sessionsSubscriptions specific"
    )
      .populate("user")
      .lean();

    const stewards = await Steward.find(researchConditions, "_id firstName lastName")
      .sort({lastName: -1})
      .lean();

    const allHtmlTemplates = await Promise.all(
      stewards.map(async (currentSteward) => {
        // Maybe this registration doesn't exist
        const registrationLinkedToSteward = await Registration.findOne({steward: currentSteward})
          .populate("user")
          .lean();

        return await createHtmlUserPlanning(
          registrationLinkedToSteward as RegistrationD,
          currentSteward as StewardD,
          project as ProjectD,
          registrationLinkedToSteward?.user as UserD,
          allRegistrations as Array<RegistrationD>,
          formFieldsToDisplay
        );
      })
    );

    // Join all the generated HTML templates together with page breaks
    const compiledHtml = allHtmlTemplates.join(
      "<div style='page-break-before: always; height: 30px'></div>"
    );
    const pdf = await printPdf(compiledHtml);
    return res.contentType("application/pdf").send(pdf);
  })
);
