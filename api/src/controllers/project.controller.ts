import {Request, Response, Router} from "express";
import {withAuthentication} from "../config/passport";
import {
  computeFunctionForAllElements,
  lightSelection,
  listAllEntities,
  readEntity,
} from "../utilities/controllersUtilities";
import {Project, ProjectD, projectMethods} from "../models/project.model";
import {FormMapping, Registration, registrationMethods} from "../models/registration.model";
import {
  endpoint,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  TypedRequestHandler,
} from "../utilities/routeUtilities";
import {
  emailNormalizationParams,
  nobodyExceptSuperAdmins,
  permit,
  projectAdmins,
  projectContributors,
  projectGuests,
  ROLES,
  validateAndSanitizeBody,
} from "../utilities/permissionsUtilities";
import {body} from "express-validator/check";
import config from "../config/config";
import {logger} from "../services/logger";
import {
  getAllEntitiesLinkedToProject,
  processProjectImport,
  ProjectImportPayload,
  rollbackDeletedElements,
} from "../helpers/project.helper";
import {Place} from "../models/place.model";
import {Category} from "../models/category.model";
import {Steward} from "../models/steward.model";
import {Session, SessionD} from "../models/session.model";
import {Slot} from "../models/slot.model";
import {Activity} from "../models/activity.model";
import {Team} from "../models/team.model";
import type {Document} from "mongoose";

export const ProjectRouter = Router();

/**
 * Generic endpoints
 */
ProjectRouter.get(...MODIFICATIONS_HISTORY("Project"));

/**
 * List all the projects for which the user is registered
 * (either as orga or as participant, depending on the frontend that is requesting projects)
 * GET /
 */
const LIST_PROJECTS = endpoint<undefined, undefined, Array<ProjectD>>(
  "/",
  [withAuthentication],
  async function (req, res) {
    const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;
    return isRequestedFromInscriptionFrontend
      ? await listAllProjectsForParticipant(req, res)
      : await listAllProjectsForOrga(req, res);
  }
);
ProjectRouter.get(...LIST_PROJECTS);

async function listAllProjectsForOrga(req: Request, res: Response) {
  const registeredProjectsIds = await Registration.distinct("project", {
    user: req.user,
    role: {$exists: true},
  });
  // Fetch the projects administrated by the user. If super admin, show all the projects of the platform
  return listAllEntities(
    Project.find(
      // Fetch the projects administrated by the user. If super admin, show all the projects of the platform
      req.user.superAdmin ? {} : {_id: registeredProjectsIds},
      lightSelection.project
    ),
    {updatedAt: "desc"},
    res
  );
}

async function listAllProjectsForParticipant(req: Request, res: Response) {
  const registeredProjectsIds = await Registration.distinct("project", {
    user: req.user,
  });
  return listAllEntities(
    Project.find({_id: registeredProjectsIds}, lightSelection.project),
    {start: "desc"},
    res
  );
}

/**
 * List all the publicly available projects on NOÉ
 * GET /public
 */
const LIST_PUBLIC_PROJECTS = endpoint<undefined, undefined, Array<ProjectD>>(
  "/public",
  [],
  async function (req, res) {
    return listAllEntities(
      Project.find({isPublic: true}, lightSelection.project),
      {start: "desc"},
      res
    );
  }
);
ProjectRouter.get(...LIST_PUBLIC_PROJECTS);

/**
 * Get a full project
 * GET /:projectId
 */
const GET_PROJECT = endpoint<ProjectId, undefined, ProjectD>(
  "/:projectId",
  [permit(projectGuests, config.urls.inscriptionFront)], // Allow all persons having a role in orga front, and any person requesting from the inscription side
  async function (req, res) {
    const registration = req.user.registration;
    const role = registration?.role; // Maybe there is no registration at all !

    await readEntity(
      req.params.projectId,
      undefined,
      res,
      (id) => Project.findByIdOrSlug(id),
      async (project: ProjectD) => {
        if (role === ROLES.ADMIN && project.ticketingMode === "helloAsso") {
          await projectMethods.refreshHelloAssoAuthToken(project);
          await projectMethods.populateHelloAssoEventCandidates(project);
        }
        await projectMethods.hideSensitiveFields(project, registration);
        return project;
      }
    );
  }
);
ProjectRouter.get(...GET_PROJECT);

/**
 * Get a project when not connected
 * GET /:projectId/public
 */
// TODO just give a light url param to the default request could be enough
const GET_PUBLIC_PROJECT = endpoint<ProjectId, undefined, ProjectD>(
  "/:projectId/public",
  [],
  async function (req, res) {
    await readEntity(req.params.projectId, undefined, res, (id) =>
      Project.findByIdOrSlug(id, lightSelection.project).lean()
    );
  }
);
ProjectRouter.get(...GET_PUBLIC_PROJECT);

/**
 * Create a project
 * POST /
 */
const CREATE_PROJECT = endpoint<undefined, New<ProjectD>, ProjectD>(
  "/",
  // Allow project creation only for super  admins, or for everybody
  [config.blockProjectsCreation ? permit(nobodyExceptSuperAdmins) : withAuthentication],
  async function (req, res) {
    const project = new Project({
      name: req.body.name,
      availabilitySlots: req.body.availabilitySlots,
      start: req.body.start,
      end: req.body.end,
      useTeams: req.body.useTeams,
      usePlaces: req.body.usePlaces,
      isPublic: req.body.isPublic,
    });
    await project.save();

    // Create the project registration linked to this project, and make the current user admin of the project
    const userRegistration = new Registration({
      role: "admin",
      project: project,
      user: req.user,
    });
    await userRegistration.save();

    // Prepare the project for response
    await projectMethods.hideSensitiveFields(project, userRegistration);
    res.status(201).json(project);
  }
);
ProjectRouter.post(...CREATE_PROJECT);

/**
 * Modify a project
 * PATCH /:projectId
 */
const MODIFY_PROJECT = endpoint<
  ProjectId,
  Modified<ProjectD & {breakfastTime: string; lunchTime: string; dinnerTime: string}>,
  ProjectD
>("/:projectId", [permit(projectContributors)], async function (req, res) {
  const projectId: string = req.params.projectId;
  // First, know who the person is (there can be no registration at all)
  const userRegistration = req.user.registration;
  const userIsAdmin = userRegistration?.role === ROLES.ADMIN;

  const project = await Project.findByIdOrSlug(projectId);
  if (!project) return res.sendStatus(404);

  const shouldUpdateAllRegistrationsDaysOfPresence =
    (req.body.availabilitySlots &&
      JSON.stringify(project.availabilitySlots.map((s) => s.toObject())) !==
        JSON.stringify(req.body.availabilitySlots)) ||
    (req.body?.breakfastTime && project.breakfastTime.toISOString() !== req.body.breakfastTime) ||
    (req.body?.lunchTime && project.lunchTime.toISOString() !== req.body.lunchTime) ||
    (req.body?.dinnerTime && project.dinnerTime.toISOString() !== req.body.dinnerTime);

  project.name = req.body.name;
  project.formComponents = req.body.formComponents;
  // Remove all the form mappings that are not complete
  project.formMapping = req.body.formMapping.filter(
    (mapping: FormMapping) => mapping?.formField && mapping?.columnName?.length > 0
  );
  project.content = req.body.content;
  project.minMaxVolunteering = req.body.minMaxVolunteering;
  project.blockVolunteeringUnsubscribeIfBeginsSoon =
    req.body.blockVolunteeringUnsubscribeIfBeginsSoon;
  project.start = req.body.start;
  project.end = req.body.end;
  project.openingState = req.body.openingState;
  project.isPublic = req.body.isPublic;
  project.blockSubscriptions = req.body.blockSubscriptions;
  project.notAllowOverlap = req.body.notAllowOverlap;
  project.notes = req.body.notes;
  project.full = req.body.full;
  project.theme = req.body.theme;
  project.secretSchedule = req.body.secretSchedule;

  project.availabilitySlots = req.body.availabilitySlots;
  project.breakfastTime = req.body.breakfastTime;
  project.lunchTime = req.body.lunchTime;
  project.dinnerTime = req.body.dinnerTime;

  // Update admin-only fields
  if (userIsAdmin) {
    // Ticketing mode
    project.ticketingMode = req.body.ticketingMode;

    // Ticketing - Custom API
    const shouldUpdateCustomTicketing = project.customTicketing !== req.body.customTicketing;
    if (shouldUpdateCustomTicketing) {
      project.customTicketing = req.body.customTicketing;
    }

    // Ticketing - HelloAsso
    const shouldUpdateHelloAssoTicketing =
      project.helloAsso?.clientId !== req.body.helloAsso?.clientId ||
      project.helloAsso?.clientSecret !== req.body.helloAsso?.clientSecret ||
      project.helloAsso?.selectedEvent !== req.body.helloAsso?.selectedEvent;
    if (shouldUpdateHelloAssoTicketing) {
      if (
        req.body.helloAsso?.clientId?.length > 0 &&
        req.body.helloAsso?.clientSecret?.length > 0
      ) {
        project.helloAsso = req.body.helloAsso;
        await projectMethods.getHelloAssoAuthTokens(project);
        await projectMethods.populateHelloAssoEventCandidates(project);
      } else {
        project.helloAsso = {
          clientId:
            req.body.helloAsso?.clientId?.length > 0 ? req.body.helloAsso?.clientId : undefined,
          clientSecret:
            req.body.helloAsso?.clientId?.length > 0 ? req.body.helloAsso?.clientSecret : undefined,
        };
      }
    }

    // Ticketing - TiBillet
    const shouldUpdateTiBilletTicketing =
      project.tiBillet?.serverUrl !== req.body.tiBillet?.serverUrl ||
      project.tiBillet?.eventSlug !== req.body.tiBillet?.eventSlug ||
      project.tiBillet?.apiKey !== req.body.tiBillet?.apiKey;
    if (shouldUpdateTiBilletTicketing) {
      project.tiBillet = req.body.tiBillet;
    }

    // Features
    project.usePlaces = req.body.usePlaces;
    project.useTeams = req.body.useTeams;
    project.useAI = req.body.useAI;
  }

  await project.save({__reason: "update()", __user: req.authenticatedUser._id});
  await projectMethods.hideSensitiveFields(project, userRegistration);

  res.status(200).json(project);

  // Compute all days of presence of all registrations if the project dates or meal times change
  if (shouldUpdateAllRegistrationsDaysOfPresence) {
    // Update asynchronously all dates of presence if it is needed
    computeFunctionForAllElements(
      await Registration.find({project: projectId}, "availabilitySlots project"),
      async (el) => {
        await registrationMethods.computeDaysOfPresence(el);
        await el.save({
          __reason: "update() days of presence",
          __user: req.authenticatedUser._id,
        });
      }
    ).then(() => logger.info("Successfully updated dates of presence of all registrations."));
  }
});
ProjectRouter.patch(...MODIFY_PROJECT);

/**
 * Import a project configuration file to modify the current project data
 * POST /:projectId/import
 */
const IMPORT_PROJECT = endpoint<
  ProjectId,
  ProjectImportPayload,
  ProjectD & {notFoundInImportIds?: Array<string>},
  {additiveImport: string; withRegistrations: string}
>(
  "/:projectId/import",
  [
    permit(projectAdmins),
    validateAndSanitizeBody(
      body("registrations.*.user.email").isEmail().normalizeEmail(emailNormalizationParams)
    ),
  ],
  async function (req, res) {
    const projectId: string = req.params.projectId;
    const additiveImportForElements = req.query.additiveImport === "true";
    const withRegistrations = req.query.withRegistrations === "true";
    const existingProject = await Project.findByIdOrSlug(projectId);
    if (!existingProject) return res.sendStatus(404);

    const importedProject = req.body;
    const {importedProjectData, notFoundInImportIds} = await processProjectImport(
      importedProject,
      existingProject,
      additiveImportForElements,
      withRegistrations
    );

    // Assign the imported project config data by replacing it when it exists in the importedProjectData
    Object.assign(existingProject, importedProjectData);
    await existingProject.save({__reason: "import()", __user: req.authenticatedUser._id});

    return res.status(200).json({...existingProject.toObject(), notFoundInImportIds});
  }
);
ProjectRouter.post(...IMPORT_PROJECT);

/**
 * Export all the data of a project in a JSON format
 * GET /:projectId/export
 * GET /:projectId/allData (/!\ deprecated)
 */
const EXPORT_PROJECT: TypedRequestHandler<
  ProjectId,
  undefined,
  unknown,
  {withRegistrations: string}
> = async (req, res) => {
  const projectId: string = req.params.projectId;
  const withRegistrations = req.query.withRegistrations === "true";
  const project = await Project.findByIdOrSlug(projectId);
  if (!project) return res.sendStatus(404);

  res.status(200).json({
    ...(await getAllEntitiesLinkedToProject(projectId, withRegistrations)),
    project,
  });
};
ProjectRouter.get(...endpoint("/:projectId/export", [permit(projectContributors)], EXPORT_PROJECT));
// Deprecated route
ProjectRouter.get(
  ...endpoint("/:projectId/allData", [permit(projectContributors)], EXPORT_PROJECT)
);

/**
 * Rollback deletion of project related objects for a given period in minutes. ("Oops" feature in Advanced panel)
 * POST /:projectId/rollback/:rollbackPeriod
 */
const ROLLBACK_PROJECT = endpoint<ProjectId & {rollbackPeriod: string}, undefined, undefined>(
  "/:projectId/rollback/:rollbackPeriod",
  [permit(projectAdmins)],
  async function (req, res) {
    const projectId = req.params.projectId;
    const rollbackPeriod: number = parseInt(req.params.rollbackPeriod);

    const rollbackStartDate = new Date();
    rollbackStartDate.setMinutes(rollbackStartDate.getMinutes() - rollbackPeriod); // Remove the period given as argument
    await Promise.all([
      rollbackDeletedElements(Place, projectId, rollbackStartDate),
      rollbackDeletedElements(Category, projectId, rollbackStartDate),
      rollbackDeletedElements(Steward, projectId, rollbackStartDate),
      rollbackDeletedElements(Session, projectId, rollbackStartDate),
      rollbackDeletedElements(Slot, projectId, rollbackStartDate),
      rollbackDeletedElements(Activity, projectId, rollbackStartDate),
      rollbackDeletedElements(Team, projectId, rollbackStartDate),
    ]);

    return res.sendStatus(200);
  }
);
ProjectRouter.get(...ROLLBACK_PROJECT);

const DELETE_PROJECT = endpoint<ProjectId, undefined, undefined>(
  "/:projectId",
  [permit(projectAdmins)],
  async function (req, res) {
    const projectId: string = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    const entitiesDictionary = await getAllEntitiesLinkedToProject(projectId, true);

    const now = new Date();

    // First, delete all the session slots because they don't have a project id inside them and
    // can't be linked back to the deleted project directly
    const slotsToDelete = (
      (entitiesDictionary.sessions as SessionD[]).map(
        (session) => session.slots?.map((slot) => slot._id) || []
      ) || []
    ).flat();
    await Slot.updateMany({_id: {$in: slotsToDelete}}, {deletedAt: now});

    // Then, aggregate and flatten all the objects of the project...
    const entities: (Document & {deletedAt?: Date})[] = [project];
    Object.entries(entitiesDictionary).forEach(([, value]) => {
      value.forEach((entity) => {
        entities.push(entity);
      });
    });

    // ... and delete each entity, including project
    await Promise.all(
      entities.map(async (entity) => {
        entity.deletedAt = now;
        await entity.save();
      })
    );

    res.sendStatus(200);
  }
);
ProjectRouter.delete(...DELETE_PROJECT);

/**
 * AI FEATURE
 * Replaces the sessions of the project with the new program generated by the AI.
 * Only the AI backend can send this type of request.
 *
 * /!\ For now, this feature is deprecated and insecure, so it is commented and disabled.
 *
 * POST /:projectId/replaceSessions
 */
// const REPLACE_PROJECT_SESSIONS = endpoint<ProjectId, Array<SessionD>, Array<SessionD>>(
//   "/:projectId/replaceSessions",
//   [],
//   async function (req, res) {
//     const projectId = req.params.projectId;
//
//     // TODO Make a really better check for the sake of everybody
//     // Safeguards: Check if the request really comes from the IA backend and if IA is activated in the project
//     const comesFromBackend = req.headers.origin !== config.urls.iaBack;
//     const aiIsActivatedInProject = (await Project.findByIdOrSlug(projectId, "useAI").lean())?.useAI;
//     if (!comesFromBackend || !aiIsActivatedInProject) return res.sendStatus(401);
//
//     // Delete the current program //
//     const notScheduleFrozenSessionsIds = await Session.find({
//       project: projectId,
//       isScheduleFrozen: false,
//     });
//
//     const sessionsToDeleteIds = notScheduleFrozenSessionsIds.map((session) => session._id);
//     const slotsToDeleteIds = notScheduleFrozenSessionsIds
//       .map((session) => session.slots) // Get the slots
//       .reduce((accSlots, slot) => accSlots.concat(slot), []) // Flatten
//       .map((slot) => slot._id); // Get the slots ids
//
//     logger.info(`-------- IA: Replacing the sessions on project ${projectId}. --------`);
//     logger.info("Sessions to delete:", sessionsToDeleteIds);
//     logger.info("Slots to delete:", slotsToDeleteIds);
//
//     await Session.updateMany(
//       {_id: sessionsToDeleteIds, project: projectId}, // stay in the current project only
//       {deletedAt: new Date()}
//     );
//
//     await Slot.updateMany(
//       {_id: slotsToDeleteIds}, // delete the slots belonging to those sessions that are not frozen
//       {deletedAt: new Date()}
//     );
//
//     // Write the new program //
//
//     for (const session of req.body) {
//       const startSlot = moment(session.start);
//       session.project = projectId;
//       const activity = await Activity.findById(session.activity);
//
//       const slots: SlotD[] = [];
//       for (const slotActivity of activity.slots) {
//         const start = startSlot.toDate();
//         startSlot.add(slotActivity.duration, "minute");
//         const slot = {
//           start: start,
//           end: startSlot.toDate(),
//           stewards: session.stewards,
//           places: [session.places],
//           duration: slotActivity.duration,
//           stewardsSessionSynchro: true,
//           placesSessionSynchro: true,
//         };
//
//         const insertedSlot = await Slot.create(slot);
//         slots.push(insertedSlot._id);
//       }
//       session.slots = slots;
//       await Session.create(session);
//     }
//     res.status(200).json(req.body);
//   }
// );
// ProjectRouter.post(...REPLACE_PROJECT_SESSIONS);
