import express, {ErrorRequestHandler} from "express";
import passport from "passport";
import cors from "cors";
import mongoose = require("mongoose");
import helmet = require("helmet");
import expressValidator = require("express-validator");
import bodyParser = require("body-parser");
import cookieParser = require("cookie-parser");
import {join} from "path";
import {UserRouter} from "./controllers/user.controller";
import {AuthRouter} from "./controllers/auth.controller";
import {SessionRouter} from "./controllers/session.controller";
import {TeamRouter} from "./controllers/team.controller";
import {ProjectRouter} from "./controllers/project.controller";
import {RegistrationRouter} from "./controllers/registration.controller";
import {ActivityRouter} from "./controllers/activity.controller";
import {StewardRouter} from "./controllers/steward.controller";
import {PlaceRouter} from "./controllers/place.controller";
import {CategoryRouter} from "./controllers/category.controller";
import {PdfRouter} from "./controllers/pdf.controller";
import {TicketingRouter} from "./controllers/ticketing.controller";
import {ComputingRouter} from "./controllers/computing.controller";
import {logger, loggerMiddleware} from "./services/logger";
import {globalErrorHandler, initSentry} from "./services/sentry";
import {initI18n} from "./config/i18n";
import config from "./config/config";
import {checkDatabaseMigrationStatus} from "./services/migrations";

export const app = express();

// Check database migration status
checkDatabaseMigrationStatus();

// Setup database
mongoose.set("strictQuery", false);
mongoose.connect(config.mongoose.uri).catch((error) => {
  logger.error(
    `MongoDB connection error to ${config.mongoose.uri}. Please make sure MongoDB is running.`,
    error
  );
  process.exit();
});

app.disable("x-powered-by");

// Initialize Sentry
if (config.env === "production") initSentry(app);

// Helmet: protection against well known vulnerabilities
app.use(helmet());

// Cross-origin ressources sharing
app.use(cors({credentials: true, origin: true}));

// Parsing options
app.use(bodyParser.json({limit: "15mb"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Request validation
app.use(expressValidator());

// Authentication
app.use(passport.initialize());

// Request logger
app.use(loggerMiddleware);

// // Internationalization
initI18n(app);

const router = express.Router();

// Then redirect to the appropriate sub-routers
app.use(router);
router.use("/users", UserRouter);
router.use("/projects", ProjectRouter);
router.use("/auth", AuthRouter);
router.use("/computing", ComputingRouter);
ProjectRouter.use("/:projectId/registrations", RegistrationRouter);
ProjectRouter.use("/:projectId/categories", CategoryRouter);
ProjectRouter.use("/:projectId/places", PlaceRouter);
ProjectRouter.use("/:projectId/stewards", StewardRouter);
ProjectRouter.use("/:projectId/activities", ActivityRouter);
ProjectRouter.use("/:projectId/sessions", SessionRouter);
ProjectRouter.use("/:projectId/teams", TeamRouter);
ProjectRouter.use("/:projectId/pdf", PdfRouter);
ProjectRouter.use("/:projectId/ticketing", TicketingRouter);

// Static assets
app.use("/assets", express.static(join("src", "assets")));

// All the rest: 404
app.get("*", (req, res) => {
  res.sendStatus(404);
});

// Global error Handler
app.use(globalErrorHandler as ErrorRequestHandler);
