module.exports = {
  async up(db, client) {
    await db
      .collection("projects")
      .updateMany({theme: {$exists: true}}, {$rename: {"theme.main": "theme.primary"}});
  },

  async down(db, client) {
    await db
      .collection("projects")
      .updateMany({theme: {$exists: true}}, {$rename: {"theme.primary": "theme.main"}});
  },
};
