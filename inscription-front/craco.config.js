const CracoLessPlugin = require("craco-less");
const SentryWebpackPlugin = require("@sentry/webpack-plugin");

const deploySentryRelease =
  process.env.NODE_ENV === "production" &&
  process.env.SENTRY_PROJECT_NAME &&
  process.env.SENTRY_AUTH_TOKEN;

const getGitHash = () =>
  require("child_process")
    .execSync("git config --global --add safe.directory /app && git rev-parse HEAD")
    .toString()
    .trim();

module.exports = {
  webpack: {
    configure: (webpackConfig) => {
      webpackConfig.module.rules.push(
        // Locales loader
        {
          test: /locales/,
          loader: "@turtle328/i18next-loader",
          options: {basenameAsNamespace: true},
        }
      );

      // Activate source maps for Sentry
      webpackConfig.devtool = "source-map";

      return webpackConfig;
    },
    ...(deploySentryRelease && {
      plugins: {
        add: [
          new SentryWebpackPlugin({
            org: "noe-app",
            project: process.env.SENTRY_PROJECT_NAME,

            // Specify the directory containing build artifacts
            include: "./build",

            // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
            // and needs the `project:releases` and `org:read` scopes
            authToken: process.env.SENTRY_AUTH_TOKEN,

            // Optionally uncomment the line below to override automatic release name detection
            release: getGitHash(),
          }),
        ],
      },
    }),
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
