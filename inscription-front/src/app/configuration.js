import logoWhite from "./images/logo-white.svg";
import logoColors from "./images/logo-colors.svg";
import logoWhiteWithWhiteText from "./images/logo-white-with-white-text.svg";
import logoColorsWithBlueText from "./images/logo-colors-with-blue-text.svg";

// Base default spinner logo to take
export const defaultSpinnerLogo = logoColors;

// Spinner logo for the fullscreen arrival loading page
export const arrivalSpinnerLogo = logoWhite;

// Logo to display in the UserConnectionPage
export const connectionPageLogo = logoWhiteWithWhiteText;

// Logo to display in a basic text environment
export const textLogo = logoColorsWithBlueText;

// Default background theme for the app
export const defaultBackgroundClassName = "bg-noe-gradient";

// Instance name
export const instanceName = process.env.REACT_APP_INSTANCE_NAME || "NOÉ";

// URLs
export const URLS = {
  ORGA_FRONT: process.env.REACT_APP_ORGA_FRONT_URL,
  INSCRIPTION_FRONT: process.env.REACT_APP_INSCRIPTION_FRONT_URL,
  API: process.env.REACT_APP_API_URL,
};
URLS.CURRENT = URLS.INSCRIPTION_FRONT;
