import React, {lazy, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import Welcome from "./welcome/Welcome";
import {Button, Badge, Tooltip, message, notification} from "antd";
import {Pending} from "./common/layout/Pending";
import {
  DoubleLeftOutlined,
  FormOutlined,
  HomeOutlined,
  UserSwitchOutlined,
  PlaySquareOutlined,
  ScheduleOutlined,
  DownloadOutlined,
  CarryOutOutlined,
} from "@ant-design/icons";
import {Router, Redirect, useNavigate} from "@reach/router";
import {currentUserActions, currentUserSelectors} from "../features/currentUser.js";
import {currentProjectSelectors, currentProjectActions} from "../features/currentProject.js";
import {LayoutStructure} from "./common/LayoutStructure";
import {displayNotification} from "../helpers/notificationUtilities";
import {VolunteeringGauge} from "./common/VolunteeringGauge";
import {useBrowserTabTitle} from "../helpers/viewUtilities";
import {registrationsActions, registrationsSelectors} from "../features/registrations";
import {viewSelectors} from "../features/view";
import {sessionsActions} from "../features/sessions";
import {
  OFFLINE_MODE,
  ACTIVATING_OFFLINE_MODE,
  OnlineOfflineSwitch,
  DISPLAY_OFFLINE_MODE_FEATURE,
  useOfflineMode,
} from "../helpers/offlineModeUtilities";
import {personName} from "../helpers/utilities";
import {instanceName, URLS} from "../app/configuration";
import {Trans, useTranslation} from "react-i18next";
import {DynamicThemeProvider} from "../style/theme";
import {MenuLayout} from "./common/layout/Menu";
import {lazyWithRetry} from "../helpers/lazyWithRetry";
const Registration = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./registration/Registration")
);
const SessionAgenda = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionAgenda")
);
const SessionList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionList")
);
const SessionShow = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionShow")
);
const ParticipantList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./participants/ParticipantList")
);

export default function ProjectLayout({"*": page, envId}) {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // ****** SELECTORS & STATES ******

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  const authenticatedRegistration = useSelector(registrationsSelectors.selectAuthenticated);
  const dirty = useSelector(currentUserSelectors.selectDirty);

  // ****** DATA FETCHING ******

  // Load project and project registration
  useEffect(() => {
    // We should not fire these actions if the offline mode is already activated.
    // Instead, fire it when offline is disabled OR when it is activating offline mode and needs to load all the data
    const shouldLoadProjectData = !OFFLINE_MODE || ACTIVATING_OFFLINE_MODE;

    if (currentUser._id && shouldLoadProjectData) {
      dispatch(currentProjectActions.load(envId))
        // Then only, load project registration, because it is dependent from the project
        // (on inscription front, we need the project to be loaded to get the registration metadata)
        .then(async () => {
          dispatch(registrationsActions.loadCurrent());

          setTimeout(() => {
            dispatch(sessionsActions.loadList({silent: true}));
          }, 3000);
        })
        .catch(() => navigate("/projects"));
    }
  }, [currentUser]);

  useOfflineMode(envId, authenticatedRegistration?._id);

  // ****** DATA DEFINITION ******

  const projectAndUserValid =
    currentUser._id &&
    currentProject?._id &&
    currentRegistration?._id &&
    authenticatedRegistration?._id;

  // Registration completion
  const {formIsOk, ticketingIsOk, everythingIsOk, firstVisit, booked} = currentRegistration || {};
  const everythingIsOkInDatabase = currentRegistration?.inDatabase?.everythingIsOk;

  // Project state
  const openingState = currentProject?.openingState;

  // User rights and registration
  const isConnectedAsAnotherUser = authenticatedUser._id !== currentUser._id;
  const authenticatedUserIsProjectOrga = !!authenticatedRegistration?.role;
  const userIsLinkedToSteward = currentRegistration?.steward !== undefined;
  const openingStateAllowsAccessToSessions =
    (userIsLinkedToSteward && openingState === "registerForStewardsOnly") ||
    openingState === "registerForAll";
  const userHasAccessToSecretSchedule = !currentProject.secretSchedule || everythingIsOkInDatabase;
  const giveAccessToSessionsPage =
    userHasAccessToSecretSchedule && openingStateAllowsAccessToSessions;
  const agendaMode = useSelector(viewSelectors.selectSessionsViewMode) === "agenda";
  // Utilities
  const pageRoot = page?.split("/").slice(0, 2).join("/");
  const registrationPagName = everythingIsOkInDatabase
    ? t("registrations:labelMyRegistration")
    : t("registrations:labelRegister");

  // ****** TAB TITLE ******
  useBrowserTabTitle(
    currentProject.name,
    pageRoot,
    {
      welcome: t("welcome:label"),
      registration: registrationPagName,
      "sessions/all": t("sessions:labelAll"),
      "sessions/subscribed": t("sessions:labelSubscribed"),
      participants: t("registrations:actions.changeIdentity"),
    },
    instanceName
  );

  // ****** NOTIFICATIONS ******

  // Persistent notif if registration incomplete
  useEffect(() => {
    if (booked !== false && !firstVisit && pageRoot !== "registration") {
      ticketingIsOk !== false || formIsOk !== false // Those can be undefined at the beginning
        ? notification.destroy("notifRegistrationIncomplete")
        : displayNotification("warning", "notifRegistrationIncomplete", {
            message: t("registrations:notifIncomplete.title"),
            description: (
              <ul style={{margin: 0}}>
                {!(formIsOk !== false) && (
                  <li>
                    <Trans i18nKey="notifIncomplete.form" ns="registrations" />
                  </li>
                )}
                {!(ticketingIsOk !== false) && (
                  <li>
                    <Trans i18nKey="notifIncomplete.ticketing" ns="registrations" />
                  </li>
                )}
              </ul>
            ),
            buttonText: t("registrations:notifIncomplete.completeMyRegistration"),
            onClickButton: () => navigate(`/${envId}/registration`),
          });
    } else {
      notification.destroy("notifRegistrationIncomplete");
    }
  }, [booked, ticketingIsOk, formIsOk, pageRoot]);

  // Message when we are not registered at all and looking at the sessions planning
  useEffect(() => {
    if (booked === false && pageRoot === "sessions") {
      message.info({
        key: "registerToSubscribe",
        content: t("registrations:messages.registerToSubscribe"),
        duration: 4,
      });
    }
  }, [booked, pageRoot]);

  // ****** SIDE MENU ******

  const collapsedSidebar = page?.includes("agenda");

  const menu = {
    top: (
      <>
        <MenuLayout
          rootNavUrl={`/${envId}/`}
          selectedItem={pageRoot}
          items={[
            userHasAccessToSecretSchedule && {
              label: t("welcome:label"),
              key: "welcome",
              icon: <HomeOutlined />,
            },
            {
              label: (
                <>
                  {registrationPagName}
                  {!dirty && (
                    <Badge
                      dot
                      count={everythingIsOk && everythingIsOkInDatabase ? 0 : 1}
                      style={{marginBottom: 10, width: 8, height: 8}}
                    />
                  )}
                </>
              ),
              key: "registration",
              icon: <FormOutlined />,
              disabled: dirty,
            },
          ]}
        />
        <Tooltip
          title={
            currentProject.secretSchedule
              ? t("common:menuNavigation.noAccess.secretSchedule")
              : !openingStateAllowsAccessToSessions
              ? t("common:menuNavigation.noAccess.projectOpeningState")
              : undefined
          }
          placement="right">
          <MenuLayout
            rootNavUrl={`/${envId}/`}
            selectedItem={page}
            disabled={!giveAccessToSessionsPage}
            title={t("common:menuNavigation.planning")}
            items={[
              // All sessions
              {
                label:
                  everythingIsOkInDatabase && !currentProject.blockSubscriptions
                    ? t("common:menuNavigation.subscribeToActivities")
                    : t("common:menuNavigation.seeAllActivities"),
                key: `sessions/all${agendaMode ? "/agenda" : ""}`,
                icon: <ScheduleOutlined />,
                disabled: dirty || !giveAccessToSessionsPage,
              },

              // Participant planning
              everythingIsOkInDatabase && {
                label: t("sessions:labelSubscribed"),
                key: `sessions/subscribed${agendaMode ? "/agenda" : ""}`,
                icon: <CarryOutOutlined />,
                disabled: dirty || !giveAccessToSessionsPage,
              },
            ]}
          />

          {/*Volunteering jauge*/}
          {giveAccessToSessionsPage && everythingIsOkInDatabase && (
            <MenuLayout
              title={
                collapsedSidebar
                  ? t("registrations:schema.voluntaryCounter.label").slice(0, -2)
                  : t("registrations:schema.voluntaryCounter.label")
              }>
              <VolunteeringGauge
                style={{
                  paddingBottom: "10pt",
                  paddingLeft: "1em",
                  width: "calc(100% - 1em)",
                  alignSelf: "center",
                }}
                registration={currentRegistration}
              />
            </MenuLayout>
          )}
        </Tooltip>
      </>
    ),

    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Identity swap
          authenticatedUserIsProjectOrga && {
            label: isConnectedAsAnotherUser ? (
              <span style={{fontWeight: "bold"}}>{personName(currentUser)}</span>
            ) : (
              t("registrations:actions.changeIdentity")
            ),
            key: "participants",
            url: `/${envId}/participants`,
            icon: <UserSwitchOutlined />,
            danger: isConnectedAsAnotherUser,
            disabled: OFFLINE_MODE,
          },

          // Stop identity swap button
          isConnectedAsAnotherUser && {
            label: (
              <Button
                danger
                ghost
                block
                onClick={() => dispatch(currentUserActions.changeConnectedAsUser(undefined))}>
                {t("common:stop")}
              </Button>
            ),
            url: false,
          },

          // Orga front
          authenticatedUserIsProjectOrga && {
            label: t("common:pagesNavigation.orgaFront"),
            url: `${URLS.ORGA_FRONT}/${envId}/${page}`,
            icon: <PlaySquareOutlined />,
          },

          // Back to events page
          {
            label: t("projects:labelMyProjects"),
            url: "/projects?no-redir",
            icon: <DoubleLeftOutlined />,
            disabled: OFFLINE_MODE,
          },

          // Offline mode
          DISPLAY_OFFLINE_MODE_FEATURE && {
            label: <OnlineOfflineSwitch />,
            icon: <DownloadOutlined />,
            url: false,
          },
        ]}
      />
    ),
  };

  return (
    <DynamicThemeProvider currentProject={currentProject}>
      <LayoutStructure
        title={currentProject.name}
        menu={menu}
        profileUser={authenticatedUser}
        displayButtonBadge={
          pageRoot !== "registration" && !(everythingIsOk && everythingIsOkInDatabase)
        }
        fadeIn
        collapsedSidebar={collapsedSidebar}>
        {projectAndUserValid && ( // If we don't know if the user has registered to the project, we wait
          <Pending.Suspense minHeight={"100vh"} animationDelay={"300ms"}>
            <Router style={{height: "100%"}}>
              {/* Redirect when switching from one frontend to another*/}
              <Redirect noThrow from="/sessions/agenda" to="../all/agenda" />

              {userHasAccessToSecretSchedule && <Welcome path="/welcome" />}
              <Registration
                path="/registration"
                giveAccessToSessionsPage={giveAccessToSessionsPage}
              />
              {giveAccessToSessionsPage && (
                <>
                  <SessionList path="/sessions/all" viewUrl="all" />
                  <SessionAgenda path="/sessions/all/agenda" viewUrl="all" />
                  {everythingIsOkInDatabase && [
                    <SessionList key="1" path="/sessions/subscribed" viewUrl="subscribed" />,
                    <SessionAgenda
                      key="2"
                      path="/sessions/subscribed/agenda"
                      viewUrl="subscribed"
                    />,
                  ]}
                  <SessionShow path="/sessions/:id" />
                </>
              )}
              {authenticatedUserIsProjectOrga && <ParticipantList path="/participants" />}
              <Redirect
                noThrow
                from="/*"
                to={
                  everythingIsOkInDatabase && giveAccessToSessionsPage
                    ? "./sessions/subscribed"
                    : "./registration"
                }
              />
            </Router>
          </Pending.Suspense>
        )}
      </LayoutStructure>
    </DynamicThemeProvider>
  );
}
