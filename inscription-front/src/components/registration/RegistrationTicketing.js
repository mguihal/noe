import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Divider, message, Space} from "antd";

import {TableElement} from "../common/TableElement";
import Search from "antd/es/input/Search";

import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {viewSelectors} from "../../features/view";
import {personName} from "../../helpers/utilities";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "../common/layout/CardElement";
import {useWebSocketConnection} from "../../helpers/webSocket";
import {currentUserSelectors} from "../../features/currentUser";
import {t} from "i18next";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {useWindowDimensions} from "../../helpers/viewUtilities";
import {currentProjectSelectors} from "../../features/currentProject";

type TicketingProps = {
  IntegratedBookingComponent: ({
    project: any,
    saveData: (silent?: true) => void,
  }) => JSX.Element,
  hasOptions: boolean,
  hasTicketName: boolean,
};

const scrollToAddManuallyInput = () => {
  const addManuallyInput = document.getElementById("addManuallyInput");
  window.scrollTo({
    top: addManuallyInput.getBoundingClientRect().top,
    behavior: "smooth",
  });
  setTimeout(() => addManuallyInput.focus(), 500);
};

const PaymentIframe = ({iframeUrl, style}) => {
  const {isMobileView} = useWindowDimensions();
  return (
    <CardElement
      borderless
      style={{overflow: "hidden", borderColor: "#ddd"}}
      className={"shadow-sm"}
      bodyStyle={{marginBottom: -7}}>
      <iframe
        src={iframeUrl}
        title="Payment iframe"
        style={{border: 0, height: isMobileView ? "120vh" : "90vh", width: "100%", ...style}}
      />
    </CardElement>
  );
};

const ticketingProps: Record<string, TicketingProps> = {
  helloAsso: {
    hasOptions: true,
    hasTicketName: true,
    IntegratedBookingComponent: ({project}) => {
      const ticketingUrl = `https://www.helloasso.com/associations/${project.helloAsso.organizationSlug}/evenements/${project.helloAsso.selectedEvent}`;
      const currentUser = useSelector(currentUserSelectors.selectUser);
      return (
        <div style={{gap: 26}} className={"containerV"}>
          <Alert
            showIcon
            type={"info"}
            message={
              <Trans
                i18nKey="ticketing.helloAssoAlert"
                ns="registrations"
                values={{email: currentUser.email}}
              />
            }
          />
          <Button onClick={scrollToAddManuallyInput} type={"link"} style={{alignSelf: "start"}}>
            {t("registrations:ticketing.alreadyHaveTicket")}
          </Button>
          <PaymentIframe
            iframeUrl={`${ticketingUrl}/widget`}
            style={{background: "#f5f5f5", paddingTop: 20}}
          />
        </div>
      );
    },
  },
  tiBillet: {
    hasIntegratedPayment: false,
    IntegratedBookingComponent: ({project, saveData}) => {
      const ticketingUrl = `https://www.helloasso.com/associations/${project.helloAsso.organizationSlug}/evenements/${project.helloAsso.selectedEvent}`;
      return (
        <>
          <Alert
            type="success"
            style={{marginBottom: 26}}
            message={
              <Trans
                i18nKey="ticketing.thanksToTiBilletAlert"
                ns="registrations"
                components={{
                  linkToTiBillet: (
                    <a
                      href="https://tibillet.org"
                      onClick={saveData}
                      target="_blank"
                      rel="noreferrer"
                    />
                  ),
                }}
              />
            }
          />
          <PaymentIframe iframeUrl={ticketingUrl} />
        </>
      );
    },
  },
  customTicketing: {
    getTicketingUrl: ({customTicketing}) => customTicketing,
  },
};

const getTicketingColumns = (tickets, ticketingMode) =>
  [
    {
      title: t("registrations:ticketing.schema.id"),
      dataIndex: "id",
    },
    ticketingProps[ticketingMode].hasTicketName && {
      title: t("registrations:ticketing.schema.userName"),
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: t("registrations:ticketing.schema.article"),
      dataIndex: "name",
    },
    {
      title: t("registrations:ticketing.schema.amount"),
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
    ticketingProps[ticketingMode].hasOptions &&
      tickets.find(({options}) => !!options) && {
        title: t("registrations:ticketing.schema.options"),
        dataIndex: "options",
        render: (text, record) =>
          record.options?.map((option) => `${option.name} (${option.amount} €)`).join(", "),
      },
  ].filter((el) => !!el);

export function RegistrationTicketing({
  saveData,
  HelpButton,
  paymentPageVisible,
  setPaymentPageVisible,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const ticketIdsString = useSelector(viewSelectors.selectSearchParams).ticketId;

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentRegistrationTickets = currentRegistration[`${currentProject.ticketingMode}Tickets`];

  const IntegratedBookingComponent =
    ticketingProps[currentProject.ticketingMode]?.IntegratedBookingComponent;

  const shouldBeVisibleForPaymentOrTableRecap =
    currentProject.ticketingMode && (paymentPageVisible || currentRegistration.ticketingIsOk);

  const addTicket = async (ticketId) => {
    saveData();

    // Make sure we don't add the ticket twice
    if (!currentRegistrationTickets?.find((ticket) => ticket.id === ticketId)) {
      await dispatch(registrationsActions.addTicketToRegistration(ticketId));
      setPaymentPageVisible(false);
    } else {
      message.warning(t("registrations:ticketing.messages.alreadyUsedTicket"));
    }
  };

  const addTickets = async (ticketIds) => {
    for (const ticketId of ticketIds) {
      await addTicket(ticketId);
    }
  };

  const removeTicket = (record) => {
    // If there is only one ticket and user has booked, then we can't
    if (currentRegistration.inDatabase.ticketingIsOk && currentRegistrationTickets.length <= 1) {
      message.error(t("registrations:ticketing.messages.unregisterBeforeRemovingTicket"));
    } else {
      // Else if there are many or if the user is not booked, just delete it and update the modif button
      dispatch(registrationsActions.removeTicketFromRegistration(record.id));
    }
  };

  // Automagically add the ticketIds if there is one in the URL and there is no ticket for the current registration
  useEffect(() => {
    if (ticketIdsString) {
      const ticketIdsToAdd = ticketIdsString.split(",");
      addTickets(ticketIdsToAdd);
    }
  }, [ticketIdsString]);

  useWebSocketConnection(
    paymentPageVisible && !!IntegratedBookingComponent,
    {type: "ticketing", id: currentUser._id},
    addTickets
  );

  return shouldBeVisibleForPaymentOrTableRecap ? (
    <div className="containerV" style={{marginBottom: 50}}>
      <Divider style={{marginBottom: 35}} />
      {paymentPageVisible && (
        <Button
          onClick={() => setPaymentPageVisible(false)}
          style={{marginBottom: 26, alignSelf: "start", paddingLeft: 0}}
          type={"link"}
          icon={<ArrowLeftOutlined />}
          size={"large"}>
          {t("registrations:backToForm")}
        </Button>
      )}
      <div className="list-element-header">
        <h3>
          {currentRegistration?.ticketingIsOk
            ? t("registrations:ticketing.labelMyTickets")
            : IntegratedBookingComponent
            ? t("registrations:ticketing.labelBookATicket")
            : t("registrations:ticketing.labelIHaveATicket")}
        </h3>
      </div>
      {currentRegistration?.ticketingIsOk && !paymentPageVisible ? (
        <>
          <Alert
            type={"success"}
            showIcon
            icon={"🎉"}
            message={t("registrations:ticketing.youHaveYourTicket")}
            style={{marginBottom: 26}}
          />
          <TableElement.Simple
            showHeader
            style={{marginBottom: 15}}
            columns={getTicketingColumns(currentRegistrationTickets, currentProject.ticketingMode)}
            onDelete={removeTicket}
            rowKey="id"
            dataSource={currentRegistrationTickets}
          />
        </>
      ) : (
        IntegratedBookingComponent &&
        paymentPageVisible && (
          <IntegratedBookingComponent project={currentProject} saveData={saveData} />
        )
      )}

      <Space wrap>
        {IntegratedBookingComponent && !currentRegistration.ticketingIsOk ? (
          t("registrations:ticketing.addManually")
        ) : (
          <strong>
            {currentRegistration?.ticketingIsOk
              ? t("registrations:ticketing.addAnotherTicket")
              : t("registrations:ticketing.addYourTicketHere")}
          </strong>
        )}
        <Search
          allowClear
          id={"addManuallyInput"}
          style={{maxWidth: 250}}
          enterButton={t("registrations:ticketing.add")}
          onSearch={addTicket}
          placeholder={t("registrations:ticketing.ticketInputPlaceholder")}
        />
      </Space>
      {IntegratedBookingComponent && (
        <div style={{marginTop: 26, textAlign: "center"}}>
          <HelpButton />
        </div>
      )}
    </div>
  ) : null;
}
