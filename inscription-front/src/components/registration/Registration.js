import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Divider, Form, message, Modal, Popconfirm, Result, Tooltip} from "antd";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {currentUserSelectors} from "../../features/currentUser";
import {CheckOutlined, FormOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import {RegistrationTicketing} from "./RegistrationTicketing";
import {useStickyShadow} from "../../helpers/viewUtilities";
import {RegistrationAvailabilities} from "./RegistrationAvailabilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {Link} from "@reach/router";
import {ConnectedAsAlert} from "../participants/utilsParticipants";
import {textLogo} from "../../app/configuration";
import {Trans} from "react-i18next";
import {t} from "i18next";
import {safeValidateFields} from "../common/FormElement";
import RegistrationForm from "../common/RegistrationForm";
import {PageHeading} from "../common/layout/PageHeading";

const NoeOpenSourceParagraph = () => (
  <div style={{marginTop: 14, opacity: 0.7}}>
    <Trans
      ns="common"
      i18nKey="noeOpenSourceParagraph"
      components={{
        noeTextLogo: (
          <img src={textLogo} height="18" style={{display: "inline-block", marginBottom: 4}} />
        ),
        mailToLink: (
          <a
            href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
            rel="noreferrer"
          />
        ),
      }}
    />
  </div>
);

const WelcomeAlert = ({message, description, type, className}) => (
  <Alert
    className={`my-3 ${className}`}
    message={message}
    description={
      <>
        {description}
        <NoeOpenSourceParagraph />
      </>
    }
    type={type}
  />
);

export const InfoAlert = ({message}) => (
  <Alert style={{border: "none", marginTop: 20, marginBottom: 10}} message={message} type="info" />
);

const FormAlerts = () => {
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  // DATA DEFINITION
  const {datesAlert, formAlert, ticketingAlert} = currentRegistration;

  return datesAlert || formAlert || ticketingAlert ? (
    <Alert
      className="my-4"
      message={t("registrations:messages.registrationIncomplete")}
      type="error"
      description={
        <ul style={{margin: 0}}>
          {[datesAlert, formAlert, ticketingAlert]
            .filter((el) => el)
            .map((alert) => (
              <li key={alert}>{alert}</li>
            ))}
        </ul>
      }
    />
  ) : null;
};

function Registration() {
  const dispatch = useDispatch();

  // *********************** //
  // ******** DATA ********* //
  // *********************** //

  // SELECTORS
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  // DATA DEFINITION
  const {booked, ticketingIsOk, firstSlotIsOk} = currentRegistration;
  const stewardLinkedToRegistration = currentRegistration.steward; // can be undefined
  const currentAvailabilities = currentRegistration.availabilitySlots;
  const projectIsNotOpened = currentProject.openingState === "notOpened";
  const ticketingMandatory = !!currentProject.ticketingMode;

  // FORMS
  const [datesForm] = Form.useForm();
  const [registrationForm] = Form.useForm();
  const [paymentPageVisible, setPaymentPageVisible] = useState(false);

  // ************************ //
  // ******* HELPERS ******** //
  // ************************ //

  // Make the bottom save button sticky
  useStickyShadow(".sticky-bottom-buttons", "bottom");

  const everythingIsOkInDatabase = currentRegistration?.inDatabase.everythingIsOk;

  // ************************* //
  // ********* REDUX ********* //
  // ************************* //

  // When the page appears, and if the user has already saved data,
  // validate forms and make the missing fields appear
  useEffect(() => {
    if (currentRegistration.booked) {
      safeValidateFields(datesForm);
      safeValidateFields(registrationForm);
    }
  }, []);

  const saveFormToRedux = () =>
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        specific: registrationForm.getFieldsValue(true),
      })
    );

  // REGISTER ACTION
  const registerToProject = async (saveSilentlyIfIncomplete = false) => {
    // 1. User has nothing or has dates but incorrect: can't save + message for dates
    // 2. User has correct dates:

    let formIsOk;
    try {
      await registrationForm.validateFields();
      formIsOk = true;
    } catch {
      formIsOk = false;
    }
    safeValidateFields(datesForm);

    const formPageIsOk = firstSlotIsOk && formIsOk;

    // If everything okay, go to the ticketing page
    if (formPageIsOk && !ticketingIsOk && ticketingMandatory && !paymentPageVisible) {
      setPaymentPageVisible(true);
      // Scroll to top of ticketing page
      const welcomeAlertElement = document.getElementById("registration-page-content");
      window.scrollTo({top: welcomeAlertElement.offsetTop, behavior: "smooth"});
      // ... Then, persist to backend if everything okay, or if incomplete registration is allowed and the first slot is properly set
      await saveFormToRedux();
      dispatch(registrationsActions.register(true)); // Don't notify when navigating to ticketing
      return;
    }
    // Check if everything is ok in Redux and in the registration form...
    const everythingOKInRedux = firstSlotIsOk && formIsOk && ticketingIsOk;

    // If saveSilently is off, give the user some insights
    if (!everythingOKInRedux && !saveSilentlyIfIncomplete) {
      // If the registration is already OK in DB but the user wants to save
      // with missing info, it is refused. Don't save data to backend and return.
      if (everythingIsOkInDatabase) {
        message.error(t("registrations:messages.registrationIncompleteCantSaveModifications"));
        return;
      }

      // If the registration is not yet complete, but the user only wants to register (and not just save data)
      if (!firstSlotIsOk) {
        message.error(t("registrations:messages.registrationIncompleteShouldSetDatesOfPresence"));
        return;
      }
    }

    // If the user is already registered, we don't want to save the data automatically of an already registered user silently, only during the first-time registration process.
    if (everythingIsOkInDatabase && saveSilentlyIfIncomplete) return;

    // ... Then, persist to backend if everything okay, or if incomplete registration is allowed and the first slot is properly set
    await saveFormToRedux();
    dispatch(registrationsActions.register(saveSilentlyIfIncomplete));
  };

  // UNREGISTER ACTION
  const unregisterFromProject = () => {
    dispatch(registrationsActions.unregister());
  };

  // ************************* //
  // ****** COMPONENTS ******* //
  // ************************* //

  const RegistrationButton = (props) => (
    <Button
      icon={!everythingIsOkInDatabase && <CheckOutlined />}
      type="primary"
      className={!everythingIsOkInDatabase && "success-button"}
      onClick={() => registerToProject(false)}
      {...props}>
      {everythingIsOkInDatabase
        ? t("registrations:main.saveModifications")
        : !ticketingMandatory || ticketingIsOk
        ? t("registrations:main.registerToTheEvent")
        : t("registrations:main.validateAndPay")}
    </Button>
  );

  const UnregistrationButton = () =>
    (!stewardLinkedToRegistration || (stewardLinkedToRegistration && everythingIsOkInDatabase)) && (
      <Popconfirm
        title={
          everythingIsOkInDatabase
            ? t("registrations:main.unregisterButton.youWillBeUnregisteredFromActivities")
            : t("registrations:main.unregisterButton.youWillLooseYouProgress")
        }
        okText={
          everythingIsOkInDatabase
            ? t("registrations:main.unregisterButton.yesUnregister")
            : t("registrations:main.unregisterButton.yesRestartFromBeginning")
        }
        okButtonProps={{danger: true}}
        cancelText={t("common:no")}
        disabled={stewardLinkedToRegistration}
        icon={<QuestionCircleOutlined style={{color: "red"}} />}
        onConfirm={unregisterFromProject}>
        <Tooltip
          title={
            stewardLinkedToRegistration
              ? t("registrations:main.unregisterButton.linkedToStewardCantUnregister")
              : undefined
          }>
          <Button danger disabled={stewardLinkedToRegistration}>
            {everythingIsOkInDatabase
              ? t("registrations:main.unregisterButton.title")
              : t("registrations:main.erase")}
          </Button>
        </Tooltip>
      </Popconfirm>
    );

  const HelpButton = () => (
    <Button
      type={"link"}
      onClick={() =>
        Modal.info({
          content: (
            <>
              <p>{t("registrations:aProblemToRegister.modal")}</p>
              <a
                href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
                rel="noreferrer">
                {process.env.REACT_APP_CONTACT_US_EMAIL}
              </a>
            </>
          ),
        })
      }>
      {t("registrations:aProblemToRegister.title")}
    </Button>
  );

  return (
    <div className="page-container">
      {(currentProject.full || projectIsNotOpened) && !booked ? (
        <Result
          icon={<div></div>} // No icon
          title={
            projectIsNotOpened
              ? t("registrations:main.eventNotOpen.notYet")
              : t("registrations:main.eventNotOpen.full")
          }
          subTitle={t("registrations:main.eventNotOpen.comeBackLater")}
        />
      ) : (
        <>
          <PageHeading
            className="tabs-page-header"
            icon={<FormOutlined />}
            title={
              everythingIsOkInDatabase
                ? t("registrations:labelMyRegistration")
                : t("registrations:labelRegister")
            }
            customButtons={
              <>
                <HelpButton />
                {booked && <UnregistrationButton />}
              </>
            }
          />

          <ConnectedAsAlert />

          {/* WELCOME MESSAGE AND ALERTS */}
          {everythingIsOkInDatabase === false ? (
            !booked ? (
              // The user is really new and has no saved registration data
              <WelcomeAlert
                message={t("registrations:main.welcomeMessages.newUser.message", {
                  userName: currentUser.firstName,
                })}
                description={t("registrations:main.welcomeMessages.newUser.description", {
                  projectName: currentProject.name,
                })}
                type="info"
              />
            ) : (
              // Data is saved, but registration is incomplete
              <WelcomeAlert
                message={t("registrations:main.welcomeMessages.registrationSaved.message")}
                description={t("registrations:main.welcomeMessages.registrationSaved.description")}
                type="info"
              />
            )
          ) : (
            // The user is successfully registered
            <WelcomeAlert
              className="bounce-in"
              message={t("registrations:main.welcomeMessages.registrationComplete.message")}
              description={
                <Trans
                  ns="registrations"
                  i18nKey="main.welcomeMessages.registrationComplete.description"
                  components={{linkToPlanning: <Link to="../sessions/subscribed" />}}
                />
              }
              type="success"
            />
          )}

          {!paymentPageVisible && <FormAlerts />}

          {/* REGISTRATION PAGE */}
          <div
            style={{marginTop: 30, marginBottom: 30}}
            className="fade-in"
            id={"registration-page-content"}>
            {/* Display first part if:
                - payment page is not visible
                - the user is already fully registered and has a ticket
            */}
            {(!paymentPageVisible || everythingIsOkInDatabase) && (
              <>
                {/* PART 1 - Availabilities */}
                <Divider style={{marginBottom: 35}} />
                <RegistrationAvailabilities
                  datesForm={datesForm}
                  currentProject={currentProject}
                  saveData={saveFormToRedux}
                  currentRegistration={currentRegistration}
                  currentAvailabilities={currentAvailabilities}
                  stewardLinkedToRegistration={stewardLinkedToRegistration}
                />

                {/* PART 2 - Form */}
                <Divider style={{marginBottom: 35}} />
                <h3 className="list-element-header">{t("registrations:form.title")}</h3>
                <p style={{color: "grey", marginBottom: 26}}>
                  <Trans
                    i18nKey="form.mandatoryFieldsNotice"
                    ns="registrations"
                    components={{
                      redColor: <span style={{color: "red"}} />,
                    }}
                  />
                </p>
                <RegistrationForm
                  saveData={saveFormToRedux}
                  formComponents={currentProject.formComponents}
                  initialValues={currentRegistration.specific}
                  form={registrationForm}
                />
              </>
            )}
            {/* PART 3 - Ticketing */}
            <RegistrationTicketing
              setPaymentPageVisible={setPaymentPageVisible}
              paymentPageVisible={paymentPageVisible}
              HelpButton={HelpButton}
              saveData={saveFormToRedux}
              saveDataFn={() => registerToProject(true)}
            />
          </div>

          {/* REGISTRATION BUTTONS BOTTOM BAR */}
          {!paymentPageVisible && (
            <div
              id="secondary-registration-buttons"
              className="sticky-bottom-buttons full-width-content"
              style={{
                position: "sticky",
                paddingBottom: 100,
                transform: "translateY(100px)",
                transition: "all 0.3s ease-in-out",
                bottom: 0,
                zIndex: 50,
                background: "white",
              }}>
              <div className="page-content">
                <div className="containerH" style={{justifyContent: "center", padding: "5px 0"}}>
                  <RegistrationButton size="large" />
                </div>
              </div>
            </div>
          )}

          {/*  Just to let some padding at the end for the secondary buttons to make a great translation and not move the whole page*/}
          <div style={{paddingBottom: 100}}></div>
        </>
      )}
    </div>
  );
}

export default Registration;
