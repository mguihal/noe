import {Alert, Button} from "antd";
import {Availability} from "../utils/Availability";
import dayjs from "dayjs";
import React, {useState} from "react";
import {FormElement} from "../common/FormElement";
import {useDispatch} from "react-redux";
import {registrationsActions} from "../../features/registrations";
import {personName} from "../../helpers/utilities";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "../common/layout/CardElement";
import {InfoAlert} from "./Registration";
import {DateTimeInput} from "../common/inputs/DateTimeInput";

const humanizeDate = (date, customFormat) => dayjs(date).format(customFormat || "D MMMM YYYY");

export function RegistrationAvailabilities({
  currentProject,
  saveData,
  currentRegistration,
  stewardLinkedToRegistration,
  currentAvailabilities,
  datesForm,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [availabilitySlotsInterface, setAvailabilitySlotsInterface] = useState(
    currentAvailabilities.length <= 1 ? "simple" : "advanced"
  );

  const firstSlotStart = currentAvailabilities[0]?.start;
  const firstSlotEnd = currentAvailabilities[0]?.end;

  const stewardLinkedHasDefinedAvailabilities =
    stewardLinkedToRegistration?.availabilitySlots?.length > 0;

  // ************************* //
  // ***** AVAILABILITIES **** //
  // ************************* //

  const changeAvailabilitySlots = (slots) =>
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        availabilitySlots: slots,
      })
    );

  return (
    <>
      {/* SIMPLE */}
      {availabilitySlotsInterface === "simple" && ( // zero availabilitySlots given: simple interface
        <div className="containerV">
          <FormElement
            form={datesForm}
            initialValues={{
              start: firstSlotStart ? dayjs(firstSlotStart) : undefined,
              end: firstSlotEnd ? dayjs(firstSlotEnd) : undefined,
            }}
            onValuesChange={(newValues, allValues) => {
              saveData();
              changeAvailabilitySlots([
                {start: allValues.start?.format(), end: allValues.end?.format()},
              ]);
            }}>
            <div className="list-element-header">
              <h3>{t("registrations:availabilities.title")}</h3>
              <InfoAlert
                message={
                  <Trans
                    i18nKey="availabilities.infoMessage"
                    ns="registrations"
                    values={{
                      start: humanizeDate(currentProject.start),
                      end: humanizeDate(currentProject.end),
                    }}
                  />
                }
              />
            </div>
            <div
              className="containerH"
              style={{float: "left", flexWrap: "wrap", whiteSpace: "nowrap"}}>
              <div className="containerH" style={{alignItems: "baseline"}}>
                {t("registrations:availabilities.iArriveOn")}
                <DateTimeInput
                  id="start"
                  name="start"
                  bordered={!firstSlotStart}
                  defaultPickerValue={dayjs(currentProject.start)}
                  disableDatesIfOutOfProject
                  rules={[
                    {
                      required: true,
                      message: t("registrations:availabilities.errors.addAStartDate"),
                    },
                  ]}
                  disableDatesBeforeNow={false}
                  style={{
                    marginLeft: firstSlotStart ? 4 : 10,
                    marginRight: firstSlotStart ? 4 : 10,
                  }}
                  onOk={() => {
                    document.getElementById("start")?.blur();
                    !firstSlotEnd && setTimeout(() => document.getElementById("end")?.focus(), 600);
                  }}
                />
              </div>
              <div className="containerH" style={{alignItems: "baseline"}}>
                {t("registrations:availabilities.andILeaveOn")}
                <DateTimeInput
                  id="end"
                  name="end"
                  bordered={!firstSlotEnd}
                  defaultPickerValue={dayjs(firstSlotStart || currentProject.start)}
                  disableDatesIfOutOfProject
                  rules={[
                    {
                      required: true,
                      message: t("registrations:availabilities.errors.addAnEndDate"),
                    },
                  ]}
                  disableDatesBeforeNow={false}
                  style={{
                    marginLeft: firstSlotEnd ? 4 : 10,
                    marginRight: firstSlotEnd ? 4 : 10,
                  }}
                  onOk={() => {
                    document.getElementById("end")?.blur();
                    !firstSlotStart &&
                      setTimeout(() => document.getElementById("start")?.focus(), 600);
                  }}
                />
                .
              </div>
            </div>
          </FormElement>
          <div className="containerH" style={{alignItems: "baseline", color: "grey"}}>
            <p>
              {t("registrations:availabilities.datesAreComplicated")}
              <Button
                type="link"
                style={{paddingLeft: 8}}
                onClick={() => setAvailabilitySlotsInterface("advanced")}>
                {t("registrations:availabilities.accessAdvancedEditor")}
              </Button>
            </p>
          </div>
        </div>
      )}

      {/* ADVANCED */}
      {availabilitySlotsInterface === "advanced" && ( // one or more availability: choice between simple and advanced
        <>
          <Availability
            title={t("registrations:availabilities.title")}
            disableDatesIfOutOfProject
            defaultPickerValue={[dayjs(currentProject.start), dayjs(currentProject.start)]}
            data={currentAvailabilities}
            onChange={changeAvailabilitySlots}
          />

          {currentAvailabilities &&
            currentAvailabilities?.length <= 1 && ( // Display only if there's zero or one slot
              <div
                className="containerH"
                style={{marginTop: 25, alignItems: "baseline", color: "grey"}}>
                <p>
                  {t("registrations:availabilities.datesAreSimple")}
                  <Button
                    type="link"
                    style={{paddingLeft: 8}}
                    onClick={() => setAvailabilitySlotsInterface("simple")}>
                    {t("registrations:availabilities.backToSimpleEditor")}
                  </Button>
                </p>
              </div>
            )}
        </>
      )}

      {stewardLinkedToRegistration && ( // Alert when the user is linked to a steward
        <Alert
          style={{marginBottom: 20}}
          message={t("registrations:availabilities.linkedToStewardAlert.message", {
            name: personName(currentRegistration.steward),
          })}
          description={
            <>
              {t("registrations:availabilities.linkedToStewardAlert.description")}
              {stewardLinkedHasDefinedAvailabilities && (
                <CardElement
                  style={{marginTop: 20, marginBottom: 0, opacity: 0.9}}
                  size="small"
                  title={t("registrations:availabilities.linkedToStewardAlert.dates.title")}>
                  <Trans
                    ns="registrations"
                    i18nKey="availabilities.linkedToStewardAlert.dates.description"
                  />
                  <Availability disabled data={stewardLinkedToRegistration.availabilitySlots} />
                </CardElement>
              )}
            </>
          }
          type="info"
        />
      )}
    </>
  );
}
