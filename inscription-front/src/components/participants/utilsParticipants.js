import {useSelector} from "react-redux";
import {currentUserSelectors} from "../../features/currentUser";
import {Alert} from "antd";
import {Trans} from "react-i18next";
import {personName} from "../../helpers/utilities";
import React from "react";

export const ConnectedAsAlert = ({fallback = null}) => {
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  return currentUser._id !== authenticatedUser._id ? (
    <Alert
      type="info"
      showIcon
      style={{marginBottom: 26}}
      message={
        <Trans
          i18nKey="alerts.currentlyConnectedAs"
          ns="users"
          values={{personName: personName(currentUser), email: currentUser?.email}}
        />
      }
    />
  ) : (
    fallback
  );
};
