import {Card, Tag, Tooltip} from "antd";
import React from "react";
import Paragraph from "antd/es/typography/Paragraph";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {listRenderer} from "../../helpers/listUtilities";
import {WarningOutlined} from "@ant-design/icons";
import {getSessionMetadata, getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {getSessionName} from "../../helpers/agendaUtilities";
import {t} from "i18next";
import {
  CategoryTagWithVolunteeringMajoration,
  getSessionInfoAlert,
  SessionFilling,
  SubscribeUnsubscribeButton,
} from "./utilsSessions";
import {getSessionSubscription} from "../../helpers/registrationsUtilities";

function SessionShowSmall({
  session,
  onSubscribe,
  onUnsubscribe,
  hoverable = false,
  style,
  onClick,
  withMargins,
}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const volunteeringCoefficient = getVolunteeringCoefficient(session);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const sessionSubscription = getSessionSubscription(currentRegistration, session);

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    shouldBeDimmed,
    inConflictWithRegistrationDates,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const cardOpacity = shouldBeDimmed ? 0.5 : 1;

  return (
    <div className="containerV" style={{height: "100%", padding: withMargins && "15px 15px"}}>
      <div style={{position: "relative", zIndex: 1}}>
        <CategoryTagWithVolunteeringMajoration
          volunteeringCoefficient={volunteeringCoefficient}
          category={session.activity?.category}
          style={{
            opacity: cardOpacity,
            color: session.activity?.category.color,
            backgroundColor: "white",
            position: "absolute",
            top: 4,
            right: -4,
          }}
        />
        {inConflictWithRegistrationDates && (
          <div
            style={{
              position: "absolute",
              background: "white",
              borderRadius: 50,
              height: 26,
              width: 26,
              paddingLeft: 4,
              paddingTop: 1.5,
              top: 4,
              left: 4,
            }}>
            <WarningOutlined style={{fontSize: 18, color: "red"}} />
          </div>
        )}
      </div>
      <Tooltip title={infoAlert?.message} mouseEnterDelay={0.6}>
        <Card
          hoverable={hoverable}
          title={
            <div style={{textAlign: "center", marginTop: 12, marginBottom: 12}}>
              <div
                style={{
                  // Allow activity name to go on 3 lines not more
                  whiteSpace: "normal",
                  fontWeight: "bold",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                }}>
                {getSessionName(session)}
              </div>
              <CategoryTagWithVolunteeringMajoration
                category={session.activity?.category}
                style={{
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  maxWidth: 300,
                  margin: "-5px 0 -10px 0",
                  opacity: 0.85,
                }}>
                {session.activity?.category.name}
              </CategoryTagWithVolunteeringMajoration>
            </div>
          }
          style={{
            opacity: cardOpacity,
            transition: "opacity 0.3s ease-in-out, box-shadow .3s, border-color .3s", // Also get the transition for bow-shadow cause otherwie it will be overriden
            border:
              volunteeringCoefficient > 0
                ? `2px solid ${session.activity.category.color}`
                : isSteward && "1px solid rgba(0, 0, 255, 0.18)",
            outline: inConflictWithRegistrationDates ? "3px solid #FF9999" : undefined,
            outlineOffset: inConflictWithRegistrationDates ? 4 : undefined,
            background: isSteward && "rgba(0, 0, 255, 0.06)",
            height: "100%",
            overflow: "hidden",
            ...style,
          }}
          headStyle={{
            background: session.activity?.category.color,
            color: "white",
            borderRadius: 0,
          }}
          onClick={onClick}>
          <div style={{textAlign: "center", marginBottom: 15}}>
            {listRenderer.longDateTimeRangeFormat(session.start, session.end)}
          </div>
          {session.activity?.summary && (
            <Paragraph ellipsis={{rows: 2}} style={{color: "grey"}}>
              {session.activity.summary}
            </Paragraph>
          )}
          <div className="containerH buttons-container">
            <strong style={{flexGrow: 0, marginBottom: 15}}>{t("sessions:filling.label")}:</strong>
            <SessionFilling
              showLabel={false}
              computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
              numberOfParticipants={session.numberParticipants}
            />
          </div>
          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              margin: 8,
              rowGap: 8,
              justifyContent: "center",
            }}>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>
        </Card>
      </Tooltip>
      <div
        style={{
          position: "relative",
          bottom: 16,
          minHeight: 32,
          marginLeft: "25%",
          marginRight: "25%",
          background: "white",
        }}>
        <SubscribeUnsubscribeButton
          onUnsubscribe={onUnsubscribe}
          onSubscribe={onSubscribe}
          sessionIsFull={sessionIsFull}
          isSteward={isSteward}
          sessionSubscription={sessionSubscription}
          tooltipMessage={infoAlert?.message}
          registrationIncomplete={registrationIncomplete}
          alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
          alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
          participantIsNotAvailable={participantIsNotAvailable}
          currentProject={currentProject}
          volunteeringBeginsSoon={volunteeringBeginsSoon}
          block
        />
      </div>
    </div>
  );
}

export default SessionShowSmall;
