import {Button, Drawer, Dropdown} from "antd";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DownOutlined,
  PictureOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import dayjs from "dayjs";
import React, {useState} from "react";
import {t} from "i18next";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {CELLS_DISPLAY_HEIGHTS} from "../../helpers/agendaUtilities";

//************************************//
//****** AGENDA BASE COMPONENTS ******//
//************************************//

// Left and right arrows to navigate in the agenda days
export const NavButton = ({type, onClick}) => (
  <Button
    type="link"
    size="large"
    icon={type === "forward" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
    onClick={onClick}
  />
);

// Renders the date labels on the side of the agenda
export const getTimeScaleLabelComponent =
  (cellDisplayHeight) =>
  ({time, ...otherProps}) => {
    // If there is no 'time', it means it's the very first, which has no time in it.
    const style = time
      ? {height: cellDisplayHeight, lineHeight: `${cellDisplayHeight}px`}
      : {height: cellDisplayHeight / 2}; // It should be twice as small.

    // Only display time when it's o'clock
    const isOClock = time?.getMinutes() === 0;

    return (
      <DayView.TimeScaleLabel
        className="timescale-cell" // Needed to get a hook
        time={isOClock ? time : undefined}
        style={style}
        {...otherProps}
      />
    );
  };

//*********************************//
//****** AGENDA CONTROLS BAR ******//
//*********************************//

export const NumberOfDaysSelector = ({
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  isMobileView = false,
}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  return (
    <Dropdown
      trigger={["click"]}
      menu={{
        onClick: (item) => setNumberOfDaysDisplayed(item.key),
        items: [
          {
            label: t("sessions:agenda.numberOfDaysSelector.allTheEvent"),
            key:
              dayjs(currentProject.end)
                .startOf("day")
                .diff(dayjs(currentProject.start).startOf("day"), "day") + 1,
          },
          {type: "divider"},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 1}), key: 1},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 3}), key: 3},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 5}), key: 5},
          {type: "divider"},
          {
            label: "Autre",
            children: [...Array(14).keys()].map((i) => ({
              label: t("sessions:agenda.numberOfDaysSelector.days", {count: i + 2}),
              key: i + 2,
            })),
          },
        ],
      }}>
      <Button>
        {isMobileView
          ? t("sessions:agenda.numberOfDaysSelector.daysMobile", {
              count: parseInt(numberOfDaysDisplayed),
            })
          : t("sessions:agenda.numberOfDaysSelector.days", {
              count: parseInt(numberOfDaysDisplayed),
            })}
        {!isMobileView && <DownOutlined style={{marginLeft: 6}} />}
      </Button>
    </Dropdown>
  );
};

export const AgendaSettingsDrawer = ({
  children,
  cellDisplayHeight,
  slotsOnEachOther,
  setAgendaParams,
  buttonStyle,
}) => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const printAgendaView = () => {
    const toHide = [
      document.getElementsByTagName("aside")[0],
      document.getElementsByTagName("header")[0],
      document.getElementsByClassName("navbar-menu-button")[0],
      document.getElementsByClassName("MuiToolbar-root")[0],
      document.getElementsByClassName("session-filter-toolbar")[0],
      document.getElementsByClassName("ant-segmented")[0],
      ...document.getElementsByClassName("ant-drawer"),
    ].filter((el) => !!el);
    const toHideWithOldValues = toHide.map((el) => ({el, value: el.style.display}));

    // Get the before-last-one, then last child
    const agendaSpace = Array.from(
      document.getElementsByClassName("scheduler-wrapper")[0].childNodes
    ).slice(-2, -1)[0].lastChild;

    toHide.forEach((el) => (el.style.display = "none"));
    agendaSpace.style.overflowY = "visible";

    window.print();

    toHideWithOldValues.forEach(({el, value}) => (el.style.display = value));
    agendaSpace.style.overflowY = "auto";
  };

  return (
    <>
      <Button
        style={buttonStyle}
        icon={<SettingOutlined />}
        type="link"
        shape="circle"
        onClick={() => setDrawerOpen(true)}
      />
      <Drawer placement="right" onClose={() => setDrawerOpen(false)} open={drawerOpen}>
        <h3>{t("sessions:agenda.settingsDrawer.title")}</h3>
        <FormElement style={{marginTop: 26}} className="container-grid">
          {children}

          {/*Select if we want to see events one on the other or one next to each other. */}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.slotsOnEachOther.label")}
            onChange={(value) => setAgendaParams({slotsOnEachOther: value})}
            defaultValue={slotsOnEachOther}
            options={[
              {
                value: false,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.false"),
              },
              {
                value: true,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.true"),
              },
            ]}
          />

          {/*Increase or decrease the cells heights*/}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.cellDisplayHeight.label")}
            onChange={(value) => setAgendaParams({cellDisplayHeight: value})}
            defaultValue={cellDisplayHeight}
            options={[
              {
                value: CELLS_DISPLAY_HEIGHTS[0],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.compact"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[1],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.comfort"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[2],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.large"),
              },
            ]}
          />

          <Button icon={<PictureOutlined />} onClick={printAgendaView}>
            {t("sessions:agenda.settingsDrawer.exportAgendaViewButton")}
          </Button>
        </FormElement>
      </Drawer>
    </>
  );
};
