import {Trans, useTranslation} from "react-i18next";
import {t} from "i18next";
import {useSelector} from "react-redux";
import {registrationsSelectors} from "../../features/registrations";
import {teamsSelectors} from "../../features/teams";
import {Button, Popconfirm, Tag, Tooltip} from "antd";
import {CheckOutlined, QuestionCircleOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import React from "react";
import {FillingInput} from "../common/inputs/FillingInput";

export const SessionFilling = ({
  computedMaxNumberOfParticipants,
  numberOfParticipants,
  showLabel = true,
  ...otherProps
}) => {
  const {t} = useTranslation();
  // cf. getMaxParticipantsBasedOnPlaces() function in backend
  if (computedMaxNumberOfParticipants === null) return t("sessions:filling.freeSubscription");

  const fillingText =
    numberOfParticipants !== undefined && computedMaxNumberOfParticipants
      ? numberOfParticipants + "/" + computedMaxNumberOfParticipants
      : 0;

  return (
    <FillingInput
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0}}}
      label={showLabel ? t("sessions:filling.label") : undefined}
      percent={(numberOfParticipants / computedMaxNumberOfParticipants) * 100}
      status={numberOfParticipants >= computedMaxNumberOfParticipants ? "exception" : ""}
      text={fillingText}
      {...otherProps}
    />
  );
};

export const getSessionInfoAlert = (
  sessionSubscription,
  isSteward,
  blockSubscriptions,
  registrationIncomplete,
  participantIsNotAvailable,
  sessionIsFull,
  alreadyStewardOnOtherSession,
  alreadySubscribedToOtherSession,
  inConflictWithRegistrationDates
) => {
  let message;
  if (isSteward) {
    message = {
      type: "info",
      message: t("sessions:alerts.isSteward"),
    };
  } else if (sessionSubscription?.team) {
    message = {
      type: "info",
      message: t("sessions:alerts.belongsToTeam"),
    };
  } else if (blockSubscriptions) {
    return; // Don't show anything if subscriptions are blocked for project
  } else if (registrationIncomplete) {
    message = {
      type: "info",
      message: t("sessions:alerts.mustCompleteRegistration"),
    };
  } else if (participantIsNotAvailable) {
    message = {
      type: "warning",
      message: t("sessions:alerts.participantIsNotAvailable"),
    };
  } else if (sessionIsFull) {
    message = {type: "info", message: t("sessions:alerts.sessionIsFull")};
  } else if (alreadyStewardOnOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadyStewardOnOtherSession"),
    };
  } else if (alreadySubscribedToOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadySubscribedToOtherSession"),
    };
  }

  // TODO this doesn't work anymore, looks like it is never shown
  if (inConflictWithRegistrationDates) {
    message = {
      message: (
        <>
          <p>{t("sessions:alerts.inConflictWithRegistrationDates")}</p>
          {message.message}
        </>
      ),
      ...(message || []),
    };
  }

  return message;
};

export const SubscribeUnsubscribeButton = ({
  sessionSubscription,
  onSubscribe,
  onUnsubscribe,
  sessionIsFull,
  registrationIncomplete,
  participantIsNotAvailable,
  isSteward,
  alreadySubscribedToOtherSession,
  alreadyStewardOnOtherSession,
  currentProject,
  tooltipMessage,
  volunteeringBeginsSoon,
  block = false,
}) => {
  const authenticatedUserRegistration = useSelector(registrationsSelectors.selectAuthenticated);
  const teams = useSelector(teamsSelectors.selectList);

  const blockSubscriptionsOnProject =
    currentProject.blockSubscriptions &&
    authenticatedUserRegistration &&
    !authenticatedUserRegistration.role;

  const blockVolunteeringUnsubscribeIfBeginsSoon =
    currentProject.blockVolunteeringUnsubscribeIfBeginsSoon;

  const registrationButtonClassName =
    alreadySubscribedToOtherSession || alreadyStewardOnOtherSession
      ? "warning-button"
      : "success-button";

  const subscriptionIsDisabled =
    sessionIsFull ||
    registrationIncomplete ||
    participantIsNotAvailable ||
    isSteward ||
    ((alreadySubscribedToOtherSession || alreadyStewardOnOtherSession) &&
      currentProject.notAllowOverlap);

  const disabledStyle = {borderColor: "rgb(100,100,100)", color: "#555"};

  if (isSteward) {
    return (
      <Tooltip title={tooltipMessage}>
        <Button icon={<UserOutlined />} type="dashed" disabled style={disabledStyle} block={block}>
          {t("stewards:label")}
        </Button>
      </Tooltip>
    );
  } else if (sessionSubscription) {
    // -------- USER IS SUBSCRIBED : show UNSUBSCRIBE button ------------

    // Case for team members
    const teamLinkedToRegistration = teams.find((t) => t._id === sessionSubscription.team);
    if (teamLinkedToRegistration) {
      return (
        <Tooltip title={tooltipMessage}>
          <Button
            icon={<TeamOutlined />}
            type="dashed"
            disabled
            style={disabledStyle}
            block={block}>
            {t("teams:teamMember")}
          </Button>
        </Tooltip>
      );
    }

    if (blockSubscriptionsOnProject) {
      return (
        <Button icon={<CheckOutlined />} disabled block={block}>
          Inscrit⋅e
        </Button>
      );
    }

    // Case for volunteering that begins soon but not allowed
    if (
      volunteeringBeginsSoon &&
      blockVolunteeringUnsubscribeIfBeginsSoon &&
      !authenticatedUserRegistration.role
    )
      return (
        <Tooltip title={t("sessions:subscribeUnsubscribeButton.volunteeringSoonWarning.tooltip")}>
          <Button disabled block={block}>
            {t("sessions:unsubscribe")}
          </Button>
        </Tooltip>
      );

    const confirmUnsubscribeMessage = volunteeringBeginsSoon ? (
      <div style={{maxWidth: 450}}>
        <Trans
          ns="sessions"
          i18nKey="subscribeUnsubscribeButton.volunteeringSoonWarning.confirmText"
        />
      </div>
    ) : (
      t("sessions:subscribeUnsubscribeButton.confirmUnsubscribe")
    );

    return (
      <Popconfirm
        title={confirmUnsubscribeMessage}
        onConfirm={onUnsubscribe}
        okText={t("common:yes")}
        okButtonProps={{danger: true}}
        icon={<QuestionCircleOutlined style={{color: "red"}} />}
        cancelText={t("common:no")}>
        <Button type={teamLinkedToRegistration && "dashed"} danger block={block}>
          {t("sessions:unsubscribe")}
        </Button>
      </Popconfirm>
    );
  } else {
    // -------- USER IS UNSUBSCRIBED : show SUBSCRIBE button ------------

    const subscribeButton = (
      <Button
        icon={<CheckOutlined />}
        type="primary"
        onClick={!tooltipMessage && onSubscribe}
        className={subscriptionIsDisabled ? undefined : registrationButtonClassName}
        disabled={subscriptionIsDisabled}
        block={block}>
        S'inscrire
      </Button>
    );

    return tooltipMessage ? (
      <Popconfirm
        title={
          <>
            <p>{tooltipMessage}</p>
            {t("sessions:stillWantToRegisterConfirm")}
          </>
        }
        onConfirm={onSubscribe}
        okText={t("common:yes")}
        okButtonProps={{className: registrationButtonClassName}}
        cancelText={t("common:no")}
        disabled={subscriptionIsDisabled}>
        <Tooltip title={tooltipMessage}>{subscribeButton}</Tooltip>
      </Popconfirm>
    ) : (
      subscribeButton
    );
  }
};

export const CategoryTagWithVolunteeringMajoration = ({
  volunteeringCoefficient,
  category,
  children,
  ...props
}) => {
  let volunteeringMajoration;
  if (volunteeringCoefficient > 0 && volunteeringCoefficient !== 1) {
    const percent = Math.round((volunteeringCoefficient - 1) * 100);
    volunteeringMajoration = `${Math.sign(percent) === 1 ? "+" : "-"}${Math.abs(percent)}%`;
  }

  return children || volunteeringMajoration ? (
    <Tag
      title={
        volunteeringMajoration
          ? t("sessions:volunteeringMajorationTag", {volunteeringMajoration})
          : undefined
      }
      color={category.color}
      {...props}>
      {children}
      {volunteeringMajoration && <strong style={{fontSize: 15}}> {volunteeringMajoration}</strong>}
    </Tag>
  ) : null;
};
