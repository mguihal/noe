import {Alert, Button, Card, List, Tag, Tooltip} from "antd";
import React from "react";
import {InputElement} from "../common/InputElement";
import {CardElement} from "../common/layout/CardElement";
import {FormElement} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {stewardsActions} from "../../features/stewards";
import {activitiesActions} from "../../features/activities";
import {placesActions} from "../../features/places";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {
  ClockCircleOutlined,
  EnvironmentOutlined,
  ExclamationCircleOutlined,
  FullscreenOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {Link, navigate} from "@reach/router";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {getSessionMetadata, getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {generateRegistrationsColumns, generateSubscriptionInfo} from "../../helpers/tableUtilities";
import {personName} from "../../helpers/utilities";
import TextDisplayer from "../common/TextDisplayer";
import {t} from "i18next";
import {URLS} from "../../app/configuration";
import {
  CategoryTagWithVolunteeringMajoration,
  getSessionInfoAlert,
  SessionFilling,
  SubscribeUnsubscribeButton,
} from "./utilsSessions";
import {getSessionName} from "../../helpers/agendaUtilities";
import {PageHeading} from "../common/layout/PageHeading";
import {getSessionSubscription} from "../../helpers/registrationsUtilities";

function SessionShow({id, navigatePathRoot, asModal = false}) {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const sessions = useSelector(sessionsSelectors.selectList);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(sessionsActions.loadList());
    },
    undefined,
    asModal // Don't clean if displayed as a modal so there is no glitch when making the page full screen
  );

  // Prevents rendering as long as we don't have crucial info to display
  if (!session._id) return null;

  const sessionSubscription = getSessionSubscription(currentRegistration, session);

  const sameTimeSessionsRegistered = session.sameTimeSessions?.filter(
    (sts) => sts.sameTimeSessionRegistered
  );

  const otherSessionsOfSameActivityRegisterable = sessions.filter((otherSession) => {
    const notCurrentSession = session._id !== otherSession._id;
    const hasSameActivity = session.activity?._id === otherSession.activity._id;
    return notCurrentSession && hasSameActivity;
  });

  const subscriptionInfoColumn = {
    title: t("sessions:schema.subscriptionInfo.label"),
    render: (text, record) =>
      generateSubscriptionInfo(record, getSessionSubscription(record, session), currentProject._id),
    sorter: (a, b) =>
      listSorter.date(
        getSessionSubscription(a, session).updatedAt,
        getSessionSubscription(b, session).updatedAt
      ),
  };

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    inConflictWithRegistrationDates,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const onSubscribe = () => dispatch(sessionsActions.subscribe());
  const onUnsubscribe = () => dispatch(sessionsActions.unsubscribe());

  return (
    <div className="page-container containerV fade-in">
      <PageHeading
        backButton={!asModal}
        className="page-header"
        title={getSessionName(session)}
        customButtons={
          <>
            {asModal && (
              <Tooltip title={t("sessions:show.showFullPage")}>
                <Button
                  type="link"
                  style={{flexGrow: 0}}
                  onClick={() => navigate(`${navigatePathRoot}${session._id}`)}
                  icon={<FullscreenOutlined />}
                />
              </Tooltip>
            )}
            <SubscribeUnsubscribeButton
              onUnsubscribe={onUnsubscribe}
              onSubscribe={onSubscribe}
              sessionIsFull={sessionIsFull}
              isSteward={isSteward}
              sessionSubscription={sessionSubscription}
              registrationIncomplete={registrationIncomplete}
              tooltipMessage={infoAlert?.message}
              alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
              alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
              participantIsNotAvailable={participantIsNotAvailable}
              currentProject={currentProject}
              volunteeringBeginsSoon={volunteeringBeginsSoon}
            />
          </>
        }
      />

      <FormElement>
        <div className="summary">
          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              marginTop: 15,
              rowGap: 8,
              marginBottom: 26,
            }}>
            <CategoryTagWithVolunteeringMajoration
              volunteeringCoefficient={getVolunteeringCoefficient(session)}
              category={session.activity?.category}>
              {session.activity?.category.name}
            </CategoryTagWithVolunteeringMajoration>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>

          {!infoAlert && sessionSubscription && (
            <Alert
              type="success"
              showIcon
              style={{marginBottom: 26}}
              message={t("sessions:show.youAreSubscribed")}
            />
          )}
          {infoAlert && <Alert style={{marginBottom: 26}} {...infoAlert} />}

          {session.activity?.summary?.length > 0 && (
            <CardElement>{session.activity?.summary}</CardElement>
          )}
        </div>
        <div className="container-grid two-thirds-one-third">
          <div style={{overflowY: "auto"}}>
            {session.activity?.description?.length > 0 && (
              <CardElement title={t("sessions:schema.description.label")}>
                <TextDisplayer value={session.activity?.description} />
              </CardElement>
            )}
            {session.participants && (
              <TableElement.WithTitle
                showHeader
                icon={<TeamOutlined />}
                title={t("registrations:label", {count: Infinity})}
                subtitle={t("sessions:show.youSeeThisInfoBecauseYouAre", {
                  role: currentRegistration.role
                    ? t("registrations:roles.orgaOfTheEvent")
                    : t("registrations:roles.stewardOnThisSession"),
                })}
                columns={[...generateRegistrationsColumns(currentProject), subscriptionInfoColumn]}
                onRow={(record) => ({
                  onDoubleClick:
                    currentRegistration.role &&
                    (() =>
                      navigate(
                        `${URLS.ORGA_FRONT}/${currentProject._id}/participants/${record._id}`
                      )),
                })}
                dataSource={session.participants}
              />
            )}
          </div>
          <div className="container-grid" style={{height: "fit-content"}}>
            <CardElement title={t("sessions:show.info")}>
              <div className="containerV container-grid">
                <InputElement.Custom
                  icon={<ClockCircleOutlined />}
                  label={t("sessions:show.timeSlot")}>
                  {session.slots.length > 1 && (
                    <Alert
                      message={t("sessions:show.thisSessionHasMultipleSlots", {
                        count: session.slots.length,
                      })}
                      style={{marginBottom: 12}}
                    />
                  )}
                  <List
                    rowKey="_id"
                    dataSource={session.slots}
                    renderItem={(slot) => (
                      <div>{listRenderer.longDateTimeRangeFormat(slot.start, slot.end)}</div>
                    )}
                  />
                </InputElement.Custom>

                {session.stewards.length > 0 && (
                  <InputElement.Custom
                    icon={<UserOutlined />}
                    label={t("stewards:label", {count: Infinity})}>
                    <List
                      dataSource={session.stewards}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{personName(item)}</Tooltip>
                        </div>
                      )}
                    />
                  </InputElement.Custom>
                )}

                <SessionFilling
                  computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
                  numberOfParticipants={session.numberParticipants}
                />

                {currentProject.usePlaces && session.places.length > 0 && (
                  <InputElement.Custom
                    label={t("sessions:show.place")}
                    icon={<EnvironmentOutlined />}
                    formItemProps={{style: {marginBottom: 0}}}>
                    <List
                      dataSource={session.places}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{item.name}</Tooltip>
                        </div>
                      )}
                    />
                  </InputElement.Custom>
                )}
              </div>
            </CardElement>

            {sameTimeSessionsRegistered?.length > 0 && (
              <CardElement
                icon={<ExclamationCircleOutlined />}
                title={t("sessions:show.conflicts")}
                style={{borderColor: "orange"}}>
                <p>
                  Ce sont les sessions auxquelles vous êtes déjà inscrit⋅e, et qui entrent en
                  conflit car elles se déroulent sur les mêmes heures.
                </p>
                <List
                  dataSource={sameTimeSessionsRegistered}
                  renderItem={(stSession) => (
                    <Link
                      to={
                        asModal
                          ? `../../${stSession.sameTimeSession._id}`
                          : `../${stSession.sameTimeSession._id}`
                      }>
                      <Card style={{marginTop: 10, borderColor: "orange"}} hoverable>
                        <strong>{stSession.sameTimeSession.activity.name}</strong>
                        <List
                          rowKey="_id"
                          dataSource={stSession.sameTimeSlots}
                          renderItem={(slot) => (
                            <div>
                              {listRenderer.longDateTimeRangeFormat(
                                slot.sameTimeSlot.start,
                                slot.sameTimeSlot.end
                              )}
                            </div>
                          )}
                        />
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
            {otherSessionsOfSameActivityRegisterable?.length > 0 && (
              <CardElement icon={<ClockCircleOutlined />} title={t("sessions:show.otherTimeSlots")}>
                <List
                  grid
                  style={{margin: -4}}
                  dataSource={otherSessionsOfSameActivityRegisterable}
                  renderItem={(session) => (
                    <Link to={asModal ? `../../${session._id}` : `../${session._id}`}>
                      <Card style={{margin: 4}} hoverable>
                        {listRenderer.longDateFormat(session.start)}
                        <br />
                        {listRenderer.timeRangeFormat(session.start, session.end)}
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
          </div>
        </div>
      </FormElement>
    </div>
  );
}

export default SessionShow;
