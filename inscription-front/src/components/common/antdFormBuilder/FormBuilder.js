import React from "react";
import isArray from "lodash/isArray";
import find from "lodash/find";
import {Col, Row} from "antd";
import FormBuilderField from "./FormBuilderField";
import "./FormBuilder.css";

const widgetMap = {};

function getWidget(widget) {
  if (!widget) return null;
  if (typeof widget === "string") {
    if (!widgetMap[widget] || !widgetMap[widget].widget) {
      throw new Error(
        `Widget '${widget}' not found, did you defined it by FormBuilder.defineComponent?`
      );
    }
    return widgetMap[widget].widget;
  }
  return widget;
}

function normalizeMeta(meta) {
  let fields = isArray(meta) ? meta : meta.fields;
  if (!fields) fields = [meta];

  fields = fields.map((field) => {
    const widget = getWidget(field.widget);
    const viewWidget = getWidget(field.viewWidget);
    const dynamic = field.dynamic !== false;
    // Find metaConvertor
    const item = find(
      Object.values(widgetMap),
      (entry) => (entry.widget === widget || entry.widget === viewWidget) && entry.metaConvertor
    );
    if (item) {
      const newField = item.metaConvertor(field);
      if (!newField) {
        throw new Error(`metaConvertor of '${String(field.widget)}' must return a field`);
      }
      return {...newField, viewWidget, widget, dynamic, type: newField.widget};
    }
    return {...field, widget, viewWidget, dynamic, type: field.widget};
  });
  if (isArray(meta) || !meta.fields) {
    return {fields};
  }
  return {
    ...meta,
    fields,
  };
}

const isDefinedConditional = (conditional) =>
  conditional?.show !== null && conditional?.when !== null && conditional?.eq !== "";

export const shouldShowField = (
  conditional: {show: boolean, when: string, eq: string},
  fields,
  dependencyFieldValue
) => {
  if (!isDefinedConditional(conditional)) return true;

  const dependencyField = fields.find((field) => field.key === conditional.when);

  const equalityFunction =
    // For checkboxes
    dependencyField.type === "checkbox"
      ? (value) => (conditional.eq?.toLowerCase() !== "false" ? value : !value)
      : // For numbers
      dependencyField.type === "number"
      ? (value) => value === parseFloat(conditional.eq)
      : // For select boxes. You can make AND assertions using " AND ", and OR assertions with ";".
      // Ex: "one;two AND three;four;five" means we need at least "one" or "two" AND "three", "four" or "five" to be valid
      dependencyField.type === "checkbox-group"
      ? (value) =>
          conditional.eq
            .split(" AND ")
            .every((andCondition) =>
              andCondition.split(";").find((eqString) => value?.includes(eqString))
            )
      : // For everything else, that is stored as string: text, select, radio, etc.
        // If there are multiple possible values separated by ";", compare to each of them:
        // Ex: "one" / "one;two" / "heyHowAreYou;someThing;someStuff"
        (value) => conditional.eq.split(";").find((eqString) => eqString === value);

  return equalityFunction(dependencyFieldValue) ? conditional.show : !conditional.show;
};

function FormBuilder(props) {
  const {getMeta, form} = props;
  const meta = getMeta ? getMeta(form, props) : props.meta;
  return <FormBuilderInner {...props} form={form ? form.current || form : null} meta={meta} />;
}

function FormBuilderInner(props) {
  const {meta, viewMode, initialValues, disabled = false, form = null} = props;
  if (!meta) return null;

  const newMeta = normalizeMeta(meta);
  newMeta.viewMode = newMeta.viewMode || viewMode;
  newMeta.initialValues = newMeta.initialValues || initialValues;
  const {fields, columns = 1, gutter = 10} = newMeta;

  const elements = fields
    .filter((field) =>
      shouldShowField(field.conditional, fields, form.getFieldValue(field.conditional.when))
    )
    .map((field) => (
      <FormBuilderField
        key={field.key}
        field={field}
        disabled={disabled}
        meta={newMeta}
        form={form}
      />
    ));

  // RENDER WITH ONE COLUMN
  if (columns === 1) {
    return elements;
  }

  // RENDER WITH MULTIPLE COLUMNS
  const rows = [];
  // for each column , how many grid cols
  const spanUnit = 24 / columns;
  // eslint-disable-next-line
  for (let i = 0; i < elements.length; ) {
    const cols = [];
    for (
      let j = 0;
      (j < columns || j === 0) && // total col span is less than columns
      i < elements.length && // element exist
      (!["left", "both"].includes(fields[i].clear) || j === 0); // field doesn't need to start a new row

    ) {
      const fieldSpan = fields[i].colSpan || 1;
      cols.push(
        <Col key={j} span={Math.min(24, spanUnit * fieldSpan)}>
          {elements[i]}
        </Col>
      );
      j += fieldSpan;
      if (["both", "right"].includes(fields[i].clear)) {
        i += 1;
        break;
      }
      i += 1;
    }
    rows.push(
      <Row key={i} gutter={gutter}>
        {cols}
      </Row>
    );
  }
  return rows;
}

FormBuilder.defineWidget = (name, widget, metaConvertor = null) => {
  if (widgetMap[name]) throw new Error(`Widget "${name}" already defined.`);
  widgetMap[name] = {
    widget,
    metaConvertor,
  };
};

FormBuilder.useForceUpdate = (filter) => {
  const [, updateState] = React.useState();
  const forceUpdate = () => updateState({});
  return forceUpdate;
};

export default FormBuilder;
