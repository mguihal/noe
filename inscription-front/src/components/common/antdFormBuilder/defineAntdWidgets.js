import {Checkbox, DatePicker, Input, InputNumber, Radio, Select} from "antd";
import isArray from "lodash/isArray";
import isPlainObject from "lodash/isPlainObject";
import React from "react";
import FormBuilder from "./FormBuilder";
import dayjs from "dayjs";
import {CardElement} from "../layout/CardElement";
import {CompatibleFormBuilder} from "../RegistrationForm";
import {PhoneInputComponent} from "../inputs/PhoneInput";

const mapOptions = (options) => {
  if (!isArray(options)) {
    throw new Error("Options should be array in form builder meta.");
  }
  return options.map((opt) => {
    if (isArray(opt)) {
      return {value: opt[0], label: opt[1]};
    } else if (isPlainObject(opt)) {
      return opt;
    } else {
      return {value: opt, label: opt};
    }
  });
};

const defineAntdWidgets = () => {
  // Text Fields
  FormBuilder.defineWidget("input", Input);
  FormBuilder.defineWidget("phoneNumber", (props) => <PhoneInputComponent bordered {...props} />);
  FormBuilder.defineWidget("textarea", Input.TextArea);
  FormBuilder.defineWidget("number", InputNumber);

  // Choice selection
  FormBuilder.defineWidget("checkbox", Checkbox, (field) => {
    return {...field, children: field.placeholder, valuePropName: "checked"};
  });
  FormBuilder.defineWidget("radio-group", Radio.Group, (field) => {
    const RadioComp = field.buttonGroup ? Radio.Button : Radio;
    if (field.options && !field.children) {
      return {
        ...field,
        widgetProps: {
          ...field.widgetProps,
          name: field.key,
        },
        children: (
          <div className={"containerV"}>
            {mapOptions(field.options).map((opt) => (
              <RadioComp value={opt.value} key={opt.value}>
                {opt.label}
              </RadioComp>
            ))}
          </div>
        ),
      };
    }
    return field;
  });
  FormBuilder.defineWidget("checkbox-group", Checkbox.Group, (field) => {
    if (field.options && !field.children) {
      return {
        ...field,
        children: (
          <div className={"containerV"} style={{alignItems: "baseline", marginLeft: -8}}>
            {mapOptions(field.options).map((opt) => (
              <Checkbox value={opt.value} key={opt.value}>
                {opt.label}
              </Checkbox>
            ))}
          </div>
        ),
      };
    }
    return field;
  });
  FormBuilder.defineWidget("select", Select, (field) => {
    if (field.options && !field.children) {
      return {
        ...field,
        children: mapOptions(field.options).map((opt) => (
          <Select.Option
            label={opt.label}
            value={opt.value}
            key={opt.value}
            disabled={opt.disabled}>
            {opt.children || opt.label}
          </Select.Option>
        )),
      };
    }
    return field;
  });

  // Dates
  FormBuilder.defineWidget("date-picker", ({value, onChange, ...props}) => (
    <DatePicker
      {...{
        ...props,
        format: dayjs.localeData().longDateFormat("LLL"),
        value: value ? dayjs(value) : undefined,
        onChange: (value) => onChange(value?.toISOString()),
        showTime: {minuteStep: 5},
      }}
    />
  ));
  FormBuilder.defineWidget("day-picker", ({value, onChange, ...props}) => (
    <DatePicker
      {...{
        ...props,
        format: dayjs.localeData().longDateFormat("L"),
        value: value ? dayjs(value, "DD/MM/YYYY") : undefined,
        onChange: (value) => onChange(value?.format("DD/MM/YYYY")),
      }}
    />
  ));

  // HTML Content
  FormBuilder.defineWidget("content", (props) => (
    <div dangerouslySetInnerHTML={{__html: props.placeholder}} />
  ));

  // Card where you can put stuff inside
  FormBuilder.defineWidget(
    "panel",
    ({placeholder, components, form, initialValues, forceUpdate}) => (
      <CardElement title={placeholder} headStyle={{background: "#fafafa"}}>
        <CompatibleFormBuilder
          form={form}
          formComponents={components}
          initialValues={initialValues}
          forceUpdate={forceUpdate}
        />
      </CardElement>
    )
  );

  // REPLACEMENTS : for now, this one doesn't work
  FormBuilder.defineWidget("tags", (props) => (
    <Input {...props} rules={[...(props.rules || []), {email: true}]} />
  ));
};

export default defineAntdWidgets;
