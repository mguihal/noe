import React, {useEffect, useMemo} from "react";
import FormBuilder from "./antdFormBuilder";
import {FormElement} from "./FormElement";
import {replaceAll} from "../../helpers/utilities";
import {phoneValidator} from "./inputs/PhoneInput";
import {useDebounce} from "../../helpers/viewUtilities";
import {flatFormInputs} from "../../helpers/registrationsUtilities";

const formioToAntdFormComponents = (formioComponents, forceUpdate) => {
  if (!formioComponents) return [];

  const antdFormComponents = [];

  for (const formioComp of formioComponents) {
    let {
      label,
      title,
      placeholder,
      description,
      tooltip,
      disabled,
      data: {values: dataValues} = {},
      values,
      validate: {required},
      key,
      rules = [],
      type: formioType,
      html,
      fields,
      content,
      multiple,
      conditional: {show, when, eq, json},
      components,
    } = formioComp;

    let options = values || dataValues;

    switch (formioType) {
      case "url":
      case "email":
        rules.unshift({type: formioType});
        break;
      case "phoneNumber":
        rules.unshift({validator: phoneValidator});
        break;
      case "htmlelement":
      case "content":
        placeholder = html || content;
        label = undefined;
        break;
      case "well":
      case "panel":
        placeholder = title;
        label = undefined;
        break;
      case "checkbox":
        placeholder = label;
        label = undefined;
        if (required) {
          // If required, modify the default required rule to only accept "true" and fail if "false" is given.
          required = undefined;
          rules.unshift({
            required: true,
            transform: (value) => value || undefined,
            type: "boolean",
          });
        }
        break;
      case "day":
        required = Object.values(fields || {}).find((field) => field.required);
        break;
      default:
        break;
    }

    const widget = replaceAll(formioType, {
      textfield: "input",
      url: "input",
      email: "input",
      radio: "radio-group",
      selectboxes: "checkbox-group",
      datetime: "date-picker",
      day: "day-picker",
      htmlelement: "content",
      well: "panel",

      // Not supported yet, so defaulted to types that are the closest from the original
      tags: "input",
      file: "content", // Would probably not display at all
    });

    const antdFormComp = {
      label,
      tooltip,
      description,
      disabled,
      options,
      required,
      key,
      placeholder,
      multiple,
      conditional: {show, when, eq, json},
      components,
      forceUpdate,

      widget,
      rules,
    };

    antdFormComponents.push(antdFormComp);
  }

  return antdFormComponents;
};

const formioToAntdFormValues = (values = {}) =>
  Object.fromEntries(
    Object.entries(values).map(([key, answer]) => [
      key,
      answer && typeof answer === "object" && !Array.isArray(answer)
        ? // Formio legacy
          Object.entries(answer)
            .filter((entry) => entry[1])
            .map((entry) => entry[0])
        : answer,
    ])
  );

export const CompatibleFormBuilder = ({formComponents, initialValues, form, forceUpdate}) => (
  <FormBuilder
    meta={{
      fields: formioToAntdFormComponents(formComponents, forceUpdate),
      initialValues: formioToAntdFormValues(initialValues),
    }}
    form={form}
  />
);

export default function RegistrationForm({
  initialValues = {},
  formComponents,
  form,
  onChange,
  saveData,
}) {
  const forceUpdate = FormBuilder.useForceUpdate();

  // saveData on unmount
  useEffect(() => {
    if (saveData) {
      return () => saveData();
    }
  }, []);

  const flatFormComponents = useMemo(() => flatFormInputs(formComponents), [formComponents]);

  // Rerender the form :
  // - after a 400ms debounce time
  // - AND only if a field with dependencies has been touched
  const debouncedForceUpdateOnValuesChange = useDebounce((values) => {
    const modifiedFields = Object.keys(values).map((key) =>
      flatFormComponents.find((comp) => comp.key === key)
    );
    // If we can find one field that depends on it, we have to update
    const hasDependencies = (modifiedField) =>
      flatFormComponents.find((field) => field.conditional.when === modifiedField.key);
    if (modifiedFields.find(hasDependencies)) forceUpdate();
  }, 400);

  return (
    <FormElement
      form={form}
      requiredMark
      onFieldsChange={onChange}
      onValuesChange={debouncedForceUpdateOnValuesChange}>
      <CompatibleFormBuilder
        formComponents={formComponents}
        initialValues={initialValues}
        forceUpdate={forceUpdate}
        form={form}
      />
    </FormElement>
  );
}
