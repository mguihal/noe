import React, {useState} from "react";
import {Alert, Button, Collapse, Form, message, Modal, Tooltip} from "antd";
import {useNavigate} from "@reach/router";
import {FormElement} from "./FormElement";
import {fieldToData as fieldToDataBase} from "../../helpers/tableUtilities";
import {useDispatch} from "react-redux";
import {CopyOutlined} from "@ant-design/icons";
import {listRenderer} from "../../helpers/listUtilities";
import {OFFLINE_MODE} from "../../helpers/offlineModeUtilities";
import {pick} from "../../helpers/utilities";
import {Trans, useTranslation} from "react-i18next";
import {PageHeading} from "./layout/PageHeading";
import {DeleteButton} from "./layout/DeleteButton";
import {Pending} from "./layout/Pending";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const ModificationsHistoryTimeline = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../common/layout/ModificationsHistoryTimeline.js")
);

export const CloneButton = ({onClick}) => {
  const {t} = useTranslation();
  return (
    <Tooltip title={t("common:editPage.cloneFromCurrentInfo")}>
      <Button type="link" style={{flexGrow: 0}} icon={<CopyOutlined />} onClick={onClick} />
    </Tooltip>
  );
};

export function EditPage({
  title,
  icon,
  i18nNs, // The namespace for translations

  record, // The object want to modify
  elementsActions, // The redux actions for this element
  onValidation, // function activated when the user validates the form  onFieldsChange, // what to do when the form has been touched
  fieldToData = fieldToDataBase, // custom fieldToData function. Defaults to the original fieldToData()
  fieldsThatNeedReduxUpdate, // The fields that need Redux to be updated when they are changed (so that they trigger a page refresh)

  deletable = false, // Display the delete button or not
  backButton = true, // [boolean] display a back button, or not | default: true
  customButtons, // [HTML elements] custom buttons if you need to display some
  createAndStayButton = true, // Display the "Créer et rester" button
  onCreate, // Action to do with the newly created element id

  clonable, // Enables the cloning feature (and displays the cloning button)
  clonedElement, // Tells if we are currently cloning the element

  formOverride,
  forceModifButtonActivation = false, // [boolean] force activation of the modif button | default: false
  outerChildren, // children that must be outside the form element
  navigateAfterValidation = true, // [false, or any string] where to go after the validation. false means no navigate. | default: -1 (means go back)
  groupEditing, // should be in the form {entities: "The entities IDs to modify", schema: "the schema definition of the object", fieldsToUpdate: "the fields keys we want to update"}

  noSaveButton = false,

  asModal, // Tell if the EditPage is displayed in a modal or not
  modalOpen, // If the EditPage modal is open or not
  setModalOpen, // The function to set if the modal is open or not

  children,

  ...formElementProps // props for the Form Element if needed
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  let [form] = Form.useForm();
  if (formOverride) form = formOverride;
  const navigate = useNavigate();
  const [modifButtonActivation, setModifButtonActivation] = useState(!!record.dirty);
  const [formData, setFormData] = useState();

  // Create page is either a creation page or a cloning page
  const isCreatePage = !groupEditing && record._id === "new";

  const context = isCreatePage
    ? clonedElement
      ? "clone"
      : "create"
    : groupEditing
    ? "groupEdit"
    : "edit";
  title = title || t(`${i18nNs}:label`, {context});

  const buttonTitle = t(`common:editPage.buttonTitle.${context}`, {
    count: groupEditing?.elements.length,
  });

  const shouldDisplayHistory = !isCreatePage && elementsActions?.loadEditingHistory;

  const validateAndNavigate = (navigateToElementAfterValidation = false) => {
    const fieldsKeysToUpdate = groupEditing
      ? groupEditing.fieldsToUpdate.map((field) => field.key)
      : undefined;
    form
      .validateFields(fieldsKeysToUpdate)
      .then(async () => {
        let elementId;
        if (onValidation) {
          elementId = await onValidation(formData, fieldsKeysToUpdate);
        } else {
          const payload = {...record, ...formData};
          if (groupEditing) {
            const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);
            for (const entityId of groupEditing.elements) {
              await dispatch(elementsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
            }
          } else {
            elementId = await dispatch(elementsActions.persist(payload));
            onCreate?.(elementId);
          }
        }

        if (noSaveButton) return;

        if (asModal) setModalOpen(false);
        // If in a modal, just close the modal
        else if (navigateToElementAfterValidation) navigate(`./${elementId}`, {replace: true});
        else if (navigateAfterValidation && !clonedElement)
          navigate(typeof navigateAfterValidation === "string" ? navigateAfterValidation : -1);
        else setModifButtonActivation(false);
      })
      .catch(() => message.error(t("common:editPage.formInvalid")));
  };

  const onChange = (changedFields, allFields) => {
    if (changedFields.length > 0) {
      setModifButtonActivation(true);

      // If there is a custom onFieldsChange function, use it
      const formData = fieldToData(allFields);
      setFormData(formData);

      // If some necessary fields are updated, then also update Redux state
      if (
        fieldsThatNeedReduxUpdate?.length > 0 &&
        changedFields.find((field) => fieldsThatNeedReduxUpdate.includes(field.name[0]))
      ) {
        dispatch(elementsActions.changeEditing({...record, ...formData, dirty: true}));
      }
    }
  };

  const onDelete = () => {
    dispatch(elementsActions.remove(record._id));
    navigateAfterValidation && navigate(-1);
  };

  const editPageContent = (
    <div className="page-container">
      <PageHeading
        icon={icon}
        backButton={backButton && !asModal}
        className="edit-page-header"
        title={title}
        buttonTitle={!noSaveButton && buttonTitle}
        onButtonClick={!noSaveButton && (() => validateAndNavigate())}
        forceModifButtonActivation={
          modifButtonActivation || forceModifButtonActivation || !!clonedElement
        }
        customButtons={
          !isCreatePage ? (
            <>
              {!groupEditing && (
                <>
                  {customButtons}
                  {clonable && (
                    <CloneButton
                      onClick={() =>
                        navigate("clone", {
                          replace: true,
                          state: {clonedElement: {...record, ...formData, _id: "new"}},
                        })
                      }
                    />
                  )}
                  {deletable && <DeleteButton onConfirm={onDelete} />}
                </>
              )}
            </>
          ) : (
            createAndStayButton &&
            !clonedElement &&
            !asModal && (
              <Tooltip title={t("common:editPage.createAndStayButton.tooltip")}>
                <Button
                  onClick={() => {
                    validateAndNavigate(true);
                  }}
                  disabled={!(modifButtonActivation || forceModifButtonActivation)}>
                  {t("common:editPage.createAndStayButton.label")}
                </Button>
              </Tooltip>
            )
          )
        }
      />

      {noSaveButton && (
        <div
          style={{
            maxHeight: record.dirty ? 300 : 0,
            position: "sticky",
            top: 20,
            zIndex: 2,
            overflow: "hidden",
            transition: "all 1s",
            transitionDelay: "0.4s",
          }}
          className="fade-in">
          <Alert
            size="small"
            type={record.dirty ? "info" : "success"}
            action={
              <Button size="small" type="primary" onClick={() => validateAndNavigate(true)}>
                Enregistrer
              </Button>
            }
            style={{marginBottom: 26}}
            message={
              record.dirty ? (
                <Trans ns="common" i18nKey="editPage.keyboardShortcutsHint" />
              ) : (
                t("common:allGood")
              )
            }
          />
        </div>
      )}

      {groupEditing && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.youAreEditingDocumentsInTheSameTime.message", {
            count: groupEditing.elements.length,
          })}
          description={
            <>
              {t("common:editPage.youAreEditingDocumentsInTheSameTime.description.1")}{" "}
              {listRenderer.listOfClickableElements(
                groupEditing.fieldsToUpdate.map((field) => field.label),
                (el) => (
                  <strong style={{color: "darkred"}}>{el}</strong>
                )
              )}
              . {t("common:editPage.youAreEditingDocumentsInTheSameTime.description.2")}
            </>
          }
        />
      )}

      {clonedElement && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.cloningInfoAlert.message")}
          description={t("common:editPage.cloningInfoAlert.description")}
        />
      )}

      {record._id && (
        <div className="fade-in">
          <FormElement
            form={form}
            onFieldsChange={onChange}
            validateAction={validateAndNavigate}
            requiredMark={isCreatePage}
            {...formElementProps}>
            {children}
          </FormElement>
          {outerChildren}
          {shouldDisplayHistory && !OFFLINE_MODE && (
            <Collapse className="fade-in">
              <Collapse.Panel header={t("common:editPage.modificationsHistory")}>
                <Pending.Suspense animationDelay={"300ms"}>
                  <ModificationsHistoryTimeline record={record} elementsActions={elementsActions} />
                </Pending.Suspense>
              </Collapse.Panel>
            </Collapse>
          )}
        </div>
      )}
    </div>
  );

  return asModal ? (
    <Modal
      width="min(90vw, 1000pt)"
      open={modalOpen}
      onCancel={() => setModalOpen(false)}
      footer={null}>
      <div className="modal-page-content">{editPageContent}</div>
    </Modal>
  ) : (
    editPageContent
  );
}
