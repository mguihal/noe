import {Slider} from "antd";
import React from "react";
import {formTag, FormTagProps} from "./formTag";
import type {SliderRangeProps, SliderSingleProps} from "antd/es/slider";

export const SliderInput = (props: FormTagProps<SliderSingleProps | SliderRangeProps>) =>
  formTag(props, (otherProps) => <Slider {...otherProps} />);
