import {DatePickerProps} from "antd";
import {RangePickerProps} from "antd/es/date-picker";
import {FormTagProps} from "./formTag";

type DateInputProps = {
  isRange?: boolean;
  blockDisabledDates?: boolean;
  disableDatesIfOutOfProject?: boolean;
  disableDatesBeforeNow?: boolean;
};

declare const DateTimeInput: ((
  props: FormTagProps<DateInputProps & DatePickerProps>
) => JSX.Element) & {
  Range: (props: FormTagProps<DateInputProps & RangePickerProps>) => JSX.Element;
};
