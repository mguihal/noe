import {formTag, FormTagProps} from "./formTag";
import {Switch, SwitchProps} from "antd";
import React from "react";

export const SwitchInput = (props: FormTagProps<SwitchProps>) =>
  formTag(
    {formItemProps: {valuePropName: "checked", ...props.formItemProps}, ...props},
    (otherProps) => <Switch {...otherProps} />
  );
