import {FormItemProps} from "antd";

type FormTagProps<T> = {
  name: string;
  i18nNs?: string;
  icon?: any;
  label?: FormItemProps["label"];
  rules?: FormItemProps["rules"];
  tooltip?: FormItemProps["tooltip"];
  formItemProps?: FormItemProps;
  inputProps?: T;
} & T;

declare function formTag<T>(props: FormTagProps<T>, inputTag: (props: T) => JSX.Element);
