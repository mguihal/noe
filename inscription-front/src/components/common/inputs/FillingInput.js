import {Progress, ProgressProps} from "antd";
import React from "react";
import {formTag, FormTagProps} from "./formTag";

export const FillingInput = (props: FormTagProps<ProgressProps>) =>
  formTag(props, (otherProps) => (
    <div className="containerH buttons-container">
      <div style={{flexGrow: 100}}>
        <Progress
          percent={otherProps.percent}
          size="small"
          showInfo={false}
          status={otherProps.status}
          {...otherProps}
        />
      </div>
      <div>{otherProps.text}</div>
    </div>
  ));
