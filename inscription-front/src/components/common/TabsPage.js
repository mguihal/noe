import React from "react";
import {Tabs} from "antd";
import {useLocation} from "@reach/router";
import {PageHeading} from "./layout/PageHeading";

export function TabsPage({
  title,
  icon,
  defaultActiveKey,
  forceModifButtonActivation,
  onValidation,
  customButtons,
  children,
  fullWidth = false,
  items,
  ...otherProps
}) {
  // Move to the appropriate tab if there is a hash in the url
  const hash = useLocation().hash.slice(1);
  if (hash !== "") defaultActiveKey = hash;

  return (
    <div className="page-container no-bottom-padding">
      <PageHeading
        className="tabs-page-header"
        icon={icon}
        title={title}
        buttonTitle={onValidation ? "Enregistrer" : undefined}
        customButtons={customButtons}
        forceModifButtonActivation={forceModifButtonActivation}
        onButtonClick={onValidation}
      />
      <div className={fullWidth ? "full-width-content" : undefined}>
        <Tabs
          defaultActiveKey={defaultActiveKey || 1}
          onChange={(activeKey) => (window.location.hash = `#${activeKey}`)}
          {...otherProps}
          renderTabBar={
            fullWidth &&
            ((props, TabBarComponent) => <TabBarComponent {...props} className="with-margins" />)
          }
          items={items}
        />
      </div>
    </div>
  );
}
