import React, {useEffect, useMemo, useRef} from "react";
import {Form, Modal} from "antd";
import {FormElement, safeValidateFields} from "./FormElement";
import {NumberInput} from "./inputs/NumberInput";
import {trySeveralTimes} from "../../helpers/agendaUtilities";
import {useTranslation} from "react-i18next";
import "react-quill/dist/quill.snow.css";
import {TextInput} from "./inputs/TextInput";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const ReactQuill = lazyWithRetry(() => import(/* webpackPrefetch: true */ "react-quill"));
const Quill = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "react-quill").then((module) => ({
    default: module["Quill"],
  }))
);

const toolbar = [
  [{size: []}],
  ["bold", "italic", "underline"],
  ["blockquote"],
  [{list: "ordered"}, {list: "bullet"}],
  ["link", "image", "video"],
  [{color: []}, {background: []}],
  [{align: []}],
];

const TextEditor = ({onChange, placeholder, value}) => {
  const {t} = useTranslation();
  const reactQuillRef = useRef();
  const [form] = Form.useForm();

  // **** Image Handler stuff ****
  const imageHandler = async () => {
    const quillEditor = reactQuillRef.current.editor;
    const range = quillEditor.getSelection();

    const onOk = () =>
      safeValidateFields(form, () => {
        const {url, width, height} = form.getFieldsValue();
        quillEditor.clipboard.dangerouslyPasteHTML(
          range.index,
          `<img src="${url}" width="${width}%" height="${height}px">`,
          Quill.sources.USER
        );
      });

    const ImageHandlerModalContent = () => {
      useEffect(() => {
        trySeveralTimes(() => document.getElementsByClassName("url-input")[0].focus());
      }, []);

      return (
        <FormElement
          form={form}
          validateAction={async () => {
            await onOk();
            Modal.destroyAll();
          }}>
          <div className="container-grid">
            <TextInput
              className="url-input"
              label={t("common:textEditor.imageUrl.label")}
              name="url"
              placeholder={t("common:textEditor.imageUrl.placeholder")}
              rules={[{type: "url", required: true}]}
            />
            <div className="container-grid two-per-row">
              <NumberInput
                label={t("common:textEditor.imageWidth.label")}
                name="width"
                bordered
                placeholder={t("common:textEditor.imageWidth.placeholder")}
                addonAfter="%"
              />
              <NumberInput
                label={t("common:textEditor.imageHeight.label")}
                name="height"
                bordered
                placeholder={t("common:textEditor.imageHeight.placeholder")}
                addonAfter="px"
              />
            </div>
          </div>
        </FormElement>
      );
    };

    Modal.confirm({icon: false, content: <ImageHandlerModalContent />, onOk});
  };

  // **** Quill Editor ****
  return useMemo(
    () => (
      <ReactQuill
        ref={reactQuillRef}
        theme="snow"
        onChange={
          onChange
            ? (value, ...rest) => {
                if (value === "<p><br></p>") value = ""; // When the text is blank, then just remove everything
                onChange(value, ...rest);
              }
            : undefined
        }
        modules={{
          toolbar: {
            container: toolbar,
            handlers: {image: imageHandler},
          },
        }}
        placeholder={placeholder}
        value={value}
      />
    ),
    []
  );
};

export default TextEditor;
