import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Input, List} from "antd";
import {useDebounce, useSavedPagination, useWindowDimensions} from "../../helpers/viewUtilities";
import {paginationPageSizes} from "../../features/view";
import {useTranslation} from "react-i18next";
import {searchInObjectsList} from "../../helpers/listUtilities";
import {PageHeading} from "./layout/PageHeading";

const {Search} = Input;

export function GridPage({
  title,
  icon,
  subtitle,
  buttonTitle,
  customButtons,
  renderItem,
  header = true,
  fullPage = true,
  searchInFields,
  dataSource,
  navigateFn,
  customControls,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination(dispatch, title);
  const {isMobileView} = useWindowDimensions();
  const [searchText, setSearchText] = useState("");

  const filteredDataSource = searchInObjectsList(searchText, dataSource, searchInFields);

  const debouncedSearch = useDebounce(setSearchText, 500);

  return (
    <>
      <div
        className={`containerV ${fullPage ? "full-width-content" : ""}`}
        style={{background: "#fafafa", position: "sticky", top: 0, zIndex: 4}}>
        {header && (
          <PageHeading
            className="list-page-header"
            icon={icon}
            title={title}
            buttonTitle={buttonTitle}
            customButtons={customButtons}
            onButtonClick={() => navigateFn("new")}
          />
        )}
        {(subtitle || searchInFields) && (
          <div className="subtitle with-margins" style={{marginBottom: 20}}>
            {subtitle}
            <div className="containerH buttons-container">
              {searchInFields && (
                <Search
                  placeholder={t("common:search")}
                  style={{minWidth: 200, flexBasis: "40%"}}
                  onChange={(e) => debouncedSearch(e.target.value)}
                  enterButton
                />
              )}
              {customControls}
            </div>
          </div>
        )}
      </div>
      <List
        style={{paddingTop: 10, marginBottom: 40, background: "white"}}
        grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 4}}
        dataSource={filteredDataSource}
        renderItem={renderItem}
        pagination={{
          position: "bottom",
          style: {textAlign: "center"},
          size: isMobileView && "small",
          showSizeChanger: true,
          onChange: setPagination,
          current: pagination.current,
          pageSize: pagination.pageSize,
          pageSizeOptions: paginationPageSizes,
        }}
      />
    </>
  );
}
