import React, {Suspense} from "react";
import {defaultSpinnerLogo} from "../../../app/configuration";
import type {PendingProps} from "./Pending";

/**
 * Default NOÉ pending animation
 * @param spinnerLogo
 * @param className
 * @param minHeight
 * @param animationDelay if not set, no delay
 * @returns {JSX.Element}
 */
export const Pending = ({
  spinnerLogo = defaultSpinnerLogo,
  className,
  minHeight = "100vh",
  animationDelay,
}: PendingProps) => (
  <div
    className={`containerH fade-in ${className}`}
    style={{justifyContent: "center", minHeight: minHeight, overflow: "visible", animationDelay}}>
    <div className="containerV" style={{justifyContent: "center", overflow: "visible"}}>
      <img className="spin" src={spinnerLogo} />
    </div>
  </div>
);

/**
 * React suspense with the NOE animatin and smooth appear included
 * @param children the component to be displayed
 * @param minHeight see PendingProps
 * @param animationDelay see PendingProps
 * @param className see PendingProps
 * @returns {JSX.Element}
 */
Pending.Suspense = ({
  children,
  minHeight = 300,
  animationDelay,
  className,
}: {
  children: JSX.Element,
} & PendingProps) => (
  <Suspense
    fallback={
      <Pending minHeight={minHeight} animationDelay={animationDelay} className={className} />
    }>
    <div className="fade-in">{children}</div>
  </Suspense>
);
