import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {useDebounce} from "../../../helpers/viewUtilities";
import {searchInObjectsList} from "../../../helpers/listUtilities";
import {Button, Modal} from "antd";
import Search from "antd/es/input/Search";
import {TableElement} from "../TableElement";
import {paginationPageSizes} from "../../../features/view";

export const ElementSelectionModal = <T>({
  title,
  subtitle,
  open,
  onOk,
  onCancel,
  large,
  customButtons,
  searchInFields = [],
  okText,
  dataSource,
  ...tableProps
}: {
  title: string,
  subtitle?: string,
  open: boolean,
  onOk: () => void,
  onCancel?: () => void,
  large?: boolean,
  customButtons?: JSX.Element,
  searchInFields?: Array<string> | ((objValue: T) => Array<string>),
  okText?: string,
  dataSource?: Array<T>,
}) => {
  const {t} = useTranslation();
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  const debouncedSetFilteredDataSource = useDebounce((searchValue, dataSource) =>
    setFilteredDataSource(searchInObjectsList(searchValue, dataSource, searchInFields))
  );

  useEffect(
    () => debouncedSetFilteredDataSource(searchValue, dataSource),
    [dataSource, searchValue]
  );

  return (
    <Modal
      className="element-selection-modal"
      title={title}
      open={open}
      width={large && "98%"}
      centered
      footer={
        <>
          {customButtons}
          <Button onClick={onCancel}>{t("common:cancel")}</Button>
          <Button type="primary" onClick={onOk}>
            {okText || t("common:ok")}
          </Button>
        </>
      }
      onCancel={onCancel}
      bodyStyle={{margin: -24}}>
      {(subtitle || searchInFields.length > 0) && (
        <div style={{padding: "10px 24px"}}>
          {subtitle}
          {searchInFields.length > 0 && (
            <Search
              allowClear
              autoFocus
              onChange={(event) => setSearchValue(event.target.value)}
              placeholder={t("common:search")}
            />
          )}
        </div>
      )}

      <TableElement.Simple
        showHeader
        scroll={{
          x: (tableProps.columns.length - 1) * 160 + 70,
          y: window.innerHeight - 260 - (subtitle ? 45 : 0) - (searchInFields.length > 0 ? 45 : 0),
        }}
        pagination={{
          position: ["bottomCenter"],
          pageSize: 40,
          size: "small",
          pageSizeOptions: paginationPageSizes,
          showSizeChanger: true,
        }}
        dataSource={filteredDataSource}
        {...tableProps}
      />
    </Modal>
  );
};
