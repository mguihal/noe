import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import React, {useRef} from "react";
import {currentProjectSelectors} from "../../../features/currentProject";
import {normalize} from "../../../helpers/listUtilities";
import {Button, message, Tooltip} from "antd";
import {FilePdfOutlined} from "@ant-design/icons";

export const GetPdfPlanningButton = ({
  id,
  elementsActions,
  tooltip,
  noText,
  type,
  customFileName,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const downloadButton = useRef();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const getPdfPlanning = () => {
    dispatch(elementsActions.getPdfPlanning(id))
      .then((pdfFile) => {
        let pdfPlanningUrl = URL.createObjectURL(pdfFile);
        const customResult = customFileName?.();
        const fileName = normalize(
          customResult
            ? t("common:pdfButton.filenameWithCustomName", {
                custom: customResult,
                projectName: currentProject.name,
              })
            : t("common:pdfButton.filename", {projectName: currentProject.name}),
          true
        ).replace(/[^\w ]/g, "-"); // Sanitize the filename

        downloadButton.current.setAttribute("href", pdfPlanningUrl);
        downloadButton.current.setAttribute("download", fileName);
        downloadButton.current.click();
      })
      .catch((err) => {
        console.log(err);
        message.error(t("common:pdfButton.pdfCouldNotBeGenerated"));
      });
  };

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />
      <Tooltip placement="bottomLeft" title={tooltip || t("common:pdfButton.tooltip")}>
        <Button
          style={{flexGrow: 0}}
          type={type || "link"}
          onClick={getPdfPlanning}
          icon={<FilePdfOutlined />}>
          {!noText && t("common:pdfButton.label")}
        </Button>
      </Tooltip>
    </>
  );
};
