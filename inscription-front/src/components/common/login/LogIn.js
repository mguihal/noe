import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {TextInput} from "../inputs/TextInput";
import {ConnectionPage} from "../ConnectionPage";
import {viewSelectors} from "../../../features/view";
import {useTranslation} from "react-i18next";
import {safeValidateFields} from "../FormElement";

export default function LogIn({navigate, envId, subtitle}) {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
    dispatch(currentUserActions.changePassword(allFields[1].value));
  };

  const logIn = () =>
    safeValidateFields(form, () => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.logIn());
    });

  const goToSignInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../signup");
  };

  const goToForgotPasswordPage = () => {
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../forgotpassword");
  };

  return (
    <ConnectionPage
      form={form}
      className="fade-in"
      envId={envId}
      subtitle={subtitle}
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      validateAction={logIn}
      buttons={
        <>
          <Button type="primary" onClick={logIn}>
            {t("common:login")}
          </Button>
          <Button onClick={goToSignInPage}>{t("common:createAccount")}</Button>
          <Button type="link" onClick={goToForgotPasswordPage}>
            {t("common:connectionPage.passwordForgottenQuestion")}
          </Button>
        </>
      }>
      <TextInput.Email
        i18nNs="users"
        name="email"
        size="large"
        autoComplete="email"
        rules={[{required: true}]}
      />
      <TextInput.Password
        i18nNs="users"
        name="password"
        size="large"
        autoComplete="current-password"
        rules={[{required: true}]}
      />
    </ConnectionPage>
  );
}
