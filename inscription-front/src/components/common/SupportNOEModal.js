import {Trans, useTranslation} from "react-i18next";
import {Alert, Button, Modal} from "antd";
import {CardElement} from "./layout/CardElement";
import {navigate} from "@reach/router";
import React from "react";

export const SupportNOEModal = ({open, setOpen}) => {
  const {t} = useTranslation();
  return (
    <Modal
      title={t("common:supportNOE.supportTheProject")}
      open={open}
      width={"min(90vw, 1000pt)"}
      onCancel={() => setOpen(false)}
      footer={null}>
      <div className={"container-grid"}>
        <Alert message={t("common:supportNOE.description")} style={{marginBottom: 26}} />
        <div className="container-grid two-per-row" style={{marginBottom: -26}}>
          <CardElement title={"☎️ " + t("common:supportNOE.contactUs.title")}>
            <Trans
              i18nKey={"supportNOE.contactUs.description"}
              ns={"common"}
              values={{contactEmail: process.env.REACT_APP_CONTACT_US_EMAIL}}
              components={{
                linkToContactEmail: (
                  <a
                    href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
                    rel="noreferrer"
                  />
                ),
              }}
            />
          </CardElement>

          <CardElement title={"📚 " + t("common:supportNOE.learn.title")}>
            {process.env.REACT_APP_DEMO_URL && (
              <>
                <p>{t("common:supportNOE.learn.createDemo.description")}</p>
                <p>
                  <Button
                    {...(process.env.REACT_APP_MODE === "DEMO"
                      ? {onClick: () => navigate("new")}
                      : {href: `${process.env.REACT_APP_DEMO_URL}/new`})}>
                    {t("common:supportNOE.learn.createDemo.button")}
                  </Button>
                </p>
              </>
            )}

            <p>{t("common:supportNOE.learn.description")}</p>
            <Button href={`${process.env.REACT_APP_WEBSITE_URL}/docs`}>
              {t("common:supportNOE.learn.button")}
            </Button>
          </CardElement>

          <CardElement title={"❤️️ " + t("common:supportNOE.donate.title")}>
            <p>{t("common:supportNOE.donate.description")}</p>
            <Button href={process.env.REACT_APP_DONATE_URL}>
              {t("common:supportNOE.donate.button")}
            </Button>
          </CardElement>

          <CardElement title={"🌐️ " + t("common:supportNOE.helpTranslate.title")}>
            <p>{t("common:supportNOE.helpTranslate.description")}</p>
            <Button href={`${process.env.REACT_APP_WEBSITE_URL}/community/translate`}>
              {t("common:supportNOE.helpTranslate.button")}
            </Button>
          </CardElement>
        </div>
      </div>
    </Modal>
  );
};
