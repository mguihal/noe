import {t} from "i18next";
import {Trans} from "react-i18next";

const toggleSidebar = () => {
  try {
    // Open the sidebar if it exists
    document.querySelector(".navbar-menu-button > *").click();
  } catch (e) {
    // nothing
  }
};

export default (isMobileView) => [
  {
    selector: ".userTourBookmarkProject:first-of-type",
    content: t("projects:userTour.projectsLists.1"),
  },
  {
    ...(isMobileView
      ? {action: toggleSidebar, actionAfter: toggleSidebar, position: "center"}
      : {selector: ".ant-menu:first-of-type"}),
    content: <Trans i18nKey="userTour.projectsLists.2" ns="projects" />,
  },
];
