import React, {useRef, useState} from "react";
import {Button, Input, message, Tag} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import {listRenderer, listSorter, normalize} from "./listUtilities";
import {flatFormInputs, WaitingInvitationTag} from "./registrationsUtilities";
import {t} from "i18next";
import {URLS} from "../app/configuration";
import {useSelector} from "react-redux";
import {viewSelectors} from "../features/view";

const {Search} = Input;

export const addKeyToItemsOfList = (data, keyName = undefined) => {
  return data ? data?.map((t, i) => ({...t, key: keyName ? t[keyName] : i})) : [];
};

export const fieldToData = (fields, acc = {}) => {
  return fields.reduce((accumulator, currentValue) => {
    accumulator[currentValue.name] = currentValue.value;
    return accumulator;
  }, acc);
};

export const dataToFields = (data, formatFieldsFn = undefined) => {
  let clone = {...data};

  formatFieldsFn && formatFieldsFn(clone);

  const fields = Object.keys(clone).reduce((accumulator, currentValue) => {
    accumulator.push({name: currentValue, value: clone[currentValue]});
    return accumulator;
  }, []);
  return fields;
};

export const useCopyColumns = (dataSource, columns, handleDisplayConfigChangeFn?) => {
  // Update the redux state, but rely on a useState because sometimes there is no proper dedicated redux state for this
  const reduxSorting = useSelector(viewSelectors.selectSorting);
  const [filtering, setFiltering] = useState(reduxSorting?.filteredInfo);

  // Getting all search texts that are set up for each column
  const columnsSearchTexts = Object.fromEntries(
    columns.map((column) => [
      column.dataIndex,
      column.searchText || ((record) => record[column.dataIndex]),
    ])
  );

  // Function that takes all the currently available filtering information from redux,
  const filteredWithColumns = (dataSource) => {
    let realDataSource = dataSource;
    if (filtering) {
      for (const [columnName, searchTextArray] of Object.entries(filtering)) {
        if (searchTextArray) {
          const value = searchTextArray[0];
          realDataSource = realDataSource.filter((record) =>
            normalize(columnsSearchTexts[columnName]?.(record))?.includes(normalize(value))
          );
        }
      }
    }

    return realDataSource;
  };
  const realDataSource = filteredWithColumns(dataSource);

  return {
    columns: columns.map((column) => ({
      ...column,
      onHeaderCell: (computedColumn) => ({
        ...computedColumn.onHeaderCell,
        onContextMenu: (e) => {
          // Prevent right click menu from appearing when right clicking on header cell
          e.preventDefault();
          // Get the function that will collect the text to search into for each item of the list
          const searchTextFn = columnsSearchTexts[computedColumn.dataIndex];
          // Map elements with that function and assemble them with a comma
          const copyableText = realDataSource?.map(searchTextFn).join(", ");
          // Write that in the clipboard
          navigator.clipboard.writeText(copyableText).then(() =>
            message.success(
              t("common:messages.elementsFromColumnCopiedToClipboard", {
                count: realDataSource?.length,
                columnTitle: column.title,
              })
            )
          );
        },
      }),
    })),
    handleDisplayConfigChange: (pagination, filters, sorter, extra) => {
      handleDisplayConfigChangeFn?.(pagination, filters, sorter, extra);
      setFiltering(filters);
    },
    currentDataSource: realDataSource,
  };
};

export const useSearchInColumns = (columns) => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters, confirm) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const enableColumnSearchProps = (dataIndex, columnTitle, searchText) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div className="containerH" style={{padding: 8}}>
        <Search
          autoComplete="new-password" // Prevent any autocomplete service to complete this, cause autoComplete="off" doesn't seem to work
          enterButton
          style={{maxWidth: 250}}
          value={selectedKeys[0]}
          // On press search button
          onSearch={() => handleSearch(selectedKeys, confirm, dataIndex)}
          // On keyboard input
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          // on press enter
          onPressEnter={(e) => {
            e.stopPropagation();
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          placeholder={t("common:filterIn", {name: columnTitle})}
          ref={searchInput}
        />
        <Button type="link" onClick={() => clearFilters && handleReset(clearFilters, confirm)}>
          {t("common:erase")}
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <div
        className={"containerH"}
        style={{
          borderRadius: 8,
          border: filtered && "1px solid var(--noe-primary)",
          background: filtered && "var(--noe-primary)",
          padding: filtered && 3.5,
        }}>
        <SearchOutlined
          style={{
            color: filtered ? "#ffffff" : "#888888",
          }}
        />
      </div>
    ),
    onFilter: (text, record) => {
      if (searchText) return normalize(searchText(record)).includes(normalize(text));
      else if (record[dataIndex]) {
        return normalize(record[dataIndex]).includes(normalize(text));
      } else {
        return "";
      }
    },
    onFilterDropdownOpenChange: (open) =>
      open && setTimeout(() => searchInput.current?.select(), 100),
  });

  return columns.map((column) => ({
    // The column data
    ...column,
    // Enable search options if needed
    ...(column.searchable
      ? enableColumnSearchProps(column.dataIndex, column.title, column.searchText)
      : undefined),
  }));
};

// Same function as in api/src/controllers/pdf.ts
export const cleanAnswer = (answer, separator = ",\n", flatFormComponents?, formField?) => {
  const formComp =
    formField && flatFormComponents && flatFormComponents.find((comp) => comp.key === formField);
  const options = formComp?.values || formComp?.data?.values || {};

  const findLabelForOption = (optValue) =>
    (options?.find && options.find((opt) => opt.value === optValue)?.label) || optValue;

  if (formComp?.type === "selectboxes") {
    const formattedAnswer = Array.isArray(answer)
      ? // New form
        answer
      : // Legacy Formio
        answer &&
        Object.entries(answer)
          .filter((entry) => entry[1])
          .map((entry) => entry[0]);
    return formattedAnswer?.map(findLabelForOption).join(separator);
  }

  if (formComp?.type === "select" || formComp?.type === "radio") {
    return findLabelForOption(answer);
  }

  return answer;
};

export const ROLES_MAPPING = [
  {value: "guest", color: "#6cdac5"},
  {value: "contrib", color: "#5f98ed"},
  {value: "admin", color: "#1330f5"},
];

export const roleTag = (roleValue, small = false) => {
  if (!roleValue) return null;
  const roleObject = ROLES_MAPPING.find((r) => r.value === roleValue);
  const roleLabel = t(`registrations:schema.role.options.${roleValue}.label`);
  return (
    <Tag color={roleObject?.color} title={small ? roleLabel : undefined}>
      {small ? roleLabel[0] : roleLabel}
    </Tag>
  );
};

// Get the columns generated from the form mapping
export const getProjectFormMappingColumns = (
  project,
  nameSuffix,
  getFormAnswers = (record) => [record.specific]
) => {
  const flatFormComponents = flatFormInputs(project.formComponents);

  const renderAnswersList = (record, formField) =>
    listRenderer.listOfClickableElements(getFormAnswers(record), (formAnswer) =>
      cleanAnswer(formAnswer?.[formField], undefined, flatFormComponents, formField)
    );

  return (
    project.formMapping?.map(({formField, columnName}) => ({
      title: columnName + (nameSuffix ? ` ${nameSuffix}` : ""),
      dataIndex: columnName,
      render: (text, record) => renderAnswersList(record, formField),
      sorter: (a, b) =>
        listSorter.text(renderAnswersList(a, formField), renderAnswersList(b, formField)),
      searchable: true,
      searchText: (record) => renderAnswersList(record, formField),
    })) || []
  );
};

// Generate participants columns
export const generateRegistrationsColumns = (project, {start, middle, end} = {}) =>
  [
    ...(start || []),
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => {
        const sessionsNoShows = record.sessionsSubscriptions?.filter(
          (sessionSubscription) => sessionSubscription.hasNotShownUp
        );
        return (
          <>
            {roleTag(record.role, true)}
            {sessionsNoShows?.length > 0 && (
              <span title={`Ce·tte participant·e a fait défaut ${sessionsNoShows.length} fois.`}>
                {sessionsNoShows.map(() => "❗").join("")}{" "}
              </span>
            )}
            {record.user?.firstName}
          </>
        );
      },
      sorter: (a, b) => listSorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => listSorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...getProjectFormMappingColumns(project),
    {
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      sorter: (a, b) => listSorter.text(a.tags?.join(" "), b.tags?.join(" ")),
      render: (text, record) => record.tags?.map((tagName) => <Tag>{tagName}</Tag>),
      searchable: true,
      searchText: (record) => record.tags?.join(" "),
    },
    project.useTeams && {
      title: t("teams:label"),
      dataIndex: "team",
      render: (text, record) =>
        record.teamsSubscriptions.map((teamSubscription) => teamSubscription.team.name).join(", "),
      sorter: (a, b) => listSorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: t("registrations:numberOfDaysOfPresence.label"),
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => listSorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);

export const generateSubscriptionInfo = (registration, sessionSubscription, projectId) => {
  let res;
  const sessionSubscriptionTeam = registration.teamsSubscriptions
    .map((ts) => ts.team)
    ?.find((t) => t._id === sessionSubscription.team);

  if (sessionSubscriptionTeam) {
    res = (
      <>
        {t("sessions:schema.subscriptionInfo.viaTeam")}{" "}
        <a href={`${URLS.ORGA_FRONT}/${projectId}/teams/${sessionSubscriptionTeam._id}`}>
          {sessionSubscriptionTeam.name}
        </a>
      </>
    );
  }

  // Add date of subscription
  res = (
    <>
      {res && (
        <>
          {res} <br />
        </>
      )}
      <span style={{color: "gray"}}>
        {listRenderer.longDateTimeFormat(sessionSubscription.updatedAt, true, false)}
      </span>
    </>
  );

  return res;
};

export const searchInRegistrationFields = (registration) => [
  registration.user.firstName,
  registration.user.lastName,
  registration.user.email,
  ...(registration.tags || []),
];
