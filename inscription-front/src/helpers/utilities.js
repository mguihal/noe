import {t} from "i18next";

export const pick = (obj, props) => {
  // Create new object
  let picked = {};

  // Loop through props and push to new object
  for (let prop of props) {
    picked[prop] = obj[prop];
  }

  // Return new object
  return picked;
};

export const personName = (person: any): string =>
  person?.firstName?.length > 0
    ? person?.lastName?.length > 0
      ? `${person.firstName} ${person.lastName}`
      : person.firstName
    : "";

/**
 * Replaces several strings with other several strings
 * @param str the source string
 * @param mapObj a map of strings to replace (key = old, value = new)
 * @returns the new string
 */
export const replaceAll = (str, mapObj) => {
  const re = new RegExp(Object.keys(mapObj).join("|"), "gi");
  return str.replace(re, (matched) => mapObj[matched]);
};

export const truncate = (str, n) => str.replace(new RegExp("(.{" + n + "})..+"), "$1...");
