import dayjs from "dayjs";
import {useEffect, useState} from "react";

//*******************************//
//******* SLOT UTILITIES ********//
//*******************************//

// DISPLAY THE SLOT NUMBER
export const slotNumberString = (slot) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s._id === slot._id) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// DISPLAY THE SLOT NAME
export const getSessionName = (session, teams?) => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName =
    teams?.find((t) => t._id === (session.team?._id || session.team))?.name || session.team?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

export const getElementsFromListInSlot = (elementName, slot, getKey: (s: any) => string) =>
  slot[elementName]?.length > 0 && slot[`${elementName}SessionSynchro`] === false
    ? slot[elementName].map(getKey)
    : slot.session[elementName].map(getKey);

//*******************************//
//****** DISPLAY UTILITIES ******//
//*******************************//

// Handles dates when end date is at midnight o'clock. Without it, any session ending at midnight will
// overflow on the next day, which is really annoying. This manager aims to remove this behavior.
export const midnightDateManager = {
  dataToDisplay: (slot) => {
    // If end date is midnight, remove one second
    let endDate = dayjs(slot.endDate);
    if (endDate.hour() === 0 && endDate.minute() === 0) endDate = endDate.subtract(1, "minute");
    return {...slot, endDate: endDate.format()};
  },
  displayToData: (existingEndDate, newEndDate) => {
    // If end date is midnight, remove one minute
    const endDate = dayjs(existingEndDate);
    newEndDate = dayjs(newEndDate);
    if (endDate.hour() === 0 && endDate.minute() === 0) newEndDate = newEndDate.add(1, "minute");
    return newEndDate;
  },
};

// USEEFFECT HELPERS
export const trySeveralTimes = (
  actionToTry,
  whatToDoAfter,
  ...{limitOfTrials = 5, frequency = 100, errorHandling}
) => {
  let trials = 0;
  const tryInterval = setInterval(() => {
    try {
      if (actionToTry()) {
        clearInterval(tryInterval);
        whatToDoAfter && whatToDoAfter();
      } else {
        trials += 1;
        if (trials >= limitOfTrials) {
          clearInterval(tryInterval);
        }
      }
    } catch (e) {
      errorHandling && errorHandling(e);
    }
  }, frequency);
};

// export const useInitialScrollToProjectAvailabilities = (agendaDisplayParams, cellDisplayHeight) => {
//   // Scroll to the optimal place should be done once. If done, will be set to true in useEffect
//   const [initialScrollIsDone, setInitialScrollIsDone] = useState(false);
//
//   // Scroll on page load to reach the best optimal place to be on the view
//   useEffect(() => {
//     // The main table element has this class (it's not the only one, but it will always come first in the querySelector)
//     if (!initialScrollIsDone && agendaDisplayParams?.shouldScroll && cellDisplayHeight) {
//       trySeveralTimes(
//         () => {
//           // TODO difference between frontends is normal
//           const mainTimeTableElement =
//             document.querySelector(".MuiTable-root")?.offsetParent?.offsetParent;
//           // Scroll to reach the diff between the agenda display and the project availabilities
//           mainTimeTableElement.scrollTop =
//             agendaDisplayParams.hoursDiff * cellDisplayHeight * (60 / CELL_DURATION_MINUTES);
//           return true;
//         },
//         () => setInitialScrollIsDone(true),
//         {frequency: 30}
//       );
//     }
//   }, [agendaDisplayParams, cellDisplayHeight]);
// };

// Synchronize the height of the little ticks on the side of the calendar
export const synchronizeTicksOnSideOfAgenda = (className, cellDisplayHeight) => {
  // The trick is to spot the timescale labels but there is no eaasy hook (class names change all the time)
  // We created a custom class on which we can get a hook, named "timescale-cell".
  trySeveralTimes(() => {
    const elements = document.querySelector(`.${className}`)?.parentNode?.parentNode?.parentNode
      ?.parentNode?.parentNode?.lastChild?.firstChild.childNodes; // Go to the TicksLayout-table-XXXX and iterate
    if (elements?.length > 0) {
      elements.forEach((el) => (el.firstChild.style.height = `${cellDisplayHeight}px`));
      return true;
    }
  });
};

//*****************************//
//******* AGENDA PARAMS *******//
//*****************************//

// Cell sizes and duration
export const CELL_DURATION_MINUTES = 30; // The smallest unit of time for drag and drop
export const CELLS_DISPLAY_HEIGHTS = [25, 35, 45];

// DEFAULT AGENDA PARAMS
export const DEFAULT_AGENDA_PARAMS = {
  slotsOnEachOther: false,
  cellDisplayHeight: CELLS_DISPLAY_HEIGHTS[1],
};

// Number of days to display by default
const MAX_DAYS_TO_DISPLAY_BY_DEFAULT = 4;
export const getNumberOfDaysToDisplay = (windowWidth, project) =>
  Math.min(
    Math.max(1, Math.ceil((windowWidth - 280) / 450)), // Screen adaption
    dayjs(project.end).diff(project.start, "day") + 1, // Not more than the project opening dates
    MAX_DAYS_TO_DISPLAY_BY_DEFAULT // Default limit
  );

// Returns the hour of the day based on hours and minutes
const hoursOfDay = (dayjsDate) => dayjsDate.minute() / 60.0 + dayjsDate.hour();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = dayjs(slot.start);
  const end = dayjs(slot.end);
  const length = end.diff(start, "minute");
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(dayjs(slot.start));
      const slotEnd = hoursOfDay(dayjs(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

export const getAgendaDisplayStartDate = (project) => {
  const now = dayjs();
  const start = dayjs(project.start);
  const end = dayjs(project.end);

  // If we are during the event, start at the current date. Else, start at project start date
  return (now.isBefore(start) || now.isAfter(end) ? start : now).unix() * 1000;
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(dayjs(project.start)),
    end: hoursOfDay(dayjs(project.end)),
  };
};

const MIN_START_DISPLAY_HOUR = 8;
const MIN_END_DISPLAY_HOUR = 22;

const halfRound = (funcName: "floor" | "ceil" | "round", val) => Math[funcName](val * 2) / 2;

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayParams = (slots, project) => {
  let params;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    params = {
      start: halfRound(
        "floor",
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: halfRound(
        "ceil",
        slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end
      ),
      shouldScroll: true,
    };

    // Set up the scroll diff
    params.hoursDiff =
      slotsMinMaxHours && slotsExistBeforeProjectHours
        ? halfRound("floor", projectMinMaxHours.start) - params.start
        : 0;
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    params = {
      start: halfRound(
        "floor",
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: halfRound(
        "ceil",
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    params = {start: MIN_START_DISPLAY_HOUR, end: MIN_END_DISPLAY_HOUR};
  }

  params.start = Math.max(params.start - 1, 0); // Display one hour before display start, but do not go below 0 hours
  params.end = Math.min(params.end + 1, 24); // Display one hour after display end, but do not go after 24 hours

  // We should at least display from MIN to MAX default display hour
  params.start = Math.min(MIN_START_DISPLAY_HOUR, params.start);
  params.end = Math.max(MIN_END_DISPLAY_HOUR, params.end);

  return params;
};
