import * as SockJS from "sockjs-client";
import {useEffect} from "react";
import {useOnlineStatus} from "./onlineStatusCheckerUtilities";

export let lastWebsocketConnectionId;

const generateSessionId = () => {
  const sessionId = (Math.random() + 1).toString(36).substring(5);
  lastWebsocketConnectionId = sessionId;
  console.log(lastWebsocketConnectionId);
  return sessionId;
};

export const createWebSocketConnection = (
  key: {type: string, id: string},
  onMessage: (data: any) => void
) => {
  const websocketUrl = `${process.env.REACT_APP_API_WEBSOCKET_URL}/ws`;

  // Generate a pseudo random Id to identify the connection
  const ws = new SockJS(websocketUrl, undefined, {sessionId: generateSessionId});
  ws.onmessage = ({data}) => onMessage(JSON.parse(data));
  ws.onopen = () => ws.send(JSON.stringify(key));
  return ws;
};

export const useWebSocketConnection = (
  activate = false,
  key: {type: string, id: string},
  onMessage: (data: string) => void
) => {
  const online = useOnlineStatus();

  useEffect(() => {
    if (activate && online) {
      try {
        const ws = createWebSocketConnection(key, onMessage);
        return () => ws.close();
      } catch (e) {
        console.error("WEBSOCKET FAILED !", e);
      }
    }
  }, [onMessage, activate, online]);
};
