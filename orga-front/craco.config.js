const CracoLessPlugin = require("craco-less");
const SentryWebpackPlugin = require("@sentry/webpack-plugin");

const deploySentryRelease =
  process.env.NODE_ENV === "production" &&
  process.env.SENTRY_PROJECT_NAME &&
  process.env.SENTRY_AUTH_TOKEN;

const getGitHash = () =>
  require("child_process")
    .execSync("git config --global --add safe.directory /app && git rev-parse HEAD")
    .toString()
    .trim();

module.exports = {
  webpack: {
    configure: {
      module: {
        rules: [
          // Locales loader
          {
            test: /locales/,
            loader: "@turtle328/i18next-loader",
            options: {basenameAsNamespace: true},
          },
        ],
      },
      // ignoreWarnings: [/Failed to parse source map/],

      // Activate source maps for Sentry
      devtool: "source-map",

      // // Optimizations of the bundle chunking
      // optimization: {
      //   splitChunks: {
      //     chunks: "all",
      //     maxInitialRequests: Infinity,
      //     minSize: 0,
      //     cacheGroups: {
      //       // longterm: {
      //       //   test: /[\\/]node_modules[\\/](react)/,
      //       //   name(module) {
      //       //     // get the name. E.g. node_modules/packageName/not/this/part.js
      //       //     // or node_modules/packageName
      //       //     const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
      //       //
      //       //     // npm package names are URL-safe, but some servers don't like @ symbols
      //       //     return `npm.${packageName.replace("@", "")}`;
      //       //   },
      //       // },
      //       vendor: {
      //         minSize: 0,
      //         test: /[\\/]node_modules[\\/]/,
      //         // name(module) {
      //         //   // get the name. E.g. node_modules/packageName/not/this/part.js
      //         //   // or node_modules/packageName
      //         //   const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
      //         //
      //         //   // npm package names are URL-safe, but some servers don't like @ symbols
      //         //   return `npm.${packageName.replace("@", "")}`;
      //         // },
      //       },
      //     },
      //   },
      // },
    },
    ...(deploySentryRelease && {
      plugins: {
        add: [
          new SentryWebpackPlugin({
            org: "noe-app",
            project: process.env.SENTRY_PROJECT_NAME,

            // Specify the directory containing build artifacts
            include: "./build",

            // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
            // and needs the `project:releases` and `org:read` scopes
            authToken: process.env.SENTRY_AUTH_TOKEN,

            // Optionally uncomment the line below to override automatic release name detection
            release: getGitHash(),
          }),
        ],
      },
    }),
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
