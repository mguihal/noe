// This function is duplicated in api/src/entities/sessions.ts
export const getMaxParticipantsBasedOnPlaces = (session) =>
  session.places?.length > 0
    ? session?.places?.reduce((acc, place) => acc + (place.maxNumberOfParticipants ?? Infinity), 0)
    : undefined;

export const isParticipantAvailable = (sessionSlots, availabilitySlots) => {
  if (!sessionSlots || !availabilitySlots) return false;

  for (const sessionSlot of sessionSlots) {
    const slotStart = new Date(sessionSlot.start);
    const slotEnd = new Date(sessionSlot.end);

    const compatibleUserAvailability = availabilitySlots.find((availabilitySlot) => {
      const availabilitySlotStart = new Date(availabilitySlot.start);
      const availabilitySlotEnd = new Date(availabilitySlot.end);

      // Return true if the slot is withing the user availability
      return availabilitySlotStart <= slotStart && availabilitySlotEnd >= slotEnd;
    });

    // If one of the slots is not compatible, then return false
    if (!compatibleUserAvailability) return false;
  }

  return true;
};

export const getVolunteeringCoefficient = (session, activities?) =>
  session.volunteeringCoefficient || session.volunteeringCoefficient === 0
    ? session.volunteeringCoefficient
    : (activities?.find((activity) => activity._id === session.activity._id) || session.activity)
        .volunteeringCoefficient || 0;
