import dayjs from "dayjs";
import {Drawer} from "antd";
import {useEffect, useMemo, useState} from "react";
import i18next from "i18next";

const compare = (a, b) => {
  if (a === b) {
    return 0;
  } else {
    const aDefined = a !== undefined && a !== null;
    const bDefined = b !== undefined && b !== null;
    if (aDefined && bDefined) return a > b ? 1 : -1;
    else if (aDefined) return 1;
    else if (bDefined) return -1;
    else return 0;
  }
};

export const normalize = (string, caseSensitive = false) => {
  const normalized =
    string
      ?.toString() // Must be done if the string is actually a number
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "") || "";
  return caseSensitive ? normalized : normalized.toLowerCase();
};

export const listSorter = {
  date: (a, b) => (a ? dayjs(a).valueOf() : -Infinity) - (b ? dayjs(b).valueOf() : -Infinity),

  text: (a, b) => compare(normalize(a).replace(/ /g, ""), normalize(b).replace(/ /g, "")),

  number: compare,
};

export const listRenderer = {
  timeFormat: (date) => (date ? dayjs(date).format("LT") : ""),

  timeRangeFormat: (startDate, endDate) =>
    startDate && endDate
      ? listRenderer.timeFormat(startDate) + " – " + listRenderer.timeFormat(endDate)
      : "",

  shortDateFormat: (date) => {
    if (!date) return "";
    const parsedDate = Date.parse(date);
    return Intl.DateTimeFormat(i18next.language, {
      day: "numeric",
      month: "numeric",
    }).format(parsedDate);
  },

  longDateFormat: (date, short = false, withYear = false) => {
    if (!date) return "";
    const parsedDate = Date.parse(date);
    let day = Intl.DateTimeFormat(i18next.language, {
      day: "numeric",
      weekday: short ? "short" : "long",
    }).format(parsedDate);
    day = day.charAt(0).toUpperCase() + day.slice(1); // capitalize day
    let month = Intl.DateTimeFormat(i18next.language, {
      month: short ? "short" : "long",
    }).format(parsedDate);
    month = month.charAt(0).toUpperCase() + month.slice(1); // capitalize month
    let year =
      withYear &&
      Intl.DateTimeFormat(i18next.language, {
        year: "numeric",
      }).format(parsedDate);
    return day + " " + month + (year ? " " + year : "");
  },

  longDateTimeFormat: (date, short, withYear) =>
    date
      ? listRenderer.longDateFormat(date, short, withYear) + " " + listRenderer.timeFormat(date)
      : "",

  longDateRangeFormat: (startDate, endDate, short, withYear) => {
    if (!startDate || !endDate) return "";
    const start = dayjs(startDate);
    const end = dayjs(endDate);
    return `${listRenderer.longDateFormat(start, short)} – ${listRenderer.longDateFormat(
      end,
      short,
      withYear
    )}`;
  },

  longDateTimeRangeFormat: (startDate, endDate, short, withYear) => {
    if (!startDate || !endDate) return "";
    const start = dayjs(startDate);
    const end = dayjs(endDate);
    if (start.isSame(end, "day")) {
      return `${listRenderer.longDateTimeFormat(
        start,
        short,
        withYear
      )} – ${listRenderer.timeFormat(end)}`;
    } else {
      return `${listRenderer.longDateTimeFormat(start, short)} – ${listRenderer.longDateTimeFormat(
        end,
        short,
        withYear
      )}`;
    }
  },

  durationFormat: (minutes, short = false) => {
    if (minutes < 60) return `${Math.floor(minutes)}min`;
    const hours = dayjs.utc().startOf("day").add(minutes, "minute").format("H[h]mm");
    const numberOfDays = Math.floor(minutes / 1440);
    return (
      `${numberOfDays > 0 ? `${numberOfDays} jours et ` : ""}${hours}` +
      (short ? "" : ` (${Math.floor(minutes)}min)`)
    );
  },

  listOfClickableElements: (elements, clickableElement) =>
    elements
      ?.map((el, index) => el && clickableElement(el, index))
      .reduce((acc, el) => (acc ? [acc, ", ", el] : el), null) || "",
};

// const getNestedField = (obj, fieldPathArray) =>
//   fieldPathArray.reduce((o, key) => (o && o[key] !== "undefined" ? o[key] : undefined), obj);

export const matchesSearchValueWords = (
  item: any,
  normalizedSearchValueWords: [string],
  searchInFields: [string] | ((item: any) => [string])
): boolean => {
  // For every word in the user search...

  // If the item is an object, and some searchInFields fields are given, then pick them.
  // Otherwise, we assume that item is already an array of strings to search into.
  const potentialMatches =
    typeof searchInFields === "function"
      ? searchInFields(item)
      : searchInFields.map((field) => item[field]);

  // Make sure that for every search word given, there is a match in our potential matches
  return normalizedSearchValueWords.every((word) =>
    potentialMatches.find((field) => field?.length && normalize(field).includes(word))
  );
};

export const searchInObjectsList = (
  searchValue: string,
  list: [any],
  searchInFields: [string] | ((item: any) => [string])
) => {
  if (!list) return;
  if (!searchValue || searchValue.length === 0) return list;

  const normalizedSearchValueWords = normalize(searchValue).split(" ");

  // Filter elements that match search value
  return list.filter((item) =>
    matchesSearchValueWords(item, normalizedSearchValueWords, searchInFields)
  );
};

export const removeDuplicates = (array) => array?.filter((v, i, a) => a.indexOf(v) === i); // Remove duplicates

export const useDrawer = (props, memo = []) => {
  const [open, setOpen] = useState(false);

  const MyDrawer = useMemo(
    () =>
      ({children}) =>
        (
          <Drawer placement="right" onClose={() => setOpen(false)} open={open} {...props}>
            {children}
          </Drawer>
        ),
    [open, ...memo]
  );

  return [setOpen, MyDrawer];
};

export const useLoadList = (loader) => {
  useEffect(() => {
    // First run initialization
    loader();

    // TODO check if there is a need to have auto refresh or not. Maybe the websocket way to do is enough ? For now, let's comment that.
    //
    // const loadIfActive = () => {
    //   // Only trigger load if the page is active
    //   if (!document.hidden) loader();
    // };
    //
    // // Reload changed data every X seconds when the page is open...
    // const intervalId = setInterval(loadIfActive, AUTO_RELOAD_SECONDS * 5 * 1000);
    //
    // // ... And reload changed data each time the page has been inactive and becomes active again.
    // window.addEventListener("visibilitychange", loadIfActive);
    //
    // // Remove listener and interval on unmount
    // return () => {
    //   window.removeEventListener("visibilitychange", loadIfActive);
    //   clearInterval(intervalId);
    // };
  }, []);
};
