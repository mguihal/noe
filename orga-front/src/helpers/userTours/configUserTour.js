import {NOESocialIcons} from "../../components/common/layout/NOESocialIcons";
import {BetaTag} from "../../components/common/layout/BetaTag";
import {selectTab} from "../userTourUtilities";

export default (currentRegistration) => [
  {
    content: "👋 Bienvenue sur NOÉ ! Voici la page de configuration de votre événement.",
    position: "center",
  },
  {
    content: (
      <>
        <BetaTag title={"new"} /> Ce petit tour de la plateforme que vous allez suivre est tout
        nouveau ! <strong>Dites-nous ce que vous en pensez à la fin</strong>, ça nous aide
        énormément ❤
      </>
    ),
    position: "center",
  },

  /**
   * General
   */
  {
    ...selectTab("main"),
    content:
      "⚙️ Dans l'onglet Général, vous pouvez configurer les informations de base de votre événement.",
  },
  {
    ...selectTab("main", ".userTourProjectAvailabilities"),
    content:
      "Pensez à renseigner les dates de votre événement : NOÉ en a besoin pour vous aider à l'organiser au mieux.",
  },

  /**
   * Members
   */
  ["admin", "contrib"].includes(currentRegistration.role) && {
    ...selectTab("members"),
    content:
      "👥 Dans l'onglet Membres, vous pouvez voir et gérer les membres de votre équipe organisatrice.",
  },
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("members", ".userTourAddMembers"),
    content:
      "Vos co-équipiè·res ne sont pas encore sur NOÉ ? Envoyez-leur une invitation par email !",
  },

  /**
   * Welcome page
   */
  {
    ...selectTab("welcome-page"),
    content:
      "🏠 La page d'accueil de votre événement a un but à la fois promotionnel et informatif. C'est cette page que vous pourrez partager à vos participant·es pour qu'iels s'inscrivent. Personnalisez-la à votre image !",
  },

  /**
   * Form
   */
  {
    ...selectTab("form"),
    content:
      "📝 Construisez le formulaire d'inscription parfait pour vos participant·es, directement dans NOÉ.",
  },
  {
    ...selectTab("form", "div[ref='sidebar']"),
    action: () => {
      selectTab("form").action();
      selectTab("edit").action();
    },
    content:
      "Utilisez les nombreux types de questions 'Basiques' ou 'Avancés', et ajoutez de la 'Mise en page'.",
  },
  {
    ...selectTab("preview"),
    action: selectTab("form").action,
    content: "Prévisualisez ensuite votre formulaire comme si vous étiez un·e participant·e !",
  },
  {
    ...selectTab("form", ".userTourFormMapping"),
    content:
      "Le Mapping vous permettra d'accéder aux réponses de vos participant.es en un clin d'oeil, partout dans NOÉ.",
  },

  /**
   * Ticketing
   */
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("ticketing"),
    content:
      "🎟️ Besoin d'intégrer une billetterie pour votre événement ? pas de problème ! NOÉ propose notamment une intégration avec Hello Asso.",
  },

  /**
   * Advanced
   */
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("advanced"),
    content:
      "Dans l'onglet Avancé, vous trouverez les fonctionnalités d'import et d'export, l'historique de modifications, et d'autres choses utiles.",
  },

  {
    content: (
      <>
        <p>Et voilà ! 🤗</p>
        <p>
          Dites-nous ce que vous avez pensé de ce tour. Clair, pas clair ? Trop court, trop long ?{" "}
          <strong>On veut tout savoir !</strong>
        </p>
        <p>
          <strong>Envoyez-nous un petit message en cliquant sur une des icônes 👇</strong>
        </p>
        <div className={"containerH"} style={{gap: 10, justifyContent: "center"}}>
          <NOESocialIcons />
        </div>
      </>
    ),
    position: "center",
  },
];
