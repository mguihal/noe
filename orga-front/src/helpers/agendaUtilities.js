import dayjs from "dayjs";
import {searchInObjectsList} from "./listUtilities";
import {personName} from "./utilities";
import {getVolunteeringCoefficient} from "./sessionsUtilities";
import {getSessionSubscription} from "./registrationsUtilities";

//*******************************//
//******* SLOT UTILITIES ********//
//*******************************//

// DISPLAY THE SLOT NUMBER
export const slotNumberString = (slot) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s._id === slot._id) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// DISPLAY THE SLOT NAME
export const getSessionName = (session, teams?) => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName =
    teams?.find((t) => t._id === (session.team?._id || session.team))?.name || session.team?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

export const getElementsFromListInSlot = (elementName, slot, getKey: (s: any) => string) =>
  slot[elementName]?.length > 0 && slot[`${elementName}SessionSynchro`] === false
    ? slot[elementName].map(getKey)
    : slot.session[elementName].map(getKey);

const specialCategoriesFilterOptions = ["volunteering", "allTheRest"];

export const filterAppointments = (slots, searchValue, categoriesFilter, registrations) => {
  const {text: searchBarValue = "", scopes = []} = searchValue;

  const searchInScope = Object.fromEntries(scopes.map((scope) => [scope, true]));

  // This code is shared across frontends, cf. inscription-front/src/features/sessions.js updateFilteredList()
  if (categoriesFilter?.length > 0) {
    // Then filter only by category so we need to remove the special categories entries from the rest
    const filterVolunteering = categoriesFilter.includes("volunteering");
    const filterAllTheRest = categoriesFilter.includes("allTheRest");
    const pureCategoriesFilter = categoriesFilter.filter(
      (category) => !specialCategoriesFilterOptions.includes(category)
    );

    slots = slots?.filter((slot) => {
      // Filter by "special categories"
      let specialCategoriesFilter;
      if (filterAllTheRest && !filterVolunteering) {
        // "- All the rest -" is toggled but not "- All the volunteering -"
        specialCategoriesFilter = getVolunteeringCoefficient(slot.session) === 0;
      } else if (filterVolunteering && !filterAllTheRest) {
        // "- All the volunteering -" is toggled but not "- All the rest -"
        specialCategoriesFilter = getVolunteeringCoefficient(slot.session) > 0;
      } else if (filterVolunteering && filterAllTheRest) {
        // Both are toggled
        specialCategoriesFilter = true; // if  we have both volunteering and allTheRest, it's like we don't filter at all
      } else {
        // None are toggled
        specialCategoriesFilter = false; // if we have none, then we don't wanna filter with it at all
      }

      return (
        specialCategoriesFilter ||
        (pureCategoriesFilter.length > 0
          ? pureCategoriesFilter.includes(slot.session.activity.category._id)
          : false) // Don't filter if there are no pure categories selected
      );
    });
  }

  return searchInObjectsList(searchBarValue, slots, (slot) => [
    searchInScope.session && slot.session.name,
    searchInScope.activity && slot.session.activity.name,
    searchInScope.category && slot.session.activity.category.name,
    ...((searchInScope.secondaryCategories && slot.session.activity.secondaryCategories) || []),
    ...((searchInScope.stewards && slot.stewards?.map(personName)) || []),
    ...((searchInScope.stewards && slot.session.stewards?.map(personName)) || []),
    ...((searchInScope.places && slot.places?.map((p) => p.name)) || []),
    ...((searchInScope.places && slot.session.places?.map((p) => p.name)) || []),
    ...((searchInScope.registrations &&
      registrations
        ?.filter((r) => getSessionSubscription(r, slot.session))
        .map((r) => personName(r.user))) ||
      []),
    searchInScope.team && slot.session.team?.name,
  ]);
};

//*******************************//
//****** DISPLAY UTILITIES ******//
//*******************************//

// Handles dates when end date is at midnight o'clock. Without it, any session ending at midnight will
// overflow on the next day, which is really annoying. This manager aims to remove this behavior.
export const midnightDateManager = {
  dataToDisplay: (slot) => {
    // If end date is midnight, remove one second
    let endDate = dayjs(slot.endDate);
    if (endDate.hour() === 0 && endDate.minute() === 0) endDate = endDate.subtract(1, "minute");
    return {...slot, endDate: endDate.format()};
  },
  displayToData: (existingEndDate, newEndDate) => {
    // If end date is midnight, remove one minute
    const endDate = dayjs(existingEndDate);
    newEndDate = dayjs(newEndDate);
    if (endDate.hour() === 0 && endDate.minute() === 0) newEndDate = newEndDate.add(1, "minute");
    return newEndDate;
  },
};

// USEEFFECT HELPERS
export const trySeveralTimes = (
  actionToTry,
  whatToDoAfter,
  ...{limitOfTrials = 5, frequency = 100, errorHandling}
) => {
  let trials = 0;
  const tryInterval = setInterval(() => {
    try {
      if (actionToTry()) {
        clearInterval(tryInterval);
        whatToDoAfter && whatToDoAfter();
      } else {
        trials += 1;
        if (trials >= limitOfTrials) {
          clearInterval(tryInterval);
        }
      }
    } catch (e) {
      errorHandling && errorHandling(e);
    }
  }, frequency);
};

// export const useInitialScrollToProjectAvailabilities = (agendaDisplayParams, cellDisplayHeight) => {
//   // Scroll to the optimal place should be done once. If done, will be set to true in useEffect
//   const [initialScrollIsDone, setInitialScrollIsDone] = useState(false);
//
//   // Scroll on page load to reach the best optimal place to be on the view
//   useEffect(() => {
//     // The main table element has this class (it's not the only one, but it will always come first in the querySelector)
//     if (!initialScrollIsDone && agendaDisplayParams?.shouldScroll && cellDisplayHeight) {
//       trySeveralTimes(
//         () => {
//           // TODO difference between frontends is normal
//           const mainTimeTableElement = document.querySelector(".MainLayout-container");
//           // Scroll to reach the diff between the agenda display and the project availabilities
//           mainTimeTableElement.scrollTop =
//             agendaDisplayParams.hoursDiff * cellDisplayHeight * (60 / CELL_DURATION_MINUTES);
//           return true;
//         },
//         () => setInitialScrollIsDone(true),
//         {frequency: 30}
//       );
//     }
//   }, [agendaDisplayParams, cellDisplayHeight]);
// };

// Synchronize the height of the little ticks on the side of the calendar
export const synchronizeTicksOnSideOfAgenda = (className, cellDisplayHeight) => {
  // The trick is to spot the timescale labels but there is no eaasy hook (class names change all the time)
  // We created a custom class on which we can get a hook, named "timescale-cell".
  trySeveralTimes(() => {
    const elements = document.querySelector(`.${className}`)?.parentNode?.parentNode?.parentNode
      ?.parentNode?.parentNode?.lastChild?.firstChild.childNodes; // Go to the TicksLayout-table-XXXX and iterate
    if (elements?.length > 0) {
      elements.forEach((el) => (el.firstChild.style.height = `${cellDisplayHeight}px`));
      return true;
    }
  });
};

//*****************************//
//******* AGENDA PARAMS *******//
//*****************************//

// Cell sizes and duration
export const CELL_DURATION_MINUTES = 30; // The smallest unit of time for drag and drop
export const CELLS_DISPLAY_HEIGHTS = [25, 35, 45];

// Max recommended number of simultaneous columns
export const RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS = 15;

// DEFAULT AGENDA PARAMS
export const DEFAULT_AGENDA_PARAMS = {
  slotsOnEachOther: false,
  cellDisplayHeight: CELLS_DISPLAY_HEIGHTS[1],
  groupByDaysFirst: true,
  defaultNewSlotDuration: 90,
  displayAvailabilities: true,
  selectedResources: [],
  showResourcesListingOnAgendaCards: [
    "places",
    "stewards",
    "registrations",
    "maxNumberOfParticipants",
  ],
  resourcesFilterSelections: {},
  showResourcesAvailabilities: {},
  categoriesFilter: [],
};

// Number of days to display by default
const MAX_DAYS_TO_DISPLAY_BY_DEFAULT = 4;
export const getNumberOfDaysToDisplay = (windowWidth, project) =>
  Math.min(
    Math.max(1, Math.ceil((windowWidth - 280) / 450)), // Screen adaption
    dayjs(project.end).diff(project.start, "day") + 1, // Not more than the project opening dates
    MAX_DAYS_TO_DISPLAY_BY_DEFAULT // Default limit
  );

// Returns the hour of the day based on hours and minutes
const hoursOfDay = (dayjsDate) => dayjsDate.minute() / 60.0 + dayjsDate.hour();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = dayjs(slot.start);
  const end = dayjs(slot.end);
  const length = end.diff(start, "minute");
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(dayjs(slot.start));
      const slotEnd = hoursOfDay(dayjs(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

export const getAgendaDisplayStartDate = (project) => {
  const now = dayjs();
  const start = dayjs(project.start);
  const end = dayjs(project.end);

  // If we are during the event, start at the current date. Else, start at project start date
  return (now.isBefore(start) || now.isAfter(end) ? start : now).unix() * 1000;
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(dayjs(project.start)),
    end: hoursOfDay(dayjs(project.end)),
  };
};

const MIN_START_DISPLAY_HOUR = 8.5;
const MIN_END_DISPLAY_HOUR = 22.5;

const halfRound = (funcName: "floor" | "ceil" | "round", val) => Math[funcName](val * 2) / 2;

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayParams = (slots, project) => {
  let params;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    params = {
      start: halfRound(
        "floor",
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: halfRound(
        "ceil",
        slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end
      ),
      shouldScroll: true,
    };

    // Set up the scroll diff
    params.hoursDiff =
      slotsMinMaxHours && slotsExistBeforeProjectHours
        ? halfRound("floor", projectMinMaxHours.start) - params.start
        : 0;
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    params = {
      start: halfRound(
        "floor",
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: halfRound(
        "ceil",
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    params = {start: MIN_START_DISPLAY_HOUR, end: MIN_END_DISPLAY_HOUR};
  }

  params.start = Math.max(params.start - 1, 0); // Display one hour before display start, but do not go below 0 hours
  params.end = Math.min(params.end + 1, 24); // Display one hour after display end, but do not go after 24 hours

  // We should at least display from MIN to MAX default display hour
  params.start = Math.min(MIN_START_DISPLAY_HOUR, params.start);
  params.end = Math.max(MIN_END_DISPLAY_HOUR, params.end);

  return params;
};
