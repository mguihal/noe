import React from "react";
import {t} from "i18next";
import {pick} from "./utilities";
import dayjs from "dayjs";

const getFieldsOptions = (obj, getLabel) =>
  Object.keys(obj || {}).map((key) => ({
    key,
    label: getLabel(key),
  }));

export const extractImportExportFields = (dataToImportExport, operationType) => {
  // Extract and format fields from the data at global and project level
  const projectFields = getFieldsOptions(dataToImportExport?.project, (key) =>
    t(`projects:schema.${key}.label`, key)
  ).filter((field) => !["start", "end"].includes(field.key)); // Don't include start and end in th choice selection, but import them automatically when the availability slots are asked by the user

  const globalFields = getFieldsOptions(dataToImportExport, (key) =>
    key === "project" ? (
      <>
        Configuration du projet
        <span style={{color: "gray"}}>
          {" "}
          - vous pourrez sélectionner plus tard ce que vous souhaitez{" "}
          {operationType === "import" ? "importer" : "exporter"}
        </span>
      </>
    ) : (
      <>
        {t(`${key}:label_other`, key)}{" "}
        <span style={{color: "gray"}}> - {dataToImportExport[key]?.length} éléments</span>
      </>
    )
  );

  return {projectFields, globalFields};
};

export const getSelectionWithDependencies = (valuesKeys, dataToImportExport) => {
  if (valuesKeys.includes("registrations")) {
    if (!valuesKeys.includes("sessions") && dataToImportExport.sessions) {
      valuesKeys.push("sessions");
    }
  }
  if (valuesKeys.includes("sessions")) {
    if (!valuesKeys.includes("activities") && dataToImportExport.activities) {
      valuesKeys.push("activities");
    }
    if (!valuesKeys.includes("teams") && dataToImportExport.teams) {
      valuesKeys.push("teams");
    }
  }

  if (valuesKeys.includes("teams")) {
    if (!valuesKeys.includes("activities") && dataToImportExport.activities) {
      valuesKeys.push("activities");
    }
  }

  if (valuesKeys.includes("activities")) {
    if (!valuesKeys.includes("categories") && dataToImportExport.categories) {
      valuesKeys.push("categories");
    }
    if (!valuesKeys.includes("stewards") && dataToImportExport.stewards) {
      valuesKeys.push("stewards");
    }
    if (!valuesKeys.includes("places") && dataToImportExport.places) {
      valuesKeys.push("places");
    }
  }
  return valuesKeys;
};

export const pickSelectedImportExportData = (
  dataToImportExport,
  globalFieldsSelection,
  projectFieldsSelection
) => {
  // Pick the global fields form the import
  const pickedData = pick(
    dataToImportExport,
    globalFieldsSelection.map(({key}) => key)
  );

  // Pick the data in the project object as well
  if (pickedData.project) {
    const projectFieldsSelectionKeys = projectFieldsSelection.map(({key}) => key);

    if (projectFieldsSelectionKeys.includes("availabilitySlots")) {
      projectFieldsSelectionKeys.push("start", "end");
    }

    console.log("keys", projectFieldsSelectionKeys);
    pickedData.project = pick(pickedData?.project, projectFieldsSelectionKeys);
  }

  return pickedData;
};

export const shiftImportDates = (dataToImport, dateShift) => {
  const shiftedData = {...dataToImport};
  const shiftDate = (dateString) =>
    dateString ? dayjs(dateString).add(dateShift, "day").format() : dateString;

  const shiftAvailabilities = (availabilitySlots) =>
    availabilitySlots
      ? availabilitySlots?.map((slot) => ({
          ...slot,
          start: shiftDate(slot.start),
          end: shiftDate(slot.end),
        }))
      : availabilitySlots;

  const applyArrayModification = (obj, key, transformFn) => {
    if (obj[key]?.length > 0) obj[key] = obj[key].map((el) => ({...el, ...transformFn(el)}));
  };

  // Shift project dates
  if (shiftedData.project) {
    shiftedData.project = {
      ...shiftedData.project,
      start: shiftDate(shiftedData.project.start),
      end: shiftDate(shiftedData.project.end),
      availabilitySlots: shiftAvailabilities(shiftedData.project.availabilitySlots),
    };
  }

  // Shift sessions dates
  applyArrayModification(shiftedData, "sessions", (session) => ({
    start: shiftDate(session.start),
    end: shiftDate(session.end),
    slots: shiftAvailabilities(session.slots),
  }));

  // Shift stewards dates
  applyArrayModification(shiftedData, "stewards", (steward) => ({
    availabilitySlots: shiftAvailabilities(steward.availabilitySlots),
  }));

  // Shift places dates
  applyArrayModification(shiftedData, "places", (place) => ({
    availabilitySlots: shiftAvailabilities(place.availabilitySlots),
  }));

  // Shift registrations dates
  applyArrayModification(shiftedData, "registrations", (registration) => ({
    availabilitySlots: shiftAvailabilities(registration.availabilitySlots),
    daysOfPresence: shiftAvailabilities(registration.daysOfPresence),
  }));

  return shiftedData;
};
