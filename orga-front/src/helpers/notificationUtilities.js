import {Button, Modal, notification} from "antd";
import {WifiOutlined} from "@ant-design/icons";
import React from "react";
import {personName} from "./utilities";
import {t} from "i18next";
import {Trans} from "react-i18next";
import {ArgsProps} from "antd/es/notification/interface";

/**
 * Display a notification for the user
 * @param type notif type : success, warning, etc
 * @param key the identificator
 * @param buttonText if there is a button, the text of the button
 * @param onClickButton if there is a button, the action on click of the button
 * @param props additionnal notification props
 */
export const displayNotification = (
  type,
  key,
  {
    buttonText,
    onClickButton,
    ...props
  }: ArgsProps & {buttonText?: React.Node, onClickButton?: () => void}
) =>
  notification[type]({
    key: key,

    btn: buttonText && onClickButton && (
      <Button
        type="primary"
        onClick={() => {
          onClickButton();
          notification.destroy(key);
        }}>
        {buttonText}
      </Button>
    ),
    duration: 0, // by default, notif will stay forever
    placement: "bottomRight",
    ...props, // Override with props if given
  });

/**
 * Demo notif when the app is in DEMO mode
 */
let demoNotifHasBeenClosed = false;
export const displayStagingInfoNotification = () => {
  // Display notif if not in production or in local development mode
  if (process.env.REACT_APP_MODE === "DEMO" && !demoNotifHasBeenClosed) {
    displayNotification("warning", "envDemoNotification", {
      message: <Trans i18nKey="demoModeNotification.message" ns="common" />,
      onClose: () => (demoNotifHasBeenClosed = true),
      description: (
        <Trans
          i18nKey="demoModeNotification.description"
          ns="common"
          values={{
            prettyProdUrl: process.env.REACT_APP_PROD_URL?.replace(/(http|https):\/\//, ""),
            contactEmail: process.env.REACT_APP_CONTACT_US_EMAIL,
          }}
          components={{
            strongOrange: <strong style={{color: "darkorange"}} />,
            linkToProdUrl: <a href={process.env.REACT_APP_PROD_URL} />,
            linkToContactEmail: (
              <a
                href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
                rel="noreferrer"
              />
            ),
          }}
        />
      ),
    });
  }
};

/**
 * Notifications when sessions inconsistencies are found
 * @param inconsistenciesList the list of inconsistencies ({type, entity, entitiesInvolved})
 */
export const displaySessionsInconsistenciesNotifications = (inconsistenciesList) => {
  if (inconsistenciesList.length > 0) {
    const listOfMessages = [];
    for (let inconsistency of inconsistenciesList) {
      for (let inconsistencyDetail of inconsistency.inconsistencies) {
        let message;
        if (inconsistencyDetail.type === "availabilitiesOverlap") {
          switch (inconsistencyDetail.entity) {
            case "project":
              message = t("sessions:inconsistenciesNotification.isOutOfProjectSlots");
              break;
            case "place":
              message = t("sessions:inconsistenciesNotification.isOutOfPlaceSlots", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved
                  .map((e) => e.name)
                  .join(", "),
              });
              break;
            case "steward":
              message = t("sessions:inconsistenciesNotification.isOutOfStewardSlots", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved.map(personName).join(", "),
              });
              break;
            default:
          }
        } else if (inconsistencyDetail.type === "alreadyUsedEntity") {
          switch (inconsistencyDetail.entity) {
            case "place":
              message = t("sessions:inconsistenciesNotification.placesConflictAtTheSameTime", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved
                  .map((e) => e.name)
                  .join(", "),
              });
              break;
            case "steward":
              message = t("sessions:inconsistenciesNotification.stewardConflictAtTheSameTime", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved.map(personName).join(", "),
              });
              break;
            default:
          }
        }
        listOfMessages.push(`La plage n°${inconsistency.slot} ${message}.`);
      }
    }
    console.log(inconsistenciesList[0]);
    displayNotification("open", `inconsistency-${inconsistenciesList[0].session}`, {
      message: t("sessions:inconsistenciesNotification.title"),
      description: (
        <ul>
          {listOfMessages.map((message) => (
            <li>{message}</li>
          ))}
        </ul>
      ),
      duration: 5 + inconsistenciesList.length * 2,
    });
  }
};

/**
 * Notification when the api server cannot be joined
 */
let serverNotAvailableModalOpen = false;
export const displayServerNotAvailableErrorModal = () => {
  if (!serverNotAvailableModalOpen) {
    serverNotAvailableModalOpen = true;
    Modal.error({
      icon: <WifiOutlined />,
      title: t("common:serverUnavailableModal.title"),
      content: <Trans ns="common" i18nKey="serverUnavailableModal.content" />,
      okText: t("common:serverUnavailableModal.tryAgain"),
      onOk: () => (serverNotAvailableModalOpen = false),
      onCancel: () => (serverNotAvailableModalOpen = false),
      closable: true,
      maskClosable: true,
    });
  }
};
