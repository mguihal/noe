import {Comparer, createEntityAdapter, IdSelector} from "@reduxjs/toolkit";
import dayjs from "dayjs";

/**
 * Generic reducers and adapters
 */

export const createCustomEntityAdapter = <T>(adapterOptions: {
  selectId?: IdSelector<T>,
  sortComparer?: false | Comparer<T>,
}) => {
  const adapter = createEntityAdapter(adapterOptions);
  return {
    ...adapter,
    reducers: {
      addToList: adapter.addOne,
      updateInList: adapter.setOne,
      removeFromList: adapter.removeOne,
      resetContext: (state, action) => {
        action.payload?.removeProject && delete state.init.project; // If we want to reset the project as well
        delete state.init.updatedAt;
      },
      initContext: (state, action) => {
        state.init.status = "pending";
      },
      initList: (state, action) => {
        // Separate objects that are new and objects that have been deleted
        const newAndUpdatedList = action.payload.list.filter((el) => !el.deletedAt);
        const deletedList = action.payload.list.filter((el) => el.deletedAt);

        // If we are reloading a list that has already been loaded, just do some modifs, else, just replace all (in case we change project)
        if (state.init.project === action.payload.project) {
          adapter.setMany(state, newAndUpdatedList);
          adapter.removeMany(
            state,
            deletedList.map((el) => el._id)
          );
        } else {
          adapter.setAll(state, newAndUpdatedList);
        }

        state.init.status = "loaded";
        state.init.updatedAt = dayjs().subtract(1, "second").unix() * 1000;
        state.init.project = action.payload.project;
      },
      changeEditing: (state, action) => {
        state.editing = {...state.editing, ...action.payload};
      },
      setEditing: (state, action) => {
        state.editing = action.payload;
      },
    },
  };
};
