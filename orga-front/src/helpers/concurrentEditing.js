import {lastWebsocketConnectionId, useWebSocketConnection} from "./webSocket";
import {registrationsActions} from "../features/registrations";
import {sessionsActions} from "../features/sessions";
import {teamsActions} from "../features/teams";
import {placesActions} from "../features/places";
import {stewardsActions} from "../features/stewards";
import {activitiesActions} from "../features/activities";
import {categoriesActions} from "../features/categories";
import {store} from "../app/store";
import React, {useCallback} from "react";

const entityActionsMapping = {
  activities: activitiesActions,
  categories: categoriesActions,
  places: placesActions,
  registrations: registrationsActions,
  sessions: sessionsActions,
  stewards: stewardsActions,
  teams: teamsActions,
};

// We designed a React memo component so it does only gets re-rendered
// (and the connection reset) when we change projects.
export const ConcurrentEditingWorker = React.memo(
  ({currentProjectId}) => {
    const onMessage = useCallback(({action, endpoint, wsConn}) => {
      // Don't do any update if the current client is the one which made it
      if (wsConn === lastWebsocketConnectionId) return;

      if (action === "update") {
        const entityActions = entityActionsMapping[endpoint];
        entityActions && store.dispatch(entityActions.loadList({forceLoad: true}));
      }
    });

    useWebSocketConnection(
      currentProjectId,
      {type: "concurrentEditing", id: currentProjectId},
      onMessage
    );
    return null;
  },
  (pp, np) => pp.currentProjectId === np.currentProjectId
);
