import {slotEndIsBeforeBeginning} from "../components/utils/Availability";
import {MailOutlined} from "@ant-design/icons";
import {Tag} from "antd";
import {useTranslation} from "react-i18next";
import {t} from "i18next";
import {truncate} from "./utilities";
import {shouldShowField} from "../components/common/antdFormBuilder/FormBuilder";

export const flatFormInputs = (components) => {
  if (!components) return [];

  const tree = components.map((comp) => {
    if (comp.input) {
      // If the component is a simple one, return it
      return comp;
    } else {
      let branchs = [];
      // Put together all child components present in the "components" and "columns" arguments, and then apply recursively the flatFormInputs function on it
      if (comp.components) branchs = branchs.concat(comp.components);
      if (comp.columns) branchs = branchs.concat(comp.columns);
      return flatFormInputs(branchs);
    }
  });
  // Flatten the return value
  return tree.flat();
};

export const getRegistrationMetadata = (registration, project) => {
  const data = {};
  if (registration) {
    // Availabilities are clean
    const firstSlot = registration.availabilitySlots?.[0];
    const firstSlotIsDefined = !!(firstSlot?.start && firstSlot?.end);
    data.firstSlotEndIsBeforeBeginning = firstSlotIsDefined && slotEndIsBeforeBeginning(firstSlot);
    data.firstSlotIsOk = firstSlotIsDefined && !data.firstSlotEndIsBeforeBeginning;

    data.datesAlert =
      registration.booked && !firstSlotIsDefined
        ? t("registrations:registrationValidation.selectDatesToRegister")
        : data.firstSlotEndIsBeforeBeginning
        ? t("registrations:registrationValidation.endCannotBeBeforeStart")
        : undefined;

    // All mandatory form fields are filled
    const unfulfilledMandatoryFormFields = [];
    const flatFormComponents = flatFormInputs(project.formComponents);
    for (let formComponent of flatFormComponents) {
      if (formComponent.validate.required) {
        let invalid = false;
        if (registration.specific) {
          const value = registration.specific[formComponent.key];

          if (shouldShowField(formComponent.conditional, flatFormComponents, value)) {
            if (
              // No value at all
              value === undefined ||
              value === null ||
              //Empty text
              value === "" ||
              // Don't know
              value === [] ||
              // Checkbox
              value === false
            ) {
              invalid = true;
            } else if (typeof value === "object") {
              // Checkbox group
              const numberSelected = Array.isArray(value)
                ? value.length // New Antd form
                : Object.values(value).filter((option) => option === true).length; // Formio Legacy

              if (
                numberSelected < (formComponent.validate.minSelectedCount ?? 1) ||
                numberSelected > (formComponent.validate.maxSelectedCount ?? Infinity)
              ) {
                invalid = true;
              }
            }
          }
        } else {
          invalid = true;
        }

        if (invalid) unfulfilledMandatoryFormFields.push(formComponent.label);
      }
    }
    data.formIsOk = unfulfilledMandatoryFormFields.length === 0;
    data.formAlert =
      registration.booked && !data.formIsOk
        ? t("registrations:registrationValidation.someFormFieldsAreMandatory", {
            list: unfulfilledMandatoryFormFields.map((field) => truncate(field, 30)).join('", "'),
          })
        : undefined;

    // Mandatory Ticketing
    data.ticketingIsOk =
      !project.ticketingMode || registration?.[`${project.ticketingMode}Tickets`]?.length > 0;
    data.ticketingAlert =
      registration.booked && !data.ticketingIsOk
        ? t("registrations:registrationValidation.ticketingIsMandatory")
        : undefined;

    data.everythingIsOk = data.firstSlotIsOk && data.formIsOk && data.ticketingIsOk;

    return data;
  }
};

export const getInitializedRegistration = (registration, project, initializeDatabaseMetadata) => {
  const validationFields = getRegistrationMetadata(registration, project);
  const newRegistration = {
    ...registration,
    ...validationFields, // instant validation fields, always up to date
    touched: !initializeDatabaseMetadata, // To tell if the registration has been touched and is not in sync anymore with database
  };
  if (initializeDatabaseMetadata) {
    newRegistration.inDatabase = validationFields; // To keep a copy of the saved state
  }
  return newRegistration;
};

export const getSessionSubscription = (registration, session) =>
  registration?.sessionsSubscriptions?.find((ss) => ss.session === session?._id);

export const WaitingInvitationTag = () => {
  const {t} = useTranslation();
  return (
    <Tag icon={<MailOutlined />} color="processing">
      {t("users:waitingInvitation")}
    </Tag>
  );
};
