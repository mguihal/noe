import React, {useRef, useState} from "react";
import {Button, Checkbox, Input, message, Tag, Tooltip} from "antd";
import {MessageOutlined, SearchOutlined} from "@ant-design/icons";
import {listRenderer, listSorter, normalize} from "./listUtilities";
import {Link} from "@reach/router";
import {registrationsActions, registrationsSelectors} from "../features/registrations";
import {InputElement} from "../components/common/InputElement";
import {useDispatch, useSelector} from "react-redux";
import dayjs from "dayjs";
import {sessionsActions, sessionsSelectors} from "../features/sessions";
import {personName} from "./utilities";
import {FormElement} from "../components/common/FormElement";
import {
  flatFormInputs,
  getSessionSubscription,
  WaitingInvitationTag,
} from "./registrationsUtilities";
import i18next, {t} from "i18next";
import {SmsManager} from "@byteowls/capacitor-sms";
import {Capacitor} from "@capacitor/core";
import {SessionFilling} from "../components/sessions/utilsSessions";
import {viewSelectors} from "../features/view";

const {Search} = Input;

export const addKeyToItemsOfList = (data, keyName = undefined) => {
  return data ? data?.map((t, i) => ({...t, key: keyName ? t[keyName] : i})) : [];
};

export const fieldToData = (fields, acc = {}) => {
  return fields.reduce((accumulator, currentValue) => {
    accumulator[currentValue.name] = currentValue.value;
    return accumulator;
  }, acc);
};

export const dataToFields = (data, formatFieldsFn = undefined) => {
  let clone = {...data};

  formatFieldsFn && formatFieldsFn(clone);

  const fields = Object.keys(clone).reduce((accumulator, currentValue) => {
    accumulator.push({name: currentValue, value: clone[currentValue]});
    return accumulator;
  }, []);
  return fields;
};

export const useCopyColumns = (dataSource, columns, handleDisplayConfigChangeFn?) => {
  // Update the redux state, but rely on a useState because sometimes there is no proper dedicated redux state for this
  const reduxSorting = useSelector(viewSelectors.selectSorting);
  const [filtering, setFiltering] = useState(reduxSorting?.filteredInfo);

  // Getting all search texts that are set up for each column
  const columnsSearchTexts = Object.fromEntries(
    columns.map((column) => [
      column.dataIndex,
      column.searchText || ((record) => record[column.dataIndex]),
    ])
  );

  // Function that takes all the currently available filtering information from redux,
  const filteredWithColumns = (dataSource) => {
    let realDataSource = dataSource;
    if (filtering) {
      for (const [columnName, searchTextArray] of Object.entries(filtering)) {
        if (searchTextArray) {
          const value = searchTextArray[0];
          realDataSource = realDataSource.filter((record) =>
            normalize(columnsSearchTexts[columnName]?.(record))?.includes(normalize(value))
          );
        }
      }
    }

    return realDataSource;
  };
  const realDataSource = filteredWithColumns(dataSource);

  return {
    columns: columns.map((column) => ({
      ...column,
      onHeaderCell: (computedColumn) => ({
        ...computedColumn.onHeaderCell,
        onContextMenu: (e) => {
          // Prevent right click menu from appearing when right clicking on header cell
          e.preventDefault();
          // Get the function that will collect the text to search into for each item of the list
          const searchTextFn = columnsSearchTexts[computedColumn.dataIndex];
          // Map elements with that function and assemble them with a comma
          const copyableText = realDataSource?.map(searchTextFn).join(", ");
          // Write that in the clipboard
          navigator.clipboard.writeText(copyableText).then(() =>
            message.success(
              t("common:messages.elementsFromColumnCopiedToClipboard", {
                count: realDataSource?.length,
                columnTitle: column.title,
              })
            )
          );
        },
      }),
    })),
    handleDisplayConfigChange: (pagination, filters, sorter, extra) => {
      handleDisplayConfigChangeFn?.(pagination, filters, sorter, extra);
      setFiltering(filters);
    },
    currentDataSource: realDataSource,
  };
};

export const useSearchInColumns = (columns) => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters, confirm) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const enableColumnSearchProps = (dataIndex, columnTitle, searchText) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div className="containerH" style={{padding: 8}}>
        <Search
          autoComplete="new-password" // Prevent any autocomplete service to complete this, cause autoComplete="off" doesn't seem to work
          enterButton
          style={{maxWidth: 250}}
          value={selectedKeys[0]}
          // On press search button
          onSearch={() => handleSearch(selectedKeys, confirm, dataIndex)}
          // On keyboard input
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          // on press enter
          onPressEnter={(e) => {
            e.stopPropagation();
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          placeholder={t("common:filterIn", {name: columnTitle})}
          ref={searchInput}
        />
        <Button type="link" onClick={() => clearFilters && handleReset(clearFilters, confirm)}>
          {t("common:erase")}
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <div
        className={"containerH"}
        style={{
          borderRadius: 8,
          border: filtered && "1px solid var(--noe-primary)",
          background: filtered && "var(--noe-primary)",
          padding: filtered && 3.5,
        }}>
        <SearchOutlined
          style={{
            color: filtered ? "#ffffff" : "#888888",
          }}
        />
      </div>
    ),
    onFilter: (text, record) => {
      if (searchText) return normalize(searchText(record)).includes(normalize(text));
      else if (record[dataIndex]) {
        return normalize(record[dataIndex]).includes(normalize(text));
      } else {
        return "";
      }
    },
    onFilterDropdownOpenChange: (open) =>
      open && setTimeout(() => searchInput.current?.select(), 100),
  });

  return columns.map((column) => ({
    // The column data
    ...column,
    // Enable search options if needed
    ...(column.searchable
      ? enableColumnSearchProps(column.dataIndex, column.title, column.searchText)
      : undefined),
  }));
};

export const columnsStewards = [
  {
    title: "Prénom",
    dataIndex: "firstName",
    sorter: (a, b) => listSorter.text(a.firstName, b.firstName),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Nom",
    dataIndex: "lastName",
    sorter: (a, b) => listSorter.text(a.lastName, b.lastName),
    searchable: true,
  },
];

export const columnsPlaces = [
  {
    title: "Nom",
    dataIndex: "name",
    sorter: (a, b) => listSorter.text(a.name, b.name),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Jauge max",
    dataIndex: "maxNumberOfParticipants",
    render: (text) =>
      text ? `jauge de ${text}` : <span style={{color: "gray"}}>pas de jauge max.</span>,
    sorter: (a, b) => listSorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
  },
];

// Same function as in api/src/controllers/pdf.ts
export const cleanAnswer = (answer, separator = ",\n", flatFormComponents?, formField?) => {
  const formComp =
    formField && flatFormComponents && flatFormComponents.find((comp) => comp.key === formField);
  const options = formComp?.values || formComp?.data?.values || {};

  const findLabelForOption = (optValue) =>
    (options?.find && options.find((opt) => opt.value === optValue)?.label) || optValue;

  if (formComp?.type === "selectboxes") {
    const formattedAnswer = Array.isArray(answer)
      ? // New form
        answer
      : // Legacy Formio
        answer &&
        Object.entries(answer)
          .filter((entry) => entry[1])
          .map((entry) => entry[0]);
    return formattedAnswer?.map(findLabelForOption).join(separator);
  }

  if (formComp?.type === "select" || formComp?.type === "radio") {
    return findLabelForOption(answer);
  }

  return answer;
};

const EditableInlineTags = ({name, elementsActions, elementsSelectors, record}) => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);

  const modifyTagsFn = (recordId) => (tags) => {
    const recordToUpdate = {_id: recordId, [name]: tags};
    dispatch(elementsActions.persist(recordToUpdate));
  };

  return editMode ? (
    <FormElement initialValues={{[name]: record[name]}}>
      <InputElement.TagsSelect
        autoFocus={true}
        name={name}
        formItemProps={{style: {marginBottom: 0}}}
        elementsSelectors={elementsSelectors}
        onChange={modifyTagsFn(record._id)}
      />
    </FormElement>
  ) : (
    <div
      onClick={() => setEditMode(true)}
      style={{
        cursor: "pointer",
        minHeight: 32,
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        rowGap: 2,
      }}>
      {record[name]?.map((tagName, index) => (
        <Tag key={index}>{tagName}</Tag>
      ))}
    </div>
  );
};

export const editableTagsColumn = (name, title, elementsActions, elementsSelectors) => ({
  title,
  dataIndex: name,
  sorter: (a, b) => listSorter.text(a[name]?.join(" "), b[name]?.join(" ")),
  render: (text, record) => (
    <EditableInlineTags
      record={record}
      name={name}
      elementsActions={elementsActions}
      elementsSelectors={elementsSelectors}
    />
  ),
  searchable: true,
  searchText: (record) => record[name]?.join(" "),
});

export const registrationDispoColumn = {
  title: "Disponibilité",
  dataIndex: "dispo",
  render: (text, record) => {
    const nextOrCurrentSession = record.nextOrCurrentSession;
    if (nextOrCurrentSession) {
      const nextSessionIn = -dayjs().diff(nextOrCurrentSession.startDate, "minute");
      if (nextSessionIn <= 0) {
        return <Link to={`../sessions/${nextOrCurrentSession.sessionId}`}>Occupé.e</Link>;
      } else if (nextSessionIn < 120) {
        return <Link to={`../sessions/${nextOrCurrentSession.sessionId}`}>Bientôt occupé.e</Link>;
      }
    }

    return "Libre";
  },
  sorter: (a, b) =>
    listSorter.date(a.nextOrCurrentSession?.startDate, b.nextOrCurrentSession?.startDate),
};

export const ROLES_MAPPING = [
  {value: "guest", color: "#6cdac5"},
  {value: "contrib", color: "#5f98ed"},
  {value: "admin", color: "#1330f5"},
];

export const roleTag = (roleValue, small = false) => {
  if (!roleValue) return null;
  const roleObject = ROLES_MAPPING.find((r) => r.value === roleValue);
  const roleLabel = t(`registrations:schema.role.options.${roleValue}.label`);
  return (
    <Tag color={roleObject?.color} title={small ? roleLabel : undefined}>
      {small ? roleLabel[0] : roleLabel}
    </Tag>
  );
};

// Get the columns generated from the form mapping
export const getProjectFormMappingColumns = (
  project,
  nameSuffix,
  getFormAnswers = (record) => [record.specific]
) => {
  const flatFormComponents = flatFormInputs(project.formComponents);

  const renderAnswersList = (record, formField) =>
    listRenderer.listOfClickableElements(getFormAnswers(record), (formAnswer) =>
      cleanAnswer(formAnswer?.[formField], undefined, flatFormComponents, formField)
    );

  return (
    project.formMapping?.map(({formField, columnName}) => ({
      title: columnName + (nameSuffix ? ` ${nameSuffix}` : ""),
      dataIndex: columnName,
      render: (text, record) => renderAnswersList(record, formField),
      sorter: (a, b) =>
        listSorter.text(renderAnswersList(a, formField), renderAnswersList(b, formField)),
      searchable: true,
      searchText: (record) => renderAnswersList(record, formField),
    })) || []
  );
};

// Generate participants columns
export const generateRegistrationsColumns = (project, {start, middle, end} = {}) =>
  [
    ...(start || []),
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => {
        const sessionsNoShows = record.sessionsSubscriptions?.filter(
          (sessionSubscription) => sessionSubscription.hasNotShownUp
        );
        return (
          <>
            {roleTag(record.role, true)}
            {sessionsNoShows?.length > 0 && (
              <span title={`Ce·tte participant·e a fait défaut ${sessionsNoShows.length} fois.`}>
                {sessionsNoShows.map(() => "❗").join("")}{" "}
              </span>
            )}
            {record.user?.firstName}
          </>
        );
      },
      sorter: (a, b) => listSorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => listSorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...getProjectFormMappingColumns(project),
    editableTagsColumn(
      "tags",
      t("common:schema.tags.label"),
      registrationsActions,
      registrationsSelectors
    ),
    project.useTeams && {
      title: "Équipe",
      dataIndex: "team",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.teamsSubscriptions, (el, index) => (
          <Link to={`../teams/${el.team._id}`} key={index}>
            {el.team.name}
          </Link>
        )),
      sorter: (a, b) => listSorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: t("registrations:numberOfDaysOfPresence.label"),
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => listSorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);

const getCategoryTitle = (el) => el.activity?.category?.name;
const getSessionName = (session, simpleMode = true, path?) => {
  const sessionNamePrefix = session.name?.length > 0 ? session.name + " - " : "";
  return simpleMode ? (
    sessionNamePrefix + session.activity?.name
  ) : (
    <>
      {sessionNamePrefix}
      <Link to={`${path}/activities/${session.activity?._id}`}>{session.activity?.name}</Link>
    </>
  );
};
export const generateSessionsColumns = (
  path,
  showPlaces,
  showTeams,
  byDate = false,
  simpleMode = false,
  registrations,
  showSms = false,
  currentProject
) => {
  const phoneAnswerFields =
    showSms && currentProject?.formMapping.filter((mapping) => mapping.isPhone);

  return [
    {
      title: "Catégorie",
      dataIndex: "category",
      sorter: (a, b) => listSorter.text(getCategoryTitle(a), getCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`${path}/categories/${record.activity?.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            color={record.activity?.category?.color}>
            {getCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      ellipsis: true,
      searchText: getCategoryTitle,
    },
    {
      title: "Activité",
      dataIndex: "activity",
      render: (text, record) => getSessionName(record, simpleMode, path),
      // Filter by activity name, then by session name
      sorter: (a, b) =>
        listSorter.text(a.activity?.name, b.activity?.name) || listSorter.text(a.name, b.name),
      searchable: true,
      searchText: (record) => getSessionName(record),
    },
    {
      title: "Début – Fin",
      dataIndex: "start",
      sorter: (a, b) => listSorter.date(a.start, b.start),
      render: (text, record) =>
        listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
      defaultSortOrder: byDate && "ascend",
      width: i18next.language === "fr" ? 200 : 225,
      searchable: true,
      searchText: (record) => listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
    },
    {
      title: "Encadrant⋅es",
      dataIndex: "stewards",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.stewards, (el, index) =>
          simpleMode ? (
            personName(el)
          ) : (
            <Link to={`${path}/stewards/${el._id}`} key={index}>
              {personName(el)}
            </Link>
          )
        ),
      sorter: (a, b) =>
        listSorter.text(
          a.stewards.map(personName).join(", "),
          b.stewards.map(personName).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.stewards.map(personName).join(", "),
    },
    showPlaces && {
      title: "Espaces",
      dataIndex: "places",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.places, (el, index) =>
          simpleMode ? (
            el.name
          ) : (
            <Link to={`${path}/places/${el._id}`} key={index}>
              {el.name}
            </Link>
          )
        ),
      sorter: (a, b) =>
        listSorter.text(
          a.places.map((el) => el.name).join(", "),
          b.places.map((el) => el.name).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.places.map((el) => el.name).join(", "),
    },
    showTeams && {
      title: "Équipe",
      dataIndex: "team",
      render: (text, record) => (
        <Link to={`../teams/${record.team?._id}`}>{record.team?.name}</Link>
      ),
      sorter: (a, b) => listSorter.text(a.team?.name, b.team?.name),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.team?.name,
    },
    editableTagsColumn("tags", "Tags", sessionsActions, sessionsSelectors),
    {
      title: "Remplissage",
      dataIndex: "numberParticipants",
      render: (text, record) => {
        return (
          <SessionFilling
            computedMaxNumberOfParticipants={record.computedMaxNumberOfParticipants}
            numberOfParticipants={record.numberParticipants}
            showLabel={false}
          />
        );
      },
      sorter: (a, b) => {
        const maxA = a.computedMaxNumberOfParticipants;
        const maxB = b.computedMaxNumberOfParticipants;
        return listSorter.number(
          maxA !== undefined && maxA !== 0 ? a.numberParticipants / maxA : -1,
          maxB !== undefined && maxB !== 0 ? b.numberParticipants / maxB : -1
        );
      },
      width: 150,
    },
    registrations && {
      title: "Arrivées",
      dataIndex: "allArrived",
      render: (text, record) => {
        const numberOfArrivedParticipants = registrations.filter(
          (r) => r.hasCheckedIn && getSessionSubscription(r, record)
        )?.length;
        const color = numberOfArrivedParticipants >= record.numberParticipants ? "green" : "red";
        const string = record.numberParticipants
          ? `${numberOfArrivedParticipants}/${record.numberParticipants}`
          : numberOfArrivedParticipants;
        return record.numberParticipants > 0 ? (
          <span
            title={`Sur les ${record.numberParticipants} participant⋅es inscrites, ${numberOfArrivedParticipants} sont arrivé⋅es à l'événement\n(case "Arrivé.e" dans la liste des participant⋅es).`}
            style={{color}}>
            {string}
          </span>
        ) : (
          ""
        );
      },
      width: 95,
    },
    phoneAnswerFields?.length > 0 && {
      dataIndex: "sms",
      width: 45,
      render: (text, record) => {
        const participants = registrations.filter((r) => getSessionSubscription(r, record));
        return phoneAnswerFields.map((phoneField) => (
          <Tooltip
            title={"Envoyer un SMS groupé aux participant·es"}
            placement="topLeft"
            key={phoneField}>
            <Button
              type={"link"}
              icon={<MessageOutlined />}
              onClick={() => {
                const phoneNumbers = participants
                  .map((r) => r.specific?.[phoneField.formField])
                  .filter((el) => el && el !== "");

                if (phoneNumbers.length === 0) {
                  message.warning("Pas de numéros de téléphone trouvés.");
                  return;
                }

                const textMessage = createSmsMessage(record, phoneField.defaultTextMessage);

                console.log("Message :", textMessage);
                console.log("Recipients :", phoneNumbers);

                if (Capacitor.isNativePlatform()) {
                  SmsManager.send({
                    numbers: phoneNumbers,
                    text: textMessage,
                  }).catch((err) => console.log("SMS creation failed", err));
                } else {
                  message.info(
                    <span>
                      Profitez de la fonctionnalité SMS groupés sur notre appli mobile !{" "}
                      <Button
                        type={"primary"}
                        style={{marginLeft: 8}}
                        href="https://gitlab.com/alternatiba/noe/-/releases"
                        target="_blank"
                        rel="noreferrer">
                        J'installe l'application !
                      </Button>
                    </span>
                  );
                }
              }}
            />
          </Tooltip>
        ));
      },
    },
  ].filter((el) => !!el);
};

export const SessionNoShowCheckbox = ({
  session,
  registration,
  registrations,
  disabled,
  dispatch,
}) => {
  const sessionSubscription = getSessionSubscription(registration, session);

  const checkedBy =
    sessionSubscription?.hasNotShownUp &&
    registrations.find((r) => r.user._id === sessionSubscription.hasNotShownUp);

  const toggle =
    !disabled &&
    ((event) => {
      dispatch(
        registrationsActions.hasNotShownUpInSession(
          registration._id,
          session._id,
          event.target.checked
        )
      );
    });

  if (!sessionSubscription) return null;
  if (disabled) return sessionSubscription?.hasNotShownUp ? "❗" : null;

  return (
    <Tooltip
      title={
        checkedBy && (
          <>
            Coché par{" "}
            <Link
              to={`../../participants/${checkedBy._id}`}
              style={{color: "white", textDecoration: "underline"}}>
              {personName(checkedBy.user)}
            </Link>
          </>
        )
      }>
      <Checkbox checked={sessionSubscription?.hasNotShownUp} onClick={toggle} disabled={disabled} />
    </Tooltip>
  );
};

const createSmsMessage = (session, defaultTextMessage) =>
  defaultTextMessage
    ?.replaceAll("{{sessionName}}", getSessionName(session))
    .replaceAll("{{startDate}}", listRenderer.longDateFormat(session.start))
    .replaceAll("{{endDate}}", listRenderer.longDateFormat(session.end))
    .replaceAll("{{startTime}}", listRenderer.timeFormat(session.start))
    .replaceAll("{{endTime}}", listRenderer.timeFormat(session.end))
    .replaceAll("{{startDateTime}}", listRenderer.longDateTimeFormat(session.start))
    .replaceAll("{{endDateTime}}", listRenderer.longDateTimeFormat(session.end));

export const generateColumnsTeams = (path) => [
  {
    title: "Catégorie",
    dataIndex: "category",
    sorter: (a, b) => listSorter.text(getCategoryTitle(a), getCategoryTitle(b)),
    render: (text, record) => (
      <Link to={`${path}/categories/${record.activity?.category?._id}`}>
        <Tag
          style={{
            cursor: "pointer",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            overflow: "hidden",
            maxWidth: 125,
          }}
          color={record.activity?.category?.color}>
          {getCategoryTitle(record)}
        </Tag>
      </Link>
    ),
    searchable: true,
    width: 140,
    ellipsis: true,
    searchText: getCategoryTitle,
  },
  {
    title: "Activité liée",
    dataIndex: "activity",
    render: (text, record) => (
      <Link to={`${path}/activities/${record.activity?._id}`}>{record.activity?.name}</Link>
    ),
    sorter: (a, b) => listSorter.text(a.activity?.name, b.activity?.name),
    searchable: true,
    searchText: (record) => record.activity?.name,
  },
  {
    title: "Nom",
    dataIndex: "name",
    sorter: (a, b) => listSorter.text(a.name, b.name),
    searchable: true,
  },
];

export const generateSubscriptionInfo = (record, sessionSubscription, registrations, showTeam) => {
  if (!sessionSubscription) return "Inscription non enregistrée";
  let res;

  // added by who ?
  if (registrations) {
    const subscribedByRegistration = registrations.find(
      (r) => r.user._id === sessionSubscription.subscribedBy
    );

    if (subscribedByRegistration?._id === record._id) res = "Manuelle";
    else if (subscribedByRegistration?.role) {
      const subscribedByName = ` ${personName(subscribedByRegistration.user)}`;

      res = (
        <>
          Par{" "}
          <Link to={`../../participants/${subscribedByRegistration._id}`}>{subscribedByName}</Link>
        </>
      );
    }
  }

  // Added as a session or team subscription ?
  if (showTeam) {
    const sessionSubscriptionTeam = record.teamsSubscriptions
      .map((ts) => ts.team)
      ?.find((t) => t._id === sessionSubscription.team);
    if (sessionSubscriptionTeam) {
      res = (
        <>
          {res}
          <br />
          {t("sessions:schema.subscriptionInfo.viaTeam")}{" "}
          <Link to={`../../teams/${sessionSubscriptionTeam._id}`}>
            {sessionSubscriptionTeam.name}
          </Link>
        </>
      );
    }
  }

  // Add date of subscription
  res = (
    <>
      {res}
      <br />
      <span style={{color: "gray"}}>
        {listRenderer.longDateTimeFormat(sessionSubscription.updatedAt, true, false)}
      </span>
    </>
  );

  return res;
};

export const searchInSessionFields = (session) => [
  session.name,
  session.activity.name,
  session.activity.category.name,
  ...(session.activity.secondaryCategories || []),
  ...(session.stewards?.map(personName) || []),
  ...(session.places?.map((p) => p.name) || []),
  session.team?.name,
];

export const searchInActivityFields = (activity) => [
  activity.name,
  activity.category.name,
  ...(activity.secondaryCategories || []),
  ...(activity.stewards?.map(personName) || []),
  ...(activity.places?.map((p) => p.name) || []),
];

export const searchInRegistrationFields = (registration) => [
  registration.user.firstName,
  registration.user.lastName,
  registration.user.email,
  ...(registration.tags || []),
];
