import {useState, useEffect, Suspense} from "react";
import {useDispatch} from "react-redux";

export const isValidObjectId = (id: string) => /[0-9a-z]{24}/.exec(id);

export const useLoadEditing = (
  elementsActions,
  idInUrl,
  additionalActions,
  clonedElement,
  dontClean
) => {
  const dispatch = useDispatch();

  useEffect(() => {
    additionalActions && additionalActions();
    if (idInUrl) {
      if (idInUrl === "clone") dispatch(elementsActions.changeEditing(clonedElement));
      else if (idInUrl === "groupedit") dispatch(elementsActions.loadEditing("new"));
      else dispatch(elementsActions.loadEditing(idInUrl));
    }

    if (!dontClean) return () => dispatch(elementsActions.setEditing({}));
  }, [idInUrl]);
};

export const useNewElementModal = (
  ElementEdit: any
  // formInstance?: FormInstance,
  // fieldName?: string | number | Array<string | number>,
  // setValue = (id) => id
) => {
  const [showNewEntityModal, setShowNewEntityModal] = useState(false);

  // Return the CreateButton, and the Modal Component
  return [
    setShowNewEntityModal,
    () =>
      showNewEntityModal ? (
        <Suspense fallback={() => null}>
          <ElementEdit
            asModal
            modalOpen={true}
            setModalOpen={setShowNewEntityModal}
            id="new"
            // TODO: Does not work at all, we need redu update but can't do this yet
            // onCreate={
            //   formInstance &&
            //   fieldName &&
            //   ((elementId) => formInstance.setFieldValue(fieldName, setValue(elementId)))
            // }
          />
        </Suspense>
      ) : null,
  ];
};
