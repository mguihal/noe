import {createSlice} from "@reduxjs/toolkit";
import dayjs from "dayjs";
import {
  fetchWithMessages,
  loadListFromBackend,
  loadEntityFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {displaySessionsInconsistenciesNotifications} from "../helpers/notificationUtilities";
import {registrationsActions, registrationsSelectors} from "./registrations";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {
  getElementsFromListInSlot,
  getSessionName,
  slotNumberString,
} from "../helpers/agendaUtilities";
import {personName} from "../helpers/utilities";
import {createCustomEntityAdapter} from "../helpers/customEntityAdapter";
import {listSorter} from "../helpers/listUtilities";
import {teamsSelectors} from "./teams";
import {getSessionSubscription} from "../helpers/registrationsUtilities";

const getSessionEnd = (session) => {
  if (session.slots?.length > 1) {
    return dayjs.max(...session.slots.map((s) => dayjs(s.end))).format();
  } else {
    return session.slots[0] ? dayjs(session.slots[0].end) : undefined;
  }
};

const getSessionStart = (session) => {
  if (session.slots?.length > 1) {
    return dayjs.min(...session.slots.map((s) => dayjs(s.start))).format();
  } else {
    return session.slots[0] ? dayjs(session.slots[0].start) : undefined;
  }
};

const getSessionStartAndEnd = (session, defaultStart) => {
  return {start: getSessionStart(session) || defaultStart, end: getSessionEnd(session)};
};

const validAvailability = (slot, direction, project) => {
  let availabilitySlots;

  if (slot.places[0] && slot.places[0].availabilitySlots) {
    availabilitySlots = slot.places[0].availabilitySlots.map((a) => ({
      start: dayjs(a.start),
      end: dayjs(a.end),
    }));
  } else {
    availabilitySlots = project.project.availabilitySlots.map((a) => ({
      start: dayjs(a.start),
      end: dayjs(a.end),
    }));
  }

  availabilitySlots.sort((a, b) => a.start - b.start); //asc
  const dayjsSlot = {...slot, start: dayjs(slot.start), end: dayjs(slot.end)};
  let out;
  for (const [index, availabilitySlot] of availabilitySlots.entries()) {
    if (dayjsSlot.start >= availabilitySlot.start && dayjsSlot.end <= availabilitySlot.end) {
      //in
      out = dayjsSlot;
      break;
    } else if (dayjsSlot.start < availabilitySlot.start && index === 0) {
      //trop tot en limite
      let outEnd = availabilitySlot.start.add(dayjsSlot.duration, "minute");
      out = {
        ...dayjsSlot,
        end: outEnd,
        start: availabilitySlot.start,
      };
      // console.log('break too early');
      break;
    } else if (dayjsSlot.end > availabilitySlot.end && index === availabilitySlots.length - 1) {
      //pas trop tard en limite
      let outStart = availabilitySlot.end.subtract(dayjsSlot.duration, "minute");
      out = {
        ...dayjsSlot,
        start: outStart,
        end: availabilitySlot.end,
      };
      // console.log('break too late');
      break;
    } else if (index !== availabilitySlots.length - 1) {
      const nextAvaibility = availabilitySlots[index + 1];
      if (dayjsSlot.end > availabilitySlot.end && dayjsSlot.start < nextAvaibility.start) {
        //entre deux
        let outEnd;
        if (direction === "backward") {
          outEnd = availabilitySlot.end.clone();
        } else if (direction === "forward") {
          outEnd = nextAvaibility.start.clone();
        }
        outEnd = outEnd.add(dayjsSlot.duration, "minute");
        out = {
          ...dayjsSlot,
          start: nextAvaibility.start,
          end: outEnd,
        };
      }
    }
  }
  return out;
};

const postponeSlotInPlacesOpenTimeRecursiv = (slot, project, direction) => {
  const existingSlots = project.slots.map((es) => ({
    ...es,
    start: dayjs(es.start),
    end: dayjs(es.end),
  }));
  const key = existingSlots.map((s) => s._id).indexOf(slot._id);

  const validAvabilityOnly = false;

  let validSlot = slot;
  if (validAvabilityOnly) {
    validSlot = validAvailability(slot, direction, project);
  }

  let postponeHitory;
  switch (direction) {
    case "backward":
      if (key !== 0) {
        let postponeCandidate = existingSlots[key - 1];
        if (postponeCandidate.end > validSlot.start) {
          let postponeCandidateStart = validSlot.start.subtract(
            postponeCandidate.duration,
            "minute"
          );
          postponeCandidate = {
            ...postponeCandidate,
            end: validSlot.start.clone(),
            start: postponeCandidateStart,
          };
        }
        postponeHitory = postponeSlotInPlacesOpenTimeRecursiv(
          postponeCandidate,
          project,
          direction
        );
        const lastHistory = postponeHitory[postponeHitory.length - 1];
        if (lastHistory.end > validSlot.start) {
          let validSlotEnd = lastHistory.end.add(validSlot.duration, "minute");
          validSlot = {
            ...validSlot,
            start: lastHistory.end.clone(),
            end: validSlotEnd,
          };
        }
        if (validAvabilityOnly) {
          validSlot = validAvailability(validSlot, "forward", project);
        }

        postponeHitory.push(validSlot);
      } else {
        if (validAvabilityOnly) {
          return [validAvailability(slot, direction, project)];
        } else {
          return [slot];
        }
      }
      break;
    case "forward":
      if (key !== existingSlots.length - 1) {
        let postponeCandidate = existingSlots[key + 1];
        if (postponeCandidate.start < validSlot.end) {
          let postponeCandidateEnd = validSlot.end.add(postponeCandidate.duration, "minute");
          postponeCandidate = {
            ...postponeCandidate,
            start: validSlot.end.clone(),
            end: postponeCandidateEnd,
          };
        }
        postponeHitory = postponeSlotInPlacesOpenTimeRecursiv(
          postponeCandidate,
          project,
          direction
        );
        const lastHistory = postponeHitory[postponeHitory.length - 1];
        if (lastHistory.start < validSlot.end) {
          let validSlotStart = lastHistory.start.subtract(validSlot.duration, "minute");
          validSlot = {
            ...validSlot,
            end: lastHistory.start.clone(),
            start: validSlotStart,
          };
        }
        if (validAvabilityOnly) {
          validSlot = validAvailability(validSlot, "backward", project);
        }
        postponeHitory.push(validSlot);
      } else {
        if (validAvabilityOnly) {
          return [validAvailability(slot, direction, project)];
        } else {
          return [slot];
        }
      }
      break;
    default:
  }
  return postponeHitory;
};

const postponeSlotInPlacesOpenTime = (rawCurrentSlot, session) => {
  let currentSlot = {
    ...rawCurrentSlot,
    start: dayjs(rawCurrentSlot.start),
    end: dayjs(rawCurrentSlot.end),
  };

  const existingSlots = session.slots.map((es) => ({
    ...es,
    start: dayjs(es.start),
    end: dayjs(es.end),
  }));

  // Choose the direction. If we don't know the ID of the slot (ie the slot is new),
  // let's say 'forward'
  const oldCurrentSlotIndex = existingSlots.map((s) => s._id).indexOf(currentSlot._id);

  let oldCurrentSlot;
  if (oldCurrentSlotIndex !== -1) {
    // If found, get the old slot data in the existing slots array
    oldCurrentSlot = existingSlots[oldCurrentSlotIndex];
  } else {
    // If not found (= it's a new added slot), then use the current slot as the old slot
    oldCurrentSlot = currentSlot;
  }

  let direction;
  if (currentSlot.start.isBefore(oldCurrentSlot.start)) {
    direction = "backward";
  } else if (currentSlot.end.isAfter(oldCurrentSlot.end)) {
    direction = "forward";
  }

  let postponeResult;
  if (direction) {
    postponeResult = postponeSlotInPlacesOpenTimeRecursiv(currentSlot, session, direction);
  } else {
    postponeResult = [currentSlot];
  }

  const out = [];
  for (const postponeSlot of postponeResult) {
    if (typeof postponeSlot !== "object") {
      throw Error("Found dirty values in the postpone recommandations");
    }
    const existingSlot = existingSlots.find((es) => es._id == postponeSlot._id);
    if (existingSlot != undefined) {
      if (
        existingSlot.start.diff(postponeSlot.start) !== 0 ||
        existingSlot.end.diff(postponeSlot.end) !== 0
      ) {
        out.push(postponeSlot);
      }
    } else {
      out.push(postponeSlot);
    }
  }
  return out;
};

const displayInconsistencies = (sessionEditingState, projectId) =>
  // just check it with a little delay, cause it takes long
  setTimeout(
    () =>
      fetchWithMessages(`projects/${projectId}/sessions/checkInconsistencies`, {
        method: "POST",
        body: sessionEditingState,
      }).then((data) => displaySessionsInconsistenciesNotifications(data)),
    1500
  );

const sessionsAdapter = createCustomEntityAdapter({
  selectId: (el) => el._id,
  sortComparer: (a, b) => listSorter.date(a.start, b.start),
});
const reducers = sessionsAdapter.reducers;

export const sessionsSlice = createSlice({
  name: "sessions",
  initialState: sessionsAdapter.getInitialState({
    init: {status: "idle"},
    editing: {},
    slotList: undefined,
    inconsistenciesList: [],
  }),
  reducers: {
    addToList: (state, action) => {
      sessionsAdapter.addOne(state, action);
      state.slotList = [
        ...state.slotList,
        ...action.payload.slots.map((slot) => ({...slot, session: action.payload})),
      ];
    },
    updateInList: (state, action) => {
      sessionsAdapter.setOne(state, action);
      state.slotList = [
        ...state.slotList.filter((slot) => slot.session._id !== action.payload._id),
        ...action.payload.slots.map((slot) => ({...slot, session: action.payload})),
      ];
    },
    removeFromList: (state, action) => {
      sessionsAdapter.removeOne(state, action);
      state.slotList = state.slotList.filter((slot) => slot.session._id !== action.payload);
    },
    initContext: reducers.initContext,
    resetContext: reducers.resetContext,
    initList: (state, action) => {
      reducers.initList(state, action);
      state.slotList = Object.values(state.entities)
        .map((session) => session.slots.map((slot) => ({...slot, session})))
        .flat();
    },
    selectEditingByIdFromList: (state, action) => {
      state.editing = state.entities[action.payload];
    },
    changeEditing: (state, action) => {
      let newSession = action.payload;
      const oldSession = state.editing;

      // Auto-update the default slots of the session when we change the activity
      if (
        newSession.activity?.slots?.length > 0 && // If an activity is selected and has some slots
        ((oldSession.activity?._id && newSession.activity._id !== oldSession.activity._id) || // There was an old activity, but it has changed, or...
          (!oldSession.activity?._id && newSession._id === "new")) // ... the session is new and the activity just got selected
      ) {
        // get the slot durations from the activity if there are some
        if (newSession.activity.slots?.length > 0) {
          newSession.slots = newSession.activity.slots.map((s) => ({
            duration: s.duration,
            stewardsSessionSynchro: true,
            placesSessionSynchro: true,
          }));

          // Create the slots based on the session start
          let timeTrace = dayjs(newSession.start);
          for (const slot of newSession.slots) {
            slot.start = timeTrace.format();
            timeTrace = timeTrace.add(slot.duration, "minute");
            slot.end = timeTrace.format();
          }
        }
      }

      state.editing = {
        ...newSession,
        stewards: newSession.stewards || state.editing.stewards,
        places: newSession.places || state.editing.places,
        slots: newSession.slots || state.editing.slots,
        ...getSessionStartAndEnd(newSession),
      };
    },
    setEditing: reducers.setEditing,
    removeSlot: (state, action) => {
      state.editing.slots.splice(
        state.editing.slots.map((s) => s._id).indexOf(action.payload._id),
        1
      );
    },
    createSlot: (state, action) => {
      let newSlot = action.payload;

      // Check if we have to postpone other slots of this session if some of them are overlapping
      const slotsPostponed = postponeSlotInPlacesOpenTime(newSlot, state.editing);
      // The new slot has no id, find it back in the postponed slots
      newSlot = slotsPostponed.find((sp) => sp._id === undefined);

      state.editing.slots.push(newSlot);
    },
    addToEditing: (state, action) => {
      const field = action.payload.field;
      state.editing[field] = [...(state.editing[field] || []), ...action.payload.payload];
      for (let slot of state.editing.slots) {
        if (slot[`${field}SessionSynchro`]) {
          slot[field] = state.editing[field];
        }
      }
    },
    removeToEditing: (state, action) => {
      const field = action.payload.field;
      state.editing[field] = state.editing[field].filter(
        (t) => t._id !== action.payload.payload._id
      );
      for (let slot of state.editing.slots) {
        if (slot[`${field}SessionSynchro`]) {
          slot[field] = state.editing[field];
        }
      }
    },
  },
});

const asyncActions = {
  loadList:
    ({forceLoad, silent} = {}) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id;

      await loadListFromBackend(
        "sessions",
        projectId,
        state.sessions.init,
        () => dispatch(sessionsActions.initContext(projectId)),
        (data) => dispatch(sessionsActions.initList({list: data.list, project: projectId})),
        forceLoad,
        !silent
      );
    },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "sessions",
      entityId,
      projectId,
      state.sessions.editing,
      () =>
        dispatch(
          sessionsActions.setEditing({
            _id: "new",
            start: state.currentProject.project.start,
            stewards: [],
            places: [],
            slots: [],
          })
        ),
      (data) => dispatch(sessionsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE && dispatch(sessionsActions.setEditing(state.sessions.entities[entityId])),
      }
    );
  },
  persist:
    (fieldsToUpdate?: any, checkInconsistencies = false, optimisticUpdate = false) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

      // Pseudo optimistic update for slot times to give an instant feeling in the agenda view:
      optimisticUpdate && dispatch(sessionsActions.updateInList(state.sessions.editing));

      // If some fields are given as argument, directly take this to update the registration
      const payload = fieldsToUpdate || state.sessions.editing;

      return persistEntityInBackend(
        "sessions",
        {...payload, project: projectId},
        projectId,
        ({registrationsToUpdateIds, ...session}) => {
          dispatch(sessionsActions.addToList(session));
          dispatch(registrationsActions.loadList({forceLoad: true}));
        },
        ({registrationsToUpdateIds, ...session}) => {
          dispatch(sessionsActions.updateInList(session));
          dispatch(registrationsActions.loadList({forceLoad: true}));
        }
      );

      // if asked, check the inconsistencies
      checkInconsistencies &&
        displayInconsistencies(state.sessions.editing, state.currentProject.project._id);
    },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    // TODO refaire une passe sur toutes les .list
    // réadapter le removeEntityFromBackend

    await removeEntityInBackend(
      "sessions",
      entityId,
      projectId,
      sessionsSelectors.selectList(state),
      (_, {registrationsToUpdateIds}) => {
        dispatch(sessionsActions.removeFromList(entityId));
        dispatch(registrationsActions.loadList({forceLoad: true}));
      },
      true
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "sessions",
        `${entityId}/history`,
        projectId,
        state.sessions.editing,
        null,
        (data) => dispatch(sessionsActions.setEditing({...state.sessions.editing, history: data}))
      ));
  },
  checkInconsistencies: () => async (dispatch, getState) => {
    const state = getState();
    displayInconsistencies(state.sessions.editing, state.currentProject.project._id);
  },
  updateSlotToEditing: (newSlotData) => async (dispatch, getState) => {
    // Add duration to the slot data
    newSlotData.duration = dayjs(newSlotData.end).diff(dayjs(newSlotData.start), "minute");

    const state = getState();

    // Get all the slots of the current session in the redux state
    const currentSession = {...state.sessions.editing};
    const existingSlots = currentSession.slots.map((s) => ({...s}));

    try {
      // Get all the slots that need to be postponed and for each of them, update them with new start and end dates
      // This manages all the setup for the start and end dates
      const slotsPostponed = postponeSlotInPlacesOpenTime(newSlotData, state.sessions.editing);
      for (const slotPostponed of slotsPostponed) {
        let slotToUpdate = existingSlots.find((es) => es._id === slotPostponed._id);
        if (slotToUpdate) {
          slotToUpdate.start = slotPostponed.start.format();
          slotToUpdate.end = slotPostponed.end.format();
        }
      }
    } catch (error) {
      console.log(error.message, "Stopping.");
      return;
    }

    // If the slot has places synchro with its session, change the places everywhere to keep the synchronisation
    if (newSlotData.places && newSlotData.placesSessionSynchro) {
      // Load the slot places into the session editing state
      currentSession.places = newSlotData.places;
      // Update all the other slots that are also synchronized
      for (const inspectedSlot of existingSlots) {
        if (inspectedSlot.placesSessionSynchro) {
          inspectedSlot.places = newSlotData.places;
        }
      }
    }

    // Same for the stewards! :)
    if (newSlotData.stewards && newSlotData.stewardsSessionSynchro) {
      // Load the slot stewards into the session editing state
      currentSession.stewards = newSlotData.stewards;
      // Update all the other slots that are also synchronized
      for (const inspectedSlot of existingSlots) {
        if (inspectedSlot.stewardsSessionSynchro) {
          inspectedSlot.stewards = newSlotData.stewards;
        }
      }
    }

    // Add all the rest. Don't include the start and end dates as they could have been changed by the function above.
    const {start, end, ...dataToUpdate} = newSlotData;
    let slotToUpdateKey = existingSlots.findIndex((es) => es._id == newSlotData._id);
    existingSlots[slotToUpdateKey] = {
      ...existingSlots[slotToUpdateKey],
      ...dataToUpdate,
    };

    //rebuild session
    currentSession.slots = existingSlots;

    // Compute the new start and end for the session based on the new data
    const sessionStartAndEnd = getSessionStartAndEnd(
      currentSession,
      state.currentProject.project.start
    );
    dispatch(sessionsActions.changeEditing({...currentSession, ...sessionStartAndEnd}));
  },
  removeSlotToEditing: (key) => async (dispatch, getState) => {
    dispatch(sessionsActions.removeSlot(key));

    const state = getState();
    const currentSession = state.sessions.editing;
    // Compute the new start and end for the session based on the new data
    const sessionStartAndEnd = getSessionStartAndEnd(
      currentSession,
      state.currentProject.project.start
    );
    dispatch(sessionsActions.changeEditing({...currentSession, ...sessionStartAndEnd}));
  },
  createSlotToEditing: (newSlot) => async (dispatch, getState) => {
    dispatch(sessionsActions.createSlot(newSlot));

    const state = getState();
    const currentSession = state.sessions.editing;
    // Compute the new start and end for the session based on the new data
    const sessionStartAndEnd = getSessionStartAndEnd(
      currentSession,
      state.currentProject.project.start
    );
    dispatch(sessionsActions.changeEditing({...currentSession, ...sessionStartAndEnd}));
  },
};

const sessionsAdapterSelectors = sessionsAdapter.getSelectors((state) => state.sessions);

export const sessionsSelectors = {
  selectEditing: (state) => state.sessions.editing,
  selectList: sessionsAdapterSelectors.selectAll,
  selectSlotsList: (state) => state.sessions.slotList,
  selectSlotsListForAgenda: (state) =>
    state.sessions.slotList?.map((slot) => ({
      ...slot,
      id: slot._id,
      title:
        slotNumberString(slot) + getSessionName(slot.session, teamsSelectors.selectList(state)),
      startDate: slot.start,
      endDate: slot.end,
      location: getElementsFromListInSlot("places", slot, (el) => el.name).join(", "),
      placeId: getElementsFromListInSlot("places", slot, (el) => el._id),
      categoryId: slot.session.activity?.category?._id,
      stewardsNames: getElementsFromListInSlot("stewards", slot, personName).join(", "),
      participantsNames: registrationsSelectors
        .selectList(state)
        ?.filter((r) => getSessionSubscription(r, slot.session))
        .map((r) => personName(r.user))
        .join(", "),
      stewardId: getElementsFromListInSlot("stewards", slot, (el) => el._id),
      color: slot.session.activity?.category?.color,
    })),
};

export const sessionsReducer = sessionsSlice.reducer;

export const sessionsActions = {
  ...sessionsSlice.actions,
  ...asyncActions,
};
