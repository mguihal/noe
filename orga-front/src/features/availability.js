import dayjs from "dayjs";

const computeStartEnd = (subject) => {
  if (subject.availabilitySlots?.length > 1) {
    subject.start = dayjs.min(subject.availabilitySlots.map((s) => dayjs(s.start))).format();
    subject.end = dayjs.max(subject.availabilitySlots.map((s) => dayjs(s.end))).format();
  } else if (subject.availabilitySlots[0]) {
    subject.start = dayjs(subject.availabilitySlots[0].start);
    subject.end = dayjs(subject.availabilitySlots[0].end);
  } else {
    subject.start = subject.end = undefined;
  }
};

export function replaceAllAvailabilitySlots(state, action) {
  let newSlots = action.payload;

  state.editing.availabilitySlots = newSlots;
  computeStartEnd(state.editing);
}

export function removeAvailabilitySlot(state, action) {
  // Get remove slot
  let freeSlot = action.payload;

  // Remove Slot in editing
  state.editing.availabilitySlots = state.editing.availabilitySlots.filter(
    (s) => !(s.start == freeSlot.start && s.end == freeSlot.end)
  );
  computeStartEnd(state.editing);
}

export function createAvailabilitySlot(state, action) {
  // Get new slot
  let freeSlot = action.payload;
  if (!state.editing.availabilitySlots) {
    state.editing.availabilitySlots = [];
  }

  // Add slot in editing
  state.editing.availabilitySlots.push(freeSlot);

  computeStartEnd(state.editing);
}

export function updateAvailabilitySlot(state, action) {
  // Get update slot and old
  let newSlot = action.payload.newSlot;
  let oldSlot = action.payload.oldSlot;

  // Update Slot in editing
  state.editing.availabilitySlots = state.editing.availabilitySlots.map((s) =>
    s.start === oldSlot.start && s.end === oldSlot.end
      ? {...s, start: newSlot.start, end: newSlot.end}
      : s
  );

  computeStartEnd(state.editing);
}
