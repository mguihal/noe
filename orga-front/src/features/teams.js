import {createSlice} from "@reduxjs/toolkit";
import {
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
  resetDependenciesContext,
  shouldAutoRefreshDependencies,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {registrationsActions} from "./registrations";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {createCustomEntityAdapter} from "../helpers/customEntityAdapter";
import {listSorter} from "../helpers/listUtilities";
import {activitiesActions} from "./activities";

const reloadElementDependencies = (dispatch) => {
  // Update all dependencies next time
  dispatch(sessionsActions.loadList({forceLoad: true}));
  dispatch(registrationsActions.loadList({forceLoad: true}));
};

const teamsAdapter = createCustomEntityAdapter({
  selectId: (el) => el._id,
  sortComparer: (a, b) => listSorter.text(a.name, b.name),
});

export const teamsSlice = createSlice({
  name: "teams",
  initialState: teamsAdapter.getInitialState({
    init: {status: "idle"},
    editing: {},
  }),
  reducers: teamsAdapter.reducers,
});

const asyncActions = {
  loadList:
    ({forceLoad, silent} = {}) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id;

      await loadListFromBackend(
        "teams",
        projectId,
        state.teams.init,
        () => dispatch(teamsActions.initContext(projectId)),
        (data) => {
          dispatch(teamsActions.initList({list: data, project: projectId}));
          // If there were some new changes (using forceLoad), also force update of dependencies
          shouldAutoRefreshDependencies(forceLoad, data) &&
            resetDependenciesContext(dispatch, sessionsActions, registrationsActions);
        },
        forceLoad,
        !silent
      );
    },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "teams",
      entityId,
      projectId,
      state.teams.editing,
      () =>
        dispatch(
          teamsActions.setEditing({
            _id: "new",
            stewards: [],
            places: [],
          })
        ),
      (data) => dispatch(teamsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE && dispatch(teamsActions.setEditing(state.teams.entities[entityId])),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.teams.editing;

    return persistEntityInBackend(
      "teams",
      {...payload, project: projectId},
      projectId,
      ({sessionsToUpdateIds, registrationsToUpdateIds, ...team}) => {
        dispatch(teamsActions.addToList(team));
        reloadElementDependencies(dispatch);
      },
      ({sessionsToUpdateIds, registrationsToUpdateIds, ...team}) => {
        dispatch(teamsActions.updateInList(team));
        reloadElementDependencies(dispatch);
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "teams",
      entityId,
      projectId,
      teamsSelectors.selectList(state),
      () => {
        dispatch(teamsActions.removeFromList(entityId));
        reloadElementDependencies(dispatch);
      },
      true
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "teams",
        `${entityId}/history`,
        projectId,
        state.teams.editing,
        null,
        (data) => dispatch(teamsActions.changeEditing({history: data}))
      ));
  },
};

const teamsAdapterSelectors = teamsAdapter.getSelectors((state) => state.teams);

export const teamsSelectors = {
  selectEditing: (state) => state.teams.editing,
  selectList: teamsAdapterSelectors.selectAll,
};

export const teamsReducer = teamsSlice.reducer;

export const teamsActions = {
  ...teamsSlice.actions,
  ...asyncActions,
};
