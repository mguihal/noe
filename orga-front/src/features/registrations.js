import {createSlice} from "@reduxjs/toolkit";
import {
  shouldAutoRefreshDependencies,
  fetchWithMessages,
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
  resetDependenciesContext,
} from "../helpers/reduxUtilities";
import {sessionsActions, sessionsSelectors} from "./sessions";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {getRegistrationMetadata} from "../helpers/registrationsUtilities";
import dayjs from "dayjs";
import {getNextOrCurrentSession} from "../components/participants/utilsParticipants";
import {projectsSelectors} from "./projects";
import {teamsSelectors} from "./teams";
import {createCustomEntityAdapter} from "../helpers/customEntityAdapter";
import {listSorter} from "../helpers/listUtilities";

const registrationsAdapter = createCustomEntityAdapter({
  selectId: (el) => el._id,
  sortComparer: (a, b) => listSorter.date(a.createdAt, b.createdAt),
});

export const registrationsSlice = createSlice({
  name: "registrations",
  initialState: registrationsAdapter.getInitialState({
    init: {status: "idle"},
    editing: {},
    current: undefined,
  }),
  reducers: {
    ...registrationsAdapter.reducers,
    setCurrent: (state, action) => {
      state.current = action.payload;
    },
    changeCurrent: (state, action) => {
      state.current = {...state.current, ...action.payload};
    },
  },
});

const asyncActions = {
  loadList:
    ({forceLoad, silent} = {}) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id;

      await loadListFromBackend(
        "registrations",
        projectId,
        state.registrations.init,
        () => dispatch(registrationsActions.initContext(projectId)),
        (data) => {
          dispatch(registrationsActions.initList({list: data, project: projectId}));
          // If there were some new changes (using forceLoad), also force update of dependencies
          shouldAutoRefreshDependencies(forceLoad, data) &&
            resetDependenciesContext(dispatch, sessionsActions);
        },
        forceLoad,
        !silent
      );
    },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "registrations",
      entityId,
      projectId,
      state.registrations.editing,
      null,
      (data) => dispatch(registrationsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(registrationsActions.setEditing(state.registrations.entities[entityId])),
      }
    );
  },
  loadCurrent: (envId) => async (dispatch, getState) => {
    return loadEntityFromBackend("registrations", "current", envId, undefined, null, (data) =>
      dispatch(registrationsActions.changeCurrent(data))
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given
    const registrationState = state.registrations;

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || registrationState.editing;

    return persistEntityInBackend(
      "registrations",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(registrationsActions.addToList(data)),
      (data) => {
        dispatch(registrationsActions.updateInList(data));
        // If the updated registration is the user's own registration, then update it as well
        if (data.user._id === registrationState.current.user._id) {
          dispatch(registrationsActions.changeCurrent(data));
        }
        resetDependenciesContext(dispatch, sessionsActions);
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "registrations",
      entityId,
      projectId,
      registrationsSelectors.selectList(state),
      () => dispatch(registrationsActions.removeFromList(entityId))
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "registrations",
        `${entityId}/history`,
        projectId,
        state.registrations.editing,
        null,
        (data) => dispatch(registrationsActions.changeEditing({history: data}))
      ));
  },
  getPdfPlanning: (id) => async (dispatch, getState) => {
    const state = getState();

    id = id || state.registrations.editing._id;
    try {
      return await fetchWithMessages(
        `projects/${state.currentProject.project._id}/pdf/registrationPlanning/${id}`,
        {isBlob: true, method: "GET"},
        {},
        false,
        true
      );
    } catch {
      return Promise.reject();
    }
  },
  unregister: () => async (dispatch, getState) => {
    const state = getState();

    const currentProject = state.currentProject.project;
    const currentRegistration = state.registrations.editing;

    const updatedCurrentRegistration = await fetchWithMessages(
      `projects/${currentProject._id}/registrations/${currentRegistration._id}`,
      {
        method: "PATCH",
        body: {
          booked: false,
          availabilitySlots: [],
          specific: {},
          sessionsSubscriptions: [],
          teamsSubscriptions: [],
          steward: null,
          helloAssoTickets: [],
          customTicketingTickets: [],
        },
      },
      {200: "Désinscription réussie"}
    );

    // Update in list
    await dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
    await dispatch(registrationsActions.changeEditing(updatedCurrentRegistration));
  },
  hasNotShownUpInSession:
    (registrationId, sessionId, hasNotShownUp) => async (dispatch, getState) => {
      const state = getState();

      const currentProject = state.currentProject.project;

      const updatedCurrentRegistration = await fetchWithMessages(
        `projects/${currentProject._id}/registrations/${registrationId}/noShow/${sessionId}`,
        {
          method: "POST",
          body: {hasNotShownUp},
        },
        {200: "Modification réussie."}
      );

      // Update in list
      await dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
    },
  sendInvitationEmail: (user, registrationAdditionalData) => async (dispatch, getState) => {
    const state = getState();

    try {
      await fetchWithMessages(
        `projects/${state.currentProject.project._id}/registrations/sendInvitationEmail`,
        {method: "POST", body: {user, ...registrationAdditionalData}},
        {200: "L'email d'invitation a bien été envoyé."},
        undefined,
        true
      );
      await dispatch(registrationsActions.loadList({forceLoad: true}));
    } catch {
      return Promise.reject();
    }
  },
};

const registrationsAdapterSelectors = registrationsAdapter.getSelectors(
  (state) => state.registrations
);

export const registrationsSelectors = {
  selectEditing: (state) => state.registrations.editing,
  selectCurrent: (state) => state.registrations.current,
  selectList: registrationsAdapterSelectors.selectAll,
  selectListWithMetadata: (state) =>
    registrationsAdapterSelectors.selectAll(state)?.map((r) => ({
      ...r,
      ...getRegistrationMetadata(r, projectsSelectors.selectEditing(state)),
      teamsSubscriptionsNames: r.teamsSubscriptions?.map((t) => t.name).join(", "),
      nextOrCurrentSession: getNextOrCurrentSession(
        r,
        sessionsSelectors.selectSlotsList(state),
        teamsSelectors.selectList(state)
      ),
      arrivalDateTime: [...(r.availabilitySlots || [])].sort(
        (a, b) => dayjs(a.start) - dayjs(b.start)
      )?.[0]?.start,
      departureDateTime: [...(r.availabilitySlots || [])].sort(
        (a, b) => dayjs(b.end) - dayjs(a.end)
      )?.[0]?.end,
    })),
};
export const registrationsReducer = registrationsSlice.reducer;

export const registrationsActions = {
  ...registrationsSlice.actions,
  ...asyncActions,
};
