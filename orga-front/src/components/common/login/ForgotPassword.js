import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {TextInput} from "../inputs/TextInput";
import {navigate} from "@reach/router";
import {ConnectionPage} from "../ConnectionPage";
import {viewSelectors} from "../../../features/view";
import {useTranslation} from "react-i18next";
import {safeValidateFields} from "../FormElement";

export default function ForgotPassword() {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
  };

  const sendPasswordRecoveryMail = () => {
    safeValidateFields(form, () => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.forgotPassword());
    });
  };

  const goToLogInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("./login");
  };

  return (
    <ConnectionPage
      form={form}
      subtitle={t("common:connectionPage.forgottenPassword")}
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
      }}
      onFieldsChange={onChange}
      validateAction={sendPasswordRecoveryMail}
      buttons={
        <>
          <Button type="primary" onClick={sendPasswordRecoveryMail} htmlType="submit">
            {t("common:connectionPage.sendPasswordRecoveryEmail")}
          </Button>
          <Button type="link" onClick={goToLogInPage}>
            {t("common:connectionPage.backToConnectionPage")}
          </Button>
        </>
      }>
      <TextInput.Email
        label={t("common:connectionPage.recoveryEmail.label")}
        name="email"
        size="large"
        placeholder={t("common:connectionPage.recoveryEmail.placeholder")}
        rules={[{required: true}]}
      />
    </ConnectionPage>
  );
}
