import {t} from "i18next";

import {ColorPickerField} from "@react-page/editor";

// The divider plugin
import divider from "@react-page/plugins-divider";

// The spacer plugin
import spacer from "@react-page/plugins-spacer";
import "@react-page/plugins-spacer/lib/index.css";

// The video plugin
import video from "@react-page/plugins-video";
import "@react-page/plugins-video/lib/index.css";

// Customized plugins
import {imageP, slateP, backgroundP} from "./customPlugins";

const plugins = [slateP, backgroundP, spacer, divider, imageP, video];

const addStylesAndMarginDefaultControl = (plugins) =>
  plugins.map((plugin) => ({
    ...plugin,
    cellStyle: (data) => ({
      paddingLeft: data.paddingLeft,
      paddingRight: data.paddingRight,
      paddingTop: data.paddingTop,
      paddingBottom: data.paddingBottom,
      marginLeft: data.marginLeft,
      marginRight: data.marginRight,
      marginTop: data.marginTop,
      marginBottom: data.marginBottom,
      height: `calc(100% - calc(${data.marginTop}px + ${data.marginBottom}px))`,
      width: `calc(100% - calc(${data.marginLeft}px + ${data.marginRight}px))`,
      background: data.background,
      color: `${data.textColor} !important`,
      border: data.borderWidth > 0 ? `${data.borderWidth}px solid ${data.borderColor}` : undefined,
      borderRadius: data.borderRadius,
    }),
    cellSpacing: (data) => ({x: data.cellSpacingX, y: data.cellSpacingY}),
    controls: [
      {title: t("common:reactPage.controls"), controls: plugin.controls},
      {
        title: t("common:reactPage.margins.title"),
        controls: {
          type: "autoform",
          columnCount: 3,
          schema: {
            properties: {
              marginTop: {
                title: t("common:reactPage.margins.topExt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginBottom: {
                title: t("common:reactPage.margins.bottomExt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginLeft: {
                title: t("common:reactPage.margins.leftExt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginRight: {
                title: t("common:reactPage.margins.rightExt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingTop: {
                title: t("common:reactPage.margins.topInt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingBottom: {
                title: t("common:reactPage.margins.bottomInt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingLeft: {
                title: t("common:reactPage.margins.leftInt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingRight: {
                title: t("common:reactPage.margins.rightInt"),
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              cellSpacingX: {
                title: t("common:reactPage.margins.horizontalCellSpacing"),
                type: "number",
                default: 0,
                multipleOf: 1,
              },
              cellSpacingY: {
                title: t("common:reactPage.margins.verticalCellSpacing"),
                type: "number",
                default: 0,
                multipleOf: 1,
              },
            },
          },
        },
      },
      {
        title: t("common:reactPage.styles.title"),
        controls: {
          type: "autoform",
          columnCount: 3,
          schema: {
            properties: {
              borderWidth: {
                title: t("common:reactPage.styles.borderWidth"),
                type: "number",
                default: 0,
                multipleOf: 1,
              },
              borderRadius: {
                title: t("common:reactPage.styles.borderRadius"),
                type: "number",
                default: 0,
                multipleOf: 1,
              },
              borderColor: {
                title: t("common:reactPage.styles.borderColor"),
                type: "string",
                default: "#000000",
                uniforms: {component: ColorPickerField},
              },
              textColor: {
                title: t("common:reactPage.styles.textColor"),
                type: "string",
                default: "#000000",
                uniforms: {component: ColorPickerField},
              },
              backgroundColor: {
                title: t("common:reactPage.styles.backgroundColor"),
                type: "string",
                default: "#ffffff",
                uniforms: {component: ColorPickerField},
              },
            },
          },
        },
      },
    ],
    bottomToolbar: {dark: true},
  }));

export default addStylesAndMarginDefaultControl(plugins);
