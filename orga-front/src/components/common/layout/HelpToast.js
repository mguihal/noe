import {Button, Tooltip} from "antd";
import {QuestionOutlined} from "@ant-design/icons";
import React from "react";

export const HelpToast = ({tooltipMessage, ...buttonProps}) => (
  <div style={{position: "fixed", bottom: 10, right: 10, zIndex: 110}}>
    <Tooltip placement="leftBottom" title={tooltipMessage}>
      <Button type="primary" shape="circle" size="large" className="shadow" {...buttonProps}>
        <QuestionOutlined />
      </Button>
    </Tooltip>
  </div>
);
