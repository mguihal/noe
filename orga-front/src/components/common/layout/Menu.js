import {useNavigate} from "@reach/router";
import {Divider, Menu as AntMenu} from "antd";
import React from "react";

export const SidebarDivider = ({color}) => (
  <Divider
    style={{
      borderTop: "1px solid " + color,
      margin: "15pt 30% 0 30%",
      minWidth: "unset",
      width: "unset",
    }}
  />
);

export const MenuLayout = ({
  disabled,
  disabledMessage,
  title,
  selectedItem,
  children,
  items,
  rootNavUrl = "",
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const navigate = useNavigate();
  const color = disabled ? "rgba(255,255, 255, 0.3)" : "rgba(255, 255, 255, 0.8)";

  // If there are some items, then add the navigation. If the url is explicitely set to false,
  // and there is no already defined onClick action, then don't add the onClick function
  items?.forEach((item) => {
    if (item && item.url !== false && item.onClick === undefined)
      item.onClick = ({key}) => navigate(item.url || rootNavUrl + key);
    else delete item?.url;
  });

  return (
    <div title={disabled ? disabledMessage : undefined}>
      {title && (
        <>
          <SidebarDivider color={color} />
          <p className="text-center" style={{color: color, marginTop: "25px"}}>
            {title.toUpperCase()}
          </p>
        </>
      )}
      {items ? <AntMenu theme="dark" selectedKeys={[selectedItem]} items={items} /> : children}
    </div>
  );
};
