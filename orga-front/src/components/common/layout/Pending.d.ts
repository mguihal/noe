export type PendingProps = {
  spinnerLogo?: string;
  className?: string;
  minHeight?: CSSStyleDeclaration["minHeight"];
  animationDelay?: CSSStyleDeclaration["animationDelay"];
};

declare const Pending: ((props: PendingProps) => JSX.Element) & {
  Suspense: (props: {children: JSX.Element} & PendingProps) => JSX.Element;
};
