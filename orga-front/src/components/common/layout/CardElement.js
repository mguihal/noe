import {Card} from "antd";
import React from "react";
import type {CardProps} from "antd";

export const CardElement = ({
  title,
  subtitle,
  icon,
  customButtons,
  children,
  borderless,
  greyedOut,
  style,
  bodyStyle,
  ...otherProps
}: {
  title?: string,
  subtitle?: string,
  icon?: any,
  customButtons?: JSX.Element,
  borderless?: boolean,
  greyedOut?: boolean,
} & CardProps) => (
  <Card
    title={
      title ? (
        <div className="header-space-between" style={{gap: 10, marginTop: 12, marginBottom: 12}}>
          {icon && <span style={{opacity: 0.8}}>{icon}</span>}
          <div style={{flexGrow: 1000}}>{title}</div>
          {customButtons && <div className="containerH buttons-container">{customButtons}</div>}
        </div>
      ) : undefined
    }
    style={{
      flexGrow: 1,
      marginBottom: 26,
      backgroundColor: greyedOut ? "#fafafa" : undefined,
      ...style,
    }}
    bodyStyle={{
      padding: borderless ? 0 : undefined,
      ...bodyStyle,
    }}
    {...otherProps}>
    {subtitle && (borderless ? <div style={{padding: "10px 24px"}}>{subtitle}</div> : subtitle)}
    {children}
  </Card>
);
