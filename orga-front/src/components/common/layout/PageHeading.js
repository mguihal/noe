import {Button} from "antd";
import {navigate} from "@reach/router";
import {ArrowLeftOutlined} from "@ant-design/icons";
import React from "react";

export const PageHeading = ({
  title,
  icon,
  buttonTitle,
  onButtonClick,
  customButtons,
  className,
  backButton = false,
  forceModifButtonActivation = true,
}: {
  title: string,
  icon: any,
  buttonTitle?: string,
  onButtonClick?: any,
  customButtons?: JSX.Element,
  className?: string,
  backButton?: boolean,
  forceModifButtonActivation?: boolean,
}) => (
  <div className={`header-space-between ${className}`}>
    {/*flexGrow is forced to 1000 to give better rendering in mobile view mode. No better solution found up to now.*/}
    <div style={{flexGrow: 1000, paddingBottom: 5}}>
      {backButton && (
        <Button
          type="link"
          onClick={() => navigate(-1)}
          style={{paddingRight: "20pt", paddingLeft: 5, display: "inline"}}>
          <ArrowLeftOutlined style={{fontSize: "20pt"}} />
        </Button>
      )}
      <h1 style={{margin: 0, display: "inline"}}>
        {icon && <span style={{paddingRight: 15, opacity: 0.8}}>{icon}</span>}
        {title}
      </h1>
    </div>
    <div className="containerH buttons-container" style={{flexGrow: 1, alignItems: "center"}}>
      {customButtons}
      {buttonTitle && (
        <Button type="primary" disabled={!forceModifButtonActivation} onClick={onButtonClick}>
          {buttonTitle}
        </Button>
      )}
    </div>
  </div>
);
