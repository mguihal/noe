import {useLocalStorageState} from "../../../helpers/localStorageUtilities";
import {Alert} from "antd";
import React from "react";

export const ClosableAlert = ({localStorageKey, ...props}) => {
  if (!localStorageKey) throw Error("Need key to display ClosableAlert");
  const [open, setOpen] = useLocalStorageState("closable-alert-" + localStorageKey, true);
  return open ? <Alert closable afterClose={() => setOpen(false)} {...props} /> : null;
};
