import React from "react";
import {FacebookOutlined, GitlabOutlined, InstagramOutlined, MailOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";

export const NOESocialIcons = ({style}) => {
  const {t} = useTranslation();
  return (
    <>
      <a
        href="https://gitlab.com/alternatiba/noe/-/blob/master/CONTRIBUTING.md"
        target="_blank"
        rel="noreferrer"
        title={"Gitlab"}>
        <GitlabOutlined style={style} />
      </a>
      <a
        href="https://www.facebook.com/noeappio"
        target="_blank"
        rel="noreferrer"
        title={"Facebook"}>
        <FacebookOutlined style={style} />
      </a>
      <a
        href="https://www.instagram.com/noeappio"
        target="_blank"
        rel="noreferrer"
        title={"Instagram"}>
        <InstagramOutlined style={style} />
      </a>
      <a
        href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=${t(
          "common:mailtoSubjectKnowMoreAboutNOE"
        )}`}
        rel="noreferrer"
        title={"Email"}>
        <MailOutlined style={style} />
      </a>
    </>
  );
};
