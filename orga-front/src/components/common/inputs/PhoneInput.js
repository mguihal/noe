import {formTag} from "./formTag";
import React, {useState} from "react";
import {Input} from "antd";
import type {FormTagProps} from "./formTag";
import type {InputProps} from "antd";
import {t} from "i18next";
import unknownFlag from "../../../app/images/unknownFlag.png";

export const phoneValidator = async (rule, value) => {
  // No phone is allowed, use a {required: true} rule if you want to make it mandatory
  if (!value || value === "") return Promise.resolve();

  if (value[0] !== "+") {
    return Promise.reject(t("common:inputElement.error.preferInternationalPhoneNumber"));
  }

  const {isValidPhoneNumber} = await import("libphonenumber-js");
  try {
    if (isValidPhoneNumber(value)) {
      return Promise.resolve();
    } else {
      return Promise.reject(t("common:inputElement.error.enterAValidPhoneNumber"));
    }
  } catch (e) {
    return Promise.reject(t("common:inputElement.error.enterAValidPhoneNumber"));
  }
};

// Flag icons thanks to the free service https://flagpedia.net/
export const getFlagIcon = (country2Code: string) => {
  const src =
    !country2Code || country2Code.length !== 2
      ? unknownFlag
      : `https://flagcdn.com/16x12/${country2Code.toLowerCase()}.png`;

  return <img src={src} width="16" height="12" />;
};

export const PhoneInputComponent = (props: InputProps) => {
  const [currentCountryCode, setCurrentCountryCode] = useState();

  const getPhoneCountry = async (value): Promise<string | undefined> => {
    const {AsYouType} = await import("libphonenumber-js");
    const asYouType = new AsYouType();
    asYouType.input(value);
    console.log(asYouType.getCountry());

    return asYouType.getCountry();
  };

  return (
    <Input
      bordered={false}
      prefix={getFlagIcon(currentCountryCode)}
      {...props}
      onChange={({target: {value}}) => {
        props.onChange?.(value);
        // onChange should be sync so we put that in the .then()
        getPhoneCountry(value).then(setCurrentCountryCode);
      }}
    />
  );
};

export const PhoneInput = (props: FormTagProps<InputProps>) =>
  formTag(
    {
      autoComplete: "email",
      rules: [...(props.rules || []), {validator: phoneValidator}],
      ...props,
    },
    PhoneInputComponent
  );
