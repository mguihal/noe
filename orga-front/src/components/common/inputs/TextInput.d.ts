import {FormTagProps} from "./formTag";
import type {InputProps} from "antd";
import type {PasswordProps, TextAreaProps} from "antd/es/input";

declare const TextInput: ((props: FormTagProps<InputProps>) => JSX.Element) & {
  Email: (props: FormTagProps<TextAreaProps>) => JSX.Element;
  Area: (props: FormTagProps<TextAreaProps>) => JSX.Element;
  Password: (props: FormTagProps<PasswordProps>) => JSX.Element;
};
