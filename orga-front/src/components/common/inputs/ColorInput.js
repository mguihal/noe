import {ColorPicker, InputNumber, InputNumberProps} from "antd";
import React from "react";
import {formTag, FormTagProps} from "./formTag";
import {ColorPickerProps} from "antd/es/color-picker/ColorPicker";
import {Color} from "antd/es/color-picker";

export const ColorInput = (props: FormTagProps<ColorPickerProps>) =>
  formTag(
    {
      ...props,
      formItemProps: {
        normalize: (value) => {
          console.log(value);
          let hexVal = typeof value === "string" ? value : value?.toHex();
          console.log(hexVal.length, hexVal, hexVal.substring(6, 8));
          if (hexVal.length === 8 && hexVal.substring(6, 8) === "00") return undefined;
          if (hexVal) return "#" + hexVal.substring(0, 6);
        },
      },
    },
    (otherProps) => <ColorPicker allowClear {...otherProps} />
  );
