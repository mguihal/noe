import {TimePicker, TimePickerProps} from "antd";
import {formTag, FormTagProps} from "./formTag";
import React from "react";

export const TimeInput = (props: FormTagProps<TimePickerProps>) =>
  formTag(props, (otherProps) => (
    <TimePicker minuteStep={5} bordered={false} format="HH:mm" {...otherProps} />
  ));
