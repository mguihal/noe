import dayjs from "dayjs";
import {useSelector} from "react-redux";
import {DatePicker} from "antd";
import React from "react";
import {formTag} from "./formTag";
import {currentProjectSelectors} from "../../../features/currentProject";
import type {DateInputProps} from "./DateTimeInput";

const disabledDateBeforeNow = (current) => {
  const now = dayjs();
  return current && current.isBefore(now, "day");
};

const disabledDateOutOfProject = (current, project) => {
  const start = dayjs(project.start),
    end = dayjs(project.end);
  return current && (current.isBefore(start, "day") || current.isAfter(end, "day"));
};

const isADisabledDate = (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => {
  return (current) => {
    return (
      (disableDatesIfOutOfProject ? disabledDateOutOfProject(current, project) : false) ||
      (disableDatesBeforeNow !== false ? disabledDateBeforeNow(current) : false)
    );
  };
};

const setGreyedOutDates =
  (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => (current) => {
    const disabled = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    )(current);
    return (
      <div className={`ant-picker-cell-inner ${disabled ? "disabled-date" : undefined}`}>
        {current.date()}
      </div>
    );
  };

/**
 * Raw date picker input
 */
export function DatePickerInput({
  isRange, //Display range picker or date picker
  blockDisabledDates, // Block dates out of the project
  disableDatesIfOutOfProject, // grey out either only days before today, or also the dates out of the project
  disableDatesBeforeNow,
  ...props
}: DateInputProps) {
  const project = useSelector(currentProjectSelectors.selectProject);

  const newProps = {
    onChange: (value) => {
      // Reset seconds and milliseconds to zero
      if (isRange) {
        value?.forEach((date) => date.set({second: 0, millisecond: 0}));
      } else {
        value?.set({second: 0, millisecond: 0});
      }
      props.onChange(value);
    },
    bordered: false,
    format: dayjs.localeData().longDateFormat("LLL"),
    showTime: {minuteStep: 5},
    popupClassName: "date-picker-panel",
  };

  if (blockDisabledDates) {
    newProps.disabledDate = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  } else {
    newProps.dateRender = setGreyedOutDates(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  }

  return isRange ? (
    <DatePicker.RangePicker {...newProps} {...props} />
  ) : (
    <DatePicker {...newProps} {...props} />
  );
}

/**
 * Basic Datetime input
 */
export const DateTimeInput = (props) =>
  formTag(props, (otherProps) => <DatePickerInput {...otherProps} />);

/**
 * Range Datetime input
 */
DateTimeInput.Range = (props) =>
  formTag(props, (otherProps) => <DatePickerInput isRange {...otherProps} />);
