import {Input} from "antd";
import React from "react";
import {formTag} from "./formTag";
import {EyeInvisibleOutlined, EyeOutlined} from "@ant-design/icons";

/**
 * Basic Text input
 */
export const TextInput = (props) =>
  formTag(props, (otherProps) => <Input bordered={false} {...otherProps} />);

/**
 * Email Text input
 */
TextInput.Email = (props) =>
  formTag(
    {
      ...props,
      rules: [...(props.rules || []), {type: "email"}],
      normalize: (value) =>
        // Set lowercase and exclude all characters not included in \w\d!#$%&’*+-/=?^_`{|}~@
        value ? value.toLowerCase().replace(/[^\w\d!#$%&’*+-/=?^_`{|}~@]/, "") : value,
    },
    (otherProps) => <Input bordered={false} autoComplete={"email"} {...otherProps} />
  );

/**
 * Password Text input
 */
TextInput.Password = (props) =>
  formTag(props, (otherProps) => (
    <Input.Password
      bordered={false}
      iconRender={(visible) => (visible ? <EyeOutlined /> : <EyeInvisibleOutlined />)}
      {...otherProps}
    />
  ));

/**
 * Multiline Text input
 */
TextInput.Area = (props) =>
  formTag(props, (otherProps) => (
    <Input.TextArea
      autoSize={{minRows: 2}}
      // allow to return to the lines by overriding the validation of the web page with "enter"
      onKeyDown={(event) => event.key === "Enter" && event.stopPropagation()}
      bordered={false}
      {...otherProps}
    />
  ));
