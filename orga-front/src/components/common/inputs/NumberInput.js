import {InputNumber, InputNumberProps} from "antd";
import React from "react";
import {formTag, FormTagProps} from "./formTag";

export const NumberInput = (props: FormTagProps<InputNumberProps>) =>
  formTag(props, (otherProps) => <InputNumber bordered={false} keyboard {...otherProps} />);
