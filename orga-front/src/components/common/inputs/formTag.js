import {Form} from "antd";
import React from "react";
import {t} from "i18next";
import {FormTagProps} from "./formTag";

export const formTag = (
  {
    i18nNs,
    icon,
    name,
    label,
    rules,
    tooltip,
    normalize,
    formItemProps,
    inputProps,
    ...otherProps
  }: FormTagProps,
  inputTag
) => {
  if (i18nNs) {
    const keyRoot = `${i18nNs}:schema.${name}`;
    label = t(`${keyRoot}.label`, {defaultValue: label || null});
    otherProps.placeholder = t(`${keyRoot}.placeholder`, {
      defaultValue: otherProps.placeholder || null,
    });
    tooltip = t(`${keyRoot}.tooltip`, {defaultValue: tooltip || null});
  }
  return (
    <Form.Item
      label={
        icon ? (
          <>
            <span style={{marginRight: 8, opacity: 0.8}}>{icon}</span> {label}
          </>
        ) : (
          label
        )
      }
      name={name}
      tooltip={tooltip}
      rules={rules}
      normalize={normalize}
      {...formItemProps}>
      {inputTag({...otherProps, ...inputProps})}
    </Form.Item>
  );
};
