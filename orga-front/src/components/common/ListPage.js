import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, message, Modal, Table, Tooltip, Upload} from "antd";
import {
  EditOutlined,
  FileExcelOutlined,
  ImportOutlined,
  InboxOutlined,
  SearchOutlined,
  SelectOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {BetaTag} from "./layout/BetaTag";
import {CardElement} from "./layout/CardElement";
import {InputElement} from "./InputElement";

import {
  getFullHeightWithMargins,
  navbarHeight,
  useDebounce,
  useSavedPagination,
  useWindowDimensions,
} from "../../helpers/viewUtilities";
import {paginationPageSizes, viewActions, viewSelectors} from "../../features/view";
import {DeleteButton} from "./layout/DeleteButton";
import {
  addKeyToItemsOfList,
  useCopyColumns,
  useSearchInColumns,
} from "../../helpers/tableUtilities";
import {listSorter, searchInObjectsList, useDrawer} from "../../helpers/listUtilities";
import {navigate, useMatch} from "@reach/router";
import {fetchWithMessages} from "../../helpers/reduxUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";
import {FormElement} from "./FormElement";
import {Trans, useTranslation} from "react-i18next";
import {TableElement} from "./TableElement";
import {CsvExportButton} from "../../helpers/filesUtilities";
import Search from "antd/es/input/Search";
import {ElementSelectionModal} from "./layout/ElementSelectionModal";
import {PageHeading} from "./layout/PageHeading";

const useEntitySchema = ({filterSchema, forceEndpoint, forceSchema}) => {
  const [entitySchema, setEntitySchema] = useState([]);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const {matchedEndpoint} = useMatch("/:envId/:matchedEndpoint");
  const endpoint = forceEndpoint || matchedEndpoint;

  useEffect(() => {
    if (forceSchema) {
      setEntitySchema(forceSchema);
    } else {
      fetchWithMessages(`projects/${currentProject._id}/${endpoint}/schema`, {
        method: "GET",
      }).then((schema) => setEntitySchema(schema.filter(filterSchema || (() => true))));
    }
  }, []);

  return [entitySchema, endpoint];
};

const GroupEditionButton = ({selectedRowKeys}) => {
  const {t} = useTranslation();
  const [open, setOpen] = useState(false);
  const {useAI, usePlaces, useTeams} = useSelector(currentProjectSelectors.selectProject);
  const [selectedFields, setSelectedFields] = useState([]);
  const [entitySchema, endpoint] = useEntitySchema({
    filterSchema: (field) => {
      if (
        (!useAI && field.useAI) ||
        (!usePlaces && field.usePlaces) ||
        (!useTeams && field.useTeams) ||
        field.noGroupEditing
      ) {
        return false;
      } else {
        return true;
      }
    },
  });

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setSelectedFields(selectedRowObject);
    },
  };

  return (
    <>
      <Tooltip title={t("common:listPage.groupedEdition.tooltip")}>
        <Button
          icon={<EditOutlined />}
          type="link"
          disabled={selectedRowKeys.length === 0}
          onClick={() => setOpen(true)}
        />
      </Tooltip>

      <ElementSelectionModal
        title={t("common:listPage.groupedEdition.modalTitle")}
        subtitle={t("common:listPage.groupedEdition.modalSubtitle")}
        open={open}
        onOk={() => {
          selectedFields.length > 0
            ? navigate(`${endpoint}/groupedit`, {
                state: {
                  groupEditing: {
                    fieldsToUpdate: selectedFields,
                    elements: selectedRowKeys,
                  },
                },
              })
            : message.error(t("common:listPage.groupedEdition.selectAtLeastOne"));
        }}
        onCancel={() => setOpen(false)}
        rowSelection={rowSelection}
        selectedRowKeys={selectedFields}
        rowKey="key"
        size="small"
        showHeader={false}
        setSelectedRowKeys={setSelectedFields}
        columns={[
          {
            dataIndex: "label",
            defaultSortOrder: "ascend",
            sorter: (a, b) => listSorter.text(a.label, b.label),
          },
        ]}
        dataSource={entitySchema}
      />
    </>
  );
};

export const CsvGroupImportButton = ({
  title,
  elementsName,
  forceEndpoint,
  forceSchema,
  keepUnknownEntries,
  onOk,
}) => {
  const {t} = useTranslation();
  const [modalOpen, setModalOpen] = useState(false);
  const [entitiesToImport, setEntitiesToImport] = useState([]);
  const [errors, setErrors] = useState([]);
  const [columnsFromImport, setColumnsFromImport] = useState([]);
  const [entitySchema, endpoint] = useEntitySchema({forceEndpoint, forceSchema});
  const allowedFields = entitySchema.map((field) => field.key);
  const schemaColumns = entitySchema.map((field) => ({dataIndex: field.key, title: field.label}));

  const handleImport = () => {
    onOk(entitiesToImport, allowedFields);
    setEntitiesToImport([]);
    setModalOpen(false);
  };

  const parseJsonFromCsvEntry = (element) => {
    if ((typeof element === "string" && element[0] === "{") || element[0] === "[") {
      element = element.replace(/[“”]/g, '"').replace(/[‘’]/g, "'");
      return JSON.parse(element);
    } else {
      return element;
    }
  };

  const parseCsvFile = (file) => {
    let reader = new FileReader();
    setEntitiesToImport([]);
    setErrors([]);
    reader.onload = async function (e) {
      // Import papaparse dynamically
      const {parse} = await import(/* webpackPrefetch: true */ "papaparse");

      parse(e.target.result?.trim(), {
        header: true,
        dynamicTyping: true,
        complete: (result) => {
          const rowsWithErrors = result.errors.map((error) => error.row);

          const additionalParsingErrors = [];
          const cleanAndParseRow = (row, index) => {
            if (rowsWithErrors.includes(index)) return;
            try {
              return Object.fromEntries(
                Object.entries(row)
                  // If the value is not valid, remove it
                  .filter(
                    ([key, val]) =>
                      (keepUnknownEntries || allowedFields.includes(key)) &&
                      val !== "" &&
                      val !== null
                  )
                  // If the string is JSON, then parse it
                  .map(([key, val]) => [key, parseJsonFromCsvEntry(val)])
              );
            } catch (e) {
              additionalParsingErrors.push({row: index, type: e.name, message: e.message});
            }
          };

          setColumnsFromImport([
            ...schemaColumns,
            ...result.meta.fields
              .filter((field) => !allowedFields.includes(field))
              .map((field) => ({dataIndex: field, title: field})),
          ]);
          setEntitiesToImport(result.data.map(cleanAndParseRow).filter((row) => !!row));
          setErrors([...result.errors, ...additionalParsingErrors]);
        },
      });
    };
    reader.readAsText(file);
  };

  const ExampleImportFileButton = () => (
    <CsvExportButton
      getExportName={() => t("common:csvImportButton.filename", {elementsName})}
      dataExportFunction={() => [
        entitySchema.reduce((acc, field) => {
          acc[field.key] = t("common:csvImportButton.rowPlaceholder", {
            fieldLabel: field.label.toLowerCase(),
          });
          return acc;
        }, {}),
      ]}
      icon={<FileExcelOutlined />}
      type="primary"
      style={{marginBottom: 15}}>
      {t("common:csvImportButton.downloadTheExampleFile")}
    </CsvExportButton>
  );

  return (
    <>
      <Tooltip title={t("common:csvImportButton.importFromCsvFile")}>
        <Button type="link" icon={<ImportOutlined />} onClick={() => setModalOpen(true)}>
          {title}
        </Button>
      </Tooltip>

      <Modal
        centered
        title={t("common:csvImportButton.importInCsv")}
        open={modalOpen}
        onCancel={() => setModalOpen(false)}
        onOk={handleImport}
        width="98%"
        okText={t("common:csvImportButton.importElements")}>
        <Alert
          type="warning"
          icon={<BetaTag />}
          message={t("common:csvImportButton.experimentalFeatureAlert.message")}
          description={t("common:csvImportButton.experimentalFeatureAlert.description")}
          showIcon
          style={{marginBottom: 26}}
        />

        {errors.length > 0 && (
          <Alert
            type="error"
            message={t("common:csvImportButton.errorsFound.message")}
            description={
              <>
                <p>{t("common:csvImportButton.errorsFound.description")}</p>
                <ul>
                  {errors.map((error) => (
                    <li>
                      <strong>
                        Ligne {error.row + 1} - {error.type} :
                      </strong>{" "}
                      {error.message}
                    </li>
                  ))}
                </ul>
              </>
            }
            showIcon
            style={{marginBottom: 26}}
          />
        )}
        <div style={{marginBottom: 26}}>
          <Upload.Dragger
            beforeUpload={(file) => {
              parseCsvFile(file);
              return false;
            }}
            maxCount={1}
            accept="text/csv"
            showUploadList={true}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">{t("common:csvImportButton.clickOrDragFile")}</p>
          </Upload.Dragger>
        </div>
        {entitiesToImport.length > 0 ? (
          <TableElement.WithTitle
            title={t("common:csvImportButton.importPreviewTitle")}
            dataSource={addKeyToItemsOfList(entitiesToImport)}
            columns={columnsFromImport}
            rowKey="key"
            scroll={{
              x: (columnsFromImport.length - 1) * 160,
              y: window.innerHeight - 500,
            }}
            showHeader
          />
        ) : (
          <Alert
            type="info"
            message={t("common:csvImportButton.needHelpAlert.message")}
            description={
              <>
                <Trans
                  i18nKey="csvImportButton.needHelpAlert.description"
                  ns="common"
                  values={{
                    endpoint,
                    elementsName: elementsName.toLowerCase(),
                  }}
                  components={{
                    tt: <tt />,
                    exampleImportFileButton: <ExampleImportFileButton />,
                  }}
                />
              </>
            }
            style={{marginBottom: 26}}
          />
        )}
      </Modal>
    </>
  );
};

export const useSearchBar = () => {
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const [showSearch, setShowSearch] = useState(false);
  const search = useSelector(viewSelectors.selectSearch);

  const debouncedChangeSearch = useDebounce((searchValue) =>
    dispatch(viewActions.changeSearch(searchValue))
  );

  return isMobileView && !showSearch ? (
    <Button icon={<SearchOutlined />} onClick={() => setShowSearch(true)} />
  ) : (
    <Search
      defaultValue={search}
      style={{width: 250}}
      allowClear
      autoFocus={isMobileView}
      onChange={(event) => debouncedChangeSearch(event.target.value)}
      placeholder={t("common:search")}
    />
  );
};

export function ListPage({
  i18nNs,
  title,
  icon,
  subtitle,
  customButtons,
  multipleActionsButtons: MultipleActionsButtons,
  buttonTitle,
  settingsDrawerContent,

  header = true,
  fullPage = true,

  elementsActions,
  columns: rawColumns,
  dataSource,

  creatable = true,
  editable = true,
  deletable = true,
  clickable,
  navigateFn,
  noActionIcons = false,
  groupEditable = false,
  groupImportable = false,
  searchInFields = [],

  rowClassName,
  expandable,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination();
  const {isMobileView} = useWindowDimensions();
  const sorting = useSelector(viewSelectors.selectSorting);
  const search = useSelector(viewSelectors.selectSearch);
  const [displaySize, setDisplaySize] = useLocalStorageState(
    "listDisplaySize",
    isMobileView ? "small" : "middle"
  );
  const [setSettingsDrawerOpen, SettingsDrawer] = useDrawer({
    width: isMobileView ? undefined : "700px",
  });

  // If an i18n namespace is given, get the title and button name from there
  if (i18nNs) {
    title = title || t(`${i18nNs}:label`, {count: Infinity});
    buttonTitle = creatable && (buttonTitle || t(`${i18nNs}:label`, {context: "create"}));
    if (buttonTitle && isMobileView) buttonTitle = buttonTitle.split(" ")[0];
  }

  const filteredDataSource = searchInObjectsList(search, dataSource, searchInFields);
  const searchBar = useSearchBar();

  /*********** Multiple selection **************************/

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [enableMultipleSelection, setEnableMultipleSelection] = useState(false);

  const rowSelection = enableMultipleSelection && {
    selectedRowKeys,
    onChange: (newSelectedRowKeys) => setSelectedRowKeys(newSelectedRowKeys),
  };

  const onBatchDelete = async () => {
    const notDeleted = [];
    for (const id of selectedRowKeys) {
      try {
        await dispatch(elementsActions.remove(id));
      } catch {
        notDeleted.push(id);
      }
    }
    setSelectedRowKeys(notDeleted);
  };

  /*********** Sorting / Filtering memorization ************/

  let {columns, handleDisplayConfigChange, currentDataSource} = useCopyColumns(
    filteredDataSource,
    rawColumns,
    (pagination, filters, sorter, extra) => {
      dispatch(viewActions.changeSorting({filteredInfo: filters, sortedInfo: sorter}));
    }
  );

  const clearDisplayConfig = () => {
    dispatch(viewActions.changeSorting({}));
  };

  columns = columns.map((column) => {
    const savedSortingOrder =
      sorting?.sortedInfo?.column?.dataIndex === column.dataIndex && sorting?.sortedInfo?.order;
    return {
      // The column data
      ...column,
      // Add the saved sorting order if found
      sortOrder: savedSortingOrder || column.sortOrder,
      filteredValue: sorting?.filteredInfo?.[column.dataIndex],
    };
  });

  columns = useSearchInColumns(columns);

  if (!noActionIcons && (editable || deletable)) {
    columns.push({
      key: "action",
      width: 9 + (editable ? 32 + 9 : 0) + (deletable ? 32 + 9 : 0),
      render: (text, record) => (
        <div
          className="containerH buttons-container"
          style={{justifyContent: "flex-end", flexWrap: "nowrap"}}>
          {editable && (
            <Button
              style={{flexGrow: 0}}
              type="link"
              icon={<EditOutlined />}
              onClick={() => navigateFn(record.slug || record._id)}
            />
          )}
          {deletable && (
            <DeleteButton onConfirm={() => dispatch(elementsActions.remove(record._id))} />
          )}
        </div>
      ),
    });
  }

  const resize = () => {
    if (header) {
      try {
        const heightOffset =
          getFullHeightWithMargins(document.querySelector(".ant-table-header")) + // table header row
          getFullHeightWithMargins(document.querySelector(".subtitle")) + // subtitle
          getFullHeightWithMargins(document.querySelector(".ant-table-pagination")) + // table footer row
          getFullHeightWithMargins(document.querySelector(".list-page-header")) + // page title header
          navbarHeight() + // The height of the navbar if we're in mobile mode
          3; // Security margin
        const tableBody = document.querySelector(".ant-table-body");
        tableBody.style.maxHeight = tableBody.style.minHeight =
          "calc(100vh - " + heightOffset + "px)";
      } catch {
        //Do nothing
      }
    }
  };
  useEffect(resize);
  useEffect(() => setTimeout(resize, 3));

  return (
    <div
      className={`containerV ${fullPage ? "full-width-content" : ""}`}
      style={{background: "#fafafa"}}>
      {header && (
        <PageHeading
          className="list-page-header"
          title={title}
          icon={icon}
          customButtons={
            <>
              {!enableMultipleSelection && customButtons}

              {sorting?.sortedInfo?.column && ( // There is a sorter activated
                <Button type="link" onClick={clearDisplayConfig}>
                  {t("common:listPage.clearSorting")}
                </Button>
              )}

              {(deletable || MultipleActionsButtons || groupEditable) &&
                (enableMultipleSelection ? (
                  <>
                    <span style={{marginRight: 8}}>{selectedRowKeys.length} sélectionnés.</span>

                    {MultipleActionsButtons && (
                      <MultipleActionsButtons
                        selectedRowKeys={selectedRowKeys}
                        setSelectedRowKeys={setSelectedRowKeys}
                      />
                    )}

                    {groupEditable && <GroupEditionButton selectedRowKeys={selectedRowKeys} />}

                    {deletable && (
                      <DeleteButton
                        onConfirm={onBatchDelete}
                        disabled={selectedRowKeys.length === 0}
                      />
                    )}

                    <Button
                      type="link"
                      danger
                      onClick={() => {
                        setEnableMultipleSelection(false);
                        setSelectedRowKeys([]);
                      }}>
                      Annuler
                    </Button>
                  </>
                ) : (
                  <Tooltip title={t("common:listPage.groupedSelection.tooltip")}>
                    <Button
                      type="link"
                      style={{marginRight: 12}}
                      icon={<SelectOutlined />}
                      onClick={() => setEnableMultipleSelection(true)}
                    />
                  </Tooltip>
                ))}

              {groupImportable && (
                <CsvGroupImportButton
                  onOk={(entitiesToImport) =>
                    entitiesToImport.forEach((el) =>
                      dispatch(elementsActions.persist({_id: "new", ...el}))
                    )
                  }
                  elementsName={title}
                />
              )}

              {searchInFields?.length > 0 && searchBar}
            </>
          }
          buttonTitle={buttonTitle}
          onButtonClick={() => navigateFn("new")}
        />
      )}
      {subtitle && <div className="subtitle with-margins">{subtitle}</div>}
      <div>
        <div style={{position: "fixed", right: 10, zIndex: 3}}>
          <Tooltip title={t("common:listPage.displaySettings.tooltip")} placement="topRight">
            <Button
              icon={<SettingOutlined />}
              style={{transform: "translateY(8px)"}}
              className="shadow"
              onClick={() => setSettingsDrawerOpen(true)}
            />
          </Tooltip>
        </div>

        <Table
          scroll={{
            y: "100vh",
            x: (columns.length - 1) * 160 + 70,
          }}
          columns={columns}
          rowClassName={rowClassName || ((record) => record.updatedAt && "ant-table-row-updated")}
          pagination={{
            position: ["bottomCenter"],
            current: pagination.current,
            pageSize: pagination.pageSize,
            pageSizeOptions: paginationPageSizes,
            total: currentDataSource.length,
            showSizeChanger: true,
            showTotal: (total, range) =>
              t("common:listPage.xtoYOnZElements", {
                from: range[0],
                to: range[1] > total ? total : range[1],
                total,
                elementsLabel: t(`${i18nNs}:label`, {count: total}),
                context: isMobileView ? "short" : "",
              }).toLowerCase(),
            onChange: setPagination,
          }}
          onRow={
            clickable !== false &&
            navigateFn &&
            ((record) => ({onDoubleClick: () => navigateFn(record.slug || record._id)}))
          }
          expandable={expandable}
          size={displaySize}
          rowKey="_id"
          dataSource={filteredDataSource}
          rowSelection={rowSelection}
          onChange={handleDisplayConfigChange}
        />
      </div>

      <SettingsDrawer>
        <h3 style={{marginBottom: 20}}>{t("common:settings")}</h3>
        {settingsDrawerContent}
        <CardElement title={t("common:listPage.settings.displayDensity.cardTitle")}>
          <FormElement>
            <InputElement.Select
              label={t("common:listPage.settings.displayDensity.label")}
              onChange={setDisplaySize}
              defaultValue={displaySize}
              options={[
                {value: "small", label: t("common:listPage.settings.displayDensity.options.small")},
                {
                  value: "middle",
                  label: t("common:listPage.settings.displayDensity.options.comfort"),
                },
              ]}
            />
          </FormElement>
        </CardElement>
      </SettingsDrawer>
    </div>
  );
}
