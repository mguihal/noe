import React from "react";
import "react-quill/dist/quill.snow.css";

export const TextDisplayer = ({value}) => (
  <div className="ql-snow">
    <div className="ql-editor ql-editor-readonly" dangerouslySetInnerHTML={{__html: value}} />
  </div>
);

export default TextDisplayer;
