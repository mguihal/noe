import {Button, FormItemProps, Select, Tooltip} from "antd";
import React from "react";
import {useSelector} from "react-redux";
import {navigate} from "@reach/router";
import {EyeOutlined} from "@ant-design/icons";
import {listSorter, normalize, removeDuplicates} from "../../helpers/listUtilities";
import {useTranslation} from "react-i18next";
import TextEditor from "./TextEditor";
import {Pending} from "./layout/Pending";
import {formTag} from "./inputs/formTag";
export const InputElement = () => null;

type InputElementProps = {
  i18nNs?: string,
  name: string,
  label?: string,
  rules?: string,
  tooltip?: string,
  formItemProps?: FormItemProps,
};

/**
 * TEXT COMPONENTS
 */

InputElement.Editor = (props: InputElementProps) => (
  // Need to add the suspense around the form tag, because if in between the form data doesn't pass and it creates bugs
  <Pending.Suspense animationDelay={"300ms"}>
    {formTag(props, (otherProps) => (
      <TextEditor {...otherProps} />
    ))}
  </Pending.Suspense>
);

const TagsSelect = ({elementsSelectors, ...props}: InputElementProps) => {
  const elements = useSelector(elementsSelectors.selectList);
  const tagsOptions = removeDuplicates(
    elements?.reduce((acc, a) => (a[props.name] ? [...a[props.name], ...acc] : acc), [])
  )
    .sort(listSorter.text)
    .map((string) => ({
      key: string,
      value: string,
      label: string,
    }));

  return <InputElement.Select mode="tags" options={tagsOptions} {...props} />;
};
InputElement.TagsSelect = TagsSelect;

/**
 * CHOICE COMPONENTS
 */
InputElement.Select = (props: InputElementProps) =>
  formTag(props, (otherProps) => (
    <Select
      bordered={false}
      filterOption={(inputValue, option) =>
        normalize(option?.label).indexOf(normalize(inputValue)) !== -1
      }
      // prevent selection with Enter to trigger full page validation
      onKeyDown={(event) => event.key === "Enter" && event.stopPropagation()}
      {...otherProps}>
      {props.children}
    </Select>
  ));

/**
 * WRAPPER COMPONENTS
 */

InputElement.Custom = (props: InputElementProps) => formTag(props, (otherProps) => props.children);

const WithEntityLinks = ({endpoint, entity, children, createButtonText, setShowNewEntityModal}) => {
  const {t} = useTranslation();
  return (
    <div>
      <div
        className="containerH buttons-container"
        style={{alignItems: "start", flexWrap: "nowrap", marginBottom: 24}}>
        {children}
        {entity && (
          <Tooltip title={t("common:inputElement.accessToSelectedElement")} placement="topRight">
            <Button
              style={{marginTop: 35, flexGrow: 0, paddingRight: 0, marginRight: 0}}
              type="link"
              onClick={() => navigate(`../${endpoint}/${entity?._id || entity}`)}
              icon={<EyeOutlined />}
            />
          </Tooltip>
        )}
        {createButtonText && setShowNewEntityModal && (
          <Tooltip title={createButtonText} placement="topRight">
            <Button
              type="link"
              onClick={() => setShowNewEntityModal(true)}
              style={{
                marginTop: 35,
                flexGrow: 0,
                paddingLeft: 5,
                paddingRight: 0,
                marginRight: 0,
              }}>
              {createButtonText.split(" ")[0]}
            </Button>
          </Tooltip>
        )}
      </div>
    </div>
  );
};
InputElement.WithEntityLinks = WithEntityLinks;
