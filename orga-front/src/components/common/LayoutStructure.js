import React, {useState} from "react";
import {Alert, Avatar, Badge, Button, Drawer, Layout, Result, Tag} from "antd";
import {MyAccount} from "../myAccount/MyAccount";
import {
  CloseOutlined,
  DownloadOutlined,
  EditOutlined,
  LoginOutlined,
  MenuOutlined,
  SettingOutlined,
  UserOutlined,
  WarningOutlined,
  WifiOutlined,
} from "@ant-design/icons";
import {arrivalSpinnerLogo, defaultBackgroundClassName} from "../../app/configuration";
import {useWindowDimensions} from "../../helpers/viewUtilities";
import {ErrorBoundary} from "@sentry/react";
import {OFFLINE_MODE} from "../../helpers/offlineModeUtilities";
import {useOnlineStatus} from "../../helpers/onlineStatusCheckerUtilities";
import {useTranslation} from "react-i18next";
import {personName} from "../../helpers/utilities";
import {MenuLayout, SidebarDivider} from "./layout/Menu";
import {Pending} from "./layout/Pending";
import {NOESocialIcons} from "./layout/NOESocialIcons";
import {useLocation} from "@reach/router";

const {Sider, Header, Content} = Layout;

/**
 * SIDEBAR
 */

const Sidebar = ({title, children, collapsed, ribbon}) => {
  const {isMobileView} = useWindowDimensions();

  const sider = (
    <Sider
      collapsed={collapsed}
      breakpoint="md"
      style={{overflow: "auto", height: "100vh", background: "var(--noe-bg)"}}
      width={210}>
      <div className="sidebar-container">
        <h3
          className="ant-typography text-white text-center"
          style={{
            padding: "5pt",
            paddingTop: "15pt",
            flexGrow: 0,
          }}>
          {title}
        </h3>
        {children}
      </div>
    </Sider>
  );
  return isMobileView ? null : (
    <div style={{position: "sticky", top: 0, height: "100vh", zIndex: 100}}>
      {ribbon ? (
        <Badge.Ribbon text={ribbon} color="#6cdac5">
          {sider}
        </Badge.Ribbon>
      ) : (
        sider
      )}
    </div>
  );
};

const SidebarBannerText = ({displayCondition, children, className}) => (
  <div
    className={`text-white d-flex justify-content-center align-items-center ${className}`}
    style={{
      minHeight: displayCondition ? 25 : 0,
      maxHeight: displayCondition ? 25 : 0,
      opacity: displayCondition ? 1 : 0,
      overflow: "hidden",
      transition: "all 0.8s ease-in-out",
    }}>
    <strong>{children}</strong>
  </div>
);

const ProfileSideBarMenuItem = ({user, setShowProfile, collapsedSidebar, isMobileView}) => {
  const {t} = useTranslation();

  const collapsedName = !isMobileView && collapsedSidebar;
  return user?._id ? (
    <div
      onClick={() => setShowProfile(true)}
      style={{
        cursor: "pointer",
        paddingTop: 7,
        paddingBottom: 10,
        paddingLeft: !collapsedName && 16,
        textAlign: collapsedName && "center",
        opacity: 0.9,
        overflowX: "hidden",
        whiteSpace: "nowrap",
      }}>
      <Badge
        style={{marginTop: 27, marginRight: 2}}
        count={
          <SettingOutlined
            style={{backgroundColor: "white", borderRadius: "50%", padding: 2, fontSize: 12}}
          />
        }>
        <Avatar
          style={{display: "inline-block", outline: "2px solid white"}}
          className="bg-noe-gradient"
          icon={<UserOutlined />}
        />
      </Badge>

      {!collapsedName && (
        <div style={{display: "inline-block", fontWeight: "bold", color: "white", paddingLeft: 14}}>
          {personName(user)}
        </div>
      )}
    </div>
  ) : (
    <MenuLayout
      items={[
        {label: t("common:login"), key: "login", icon: <LoginOutlined />},
        {label: t("common:createAccount"), key: "signup", icon: <EditOutlined />},
      ]}
    />
  );
};

/**
 * MOBILE NAVBAR
 */

const Navbar = ({title, children, displayButtonBadge, ribbon}) => {
  const [open, setOpen] = useState(false);

  const online = useOnlineStatus();

  const IconContainer = ({className, style, children}) => (
    <div
      className={className}
      style={{
        borderRadius: 50,
        border: "1px solid var(--noe-bg)",
        minHeight: 32,
        minWidth: 32,
        marginBottom: 3,
        marginRight: 8,
        paddingLeft: 6,
        paddingTop: 6,
        ...style,
      }}>
      {children}
    </div>
  );

  const header = (
    <Header
      className="containerH mobile-navbar-container"
      style={{backgroundColor: "var(--noe-bg)"}}>
      <h4
        style={{
          fontSize: "18px",
          color: "white",
          display: "flex",
          alignItems: "baseline",
          whiteSpace: "wrap",
          margin: "auto",
          paddingTop: 5,
          fontWeight: "bold",
        }}>
        {OFFLINE_MODE && ( // Offline mode activated icon
          <IconContainer className="bg-info" style={{zIndex: 1}}>
            <DownloadOutlined />
          </IconContainer>
        )}
        {OFFLINE_MODE &&
          online && ( // Available internet connection while in offline mode
            <IconContainer className="bg-success" style={{marginLeft: -20}}>
              <WifiOutlined />
            </IconContainer>
          )}

        {process.env.REACT_APP_MODE === "DEMO" && <Tag color="#ff4d4f">DÉMO</Tag>}

        {title}
      </h4>
    </Header>
  );

  return (
    <>
      <Badge
        className="navbar-menu-button"
        dot
        count={displayButtonBadge && !open ? 1 : 0}
        style={{top: 5, right: 5, width: 10, height: 10}}>
        <Button
          type="primary"
          size="large"
          icon={open ? <CloseOutlined /> : <MenuOutlined />}
          onClick={() => setOpen(!open)}
          // Make the button yellow if no connection while not in offline mode
          className={"shadow" + (online || OFFLINE_MODE ? "" : " warning-button")}
        />
      </Badge>
      <div style={{overflow: "hidden"}}>
        {ribbon ? (
          <Badge.Ribbon text={<span style={{marginRight: 5}}>{ribbon}</span>} color="#6cdac5">
            {header}
          </Badge.Ribbon>
        ) : (
          header
        )}
      </div>
      <Drawer
        width={210}
        rootClassName="mobile-sidebar-drawer"
        placement="left"
        zIndex={99}
        onClose={() => setOpen(false)}
        open={open}
        headerStyle={{background: "var(--noe-bg)", borderRadius: 0}}
        bodyStyle={{
          background: "var(--noe-bg)",
          padding: 0,
        }}>
        <div className="sidebar-container" onClick={() => setOpen(false)}>
          {children}
        </div>
      </Drawer>
    </>
  );
};

/**
 * GLOBAL LAYOUT STRUCTURE
 */

export function LayoutStructure({
  title,
  ribbon,
  menu,
  fadeIn,
  showSocialIcons = true,
  children,
  profileUser,
  displayButtonBadge,
  collapsedSidebar,
}) {
  const {t} = useTranslation();
  const online = useOnlineStatus();
  const {isMobileView} = useWindowDimensions();

  // Profile modal state
  const [showProfile, setShowProfile] = useState(false);

  menu = (
    <>
      {/* Online/offline small banner */}
      <div
        style={{
          position: "sticky",
          zIndex: 1000,
          top: 0,
          flexGrow: 0,
          flexShrink: 0,
          marginBottom: 5,
          display: "flex",
          flexDirection: "column",
          gap: 3,
        }}>
        {/* Offline mode banners */}
        <SidebarBannerText displayCondition={OFFLINE_MODE} className={"bg-info"}>
          <DownloadOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.offlineModeActivated")}`}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={online && OFFLINE_MODE} className={"bg-success"}>
          <WifiOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.connectionAvailable")}`}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={!online && !OFFLINE_MODE} className={"bg-warning"}>
          <WarningOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.youAreOffline")}`}
        </SidebarBannerText>

        {/* Demo mode banner */}
        <SidebarBannerText
          className={"bg-danger"}
          displayCondition={process.env.REACT_APP_MODE === "DEMO"}>
          {t("common:demoMode")}
        </SidebarBannerText>
      </div>

      <div className="sidebar-top">
        {menu.top}

        {showSocialIcons && (
          <>
            <SidebarDivider color="rgba(255,255,255,0.9)" />
            <div className="social-icons-container" style={{textAlign: "center", marginTop: 30}}>
              <NOESocialIcons style={{fontSize: 16, margin: 8}} />
            </div>
          </>
        )}
      </div>

      <div className="sidebar-footer" style={{marginTop: "15px"}}>
        {menu.footer}
        <ProfileSideBarMenuItem
          user={profileUser}
          setShowProfile={setShowProfile}
          collapsedSidebar={collapsedSidebar}
          isMobileView={isMobileView}
        />
      </div>
    </>
  );
  // Check if there is a title (means that the project is loaded). If yes, then display the layout, else just wait with loading screen
  return title ? (
    <>
      <Layout>
        {/*The overlay div that makes a great transition when we open the app*/}
        {fadeIn && (
          <div
            className={`${defaultBackgroundClassName} fade-out`}
            style={{
              position: "fixed",
              width: "100%",
              height: "100%",
              zIndex: 10000,
            }}
          />
        )}

        {isMobileView && (
          <Navbar displayButtonBadge={displayButtonBadge} title={title} ribbon={ribbon}>
            {menu}
          </Navbar>
        )}
        {/* Page content: sidebar and main content */}
        <Layout style={{background: "white"}}>
          <Sidebar
            collapsed={collapsedSidebar}
            title={
              collapsedSidebar
                ? title
                    ?.split(" ")
                    .map((word) => word.charAt(0))
                    .join("")
                : title
            }
            ribbon={ribbon}>
            {menu}
          </Sidebar>

          <CustomErrorBoundary>
            <Content className="page-content">
              {OFFLINE_MODE && (
                <div className="full-width-content">
                  <Alert
                    closable
                    type="info"
                    banner
                    message={t("common:offlineMode.warningIncompleteData")}
                  />
                </div>
              )}
              {children}
            </Content>
          </CustomErrorBoundary>
        </Layout>

        <MyAccount openState={[showProfile, setShowProfile]} />
      </Layout>
    </>
  ) : (
    <Pending spinnerLogo={arrivalSpinnerLogo} className={defaultBackgroundClassName} />
  );
}

/**
 * ERROR BOUNDARY
 */

export const CustomErrorBoundary = ({children, onError, extra}) => {
  const {t} = useTranslation();
  return (
    <ErrorBoundary
      key={window.location.pathname}
      onError={onError}
      fallback={() => (
        <Result
          style={{margin: "auto"}}
          status="404"
          title={t("common:errorBoundary.title")}
          subTitle={
            <>
              <p>{t("common:errorBoundary.subTitle.helpUs")}</p>

              <p>
                <a
                  href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=Bug%20NOÉ%20(${window.location.href})`}
                  rel="noreferrer">
                  {t("common:errorBoundary.subTitle.sendEmail")}
                </a>
              </p>
              <p>
                <a href="https://m.me/noeappio" target="_blank" rel="noreferrer">
                  {t("common:errorBoundary.subTitle.socialNetworks")}
                </a>
              </p>
              <p>{t("common:errorBoundary.subTitle.takesOneMinute")}</p>
              <p>{t("common:thanks")} 🥰</p>
            </>
          }
          extra={extra}
        />
      )}>
      {children}
    </ErrorBoundary>
  );
};
