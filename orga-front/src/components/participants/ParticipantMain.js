import React, {lazy, Suspense, useEffect, useState} from "react";
import {message} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {TabsPage} from "../common/TabsPage";
import ParticipantList from "./ParticipantList";
import {projectsActions, projectsSelectors} from "../../features/projects";
import dayjs from "dayjs";
import {CsvExportButton} from "../../helpers/filesUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {cleanAnswer} from "../../helpers/tableUtilities";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {listRenderer, removeDuplicates, useLoadList} from "../../helpers/listUtilities";
import {personName} from "../../helpers/utilities";
import {displayLoadingMessage, killLoadingMessage} from "../../helpers/reduxUtilities";
import {CsvGroupImportButton, useSearchBar} from "../common/ListPage";
import {useTranslation} from "react-i18next";
import {BetaTag} from "../common/layout/BetaTag";
import {useWindowDimensions} from "../../helpers/viewUtilities";
import {TeamOutlined} from "@ant-design/icons";
import {getSessionsPlanningForRegistration} from "./utilsParticipants";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
import {stewardsActions} from "../../features/stewards";

const ParticipantDashboard = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./ParticipantDashboard")
);

function ParticipantMain({envId, navigate}) {
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const dispatch = useDispatch();
  const [displayUnbookedUsers, setDisplayUnbookedUsers] = useState(false);
  const [advancedMode, setAdvancedMode] = useState(false);
  // We need to destructure it so we can then modify it in the getRegistrationMetadata function
  const project = useSelector(projectsSelectors.selectEditing);
  const registrationsWithMetadata = useSelector(registrationsSelectors.selectListWithMetadata);
  const allSlots = useSelector(sessionsSelectors.selectSlotsList);
  const teams = useSelector(teamsSelectors.selectList);

  useLoadList(() => {
    dispatch(registrationsActions.loadList()).then(() => {
      // Load registrations first, because it's critical, then the rest can be loaded
      dispatch(sessionsActions.loadList());
      dispatch(teamsActions.loadList());
      dispatch(stewardsActions.loadList());
    });
  });

  const searchBar = useSearchBar();

  const handleRegistrationsGroupImport = (entitiesToImport, allowedFields) => {
    const errors = [];

    // Prepare the imported registrations
    const preparedRegistrations = entitiesToImport
      .map(({firstName, lastName, email, arrivalDateTime, departureDateTime, ...row}, index) => {
        // Dispatch the values to registration data or to form data based on if it i allowed or not
        const registrationData = {};
        const formData = {};
        Object.keys(row).forEach((key) => {
          if (allowedFields.includes(key)) registrationData[key] = row[key];
          else formData[key] = row[key];
        });

        // User must be defined
        if (!firstName || !lastName || !email) {
          errors.push(
            `La ligne ${index} n'est pas valide. Il manque des informations sur l'utilisateur.`
          );
          return null;
        }

        // Availability slots must be correct dates
        const arrivalDate = arrivalDateTime && dayjs(arrivalDateTime, "L LT");
        const departureDate = departureDateTime && dayjs(departureDateTime, "L LT");
        if (arrivalDate?.isValid() && departureDate?.isValid()) {
          registrationData.availabilitySlots = [{start: arrivalDate, end: departureDate}];
        } else {
          errors.push(
            `La ligne ${index} n'est pas valide. Les dates de départ ou d'arrivée ne sont pas bien formatées. ` +
              `Formatez de cette manière: ${dayjs().format("L LT")}.`
          );
          return null;
        }

        return {
          ...registrationData,
          specific: formData,
          user: {firstName, lastName, email},
        };
      })
      .filter((el) => !!el);

    // Launch the import only if no errors
    if (errors.length > 0) {
      errors.forEach((error) => message.error(error, 10));
    } else {
      dispatch(projectsActions.import({registrations: preparedRegistrations}, true, true));
    }
  };

  const buildParticipantExport = () => {
    const ticketsFieldName = project.ticketingMode && `${project.ticketingMode}Tickets`;

    const baseColumns = [
      "Prénom",
      "Nom",
      "Email",
      "Dates OK",
      "Formulaire OK",
      "Billetterie OK",
      "Inscription commencée",
      "Arrivé.e",
      `Détail billets (${project.ticketingMode || " - "})`,
      "Montant total payé",
      "Encadrant⋅e lié⋅e",
      "Équipes",
      "Date d'arrivée",
      "Date de départ",
      "Dates avancées",
      "Nombre de jours comptabilisés",
      "Nombre de jours total",
      "Jauge de bénévolat (minutes/jour)",
      "Rôle",
    ];

    let allFormColumns = [],
      allPlanningColumns = [];

    const exportedData = registrationsWithMetadata
      .map((registration) => {
        // Compute min and max arrival and departure date
        const earliestArrivalDate = registration.availabilitySlots.reduce(
          (accumulator, currentValue) => {
            const start = dayjs(currentValue.start);
            return !accumulator || start < accumulator ? start : accumulator;
          },
          undefined
        );
        const latestLeavingDate = registration.availabilitySlots.reduce(
          (accumulator, currentValue) => {
            const end = dayjs(currentValue.end);
            return !accumulator || end > accumulator ? end : accumulator;
          },
          undefined
        );

        const sessionsPlanning = getSessionsPlanningForRegistration(
          registration,
          allSlots,
          teams,
          allPlanningColumns
        );

        // Format it to a single total string
        const sessionPlanningTotal = sessionsPlanning.map((slotInfo) => slotInfo.value).join(" | ");
        // And also group it by day
        const sessionPlanningByDate = sessionsPlanning.reduce(
          (groupByDayAcc, {startDate, value}) => {
            const formattedDate = listRenderer.longDateFormat(startDate, true);
            return {
              ...groupByDayAcc,
              [formattedDate]:
                groupByDayAcc[formattedDate]?.length > 0
                  ? groupByDayAcc[formattedDate] + " | " + value
                  : value,
            };
          },
          {}
        );

        // Clean the form answers
        const cleanedFormAnswers =
          registration.specific &&
          Object.entries(registration.specific).reduce((acc, [key, value]) => {
            return {...acc, [key]: cleanAnswer(value, ";")};
          }, {});
        // Add the fields to the form columns
        cleanedFormAnswers && allFormColumns.push(...Object.keys(cleanedFormAnswers));

        // Return all the registration data
        return {
          createdAt: dayjs(registration.createdAt),
          INFOS: ">>",
          Prénom: registration.user?.firstName,
          Nom: registration.user?.lastName,
          Email: registration.user?.email,
          "Dates OK": registration.firstSlotIsOk,
          "Formulaire OK": registration.formIsOk,
          "Billetterie OK": registration.ticketingIsOk,
          "Inscription commencée": registration.booked,
          "Arrivé.e": registration.hasCheckedIn,
          [`Détail billets (${project.ticketingMode || " - "})`]:
            project.ticketingMode &&
            registration[ticketsFieldName]?.map((ticket) => ticket.id).join(", "),
          "Montant total payé":
            project.ticketingMode &&
            registration[ticketsFieldName]?.reduce((acc, ticket) => acc + ticket.amount, 0) / 100,
          "Encadrant⋅e lié⋅e": personName(registration.steward),
          Équipes: registration.teamsSubscriptionsNames,
          "Date d'arrivée": earliestArrivalDate?.format("LLL"),
          "Date de départ": latestLeavingDate?.format("LLL"),
          "Dates avancées": registration.availabilitySlots?.length > 1,
          "Nombre de jours comptabilisés": registration.numberOfDaysOfPresence,
          "Nombre de jours total": registration.daysOfPresence.length,
          Rôle: registration.role,
          "Jauge de bénévolat (minutes/jour)": Math.round(registration.voluntaryCounter),
          PLANNING: ">>",
          "Tout l'événement": sessionPlanningTotal,
          ...sessionPlanningByDate,
          "FORMULAIRE D'INSCRIPTION": ">>",
          ...cleanedFormAnswers,
        };
      })
      .sort((a, b) => a.createdAt.diff(b.createdAt)); // Sort it by creation date

    // Compute the final columns layout of the CSV file
    allPlanningColumns = allPlanningColumns
      .sort((a, b) => a.diff(b))
      .map((startDate) => listRenderer.longDateFormat(startDate, true));

    const columnsLayout = [
      "createdAt",
      "INFOS",
      ...baseColumns,
      "PLANNING",
      "Tout l'événement",
      ...removeDuplicates(allPlanningColumns),
      "FORMULAIRE D'INSCRIPTION",
      ...removeDuplicates(allFormColumns),
    ];

    // Return the data with the column layout
    return [columnsLayout, ...exportedData];
  };

  return (
    <TabsPage
      title="Participant⋅es"
      icon={<TeamOutlined />}
      fullWidth
      customButtons={
        <>
          <CsvExportButton
            tooltip="Exporter les participant⋅es au format CSV"
            getExportName={() =>
              `Export des participant⋅es - ${project.name} - ${dayjs().format(
                "DD-MM-YYYY HH[h]mm"
              )}.csv`
            }
            withFields
            dataExportFunction={buildParticipantExport}>
            Exporter
          </CsvExportButton>
          <CsvGroupImportButton
            title={
              <>
                {!isMobileView && "Importer"} <BetaTag />
              </>
            }
            keepUnknownEntries
            onOk={handleRegistrationsGroupImport}
            elementsName={t("registrations:label_other")}
            forceEndpoint={"registrations"}
            forceSchema={[
              {key: "firstName", label: t("users:schema.firstName.label")},
              {key: "lastName", label: t("users:schema.lastName.label")},
              {key: "email", label: t("users:schema.email.label")},
              {key: "arrivalDateTime", label: t("registrations:schema.arrivalDateTime.label")},
              {key: "departureDateTime", label: t("registrations:schema.departureDateTime.label")},
              {key: "steward", label: t("stewards:label")},
            ]}
          />

          {searchBar}
        </>
      }
      items={[
        {
          label: "Liste",
          key: "list",
          style: {marginTop: -15},
          children: (
            <ParticipantList
              envId={envId}
              displayUnbookedUsers={displayUnbookedUsers}
              setDisplayUnbookedUsers={setDisplayUnbookedUsers}
              advancedMode={advancedMode}
              setAdvancedMode={setAdvancedMode}
              registrations={registrationsWithMetadata}
              navigate={navigate}
            />
          ),
        },
        {
          label: "Données de fréquentation",
          key: "dashboard",
          className: "with-margins",
          children: (
            <Suspense fallback={() => <></>}>
              <ParticipantDashboard registrations={registrationsWithMetadata} />
            </Suspense>
          ),
        },
      ]}
    />
  );
}

export default ParticipantMain;
