import {Button, Tooltip} from "antd";
import {UserSwitchOutlined} from "@ant-design/icons";
import {URLS} from "../../app/configuration";
import React from "react";
import {
  getElementsFromListInSlot,
  getSessionName,
  slotNumberString,
} from "../../helpers/agendaUtilities";
import {listRenderer} from "../../helpers/listUtilities";
import dayjs from "dayjs";

const now = dayjs();

export const ChangeIdentityButton = ({registration, envId}) => (
  <Tooltip title="Changer d'identité" placement="left">
    <Button
      type="link"
      icon={<UserSwitchOutlined />}
      href={`${URLS.INSCRIPTION_FRONT}/${envId}/participants?connectedAs=${registration._id}`}
    />
  </Tooltip>
);

export const getSessionsPlanningForRegistration = (
  registration,
  allSlots,
  teams,
  allPlanningColumns?
) =>
  registration.sessionsSubscriptions
    .map((ss) => {
      const slot = allSlots.find((slot) => slot.session._id === ss.session);
      if (!slot) return;
      const title = slotNumberString(slot) + getSessionName(slot.session, teams);
      const location = getElementsFromListInSlot("places", slot, (el) => el.name).join(", ");
      const dateTimeRange = listRenderer.longDateTimeRangeFormat(slot.start, slot.end, true);
      const categoryName = slot.session.activity?.category.name;

      // Add the slot date to the dates columns if needed
      allPlanningColumns && allPlanningColumns.push(dayjs(slot.start));

      return {
        startDate: slot.start,
        endDate: slot.end,
        sessionId: slot.session._id,
        value: `${dateTimeRange} : [${categoryName}] ${title} (${location})`,
      };
    })
    .filter((slotInfo) => slotInfo !== undefined)
    .sort((a, b) => dayjs(a.startDate).diff(b.startDate));

export const getNextOrCurrentSession = (registration, allSlots, teams) =>
  allSlots &&
  teams &&
  getSessionsPlanningForRegistration(registration, allSlots, teams).find(
    (sessionPlanned) =>
      now.isBefore(sessionPlanned.startDate) ||
      (now.isAfter(sessionPlanned.startDate) && now.isBefore(sessionPlanned.endDate))
  );
