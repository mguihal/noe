import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ConfigEdit} from "./config/ConfigEdit.js";
import {
  CalendarOutlined,
  ClusterOutlined,
  DownloadOutlined,
  EnvironmentOutlined,
  DoubleLeftOutlined,
  BookOutlined,
  PlaySquareOutlined,
  ScheduleOutlined,
  TagOutlined,
  TeamOutlined,
  ToolOutlined,
  UserOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import {Redirect, Router} from "@reach/router";
import {currentUserSelectors} from "../features/currentUser.js";
import {currentProjectActions, currentProjectSelectors} from "../features/currentProject.js";
import {LayoutStructure} from "./common/LayoutStructure";
import {useBrowserTabTitle} from "../helpers/viewUtilities";
import {RegistrationEdit} from "./participants/RegistrationEdit";
import {registrationsActions, registrationsSelectors} from "../features/registrations";
import {
  OFFLINE_MODE,
  OnlineOfflineSwitch,
  DISPLAY_OFFLINE_MODE_FEATURE,
  useOfflineMode,
} from "../helpers/offlineModeUtilities";
import {Cockpit} from "./cockpit/Cockpit";
import {instanceName, URLS} from "../app/configuration";
import {DynamicThemeProvider} from "../style/theme";
import {MenuLayout} from "./common/layout/Menu";
import {Pending} from "./common/layout/Pending";
import {HelpToast} from "./common/layout/HelpToast";
import {lazyWithRetry} from "../helpers/lazyWithRetry";
import {ConcurrentEditingWorker} from "../helpers/concurrentEditing";
const ActivityList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./activities/ActivityList.js")
);
const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./stewards/StewardEdit.js")
);
const SessionList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionList.js")
);
const SessionEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionEdit.js")
);
const SessionAgenda = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionAgenda.js")
);
const ParticipantMain = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./participants/ParticipantMain.js")
);
const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./activities/ActivityEdit.js")
);
const StewardList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./stewards/StewardList.js")
);
const PlaceList = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./places/PlaceList.js"));
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./places/PlaceEdit.js"));
const CategoryList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./categories/CategoryList.js")
);
const CategoryEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./categories/CategoryEdit.js")
);
// const RunAI= lazyWithRetry(() => import(/* webpackPrefetch: true */ "./ai/RunAI.js"));
const TeamEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./teams/TeamEdit"));
const TeamList = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./teams/TeamList"));

export default function ProjectLayout({"*": page, envId, navigate}) {
  const dispatch = useDispatch();

  // ****** SELECTORS & STATES ******

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  // ****** DATA FETCHING ******

  // Load project and project registration
  useEffect(() => {
    if (currentUser._id) {
      dispatch(currentProjectActions.load(envId)).catch(() => navigate("/projects"));
    }
  }, [currentUser, envId]);
  useEffect(() => {
    //(on orga front, the registration is independent from the project)
    dispatch(registrationsActions.loadCurrent(envId));
  }, [envId]);

  useOfflineMode(currentRegistration?._id);

  // ****** DATA DEFINITION ******

  const projectAndUserValid = currentUser._id && currentProject?._id && currentRegistration?._id;

  // If there is a loaded registration, but no role, then the user doesn't have the right to access the project
  if (projectAndUserValid && !currentRegistration?.role) navigate("/projects");

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    currentProject.name,
    page,
    {
      config: "Configuration",
      categories: "Catégories",
      activities: "Activités",
      teams: "Équipes",
      stewards: "Encadrant⋅es",
      places: "Espaces",
      sessions: "Sessions",
      "sessions/agenda": "Sessions",
      participants: "Participant⋅es",
    },
    (instanceName !== "NOÉ" ? `${instanceName} ` : "") + "ORGA"
  );

  // ****** SIDE MENU ******

  const collapsedSidebar = page?.includes("agenda");

  const menu = {
    top: (
      <>
        <MenuLayout
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          items={[
            {
              label: "Cockpit",
              key: "cockpit",
              icon: <DashboardOutlined />,
            },
            {
              label: "Configuration",
              key: "config",
              icon: <ToolOutlined />,
            },
            {
              label: "Participant⋅es",
              key: "participants",
              icon: <TeamOutlined />,
            },
          ]}
        />

        <MenuLayout
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          title={collapsedSidebar ? "prog" : "programmation"}
          items={[
            {
              label: "Catégories",
              key: "categories",
              icon: <TagOutlined />,
            },
            currentProject.usePlaces && {
              label: "Espaces",
              key: "places",
              icon: <EnvironmentOutlined />,
            },
            {
              label: "Encadrant⋅es",
              key: "stewards",
              icon: <UserOutlined />,
            },
            {
              label: "Activités",
              key: "activities",
              icon: <BookOutlined />,
            },
            currentProject.useTeams && {
              label: "Équipes",
              key: "teams",
              icon: <TeamOutlined />,
            },
          ]}
        />

        <MenuLayout
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          title="planning"
          items={[
            currentProject.useAI && {
              label: "Créer avec l'IA",
              key: "ai",
              icon: <ClusterOutlined />,
            },
            {
              label: "Sessions",
              key: "sessions",
              icon: <ScheduleOutlined />,
            },
            {
              label: "Planning des sessions",
              key: "sessions/agenda",
              icon: <CalendarOutlined />,
            },
          ]}
        />
      </>
    ),

    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Inscription front
          {
            label: "Interface participant⋅e",
            url: `${URLS.INSCRIPTION_FRONT}/${envId}/${page}`,
            icon: <PlaySquareOutlined />,
          },

          // Back to events page
          {
            label: "Mes événements",
            url: "/projects?no-redir",
            icon: <DoubleLeftOutlined />,
            disabled: OFFLINE_MODE,
          },

          // Offline mode
          DISPLAY_OFFLINE_MODE_FEATURE && {
            label: <OnlineOfflineSwitch />,
            icon: <DownloadOutlined />,
            url: false,
          },
        ]}
      />
    ),
  };

  return (
    <DynamicThemeProvider currentProject={currentProject}>
      <ConcurrentEditingWorker currentProjectId={currentProject?._id} />
      <LayoutStructure
        title={currentProject.name}
        ribbon="ORGA"
        menu={menu}
        profileUser={currentUser}
        showSocialIcons={false}
        fadeIn
        collapsedSidebar={collapsedSidebar}>
        {projectAndUserValid && (
          <>
            <Pending.Suspense minHeight={"100vh"} animationDelay={"300ms"}>
              <Router style={{height: "100%"}}>
                {/* Redirect when switching from one frontend to another*/}
                <Redirect noThrow from="/sessions/subscribed/agenda" to="../../agenda" />
                <Redirect noThrow from="/sessions/subscribed" to=".." />
                <Redirect noThrow from="/sessions/all/agenda" to="../../agenda" />
                <Redirect noThrow from="/sessions/all" to=".." />

                <Cockpit path="/cockpit" />
                <ConfigEdit path="/config" />
                <ActivityList path="/activities" />
                <ActivityEdit path="/activities/:id" />
                <StewardList path="/stewards" />
                <StewardEdit path="/stewards/:id" />
                {currentProject.usePlaces && <PlaceList path="/places" />}
                {currentProject.usePlaces && <PlaceEdit path="/places/:id" />}
                <CategoryList path="/categories" />
                <CategoryEdit path="/categories/:id" />
                <ParticipantMain path="/participants" />
                <RegistrationEdit path="/participants/:id" />
                <SessionList path="/sessions" />
                <SessionEdit path="/sessions/:id" />
                <SessionAgenda path="/sessions/agenda" />
                {currentProject.useTeams && <TeamList path="/teams" />}
                {currentProject.useTeams && <TeamEdit path="/teams/:id" />}
                {/*{currentProject.useAI && <RunAI path="/ai" />}*/}

                <Redirect noThrow from="/*" to="./cockpit" />
              </Router>
            </Pending.Suspense>
            <HelpToast
              href={`${process.env.REACT_APP_WEBSITE_URL}/docs/faq`}
              target="_blank"
              tooltipMessage="Besoin d'aide ? Cliquez ici pour accéder à la FAQ et à la documentation de l'application."
            />
          </>
        )}
      </LayoutStructure>
    </DynamicThemeProvider>
  );
}
