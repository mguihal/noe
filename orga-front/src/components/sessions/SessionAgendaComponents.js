import {t} from "i18next";
import React, {useRef, useState} from "react";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {Button, Drawer, Dropdown, Input, message, Select, Space, Tag, theme} from "antd";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  BorderOutlined,
  CheckSquareOutlined,
  DownOutlined,
  PictureOutlined,
  SearchOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {projectsSelectors} from "../../features/projects";
import dayjs from "dayjs";
import {categoriesSelectors} from "../../features/categories";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {TableElement} from "../common/TableElement";
import {paginationPageSizes} from "../../features/view";
import {listSorter} from "../../helpers/listUtilities";
import {CELLS_DISPLAY_HEIGHTS} from "../../helpers/agendaUtilities";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {SwitchInput} from "../common/inputs/SwitchInput";

//************************************//
//****** AGENDA BASE COMPONENTS ******//
//************************************//

// Left and right arrows to navigate in the agenda days
export const NavButton = ({type, onClick}) => (
  <Button
    type="link"
    size="large"
    icon={type === "forward" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
    onClick={onClick}
  />
);

// Renders the date labels on the side of the agenda
export const getTimeScaleLabelComponent =
  (cellDisplayHeight) =>
  ({time, ...otherProps}) => {
    // If there is no 'time', it means it's the very first, which has no time in it.
    const style = time
      ? {height: cellDisplayHeight, lineHeight: `${cellDisplayHeight}px`}
      : {height: cellDisplayHeight / 2}; // It should be twice as small.

    // Only display time when it's o'clock
    const isOClock = time?.getMinutes() === 0;

    return (
      <DayView.TimeScaleLabel
        className="timescale-cell" // Needed to get a hook
        time={isOClock ? time : undefined}
        style={style}
        {...otherProps}
      />
    );
  };

//*********************************//
//****** AGENDA CONTROLS BAR ******//
//*********************************//

export const getAllScopes = (currentProject) =>
  [
    {label: t("activities:schema.name.label"), key: "activity"},
    {label: t("sessions:schema.name.label"), key: "session"},
    {label: t("categories:label"), key: "category"},
    {label: t("activities:schema.secondaryCategories.label"), key: "secondaryCategories"},
    {label: t("stewards:label_other"), key: "stewards"},
    currentProject.usePlaces && {label: t("places:label"), key: "places"},
    currentProject.useTeams && {label: t("teams:label"), key: "team"},
    {label: t("registrations:label_other"), key: "registrations"},
  ].filter((el) => el);

const SearchBox = ({searchValue, setSearchValue}) => {
  const searchInput = useRef();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const defaultScopes = searchValue.scopes;
  const [scopes, setScopes] = useState(defaultScopes);

  const [scopesDropdownOpen, setScopesDropdownOpen] = useState(false);

  const onChange = ({key}) => {
    const newScopes = key
      ? scopes.includes(key)
        ? scopes.filter((scope) => scope !== key)
        : [...scopes, key]
      : scopes;
    key && setScopes(newScopes);

    setSearchValue({
      text: searchInput.current.input.value,
      scopes: newScopes,
    });
  };

  return (
    <Space.Compact style={{minWidth: 150, flexBasis: "50%"}}>
      <Input.Search placeholder="Rechercher..." onSearch={onChange} ref={searchInput} />
      <Dropdown
        trigger={["click"]}
        onOpenChange={setScopesDropdownOpen}
        open={scopesDropdownOpen}
        menu={{
          items: getAllScopes(currentProject).map((scope) => ({
            ...scope,
            icon: scopes.includes(scope.key) ? (
              <CheckSquareOutlined style={{fontSize: 18, color: theme.defaultSeed.colorPrimary}} />
            ) : (
              <BorderOutlined style={{fontSize: 18}} />
            ),
            onClick: onChange,
          })),
        }}>
        <Button icon={<SettingOutlined />} onClick={onChange} />
      </Dropdown>
    </Space.Compact>
  );
};

const NumberOfDaysSelector = ({
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  isMobileView = false,
}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(projectsSelectors.selectEditing);

  return (
    <Dropdown
      trigger={["click"]}
      menu={{
        onClick: (item) => setNumberOfDaysDisplayed(item.key),
        items: [
          {
            label: t("sessions:agenda.numberOfDaysSelector.allTheEvent"),
            key:
              dayjs(currentProject.end)
                .startOf("day")
                .diff(dayjs(currentProject.start).startOf("day"), "day") + 1,
          },
          {type: "divider"},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 1}), key: 1},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 3}), key: 3},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 5}), key: 5},
          {type: "divider"},
          {
            label: "Autre",
            children: [...Array(14).keys()].map((i) => ({
              label: t("sessions:agenda.numberOfDaysSelector.days", {count: i + 2}),
              key: i + 2,
            })),
          },
        ],
      }}>
      <Button>
        {isMobileView
          ? t("sessions:agenda.numberOfDaysSelector.daysMobile", {
              count: parseInt(numberOfDaysDisplayed),
            })
          : t("sessions:agenda.numberOfDaysSelector.days", {
              count: parseInt(numberOfDaysDisplayed),
            })}
        {!isMobileView && <DownOutlined style={{marginLeft: 6}} />}
      </Button>
    </Dropdown>
  );
};

const CategoriesFilterSelector = ({categoriesFilter, setCategoriesFilter}) => {
  const categories = useSelector(categoriesSelectors.selectList);
  const categoriesReady = categories.length > 0;
  const categoriesOptions = [
    {key: "volunteering", value: "volunteering", label: "- Tout le bénévolat -"},
    {key: "allTheRest", value: "allTheRest", label: "- Tout le reste -"},
    ...categories.map((category) => ({
      key: category._id,
      value: category._id,
      label: category.name,
    })),
  ];
  return (
    <Select
      style={{minWidth: 200, flexBasis: "50%"}}
      defaultValue={categoriesReady ? categoriesFilter : undefined}
      allowClear
      mode="multiple"
      placeholder="Filtrez par catégorie..."
      onChange={setCategoriesFilter}
      options={categoriesOptions}
    />
  );
};

export const AgendaControls = ({
  searchValue,
  setSearchValue,
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  categoriesFilter,
  setCategoriesFilter,
  isMobileView,
  FullSettingsDrawer,
  GroupByControls,
}) => {
  const [sessionFilterDrawerOpen, setSessionFilterDrawerOpen] = useState(false);

  return isMobileView ? (
    <>
      {/*On mobile, display a button to open the drawer to filter the sessions*/}
      <Button
        type="primary"
        onClick={() => setSessionFilterDrawerOpen(true)}
        style={{marginRight: 10, marginLeft: 10, flexGrow: 1}}
        icon={<SearchOutlined />}>
        Filtrer
      </Button>

      <NumberOfDaysSelector
        isMobileView={true}
        numberOfDaysDisplayed={numberOfDaysDisplayed}
        setNumberOfDaysDisplayed={setNumberOfDaysDisplayed}
      />

      <Drawer
        placement="top"
        height="auto" // adjust to content
        closable={false}
        onClose={() => setSessionFilterDrawerOpen(false)}
        open={sessionFilterDrawerOpen}>
        <div className="containerH buttons-container" style={{alignItems: "center"}}>
          <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
          <CategoriesFilterSelector
            categoriesFilter={categoriesFilter}
            setCategoriesFilter={setCategoriesFilter}
          />

          <FullSettingsDrawer />
          <GroupByControls />
        </div>
      </Drawer>
    </>
  ) : (
    <div
      style={{width: "100%", alignItems: "center", flexWrap: "nowrap", overflowX: "auto"}}
      className="containerH buttons-container">
      {/*Separator*/} <div style={{width: 20}} />
      <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
      <CategoriesFilterSelector
        categoriesFilter={categoriesFilter}
        setCategoriesFilter={setCategoriesFilter}
      />
      <GroupByControls />
      {/*Separator*/} <div style={{width: 12}} />
      <NumberOfDaysSelector
        numberOfDaysDisplayed={numberOfDaysDisplayed}
        setNumberOfDaysDisplayed={setNumberOfDaysDisplayed}
      />
      {/*Separator*/} <div style={{width: 10}} />
      <FullSettingsDrawer />
    </div>
  );
};

export const AgendaSettingsDrawer = ({
  children,
  cellDisplayHeight,
  slotsOnEachOther,
  setAgendaParams,
  buttonStyle,
  showResourcesListingOnAgendaCards,
}) => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const printAgendaView = () => {
    const toHide = [
      document.getElementsByTagName("aside")[0],
      document.getElementsByTagName("header")[0],
      document.getElementsByClassName("navbar-menu-button")[0],
      document.getElementsByClassName("MuiToolbar-root")[0],
      document.getElementsByClassName("ant-drawer")[0],
    ].filter((el) => !!el);
    const agendaSpace =
      document.getElementsByClassName("scheduler-wrapper")[0].firstChild.lastChild;

    const toHideWithOldValues = toHide.map((el) => ({el, value: el.style.display}));

    toHide.forEach((el) => (el.style.display = "none"));
    agendaSpace.style.overflowY = "visible";

    window.print();

    toHideWithOldValues.forEach(({el, value}) => (el.style.display = value));
    agendaSpace.style.overflowY = "auto";
  };

  return (
    <>
      <Button
        style={buttonStyle}
        icon={<SettingOutlined />}
        type="link"
        shape="circle"
        onClick={() => setDrawerOpen(true)}
      />
      <Drawer placement="right" onClose={() => setDrawerOpen(false)} open={drawerOpen}>
        <h3>{t("sessions:agenda.settingsDrawer.title")}</h3>
        <FormElement style={{marginTop: 26}} className="container-grid">
          {children}

          {/*Select if we want to see events one on the other or one next to each other. */}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.slotsOnEachOther.label")}
            onChange={(value) => setAgendaParams({slotsOnEachOther: value})}
            defaultValue={slotsOnEachOther}
            options={[
              {
                value: false,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.false"),
              },
              {
                value: true,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.true"),
              },
            ]}
          />

          {/*Increase or decrease the cells heights*/}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.cellDisplayHeight.label")}
            onChange={(value) => setAgendaParams({cellDisplayHeight: value})}
            defaultValue={cellDisplayHeight}
            options={[
              {
                value: CELLS_DISPLAY_HEIGHTS[0],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.compact"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[1],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.comfort"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[2],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.large"),
              },
            ]}
          />

          {/*Add info to the cards on the agenda*/}
          <InputElement.Select
            label="Informations affichées sur les cartes de l'agenda"
            defaultValue={showResourcesListingOnAgendaCards}
            mode="multiple"
            placeholder="sélectionnez des informations à afficher..."
            onChange={(value) => setAgendaParams({showResourcesListingOnAgendaCards: value})}
            options={[
              {value: "places", label: "Espaces"},
              {value: "stewards", label: "Encadrant⋅es"},
              {value: "registrations", label: "Participant⋅es"},
              {value: "maxNumberOfParticipants", label: "Jauge de participant⋅es"},
            ]}
          />

          <Button icon={<PictureOutlined />} onClick={printAgendaView}>
            {t("sessions:agenda.settingsDrawer.exportAgendaViewButton")}
          </Button>
        </FormElement>
      </Drawer>
    </>
  );
};

export const ResourceFilterSelector = ({
  resource,
  changeGrouping,
  selectedResources,
  resourcesFilterSelections,
  showResourcesAvailabilities,
  agendaParams,
  setAgendaParams,
}) => {
  const [tmpResourceFilterSelection, setTmpResourceFilterSelection] = useState(
    resourcesFilterSelections[resource.resourceName]?.map((el) => ({id: el})) || []
  );
  const [drawerOpen, setDrawerOpen] = useState(false);
  const checked = selectedResources.includes(resource.fieldName);

  const rowSelectionResourceFilterSelection = {
    onChange: (selectedRowKeys, selectedRowObject) =>
      setTmpResourceFilterSelection(selectedRowObject),
  };

  const validateFilter = () => {
    changeGrouping(resource, true, {
      ...resourcesFilterSelections,
      [resource.resourceName]: tmpResourceFilterSelection.map((r) => r.id),
    });
    setDrawerOpen(false);
  };

  const resetFilter = () => {
    changeGrouping(resource, false);
    setDrawerOpen(false);
  };

  return (
    <>
      <Tag.CheckableTag
        key={resource.fieldName}
        className={`${resource.resourceName}-grouping grouping-button`}
        checked={checked}
        onChange={() =>
          resource.instances.length === 0
            ? message.warning(
                `Vous devez déjà avoir créé des ${resource.title.toLowerCase()} pour effectuer cette action.`
              )
            : !checked && selectedResources?.length >= 2
            ? message.warning(
                "Pas plus de deux filtres à la fois, autrement votre PC va exploser 😉"
              )
            : setDrawerOpen(true)
        }>
        {resource.title}
      </Tag.CheckableTag>
      <Drawer
        placement="right"
        onClose={() => setDrawerOpen(false)}
        open={drawerOpen}
        zIndex={10000}>
        {/*Display availabilities or not*/}
        {resource.hasAvailabilities && (
          <SwitchInput
            label="Affichage des disponibilités"
            tooltip={
              <>
                Afficher les disponibilités des {resource.title} sur le planning lorsque l'on active
                le groupage.
                <br />- Le rose rayé représente les moment d'indisponibilité des salles.
                <br />- Le bleu rayé représente les moments d'indisponibilité des encadrant⋅es.
              </>
            }
            checked={showResourcesAvailabilities[resource.fieldName]}
            onChange={(value) =>
              setAgendaParams({
                showResourcesAvailabilities: {
                  ...agendaParams.showResourcesAvailabilities,
                  [resource.fieldName]: value,
                },
              })
            }
          />
        )}
        <TableElement.Simple
          showHeader
          scroll={{y: "calc( 100vh - 340px)"}}
          pagination={{
            position: ["bottomCenter"],
            pageSize: 40,
            size: "small",
            pageSizeOptions: paginationPageSizes,
            showSizeChanger: true,
          }}
          rowSelection={rowSelectionResourceFilterSelection}
          selectedRowKeys={tmpResourceFilterSelection}
          setSelectedRowKeys={setTmpResourceFilterSelection}
          columns={[
            {
              title: `Filtrage des ${resource.title}`,
              dataIndex: "text",
              searchable: true,
              searchText: (record) => record.text,
              sorter: (a, b) => listSorter.text(a.text, b.text),
            },
          ]}
          rowKey="id"
          dataSource={resource.instances}
        />
        <Button
          type="primary"
          style={{width: "100%", marginTop: 8}}
          onClick={validateFilter}
          disabled={tmpResourceFilterSelection.length === 0}>
          Valider le groupage
        </Button>
        {checked && (
          <Button type="link" danger style={{width: "100%", marginTop: 8}} onClick={resetFilter}>
            Désactiver le groupage
          </Button>
        )}
      </Drawer>
    </>
  );
};
