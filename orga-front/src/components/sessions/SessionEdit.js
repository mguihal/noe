import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {
  addKeyToItemsOfList,
  columnsPlaces,
  columnsStewards,
  dataToFields as baseDataToFields,
  fieldToData,
  generateRegistrationsColumns,
  generateSubscriptionInfo,
  registrationDispoColumn,
  searchInRegistrationFields,
  SessionNoShowCheckbox,
} from "../../helpers/tableUtilities";
import {Alert, Button, Collapse, Form, message, Modal} from "antd";
import {
  BookOutlined,
  ClockCircleOutlined,
  EnvironmentOutlined,
  InfoCircleOutlined,
  ScheduleOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import dayjs from "dayjs";
import {InputElement} from "../common/InputElement";
import {FormElement, safeValidateFields} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {CardElement} from "../common/layout/CardElement";
import {EditPage} from "../common/EditPage";
import {useNewElementModal} from "../../helpers/editingUtilities";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {getMaxParticipantsBasedOnPlaces} from "../../helpers/sessionsUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {pick} from "../../helpers/utilities";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";
import {DateTimeInput} from "../common/inputs/DateTimeInput";
import {TextInput} from "../common/inputs/TextInput";
import {NumberInput} from "../common/inputs/NumberInput";
import {SwitchInput} from "../common/inputs/SwitchInput";
import {ElementSelectionModal} from "../common/layout/ElementSelectionModal";
import {Pending} from "../common/layout/Pending";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
import {getSessionSubscription} from "../../helpers/registrationsUtilities";
const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../activities/ActivityEdit")
);
const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../places/PlaceEdit"));
const TeamEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../teams/TeamEdit"));
const SessionShowSmall = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./SessionShowSmall")
);

const columnsSlots = [
  {
    dataIndex: "start",
    render: (text, record, index) =>
      listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
  },
  {
    dataIndex: "duration",
    render: listRenderer.durationFormat,
  },
];

function SessionEdit({id, location, asModal, modalOpen, setModalOpen}) {
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelector("participants");
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);
  const teams = useSelector(teamsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const dispatch = useDispatch();
  const [slotEditingForm] = Form.useForm();
  const [showModalStewards, setShowModalStewards] = useState(false);
  const [addingSteward, setAddingSteward] = useState([]);
  const [showModalPlaces, setShowModalPlaces] = useState(false);
  const [addingPlace, setAddingPlace] = useState([]);
  const [showModalRegistrations, setShowModalRegistrations] = useState(false);
  const [setShowNewActivityModal, NewActivityModal] = useNewElementModal(ActivityEdit);
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const [setShowNewPlaceModal, NewPlaceModal] = useNewElementModal(PlaceEdit);
  const [setShowNewTeamModal, NewTeamModal] = useNewElementModal(TeamEdit);
  const [addingRegistrations, setAddingRegistrations] = useState([]);
  const [showModalSlot, setShowModalSlot] = useState(false);
  const [currentSlot, setCurrentSlot] = useState();
  const [isModified, setIsModified] = useState(false);

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.useTeams && dispatch(teamsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(registrationsActions.loadList());
      dispatch(sessionsActions.loadList()); // For tags
    },
    clonedElement
  );

  const insufficientNumberOfStewards =
    session.activity && session?.stewards?.length < session?.activity?.minNumberOfStewards;

  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = session.activity?.maxNumberOfParticipants;
  const sessionMaxIsDefined = sessionMax !== undefined && sessionMax !== null;
  const maxParticipantsBasedOnPlaces = getMaxParticipantsBasedOnPlaces(session);
  const numberOfParticipantsSetIsAbovePlacesCapacity =
    sessionMax > maxParticipantsBasedOnPlaces || activityMax > maxParticipantsBasedOnPlaces;

  const sessionRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(generateRegistrationsColumns(currentProject)),
    registrationDispoColumn,
  ];
  const additionalRegistrationsColumns = [
    {
      title: "No show",
      dataIndex: "hasNotShownUp",
      render: (text, record) => (
        <SessionNoShowCheckbox
          session={session}
          registration={record}
          registrations={registrations}
          dispatch={dispatch}
        />
      ),
      filters: [
        {text: "Oui", value: true},
        {text: "Non", value: false},
      ],
      onFilter: (value, record) => {
        const noShow = getSessionSubscription(record, session).hasNotShownUp;
        return value ? noShow : !noShow; // hasNotShownUp is a user id (not a boolean), so we need to make this condition to make the filter work
      },
    },
    {
      title: "Inscription",
      render: (text, record) =>
        generateSubscriptionInfo(
          record,
          getSessionSubscription(record, session),
          registrations,
          true
        ),
      sorter: (a, b) =>
        listSorter.date(
          getSessionSubscription(a, session).updatedAt,
          getSessionSubscription(b, session).updatedAt
        ),
    },
  ];

  const getCommonStuff = (
    entityName,
    entities,
    addingEntity,
    setAddingEntity,
    setShowModalEntities
  ) => {
    const entitiesAddable = () => {
      if (currentSlot && !currentSlot[`${entityName}SessionSynchro`]) {
        if (currentSlot[entityName]) {
          return entities.filter(
            (t) => currentSlot[entityName].filter((el) => el._id === t._id).length === 0
          );
        } else {
          return entities;
        }
      } else {
        if (session[entityName]) {
          return entities.filter(
            (t) => session[entityName].filter((el) => el._id === t._id).length === 0
          );
        } else {
          return entities;
        }
      }
    };

    const entitiesAddableTable = addKeyToItemsOfList(entitiesAddable());
    const sessionEntitiesTable = addKeyToItemsOfList(session[entityName]);

    const currentSlotEntities = () => {
      if (currentSlot && currentSlot[`${entityName}SessionSynchro`]) {
        return sessionEntitiesTable;
      } else if (currentSlot && currentSlot[entityName]) {
        return currentSlot[entityName];
      } else {
        return [];
      }
    };

    const currentSlotEntitiesTable = addKeyToItemsOfList(currentSlotEntities(), "_id");

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRowObject) => {
        setAddingEntity(selectedRowObject);
      },
    };

    const addEntities = () => {
      if (currentSlot) {
        if (currentSlot[entityName]) {
          currentSlot[entityName] = [...currentSlot[entityName], ...addingEntity];
        } else {
          currentSlot[entityName] = [...addingEntity];
        }
      } else {
        dispatch(sessionsActions.addToEditing({payload: addingEntity, field: entityName}));
        dispatch(sessionsActions.checkInconsistencies());
      }
      setAddingEntity([]);
    };

    const removeEntity = (entity) => {
      if (currentSlot) {
        const index = currentSlot[entityName].map((s) => s._id).indexOf(entity._id);
        currentSlot[entityName] = [
          ...currentSlot[entityName].slice(0, index),
          ...currentSlot[entityName].slice(index + 1),
        ];
      } else {
        dispatch(sessionsActions.removeToEditing({payload: entity, field: entityName}));
        dispatch(sessionsActions.checkInconsistencies());
      }

      setAddingEntity([]);
      setIsModified(true);
    };

    const openModalEntities = () => {
      if (session.activity) {
        if (currentSlot && !currentSlot[`${entityName}SessionSynchro`]) {
          const activityEntities = entitiesAddableTable.filter((p) =>
            session.activity[entityName].map((ap) => ap._id).includes(p._id)
          );
          setAddingEntity(activityEntities);
        } else {
          const activityEntities = entitiesAddableTable.filter((p) =>
            session.activity[entityName].map((ap) => ap._id).includes(p._id)
          );
          setAddingEntity(activityEntities);
        }
      }
      setShowModalEntities(true);
    };

    return [
      entitiesAddableTable,
      sessionEntitiesTable,
      currentSlotEntitiesTable,
      rowSelection,
      addEntities,
      removeEntity,
      openModalEntities,
    ];
  };

  // STEWARDS STUFF

  const [
    stewardsAddableTable,
    sessionStewardsTable,
    currentSlotStewardsTable,
    rowSelectionSteward,
    addStewards,
    removeSteward,
    openModalStewards,
  ] = getCommonStuff("stewards", stewards, addingSteward, setAddingSteward, setShowModalStewards);

  // PLACES STUFF

  const [
    placesAddableTable,
    sessionPlacesTable,
    currentSlotPlacesTable,
    rowSelectionPlace,
    addPlaces,
    removePlace,
    openModalPlaces,
  ] = getCommonStuff("places", places, addingPlace, setAddingPlace, setShowModalPlaces);

  // SLOTS STUFF

  const openModalSlots = () => {
    setCurrentSlot({
      duration: undefined,
      start: session.end || session.start,
      end: session.end || session.start,
      placesSessionSynchro: true,
      stewardsSessionSynchro: true,
    });
    setShowModalSlot(true);
  };

  const editSlot = (record) => {
    setCurrentSlot(record);
    setShowModalSlot(true);
  };

  const sessionSlotsTable = addKeyToItemsOfList(session.slots);

  const removeSlot = (key) => {
    dispatch(sessionsActions.removeSlotToEditing(key));
    setIsModified(true);
  };

  const onChangeSlot = (changedFields, allFields) => {
    const newCurrentSlot = {...currentSlot, ...slotFieldToData(allFields)};
    if (newCurrentSlot.start && newCurrentSlot.duration) {
      newCurrentSlot.end = dayjs(newCurrentSlot.start)
        .add(newCurrentSlot.duration, "minute")
        .format();
    }
    if (newCurrentSlot.placesSessionSynchro === true) {
      newCurrentSlot.places = session.places;
    }
    if (newCurrentSlot.stewardsSessionSynchro === true) {
      newCurrentSlot.stewards = session.stewards;
    }
    setCurrentSlot(newCurrentSlot);
  };

  const onValidateSlot = () =>
    safeValidateFields(slotEditingForm, () => {
      if (currentSlot.key === undefined) {
        dispatch(sessionsActions.createSlotToEditing(currentSlot));
      } else {
        dispatch(sessionsActions.updateSlotToEditing(currentSlot));
      }
      dispatch(sessionsActions.checkInconsistencies());

      setCurrentSlot(undefined);
      setShowModalSlot(false);
      setIsModified(true);
    });

  // REGISTRATIONS STUFF

  const sessionRegistrations = registrations.filter((r) =>
    r.sessionsSubscriptions?.find((ss) => ss.session === session._id)
  );
  const registrationsAddable = registrations.filter(
    (r) => !session.registrations?.find((selected) => selected._id === r._id)
  );
  const rowSelectionRegistration = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingRegistrations(selectedRowObject);
    },
  };

  useEffect(() => {
    if (session._id)
      dispatch(sessionsActions.changeEditing({...session, registrations: sessionRegistrations}));
  }, [JSON.stringify(sessionRegistrations)]);

  const addRegistrations = () => {
    dispatch(
      sessionsActions.changeEditing({
        ...session,
        registrations: [...(session.registrations || []), ...addingRegistrations],
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
    setShowModalRegistrations(false);
  };
  const removeRegistration = (registration) => {
    dispatch(
      sessionsActions.changeEditing({
        ...session,
        registrations: session.registrations?.filter((r) => r._id !== registration._id),
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
  };

  // GLOBAL STUFF

  const onValidation = async (formData, fieldsKeysToUpdate) => {
    const payload = {...session, ...formData};

    if (fieldsKeysToUpdate) {
      // Pick also start and end because they are together with the rest
      fieldsKeysToUpdate = [...fieldsKeysToUpdate, "start", "end"];
      const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);

      for (const entityId of groupEditing.elements) {
        await dispatch(sessionsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
      }
    } else {
      if (payload.slots?.length > 0) {
        return dispatch(sessionsActions.persist(payload));
      } else {
        message.error("Vous devez créer au moins une plage pour créer cette session.");
      }
    }
  };

  const sessionFieldToData = (fields) => {
    let data = fieldToData(fields);
    if (data.start) {
      data.start = data.start.format();
    }
    if (data.end) {
      data.end = data.end.format();
    }
    if (typeof data.activity === "string") {
      data.activity = activities.find((activity) => activity._id === data.activity);
    }
    return data;
  };

  const slotFieldToData = (fields) => {
    let data = fieldToData(fields);
    data = {
      ...data,
      key: currentSlot.key,
      project: {_id: currentProject._id},
    };
    if (data.start) {
      data.start = data.start.format();
    }
    if (data.end) {
      data.end = data.end.format();
    }
    return data;
  };

  const dataToFields = (data) =>
    baseDataToFields(data, (clone) => {
      clone.start = dayjs(clone.start);
      clone.end = clone.end && dayjs(clone.end);
      clone.activity = clone.activity?._id;
      clone.team = clone.team?._id;
      clone.project = undefined;
    });

  return (
    <>
      <EditPage
        icon={<ScheduleOutlined />}
        i18nNs="sessions"
        onValidation={onValidation}
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        deletable
        elementsActions={sessionsActions}
        record={session}
        initialValues={{
          ...session,
          start: session.start && dayjs(session.start),
          end: session.end && dayjs(session.end),
          activity: session.activity?._id,
          team: session.team?._id,
        }}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["activity", "team", "name", "volunteeringCoefficient", "slots"]}
        fieldToData={sessionFieldToData}
        groupEditing={groupEditing}>
        <div className="container-grid two-thirds-one-third">
          <div className="container-grid">
            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.WithEntityLinks
                  endpoint="activities"
                  entity={session.activity}
                  createButtonText="Créer une nouvelle activité"
                  setShowNewEntityModal={setShowNewActivityModal}>
                  <InputElement.Select
                    label="Activité"
                    icon={<BookOutlined />}
                    name="activity"
                    options={activities.map((d) => ({value: d._id, label: d.name}))}
                    placeholder="sélectionnez une activité"
                    rules={[{required: true}]}
                    showSearch
                  />
                </InputElement.WithEntityLinks>

                {currentProject.useTeams && (
                  <InputElement.WithEntityLinks
                    endpoint="teams"
                    entity={session.team}
                    setShowNewEntityModal={setShowNewTeamModal}
                    createButtonText="Créer une nouvelle équipe">
                    <InputElement.Select
                      label="Équipe"
                      icon={<TeamOutlined />}
                      name="team"
                      placeholder="sélectionnez une équipe"
                      options={[
                        {value: null, label: "- Pas d'équipe -"},
                        ...teams.map((d) => ({
                          value: d._id,
                          label: d.name + (d.activity?.name ? ` (${d.activity?.name})` : ""),
                        })),
                      ]}
                      showSearch
                    />
                  </InputElement.WithEntityLinks>
                )}
                <NumberInput
                  label="Coefficient de bénévolat de la session"
                  tooltip={
                    <>
                      ↖ Champ hérité de l'activité : Le coefficient de bénévolat de la session est
                      égal au coefficient de bénévolat de l'activité, mais vous pouvez le redéfinir
                      si besoin.
                    </>
                  }
                  name="volunteeringCoefficient"
                  step={0.1}
                  placeholder={
                    "↖ " +
                    (session.activity?.volunteeringCoefficient
                      ? session.activity.volunteeringCoefficient
                      : "coef")
                  }
                  min={0}
                  max={5}
                />

                <NumberInput
                  label="Nombre maximum de participant⋅es"
                  name="maxNumberOfParticipants"
                  tooltip={
                    <>
                      <p>
                        ↖ Champ hérité de l'activité : Le nombre maximum de participant⋅es de la
                        session est égal au nombre maximum de participant⋅es de l'activité, mais
                        vous pouvez le redéfinir si besoin.
                      </p>
                      Valeurs spéciales:
                      <ul>
                        <li>
                          Rien = valeur de l'activité (et si pas de valeur dans l'activité =
                          inscription libre)
                        </li>
                        <li>
                          0 = Session invisible pour les participant.es, seulement visible par les
                          orgas et les personnes inscrites
                        </li>
                      </ul>
                    </>
                  }
                  min={0}
                  placeholder={
                    "↖ " +
                    (session.activity?.maxNumberOfParticipants
                      ? session.activity.maxNumberOfParticipants
                      : "max")
                  }
                />
              </div>
            </CardElement>

            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.Editor i18nNs="sessions" name="notes" />

                <InputElement.TagsSelect
                  i18nNs="sessions"
                  name="tags"
                  elementsSelectors={sessionsSelectors}
                />
              </div>
            </CardElement>
          </div>

          <div className="container-grid">
            <CardElement>
              <TextInput i18nNs="sessions" name="name" />
            </CardElement>

            <Pending.Suspense>
              <CardElement greyedOut>
                <SessionShowSmall
                  preview
                  style={{background: "#fafafa"}}
                  session={{...session, numberParticipants: session.registrations?.length}}
                  registrations={registrations}
                />
              </CardElement>
            </Pending.Suspense>

            {currentProject.useAI && (
              <CardElement title="Créer avec l'IA">
                <SwitchInput label="Session figée" name="isScheduleFrozen" />
              </CardElement>
            )}
          </div>
        </div>

        <div
          className={
            "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
          }>
          <div className="containerV" style={{flexBasis: "33%"}}>
            <TableElement.WithTitle
              title="Plages"
              icon={<ClockCircleOutlined />}
              buttonTitle="Ajouter une plage"
              onClickButton={openModalSlots}
              columns={columnsSlots}
              onEdit={editSlot}
              onDelete={removeSlot}
              dataSource={sessionSlotsTable}
            />
          </div>

          <div className="containerV" style={{flexBasis: "33%"}}>
            <TableElement.WithTitle
              title="Encadrant⋅es"
              icon={<UserOutlined />}
              buttonTitle="Ajouter un⋅e encadrant⋅e"
              subtitle={
                insufficientNumberOfStewards && (
                  <Alert
                    message="Le nombre d'encadrant⋅es est inférieur au nombre minimal."
                    type="warning"
                    showIcon
                  />
                )
              }
              onClickButton={openModalStewards}
              columns={columnsStewards}
              navigableRootPath="../stewards"
              onDelete={removeSteward}
              dataSource={sessionStewardsTable}
            />
          </div>

          {currentProject.usePlaces && (
            <div className="containerV" style={{flexBasis: "33%"}}>
              <TableElement.WithTitle
                title="Espaces"
                icon={<EnvironmentOutlined />}
                subtitle={
                  numberOfParticipantsSetIsAbovePlacesCapacity && (
                    <Alert
                      message={`La jauge finale de la session est contrainte par la taille des espaces définis dans la session.
                 La jauge de participants définie dans ${
                   sessionMaxIsDefined ? "la session" : "l'activité"
                 } est contrainte, et la jauge finale sera de ${maxParticipantsBasedOnPlaces}.`}
                      type="warning"
                      showIcon
                    />
                  )
                }
                buttonTitle="Ajouter un espace"
                onClickButton={openModalPlaces}
                onDelete={removePlace}
                columns={columnsPlaces}
                navigableRootPath="../places"
                dataSource={sessionPlacesTable}
              />
            </div>
          )}
        </div>

        <div className="container-grid">
          <div className="containerV" style={{flexBasis: "33%", overflow: "hidden"}}>
            <TableElement.WithTitle
              title="Participant⋅es"
              icon={<TeamOutlined />}
              subtitle="En rouge clair figurent les participant⋅es qui ne sont pas encore arrivé⋅es à l'événement."
              showHeader
              buttonTitle="Ajouter un⋅e participant⋅e"
              onClickButton={() => setShowModalRegistrations(true)}
              onDelete={removeRegistration}
              rowClassName={(record) =>
                record.hasCheckedIn ? "" : " ant-table-row-danger ant-table-row-italic"
              }
              navigableRootPath="../participants"
              columns={[...sessionRegistrationsColumns, ...additionalRegistrationsColumns]}
              dataSource={addKeyToItemsOfList(session.registrations)}
            />
          </div>
          <ElementSelectionModal
            title="Ajouter des participant⋅es à la session"
            open={showModalRegistrations}
            large
            onOk={addRegistrations}
            onCancel={() => setShowModalRegistrations(false)}
            searchInFields={searchInRegistrationFields}
            rowSelection={rowSelectionRegistration}
            selectedRowKeys={addingRegistrations}
            setSelectedRowKeys={setAddingRegistrations}
            columns={sessionRegistrationsColumns}
            dataSource={addKeyToItemsOfList(registrationsAddable)}
          />
        </div>
      </EditPage>

      <ElementSelectionModal
        title="Ajouter des encadrant⋅es en charge de cette session"
        open={showModalStewards}
        subtitle={
          session.activity?.stewards?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les encadrant⋅es éligibles de l'activité ont été
              pré-sélectionné⋅es pour plus de facilités.
            </>
          ) : undefined
        }
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            Créer un⋅e encadrant⋅e
          </Button>
        }
        onOk={() => {
          addStewards();
          setShowModalStewards(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalStewards(false)}
        searchInFields={["firstName", "lastName"]}
        rowSelection={rowSelectionSteward}
        selectedRowKeys={addingSteward}
        setSelectedRowKeys={setAddingSteward}
        columns={columnsStewards}
        dataSource={stewardsAddableTable}
      />
      <ElementSelectionModal
        title="Ajouter les espaces utilisés pour cette session"
        open={showModalPlaces}
        subtitle={
          session.activity?.places?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les espaces éligibles de l'activité ont été pré-sélectionnés
              pour plus de facilités.
            </>
          ) : undefined
        }
        customButtons={
          <Button type="link" onClick={() => setShowNewPlaceModal(true)}>
            Créer un espace
          </Button>
        }
        onOk={() => {
          addPlaces();
          setShowModalPlaces(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalPlaces(false)}
        searchInFields={["name"]}
        rowSelection={rowSelectionPlace}
        selectedRowKeys={addingPlace}
        setSelectedRowKeys={setAddingPlace}
        columns={columnsPlaces}
        dataSource={placesAddableTable}
      />
      <Modal
        title="Éditer une plage"
        open={showModalSlot}
        onOk={onValidateSlot}
        onCancel={() => {
          setCurrentSlot(undefined);
          setShowModalSlot(false);
        }}
        width={"min(90vw, 1200px)"}
        zIndex={100} /* cause otherwise the ElementsSelectionModals are behind it */
      >
        <FormElement
          form={slotEditingForm}
          fields={dataToFields(currentSlot)}
          onFieldsChange={onChangeSlot}
          validateAction={onValidateSlot}>
          <div className="container-grid two-thirds-one-third">
            <CardElement>
              <div className="container-grid two-per-row">
                <DateTimeInput
                  disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
                  label="Début"
                  name="start"
                  rules={[{required: true}]}
                />

                <div>
                  <NumberInput
                    label="Durée (minutes)"
                    name="duration"
                    min="1"
                    placeholder="durée"
                    rules={[{required: true}, {type: "number"}]}
                  />
                  {currentSlot?.duration && (
                    <div className="fade-in">
                      Ce qui représente {listRenderer.durationFormat(currentSlot.duration)}
                    </div>
                  )}
                </div>
              </div>
            </CardElement>
            <InputElement.Custom label="Récapitulatif des horaires">
              {currentSlot?.start && currentSlot?.end && currentSlot?.duration ? (
                listRenderer.longDateTimeRangeFormat(currentSlot.start, currentSlot.end)
              ) : (
                <div style={{color: "grey"}}>
                  Veuillez renseigner une date de début et une durée.
                </div>
              )}{" "}
            </InputElement.Custom>
          </div>

          <Collapse
            defaultActiveKey={
              !currentSlot?.stewardsSessionSynchro || !currentSlot?.placesSessionSynchro
                ? ["1"]
                : []
            }>
            <Collapse.Panel header="Synchronisation des encadrant⋅es et espaces (avancé)" key="1">
              <div className="container-grid two-per-row">
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <SwitchInput
                    label="Synchroniser les encadrant⋅es"
                    name="stewardsSessionSynchro"
                  />
                  {!currentSlot?.stewardsSessionSynchro && (
                    <TableElement.WithTitle
                      title="Encadrant⋅es"
                      buttonTitle="Ajouter"
                      onClickButton={
                        currentSlot && !currentSlot.stewardsSessionSynchro && openModalStewards
                      }
                      columns={columnsStewards}
                      onDelete={currentSlot && !currentSlot.stewardsSessionSynchro && removeSteward}
                      dataSource={currentSlotStewardsTable}
                    />
                  )}
                </div>

                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <SwitchInput label="Synchroniser les espaces" name="placesSessionSynchro" />
                  {!currentSlot?.placesSessionSynchro && (
                    <TableElement.WithTitle
                      title="Espaces"
                      buttonTitle="Ajouter"
                      onClickButton={
                        currentSlot && !currentSlot.placesSessionSynchro && openModalPlaces
                      }
                      columns={columnsPlaces}
                      onDelete={currentSlot && !currentSlot.placesSessionSynchro && removePlace}
                      dataSource={currentSlotPlacesTable}
                    />
                  )}
                </div>
              </div>
            </Collapse.Panel>
          </Collapse>
        </FormElement>
      </Modal>

      <NewActivityModal />
      <NewStewardModal />
      {currentProject.usePlaces && <NewPlaceModal />}
      {currentProject.useTeams && <NewTeamModal />}
    </>
  );
}

export default SessionEdit;
