import React, {lazy, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {Alert, Button, Collapse, Form, Modal} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement, safeValidateFields} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {CardElement} from "../common/layout/CardElement";
import {EditPage} from "../common/EditPage";
import {useNewElementModal} from "../../helpers/editingUtilities";
import {listRenderer} from "../../helpers/listUtilities";
import {
  addKeyToItemsOfList,
  columnsPlaces,
  columnsStewards,
  dataToFields,
  fieldToData,
  generateSessionsColumns,
} from "../../helpers/tableUtilities";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {navigate} from "@reach/router";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {ColorDot} from "../categories/utilsCategory";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";
import {Trans, useTranslation} from "react-i18next";
import {
  BookOutlined,
  ClockCircleOutlined,
  EnvironmentOutlined,
  QuestionCircleOutlined,
  ScheduleOutlined,
  TagOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {TextInput} from "../common/inputs/TextInput";
import {NumberInput} from "../common/inputs/NumberInput";
import {SwitchInput} from "../common/inputs/SwitchInput";
import {ElementSelectionModal} from "../common/layout/ElementSelectionModal";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);
const CategoryEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../categories/CategoryEdit.js")
);
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../places/PlaceEdit.js"));

function ActivityEdit({id, location, asModal, modalOpen, setModalOpen, onCreate}) {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const activity = useSelector(activitiesSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const dispatch = useDispatch();
  const [showModalSteward, setShowModalSteward] = useState(false);
  const [showModalPlace, setShowModalPlace] = useState(false);
  const [showModalSlot, setShowModalSlot] = useState(false);
  const [setShowNewCategoryModal, NewCategoryModal] = useNewElementModal(CategoryEdit);
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const [setShowNewPlaceModal, NewPlaceModal] = useNewElementModal(PlaceEdit);
  const [addingSteward, setAddingSteward] = useState([]);
  const [addingPlace, setAddingPlace] = useState([]);
  const [currentSlot, setCurrentSlot] = useState();
  const [slotForm] = Form.useForm();
  const [isModified, setIsModified] = useState(false);
  const [activitySessionsHelpCollapseKeys, setActivitySessionsHelpCollapseKeys] =
    useLocalStorageState("activitySessionsHelpCollapseKeys", false);

  const activitySessions = sessions.filter((s) => s.activity?._id === activity._id);

  const insufficientNumberOfStewards =
    activity && activity.stewards?.length < activity.minNumberOfStewards;

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    activitiesActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(categoriesActions.loadList());
      dispatch(sessionsActions.loadList());
      dispatch(activitiesActions.loadList()); // For tags
    },
    clonedElement
  );

  const stewardsAddable =
    activity.stewards === undefined
      ? []
      : stewards.filter((s) => activity.stewards.filter((as) => as._id === s._id).length === 0);
  const placesAddable =
    activity.places === undefined
      ? []
      : places.filter((p) => activity.places.filter((ap) => ap._id === p._id).length === 0);

  const activitySlotsTable = addKeyToItemsOfList(activity.slots);
  const columnsSlots = [{dataIndex: "duration", render: listRenderer.durationFormat}];

  const rowSelectionSteward = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingSteward(selectedRowObject);
    },
  };
  const rowSelectionPlace = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingPlace(selectedRowObject);
    },
  };

  const addStewards = () => {
    dispatch(activitiesActions.addStewardsToEditing(addingSteward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const addPlaces = () => {
    dispatch(activitiesActions.addPlacesToEditing(addingPlace));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSteward = (steward) => {
    dispatch(activitiesActions.removeStewardToEditing(steward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const removePlace = (place) => {
    dispatch(activitiesActions.removePlaceToEditing(place));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSlot = (slot) => {
    let slots = [...activitySlotsTable.filter((r) => r.key !== slot.key)];
    dispatch(activitiesActions.changeSlotsToEditing(slots));
    setIsModified(true);
  };

  const onChangeSlot = (changedFields, allFields) =>
    setCurrentSlot({...currentSlot, ...fieldToData(allFields)});

  const onValidateSlot = () =>
    safeValidateFields(slotForm, () => {
      let slots;
      if (currentSlot.key === undefined) {
        slots = [...activitySlotsTable, currentSlot];
      } else {
        slots = [...activitySlotsTable];
        slots[slots.findIndex((r) => r.key === currentSlot.key)] = currentSlot;
      }
      dispatch(activitiesActions.changeSlotsToEditing(slots));

      setCurrentSlot(undefined);
      slotForm.resetFields(["duration"]);
      setShowModalSlot(false);
      setIsModified(true);
    });

  const activityFieldToData = (fields) => {
    let data = fieldToData(fields);
    const categoryIndex = categories.map((d) => d._id).indexOf(data.category);
    if (categoryIndex >= 0) {
      data.category = {...categories[categoryIndex]};
    }
    return data;
  };

  return (
    <>
      <EditPage
        i18nNs="activities"
        icon={<BookOutlined />}
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        deletable
        onCreate={onCreate}
        elementsActions={activitiesActions}
        record={activity}
        initialValues={{
          ...activity,
          category: activity.category?._id,
          stewardVolunteeringCoefficient: activity.stewardVolunteeringCoefficient || 1.5,
        }}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["category"]}
        fieldToData={activityFieldToData}
        groupEditing={groupEditing}>
        <CardElement>
          <div className="container-grid three-per-row">
            <TextInput
              i18nNs="activities"
              name="name"
              rules={[
                {required: true},
                {
                  max: 75,
                  message: t("activities:schema.name.tooLong"),
                  warningOnly: true,
                },
              ]}
            />

            <InputElement.WithEntityLinks
              endpoint="categories"
              entity={activity.category}
              createButtonText={t("categories:label", {context: "createNew"})}
              setShowNewEntityModal={setShowNewCategoryModal}>
              <InputElement.Select
                icon={<TagOutlined />}
                label={t("categories:label")}
                name="category"
                placeholder={t("categories:label").toLowerCase()}
                rules={[{required: true}]}
                options={categories.map((m) => ({
                  value: m._id,
                  label: (
                    <div className="containerH" style={{alignItems: "center"}}>
                      <ColorDot
                        color={m.color}
                        size={15}
                        style={{marginBottom: 1, marginRight: 8}}
                      />
                      {m.name}
                    </div>
                  ),
                }))}
                showSearch
              />
            </InputElement.WithEntityLinks>

            <InputElement.TagsSelect
              i18nNs="activities"
              name="secondaryCategories"
              elementsSelectors={activitiesSelectors}
            />

            <SwitchInput i18nNs="activities" name="allowSameTime" />

            <NumberInput
              i18nNs="activities"
              tooltip={"↘ Champ re-définissable dans la session."}
              name="volunteeringCoefficient"
              step={0.1}
              min={0}
              max={5}
            />

            <NumberInput
              i18nNs="activities"
              name="stewardVolunteeringCoefficient"
              step={0.1}
              min={0}
              max={5}
            />

            <NumberInput
              i18nNs="activities"
              name="maxNumberOfParticipants"
              tooltip={
                <>
                  <p>↘ Champ re-définissable dans la session.</p>
                  Valeurs spéciales:
                  <ul>
                    <li>Rien = inscription libre</li>
                    <li>
                      0 = Les sessions de l'activité seront invisibles pour les participant.es,
                      seulement visibles par les orgas et les personnes inscrites
                    </li>
                  </ul>
                </>
              }
              min={0}
            />
          </div>
        </CardElement>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <InputElement.Editor i18nNs="activities" name="description" />

            <TextInput.Area
              i18nNs="activities"
              name="summary"
              rules={[
                {max: 250, message: <Trans i18nKey="schema.summary.tooLong" ns="activities" />},
              ]}
            />

            <InputElement.Editor i18nNs="activities" name="notes" />

            <InputElement.TagsSelect
              i18nNs="activities"
              name="tags"
              elementsSelectors={activitiesSelectors}
            />
          </div>
        </CardElement>

        {currentProject.useAI && (
          <CardElement title={t("activities:createWithIA")}>
            <div className="container-grid three-per-row">
              <NumberInput
                i18nNs="activities"
                name="numberOfSessions"
                min={1}
                rules={[{required: true}]}
              />
              <NumberInput i18nNs="activities" name="minNumberOfStewards" min={0} />
              <SwitchInput i18nNs="activities" name="nonBlockingActivity" />
            </div>
          </CardElement>
        )}

        <Collapse
          style={{marginBottom: 26}}
          onChange={setActivitySessionsHelpCollapseKeys}
          activeKey={currentProject.useAI ? ["sessions-help"] : activitySessionsHelpCollapseKeys}>
          <Collapse.Panel
            header={t("activities:helpForSessionsCreation.title")}
            key="sessions-help">
            <Alert
              message={
                <>
                  <Trans
                    ns="activities"
                    i18nKey="helpForSessionsCreation.information"
                    components={{questionMarkIcon: <QuestionCircleOutlined />}}
                  />
                  {currentProject.useAI && t("activities:helpForSessionsCreation.informationUseAI")}
                </>
              }
              style={{marginBottom: 20}}
            />

            <div
              style={{marginBottom: -26}}
              className={
                "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
              }>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title={t("activities:schema.slots.label")}
                  icon={<ClockCircleOutlined />}
                  buttonTitle={t("activities:schema.slots.buttonTitle")}
                  tooltip={t("activities:schema.slots.tooltip")}
                  onClickButton={() => {
                    setCurrentSlot(undefined);
                    setShowModalSlot(true);
                  }}
                  onEdit={(record) => {
                    setCurrentSlot(record);
                    setShowModalSlot(true);
                  }}
                  onDelete={removeSlot}
                  columns={columnsSlots}
                  dataSource={activitySlotsTable}
                />
              </div>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title={t("activities:schema.stewards.label")}
                  icon={<UserOutlined />}
                  buttonTitle={t("activities:schema.stewards.buttonTitle")}
                  subtitle={
                    insufficientNumberOfStewards && (
                      <Alert
                        message={t("activities:schema.stewards.numberOfStewardsIsBelowMinimum")}
                        type="warning"
                        showIcon
                      />
                    )
                  }
                  tooltip={t("activities:schema.stewards.tooltip")}
                  onClickButton={() => setShowModalSteward(true)}
                  onDelete={removeSteward}
                  columns={columnsStewards}
                  navigableRootPath="../stewards"
                  dataSource={activity.stewards}
                />
              </div>
              {currentProject.usePlaces && (
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <TableElement.WithTitle
                    title={t("activities:schema.places.label")}
                    icon={<EnvironmentOutlined />}
                    buttonTitle={t("activities:schema.places.buttonTitle")}
                    tooltip={t("activities:schema.places.tooltip")}
                    onClickButton={() => setShowModalPlace(true)}
                    onDelete={removePlace}
                    columns={columnsPlaces}
                    navigableRootPath="../places"
                    dataSource={activity.places}
                  />
                </div>
              )}
            </div>
          </Collapse.Panel>
        </Collapse>

        {activity._id !== "new" && (
          <TableElement.WithTitle
            title={t("activities:schema.sessions.label")}
            icon={<ScheduleOutlined />}
            showHeader
            buttonTitle={t("activities:schema.sessions.buttonTitle")}
            onClickButton={() => {
              dispatch(
                sessionsActions.changeEditing({
                  _id: "new",
                  activity,
                  places: [],
                  stewards: [],
                  slots: [],
                })
              );
              navigate("../sessions/new");
            }}
            navigableRootPath="../sessions"
            columns={generateSessionsColumns("../..", currentProject.usePlaces, true, true)}
            dataSource={activitySessions}
          />
        )}
      </EditPage>

      {/* MODALS */}

      <Modal
        title={t("activities:schema.slots.editModal.title")}
        open={showModalSlot}
        onOk={onValidateSlot}
        onCancel={() => {
          slotForm.resetFields(["duration"]);
          setShowModalSlot(false);
          setCurrentSlot(undefined);
        }}>
        <FormElement
          form={slotForm}
          fields={dataToFields(currentSlot)}
          onFieldsChange={onChangeSlot}
          validateAction={onValidateSlot}>
          <div className="container-grid">
            <NumberInput
              autoFocus={true}
              label={t("activities:schema.slots.editModal.duration.label")}
              placeholder={t("activities:schema.slots.editModal.duration.placeholder")}
              name="duration"
              min={1}
              rules={[{required: true}, {type: "number"}]}
            />
            {currentSlot?.duration && (
              <div className="fade-in">
                {t("activities:schema.slots.editModal.duration.whichRepresents", {
                  duration: listRenderer.durationFormat(currentSlot.duration),
                })}
              </div>
            )}
          </div>
        </FormElement>
      </Modal>

      <ElementSelectionModal
        title={t("activities:schema.slots.editModal.stewards.label")}
        open={showModalSteward}
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            {t("activities:schema.slots.editModal.stewards.buttonTitle")}
          </Button>
        }
        onOk={() => {
          addStewards();
          setShowModalSteward(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalSteward(false)}
        rowSelection={rowSelectionSteward}
        searchInFields={["firstName", "lastName"]}
        selectedRowKeys={addingSteward}
        setSelectedRowKeys={setAddingSteward}
        columns={columnsStewards}
        dataSource={stewardsAddable}
      />

      <ElementSelectionModal
        title={t("activities:schema.slots.editModal.places.label")}
        open={showModalPlace}
        customButtons={
          <Button type="link" onClick={() => setShowNewPlaceModal(true)}>
            {t("activities:schema.slots.editModal.places.buttonTitle")}
          </Button>
        }
        onOk={() => {
          addPlaces();
          setShowModalPlace(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalPlace(false)}
        rowSelection={rowSelectionPlace}
        searchInFields={["name"]}
        selectedRowKeys={addingPlace}
        setSelectedRowKeys={setAddingPlace}
        columns={columnsPlaces}
        dataSource={placesAddable}
      />

      <NewCategoryModal />
      <NewStewardModal />
      {currentProject.usePlaces && <NewPlaceModal />}
    </>
  );
}

export default ActivityEdit;
