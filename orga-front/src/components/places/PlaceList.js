import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {placesActions, placesSelectors} from "../../features/places.js";

import {ListPage} from "../common/ListPage";
import {listSorter, useLoadList} from "../../helpers/listUtilities";
import {EnvironmentOutlined} from "@ant-design/icons";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";

function PlaceList({navigate}) {
  const dispatch = useDispatch();
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelector();
  const places = useSelector(placesSelectors.selectList);

  const columns = [
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
    {
      title: "Nombre maximum de personnes",
      dataIndex: "maxNumberOfParticipants",
      sorter: (a, b) => listSorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      searchable: true,
    },
  ];

  useLoadList(() => {
    dispatch(placesActions.loadList());
  });

  return (
    <ListPage
      icon={<EnvironmentOutlined />}
      searchInFields={["name"]}
      i18nNs="places"
      elementsActions={placesActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      navigateFn={navigate}
      columns={filterBlacklistedColumns(columns)}
      dataSource={places}
      groupEditable
      groupImportable
    />
  );
}

export default PlaceList;
