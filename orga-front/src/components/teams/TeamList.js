import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "../../features/teams.js";
import {ListPage} from "../common/ListPage";
import {generateColumnsTeams} from "../../helpers/tableUtilities";
import {TeamOutlined} from "@ant-design/icons";
import {useLoadList} from "../../helpers/listUtilities";

function TeamList({navigate}) {
  const teams = useSelector(teamsSelectors.selectList);
  const dispatch = useDispatch();

  useLoadList(() => {
    dispatch(teamsActions.loadList());
  });

  return (
    <ListPage
      icon={<TeamOutlined />}
      i18nNs="teams"
      searchInFields={["name"]}
      elementsActions={teamsActions}
      navigateFn={navigate}
      columns={generateColumnsTeams("..")}
      dataSource={teams}
      groupEditable
      groupImportable
    />
  );
}

export default TeamList;
