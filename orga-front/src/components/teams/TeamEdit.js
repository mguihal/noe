import React, {lazy, useEffect, useMemo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "../../features/teams.js";
import {Button, Switch} from "antd";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {useNewElementModal} from "../../helpers/editingUtilities";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {activitiesActions, activitiesSelectors} from "../../features/activities";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {
  fieldToData,
  generateRegistrationsColumns,
  generateSessionsColumns,
  generateSubscriptionInfo,
  registrationDispoColumn,
  searchInRegistrationFields,
} from "../../helpers/tableUtilities";
import {CardElement} from "../common/layout/CardElement";
import {TableElement} from "../common/TableElement";
import {currentProjectSelectors} from "../../features/currentProject";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";
import {BookOutlined, ScheduleOutlined, TeamOutlined} from "@ant-design/icons";
import {TextInput} from "../common/inputs/TextInput";
import {ElementSelectionModal} from "../common/layout/ElementSelectionModal";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";

const SessionEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../sessions/SessionEdit")
);

function TeamEdit({id, location, asModal, modalOpen, setModalOpen, onCreate}) {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const team = useSelector(teamsSelectors.selectEditing);
  const activities = useSelector(activitiesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const [filterBlacklistedSessionsColumns] = useColumnsBlacklistingSelector("sessions");
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelector("participants");
  const [isModified, setIsModified] = useState(false);
  const [setShowNewSessionModal, NewSessionModal] = useNewElementModal(SessionEdit);
  const [showModalSessions, setShowModalSessions] = useState(false);
  const [addingSessions, setAddingSessions] = useState([]);
  const [filterSessionsWithLinkedActivity, setFilterSessionsWithLinkedActivity] = useState(true);

  const [filterRegistrationsWithAlreadyATeam, setFilterRegistrationsWithAlreadyATeam] =
    useState(true);
  const [showModalRegistrations, setShowModalRegistrations] = useState(false);
  const [addingRegistrations, setAddingRegistrations] = useState([]);

  const teamSessions = sessions.filter((s) => s.team?._id === team._id);
  const teamRegistrations = registrations.filter((r) =>
    r.teamsSubscriptions?.find((ts) => ts.team._id === team._id)
  );

  // All the elements that are available, and not selected yet
  const sessionsAddable = sessions.filter(
    (s) => !s.team?._id && !team.sessions?.find((selected) => selected._id === s._id)
  );
  const registrationsAddable = registrations.filter(
    (r) => !team.registrations?.find((selected) => selected._id === r._id)
  );

  const columnsSessionsBase = useMemo(
    () =>
      filterBlacklistedSessionsColumns(
        generateSessionsColumns("../..", currentProject.usePlaces, false, true)
      ),
    [currentProject]
  );
  const columnsSessionsSimple = useMemo(
    () =>
      filterBlacklistedParticipantsColumns(
        generateSessionsColumns("../..", currentProject.usePlaces, false, true, true)
      ),
    [currentProject]
  );

  const groupEditing = location?.state?.groupEditing;

  useLoadEditing(teamsActions, id, () => {
    dispatch(activitiesActions.loadList());
    dispatch(sessionsActions.loadList());
    dispatch(registrationsActions.loadList());
  });

  useEffect(() => {
    if (team._id) dispatch(teamsActions.changeEditing({sessions: teamSessions}));
  }, [JSON.stringify(teamSessions)]);

  useEffect(() => {
    if (team._id) dispatch(teamsActions.changeEditing({registrations: teamRegistrations}));
  }, [JSON.stringify(teamRegistrations)]);

  const rowSelectionSession = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingSessions(selectedRowObject);
    },
  };
  const rowSelectionRegistration = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingRegistrations(selectedRowObject);
    },
  };

  const addSessions = () => {
    dispatch(teamsActions.changeEditing({sessions: [...(team.sessions || []), ...addingSessions]}));
    setAddingSessions([]);
    setIsModified(true);
    setShowModalSessions(false);
  };
  const removeSession = (session) => {
    dispatch(
      teamsActions.changeEditing({sessions: team.sessions?.filter((s) => s._id !== session._id)})
    );
    setAddingSessions([]);
    setIsModified(true);
  };

  const addRegistrations = () => {
    dispatch(
      teamsActions.changeEditing({
        registrations: [...(team.registrations || []), ...addingRegistrations],
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
    setShowModalRegistrations(false);
  };
  const removeRegistration = (registration) => {
    dispatch(
      teamsActions.changeEditing({
        registrations: team.registrations?.filter((r) => r._id !== registration._id),
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
  };

  const teamFieldToData = (fields) => {
    let data = fieldToData(fields);
    if (typeof data.activity === "string") {
      data.activity = activities.find((activity) => activity._id === data.activity);
    }
    return data;
  };

  const teamRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(generateRegistrationsColumns(currentProject)),
    registrationDispoColumn,
  ];
  const teamSubscriptionInfoColumn = {
    title: "Inscription",
    render: (text, record) => {
      const teamSubscription = record.teamsSubscriptions.find((ss) => ss.team._id === team._id);
      return generateSubscriptionInfo(record, teamSubscription, registrations);
    },
  };

  return (
    <>
      <EditPage
        icon={<TeamOutlined />}
        i18nNs="teams"
        deletable
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        onCreate={onCreate}
        elementsActions={teamsActions}
        record={team}
        initialValues={{...team, activity: team.activity?._id}}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["activity"]}
        fieldToData={teamFieldToData}
        groupEditing={groupEditing}>
        <CardElement>
          <div className="container-grid two-per-row">
            <TextInput
              label="Nom de l'équipe"
              name="name"
              placeholder="nom"
              rules={[{required: true}]}
            />

            <InputElement.WithEntityLinks endpoint="activities" entity={team.activity}>
              <InputElement.Select
                label="Activité liée"
                icon={<BookOutlined />}
                name="activity"
                placeholder="activité"
                options={[
                  {value: null, label: "- Pas d'activité liée -"},
                  ...activities.map((d) => ({value: d._id, label: d.name})),
                ]}
                showSearch
              />
            </InputElement.WithEntityLinks>
          </div>
        </CardElement>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <InputElement.Editor
              label="Description détaillée"
              tooltip={
                'Elle sera affichée seulement dans la vue "pleine page" de l\'activité. ' +
                "C'est ici que vous pouvez rédiger une description plus longue de ce qui attend les participant⋅e.es"
              }
              name="description"
              placeholder="description de l'activité"
            />

            <TextInput.Area
              label="Résumé"
              name="summary"
              tooltip="Il sera affiché dans la liste des sessions et devra être très court."
              placeholder="résumé court (idéalement une quinzaine de mots)"
              rules={[
                {
                  max: 250,
                  message: (
                    <>
                      Votre résumé est trop long. ne sera pas affiché de manière optimale dans
                      l'interface utilisateur⋅ice. Considérez le résumé comme un sous-titre court
                      qui décrit en quelques mots ce qu'est l'activité. Si vous voulez donner des
                      détails sur l'activité, utilisez la <strong>Description détaillée</strong>.
                    </>
                  ),
                },
              ]}
            />

            <InputElement.Editor
              label="Notes privées pour les orgas"
              name="notes"
              placeholder="notes privées"
              tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
            />
          </div>
        </CardElement>

        <TableElement.WithTitle
          title="Sessions de l'équipe"
          icon={<ScheduleOutlined />}
          subtitle="En rouge clair figurent les sessions qui n'appartiennent pas à l'activité liée choisie."
          showHeader
          buttonTitle="Ajouter une session"
          onClickButton={() => setShowModalSessions(true)}
          onDelete={removeSession}
          rowClassName={(record) =>
            record.activity._id === team.activity?._id ? "" : "ant-table-row-danger"
          }
          navigableRootPath="../sessions"
          columns={columnsSessionsBase}
          dataSource={team.sessions}
        />
        <ElementSelectionModal
          title="Ajouter des sessions à l'équipe"
          open={showModalSessions}
          subtitle={
            team.activity && (
              <div className="containerH">
                <strong>Sessions de l'activité liée seulement</strong>
                <Switch
                  style={{marginLeft: 8}}
                  checked={filterSessionsWithLinkedActivity}
                  onChange={(value) => setFilterSessionsWithLinkedActivity(value)}
                />
              </div>
            )
          }
          large
          customButtons={
            <Button type="link" onClick={() => setShowNewSessionModal(true)}>
              Créer une session
            </Button>
          }
          onOk={addSessions}
          onCancel={() => setShowModalSessions(false)}
          rowSelection={rowSelectionSession}
          selectedRowKeys={addingSessions}
          setSelectedRowKeys={setAddingSessions}
          columns={columnsSessionsSimple}
          dataSource={
            team.activity && filterSessionsWithLinkedActivity
              ? sessionsAddable.filter((s) => s.activity._id === team?.activity._id)
              : sessionsAddable
          }
        />

        <TableElement.WithTitle
          title="Membres de l'équipe"
          icon={<TeamOutlined />}
          subtitle="En rouge clair figurent les participant⋅es qui ne sont pas inscrit⋅es à toutes les sessions prévues pour l'équipe."
          showHeader
          buttonTitle="Ajouter un⋅e membre"
          onClickButton={() => setShowModalRegistrations(true)}
          onDelete={removeRegistration}
          rowClassName={(record) =>
            record.sessionsSubscriptions.filter((ss) => ss.team === team._id).length ===
            team.sessions?.length
              ? ""
              : "ant-table-row-danger"
          }
          navigableRootPath="../participants"
          columns={[...teamRegistrationsColumns, teamSubscriptionInfoColumn]}
          dataSource={team.registrations}
        />
        <ElementSelectionModal
          title="Ajouter des membres à l'équipe"
          open={showModalRegistrations}
          subtitle={
            <div className="containerH">
              <strong>Participant⋅es sans équipe seulement</strong>
              <Switch
                style={{marginLeft: 8}}
                checked={filterRegistrationsWithAlreadyATeam}
                onChange={(value) => setFilterRegistrationsWithAlreadyATeam(value)}
              />
            </div>
          }
          large
          onOk={addRegistrations}
          onCancel={() => setShowModalRegistrations(false)}
          searchInFields={searchInRegistrationFields}
          rowSelection={rowSelectionRegistration}
          selectedRowKeys={addingRegistrations}
          setSelectedRowKeys={setAddingRegistrations}
          columns={teamRegistrationsColumns}
          dataSource={
            filterRegistrationsWithAlreadyATeam
              ? registrationsAddable.filter(
                  (r) => !r.teamsSubscriptions || r.teamsSubscriptions?.length === 0
                )
              : registrationsAddable
          }
        />
      </EditPage>

      <NewSessionModal />
    </>
  );
}

export default TeamEdit;
