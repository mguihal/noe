import {useDispatch, useSelector} from "react-redux";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import React, {lazy, useEffect, useState} from "react";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {ClosableAlert} from "../common/layout/ClosableAlert";
import {CardElement} from "../common/layout/CardElement";
import {Button, Select} from "antd";
import {EditPage} from "../common/EditPage";
import {AreaChartOutlined, DashboardOutlined, ShareAltOutlined} from "@ant-design/icons";
import Paragraph from "antd/es/typography/Paragraph";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {categoriesActions, categoriesSelectors} from "../../features/categories";
import {activitiesActions, activitiesSelectors} from "../../features/activities";
import {placesActions, placesSelectors} from "../../features/places";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import {getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {listRenderer, useLoadList} from "../../helpers/listUtilities";
import {InputElement} from "../common/InputElement";
import dayjs from "dayjs";
import {InsightCard} from "./InsightCard";
import {groupBy} from "lodash";
import {Link} from "@reach/router";
import {URLS} from "../../app/configuration";
import {Trans, useTranslation} from "react-i18next";
import {Pending} from "../common/layout/Pending";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const Pie = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@ant-design/plots").then((module) => ({
    default: module["Pie"],
  }))
);
const Sunburst = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@ant-design/plots").then((module) => ({
    default: module["Sunburst"],
  }))
);

const ShareEventButton = () => {
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const [shareLinkEndPoint, setShareLinkEndPoint] = useState("welcome");
  const [displayShareLink, setDisplayShareLink] = useState(false);
  return displayShareLink ? (
    <div className="containerH buttons-container">
      <Select value={shareLinkEndPoint} onChange={setShareLinkEndPoint}>
        <Select.Option value="welcome">
          {t("cockpit:shareEventButton.options.welcome")}
        </Select.Option>
        <Select.Option value="login">{t("cockpit:shareEventButton.options.login")}</Select.Option>
      </Select>
      <Paragraph copyable style={{fontFamily: "monospace"}}>
        {`${URLS.INSCRIPTION_FRONT}/${project._id}/${shareLinkEndPoint}`}
      </Paragraph>
    </div>
  ) : (
    <Button onClick={() => setDisplayShareLink(true)} icon={<ShareAltOutlined />} type="link">
      {t("cockpit:shareEventButton.label")}
    </Button>
  );
};

export const ChartViz = React.memo(
  ({chart: Chart, config}) => {
    config = Object.assign(
      {interactions: [{type: "element-selected"}, {type: "element-active"}]},
      config
    );

    return (
      <Pending.Suspense>
        <Chart {...config} />
      </Pending.Suspense>
    );
  },
  (pp, np) => JSON.stringify(pp) === JSON.stringify(np)
);

export interface Insight<Element> {
  name: string;
  elements: Array<Element>;
  isPlot?: boolean;
  filter?: (element: Element) => boolean;
  noAvailableData?: boolean;
  calculateOnEach?: (element: Element) => any;
  calculateOnAll?: (elements: Array<Element>) => any;
  statType?: string;
  isGood?: (result: any) => 1 | -1 | 0;
  suffix?: string;
  precisions?: Array<Insight>;
  onClick?: boolean | ((event: MouseEvent) => void);
}

export function Cockpit() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  const sessions = useSelector(sessionsSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const stewards = useSelector(stewardsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

  const registrationsWithDaysOfPresence = registrations.filter(
    (registration) => registration.availabilitySlots?.length > 0
  );
  const currentUserAloneInTheProject =
    registrations.filter((el) => el._id !== currentRegistration._id).length === 0;

  const volunteeringSessionsWithMetadata = sessions
    .map((el) => ({
      ...el,
      duration: el.slots.reduce((acc, slot) => acc + slot.duration, 0),
      volunteeringCoefficient: getVolunteeringCoefficient(el, activities),
    }))
    .filter((el) => el.volunteeringCoefficient > 0);

  const totalAmountOfVolunteering = volunteeringSessionsWithMetadata.reduce(
    (acc, el) => acc + el.duration * el.computedMaxNumberOfParticipants,
    0
  );

  /**
   * REGISTRATIONS STATS
   */

  const registrationsGroupedByTypeAndRole = Object.entries(
    groupBy(registrations, (el) =>
      el.invitationToken
        ? t("cockpit:registrations.invitations")
        : !el.everythingIsOk
        ? el.booked === false
          ? t("cockpit:registrations.unregistrations")
          : t("cockpit:registrations.incomplete")
        : t("cockpit:registrations.complete")
    )
  ).map(([name, value]) => ({
    name,
    value: value.length,
    children: Object.entries(
      groupBy(value, (el) =>
        el.role ? t("cockpit:registrations.orga") : t("cockpit:registrations.nonOrga")
      )
    ).map(([name, value]) => ({name, value: value.length, children: null})),
  }));

  const averageNumberOfDaysOfPresence =
    registrationsWithDaysOfPresence
      .map((el) => el.numberOfDaysOfPresence)
      .reduce((acc, el) => acc + el, 0) / registrationsWithDaysOfPresence.length;

  const registrationsInsights = [
    {
      name: t("cockpit:registrations.label"),
      isPlot: true,
      calculateOnAll: () => ({
        name: t("cockpit:registrations.label"),
        children: registrationsGroupedByTypeAndRole,
      }),
      noAvailableData: currentUserAloneInTheProject,
      formatter: (data) => (
        <ChartViz
          chart={Sunburst}
          config={{
            data,
            innerRadius: 0.3,
            label: {formatter: ({value}) => value, autoRotate: false},
            color: (item) => {
              switch (item["ancestor-node"]) {
                case t("cockpit:registrations.complete"):
                  return "green";
                case t("cockpit:registrations.incomplete"):
                  return "red";
                case t("cockpit:registrations.unregistrations"):
                  return "darkred";
                case t("cockpit:registrations.invitations"):
                  return "#86c4ff";
                case t("cockpit:registrations.orga"):
                  return "grey";
                case t("cockpit:registrations.nonOrga"):
                  return "darkgrey";
                default:
                  return "grey";
              }
            },
          }}
        />
      ),
      precisions: [
        {
          name: t("cockpit:registrations.complete"),
          endpoint: "participants",
          noAvailableData: currentUserAloneInTheProject,
          calculateOnAll: () =>
            registrationsGroupedByTypeAndRole.find(
              ({name}) => name === t("cockpit:registrations.complete")
            )?.value,
        },
        {
          name: t("cockpit:registrations.incomplete"),
          endpoint: "participants",
          noAvailableData: currentUserAloneInTheProject,
          calculateOnAll: () =>
            registrationsGroupedByTypeAndRole.find(
              ({name}) => name === t("cockpit:registrations.incomplete")
            )?.value,
        },
        {
          name: `${t("cockpit:registrations.complete")} + ${t("cockpit:registrations.incomplete")}`,
          endpoint: "participants",
          elements: registrations,
          filter: (el) => el.booked !== false && !el.invitationToken, // Remove also people who unregistered + with invitation token
          noAvailableData: currentUserAloneInTheProject,
          statType: "length",
        },
      ],
    },
    {
      name: t("cockpit:registrations.averageLengthOfStayOnSite"),
      endpoint: "participants",
      noAvailableData: registrationsWithDaysOfPresence.length === 0,
      calculateOnAll: () => averageNumberOfDaysOfPresence,
      suffix: "jours",
    },
    {
      name: t("cockpit:registrations.averageSessionFillingWithoutVolunteering"),
      endpoint: "sessions",
      elements: sessions,
      filter: (el) =>
        el.computedMaxNumberOfParticipants > 0 && getVolunteeringCoefficient(el, activities) === 0,
      calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
      statType: "average",
      suffix: "%",
    },
  ];

  /**
   * VOLUNTEERING STATS
   */
  const volunteeringInsights = [
    {
      name: t("cockpit:volunteering.averageVolunteeringSessionsFilling"),
      endpoint: "sessions",
      elements: volunteeringSessionsWithMetadata,
      filter: (el) => el.computedMaxNumberOfParticipants > 0, // only sessions with at least one participant allowed
      calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
      statType: "average",
      suffix: "%",
      isGood: (val) => (val > 90 ? 1 : -1),
    },
    {
      name: t("cockpit:volunteering.amountToFillInTheEvent"),
      endpoint: "sessions",
      noAvailableData: volunteeringSessionsWithMetadata.length === 0,
      calculateOnAll: () => totalAmountOfVolunteering,
      formatter: (val) => listRenderer.durationFormat(val, true),
      precisions: [
        {
          name: t("cockpit:volunteering.alreadyFilled"),
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,
          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => listRenderer.durationFormat(val, true),
        },
        {
          name: `${t("cockpit:volunteering.alreadyFilled")} (%)`,
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,

          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => Math.round((val / totalAmountOfVolunteering) * 100),
          suffix: "%",
        },
      ],
    },
    {
      name: t("cockpit:volunteering.averageVolunteeringAmountPerParticipant"),
      endpoint: "participants",
      elements: registrations,
      noAvailableData: registrations.filter((el) => el.everythingIsOk).length === 0,
      filter: (el) => el.everythingIsOk,
      calculateOnEach: (el) => el.voluntaryCounter,
      statType: "average",
      formatter: (val) => listRenderer.durationFormat(val, true),
      isGood: (val) =>
        val <= project.minMaxVolunteering[0]
          ? -1 // Red if below
          : val >= project.minMaxVolunteering[1]
          ? 0 // Neutral if above
          : 1, // Green if inside
      suffix: "/ " + t("cockpit:day"),
    },
    {
      name: t("cockpit:volunteering.optimalVolunteeringAmountPerParticipant"),
      endpoint: "participants",
      noAvailableData:
        volunteeringSessionsWithMetadata.length === 0 ||
        registrationsWithDaysOfPresence.length === 0,
      calculateOnAll: () => {
        return (
          totalAmountOfVolunteering /
          (registrationsWithDaysOfPresence.length * averageNumberOfDaysOfPresence)
        );
      },
      formatter: (val) => listRenderer.durationFormat(val, true),
    },
  ];

  /**
   * SCHEDULING STATS
   */
  const schedulingInsights = [
    {
      name: t("categories:label", {count: Infinity}),
      isPlot: true,
      elements: categories,
      calculateOnEach: (el) => {
        const numberOfSessions = sessions.filter(
          (session) => session.activity.category._id === el._id
        ).length;
        const numberOfSessionsPercentage = numberOfSessions / sessions.length;
        return {...el, numberOfSessions, numberOfSessionsPercentage};
      },
      calculateOnAll: (elements) => {
        const sorted = elements.sort((a, b) => (a.numberOfSessions > b.numberOfSessions ? -1 : 1));
        return [
          ...sorted.slice(0, 6),
          {
            name: t("cockpit:schedulingAndSupervision.other"),
            numberOfSessions: sorted.slice(6).reduce((acc, el) => acc + el.numberOfSessions, 0),
            color: "#555555",
          },
        ];
      },
      noAvailableData: categories.length === 0,
      formatter: (data) => (
        <ChartViz
          chart={Pie}
          config={{
            data,
            radius: 0.85,
            height: 200,
            angleField: "numberOfSessions",
            colorField: "name",
            color: (el) => {
              return data.find((category) => category.name === el.name).color;
            },
          }}
        />
      ),
    },
    {
      name: t("activities:label", {count: Infinity}),
      endpoint: "activities",
      elements: activities,
      statType: "length",
      precisions: [
        {
          name: t("cockpit:schedulingAndSupervision.withAtLeastOneSessionCreated"),
          elements: activities,
          filter: (el) => sessions.find((session) => session.activity._id === el._id),
          statType: "length",
        },
      ],
    },
    {
      name: t("stewards:label", {count: Infinity}),
      endpoint: "stewards",
      elements: stewards,
      statType: "length",
      precisions: [
        {
          name: t("cockpit:schedulingAndSupervision.linkedToAParticipant"),
          elements: stewards,
          filter: (el) =>
            registrations.find((registration) => registration.steward?._id === el._id),
          statType: "length",
        },
      ],
    },
    {
      name: t("sessions:label", {count: Infinity}),
      endpoint: "sessions",
      elements: sessions,
      statType: "length",
    },
    {
      name: t("places:label", {count: Infinity}),
      endpoint: "places",
      elements: places,
      statType: "length",
    },
  ];

  useLoadList(() => {
    Promise.all([
      dispatch(sessionsActions.loadList()),
      dispatch(categoriesActions.loadList()),
      dispatch(activitiesActions.loadList()),
      dispatch(stewardsActions.loadList()),
      project.usePlaces && dispatch(placesActions.loadList()),
      dispatch(registrationsActions.loadList()),
    ]);
  });

  const currentHour = dayjs().hour();

  return (
    <EditPage
      icon={<DashboardOutlined />}
      noSaveButton
      title={
        (currentHour > 19 || currentHour < 4 ? t("cockpit:goodEvening") : t("cockpit:hello")) +
        " " +
        t("cockpit:welcomeInTheCockpit")
      }
      elementsActions={projectsActions}
      navigateAfterValidation={false}
      record={project}
      fieldsThatNeedReduxUpdate={["notes"]}
      initialValues={project}
      backButton={false}
      customButtons={<ShareEventButton />}>
      <ClosableAlert
        type="success"
        localStorageKey="cockpit-welcome"
        style={{marginBottom: 26}}
        message={t("cockpit:welcomeBannerAlert.message")}
        description={<Trans i18nKey="welcomeBannerAlert.description" ns="cockpit" />}
      />
      <div className="container-grid three-per-row">
        <CardElement title={t("projects:schema.notes.label")} readOnly>
          <InputElement.Editor name="notes" placeholder={t("cockpit:schema.notes.placeholder")} />
        </CardElement>
        <CardElement
          className="fade-in"
          title={t("cockpit:registrations.label")}
          subtitle={
            <p>
              <Link to="../participants#dashboard">
                <AreaChartOutlined style={{marginRight: 8}} />
                {t("cockpit:registrations.seeMoreFrequentationData")}
              </Link>
            </p>
          }>
          {registrationsInsights?.map((insight, index) => (
            <InsightCard key={index} {...insight} />
          ))}
        </CardElement>
        <CardElement className="fade-in" title={t("cockpit:volunteering.label")}>
          {volunteeringInsights?.map((insight, index) => (
            <InsightCard key={index} {...insight} />
          ))}
        </CardElement>
        <CardElement className="fade-in" title={t("cockpit:schedulingAndSupervision.label")}>
          <div className="container-grid three-per-row">
            {schedulingInsights?.map((insight, index) => (
              <InsightCard key={index} {...insight} />
            ))}
          </div>
        </CardElement>
      </div>
    </EditPage>
  );
}
