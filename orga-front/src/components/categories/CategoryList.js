import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {ListPage} from "../common/ListPage";
import {listSorter, useLoadList} from "../../helpers/listUtilities";
import {ColorDot} from "./utilsCategory";
import {useTranslation} from "react-i18next";
import {TagOutlined} from "@ant-design/icons";

function CategoryList({navigate}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const categories = useSelector(categoriesSelectors.selectList);

  const columns = [
    {
      title: t("categories:schema.color.label"),
      dataIndex: "color",
      width: "80pt",
      render: (text, record) => <ColorDot color={record.color} />,
    },
    {
      title: t("common:schema.name.label"),
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
  ];

  useLoadList(() => {
    dispatch(categoriesActions.loadList());
  });

  return (
    <ListPage
      icon={<TagOutlined />}
      i18nNs="categories"
      searchInFields={["name"]}
      elementsActions={categoriesActions}
      navigateFn={navigate}
      columns={columns}
      dataSource={categories}
      groupEditable
      groupImportable
    />
  );
}

export default CategoryList;
