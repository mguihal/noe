import React from "react";

export const ColorDot = ({size = 26, color, style}) => (
  <div
    className="containerH"
    style={{
      height: size,
      width: size,
      backgroundColor: color,
      borderRadius: "50%",
      ...style,
    }}
  />
);
