import {CardElement} from "../common/layout/CardElement";
import {Alert, Button, Divider, Form} from "antd";
import {FormElement} from "../common/FormElement";
import {MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";
import {InputElement} from "../common/InputElement";
import {flatFormInputs} from "../../helpers/registrationsUtilities";
import {TextInput} from "../common/inputs/TextInput";
import {BetaTag} from "../common/layout/BetaTag";
import {Pending} from "../common/layout/Pending";
import React, {useEffect} from "react";
import {listRenderer} from "../../helpers/listUtilities";
import dayjs from "dayjs";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {ClosableAlert} from "../common/layout/ClosableAlert";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const FormEditor = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./form/FormEditor"));

export const FormConfig = ({
  saveFormData,
  formBuilderRef,
  mappingForm,
  setIsModified,
  delaySaveTipMessage,
  messageNotYetDisplayed,
}) => {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);

  const smsMessageTags = {
    "{{sessionName}}": "Nom de la session",
    "{{startDate}} / {{endDate}}": listRenderer.longDateFormat(dayjs()),
    "{{startDateTime}} / {{endDateTime}}": listRenderer.longDateTimeFormat(dayjs()),
    "{{startTime}} / {{endTime}}": listRenderer.timeFormat(dayjs()),
  };

  const setIsModifiedAndSaveForm = (isModified) => {
    saveFormData();
    setIsModified(isModified);
    isModified && delaySaveTipMessage();
  };

  useEffect(() => {
    // When unmounting the component, save the form data not dispatched in Redux
    return saveFormData;
  }, []);

  const changeMapping = (changedFields, {formMapping}) => {
    dispatch(projectsActions.changeEditing({formMapping}));
    formMapping.length > 0 && setIsModifiedAndSaveForm(true);
  };

  return (
    <>
      <CardElement title="Mapping des champs de formulaire" className={"userTourFormMapping"}>
        <Alert
          style={{marginBottom: 26}}
          description={
            <>
              Le mapping des champs de formulaire vous permet d'afficher certaines réponses aux
              questions de votre formulaire d'inscription dans la liste des participant⋅es partout
              dans NOÉ.
            </>
          }
        />
        {project.formComponents?.length > 0 ? (
          <FormElement form={mappingForm} onValuesChange={changeMapping} initialValues={project}>
            <Form.List name="formMapping">
              {(fields, {add, remove}) => (
                <>
                  {fields.map(({key, name, ...restField}) => (
                    <div key={key} className="containerH fade-in" style={{alignItems: "top"}}>
                      <Button
                        type="link"
                        style={{flexGrow: 0, flexShrink: 0, marginRight: 7}}
                        danger
                        icon={<MinusCircleOutlined />}
                        onClick={() => {
                          remove(name);
                          setIsModifiedAndSaveForm(true);
                        }}
                      />
                      <div
                        key={key}
                        className="containerH"
                        style={{gap: 8, alignItems: "baseline", flexWrap: "wrap"}}>
                        <strong>La réponse</strong>
                        <InputElement.Select
                          type={"link"}
                          name={[name, "formField"]}
                          placeholder="nom du champ de formulaire"
                          rules={[{required: true}]}
                          formItemProps={{style: {marginBottom: 0, minWidth: 100}}}
                          options={flatFormInputs(project.formComponents).map((formInput) => ({
                            value: formInput.key,
                            label: formInput.label,
                          }))}
                          showSearch
                        />
                        <strong>sera affichée dans la colonne</strong>
                        <TextInput
                          name={[name, "columnName"]}
                          placeholder="nom de la colonne"
                          formItemProps={{style: {marginBottom: 0}}}
                          rules={[{required: true}]}
                        />
                        <InputElement.Select
                          name={[name, "inPdfExport"]}
                          placeholder="Ajouter aux exports PDF ?"
                          formItemProps={{style: {marginBottom: 0}}}
                          options={[
                            {value: true, label: "Ajouter aux exports PDF"},
                            {value: false, label: "Ne pas ajouter aux exports PDF"},
                          ]}
                        />
                        <InputElement.Select
                          name={[name, "isPhone"]}
                          placeholder="C'est un téléphone ?"
                          formItemProps={{style: {marginBottom: 0}}}
                          options={[
                            {value: true, label: "C'est un téléphone"},
                            {value: false, label: "Ce n'est pas un téléphone"},
                          ]}
                        />

                        {mappingForm.getFieldValue("formMapping")[name]?.isPhone && (
                          <TextInput.Area
                            label={"Modèle de message par défaut pour les SMS"}
                            tooltip={
                              <>
                                En téléchargeant l'application NOÉ, vous pourrez envoyer facilement
                                des SMS groupés à vos participant·es. Ceci est le message par défaut
                                qui sera écrit (et que vous pourrez modifier avant d'envoyer).
                                Utilisez les balises spéciales pour personaliser votre message :
                                <ul>
                                  {Object.entries(smsMessageTags).map(([key, val]) => (
                                    <li key={key}>
                                      <strong>{key} :</strong> {val}
                                    </li>
                                  ))}
                                </ul>
                              </>
                            }
                            placeholder={
                              "Bonjour, tu es inscrite à la session {{sessionName}} qui commence à {{startDate}} jusqu'à {{endDate}}.\nVoici un petit rappel pour que tu ne l'oublie pas !"
                            }
                            name={[name, "defaultTextMessage"]}
                            formItemProps={{
                              style: {marginBottom: 0, marginTop: 3, flexBasis: "100%"},
                              className: "fade-in",
                            }}
                          />
                        )}
                        {mappingForm.getFieldValue("formMapping").length - 1 > name && (
                          <Divider style={{marginTop: 12}} />
                        )}
                      </div>
                    </div>
                  ))}
                  <Form.Item>
                    <Button
                      type="dashed"
                      style={{marginTop: 18}}
                      onClick={() => add()}
                      icon={<PlusOutlined />}>
                      Ajouter un mapping
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </FormElement>
        ) : (
          <div style={{color: "grey"}}>
            Vous n'avez pas encore créé de questions dans votre formulaire d'inscription. Créez des
            questions ci-dessous pour pouvoir utiliser le mapping.
          </div>
        )}
      </CardElement>

      <CardElement title="Formulaire d'inscription">
        <Pending.Suspense>
          <FormEditor
            saveFormData={saveFormData}
            formRef={formBuilderRef}
            setIsModified={setIsModified}
            delaySaveTipMessage={delaySaveTipMessage}
            messageNotYetDisplayed={messageNotYetDisplayed}
          />
        </Pending.Suspense>
      </CardElement>
    </>
  );
};
