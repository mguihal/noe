import React, {useMemo} from "react";
import {Alert} from "antd";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {addKeyToItemsOfList} from "../../helpers/tableUtilities";
import {TableElement} from "../common/TableElement";
import {listSorter} from "../../helpers/listUtilities";
import dayjs from "dayjs";
import Paragraph from "antd/es/typography/Paragraph";
import {CardElement} from "../common/layout/CardElement";
import {URLS} from "../../app/configuration";
import {TextInput} from "../common/inputs/TextInput";
import {useUserTour} from "../../helpers/userTourUtilities";
import helloAssoUserTour from "../../helpers/userTours/helloAssoUserTour";

const columnsHelloAssoEvents = [
  {
    title: "Nom de l'événement",
    dataIndex: "title",
    sorter: (a, b) => listSorter.text(a.title, b.title),
    defaultSortOrder: "descend",
    width: 500,
  },
  {
    title: "Début",
    dataIndex: "startDate",
    sorter: (a, b) => listSorter.date(a.startDate, b.startDate),
    render: (text, record) => (text !== undefined ? dayjs(text).format("LL") : text),
    width: 170,
  },
  {
    title: "Description",
    dataIndex: "description",
    width: 1000,
  },
];

export function TicketingConfig({isModified, setIsModified}) {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const ticketingIsSetUp = {
    helloAsso:
      project.helloAsso?.selectedEvent &&
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret,
    tiBillet: project.tiBillet?.serverUrl && project.tiBillet?.apiKey,
    customTicketing: project.customTicketing?.length > 0,
  };
  const ticketingIsOperational = project.ticketingMode && ticketingIsSetUp[project.ticketingMode];

  useUserTour("helloAsso", helloAssoUserTour(), {
    shouldShowNow: () =>
      project.ticketingMode === "helloAsso" && window.location.hash === "#ticketing",
    deps: [project.ticketingMode, window.location.hash],
  });

  const setHelloAssoEvent = (selectedRowObject) => {
    dispatch(
      projectsActions.changeEditing({
        helloAsso: {...project.helloAsso, selectedEvent: selectedRowObject[0].key},
      })
    );
    setIsModified(true);
  };

  const onTicketingValuesConfigChange = (_, allValues) => {
    dispatch(projectsActions.changeEditing(allValues));
    setIsModified(true);
  };

  const rowSelectionHelloAssoEvent = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setHelloAssoEvent(selectedRowObject);
    },
    type: "radio",
  };

  const TicketingCardElement = useMemo(
    () =>
      ({ticketingMode, children, ...props}) =>
        project.ticketingMode === ticketingMode ? (
          <CardElement {...props} className="fade-in" style={{marginBottom: 0, marginTop: 16}}>
            <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
              {children}
            </FormElement>
          </CardElement>
        ) : null,
    [project?.ticketingMode]
  );

  return (
    <>
      {ticketingIsOperational && !isModified && (
        <Alert
          style={{marginBottom: 26}}
          message="La billetterie est activée."
          showIcon
          type="success"
        />
      )}

      <CardElement>
        <div className="container-grid">
          <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
            <InputElement.Select
              className={"userTourTicketingOptions"}
              label="Type de billetterie"
              name="ticketingMode"
              options={[
                {value: null, label: "- Aucune -"},
                {value: "tiBillet", label: "TiBillet"},
                {value: "helloAsso", label: "Hello Asso"},
                {value: "customTicketing", label: "Validation par appel d'une URL"},
              ]}
            />
          </FormElement>

          <TicketingCardElement title="TiBillet" ticketingMode="tiBillet">
            <div className="container-grid three-per-row">
              <TextInput
                label="URL de la billetterie TiBillet"
                name={["tiBillet", "serverUrl"]}
                placeholder="https://url-de-la-billetterie-tibillet.org"
                rules={[{type: "url"}]}
              />
              <TextInput
                label="Slug de l'événement"
                name={["tiBillet", "eventSlug"]}
                placeholder="mon-evenemement-102123-xxxx"
              />
              <TextInput.Password
                label="Clé API"
                name={["tiBillet", "apiKey"]}
                placeholder="clé api"
              />
            </div>
          </TicketingCardElement>

          <TicketingCardElement
            title="Validation par appel d'une URL"
            ticketingMode="customTicketing">
            <Alert
              style={{marginBottom: 26}}
              message="Vous pouvez valider n'importe quel type de numéro de billet avec NOÉ."
              description={
                <>
                  <p>
                    NOÉ peut appeler une URL particulière pour valider des numéros de billets en
                    tous genres rentrés par les participant⋅es.
                  </p>
                  <Paragraph>
                    <p>La requête envoyée sera la suivante :</p>
                    <ul>
                      <li>
                        <strong>Type :</strong> POST
                      </li>
                      <li>
                        <strong>URL de destination :</strong> {project.customTicketing || " -"}
                      </li>
                      <li>
                        <strong>Corps de la requête :</strong>
                        <pre>{'{ ticketId: "XXXXXXX" }'}</pre>
                      </li>
                    </ul>
                    <p>
                      Toute réponse avec un <strong>code d'état HTTP égal à 200</strong> sera
                      interprété comme une validation du numéro de billet. Toute code d'état autre
                      que 200 sera interprétée comme une invalidation.
                    </p>
                    <p>Le corps de la réponse peut contenir les éléments suivants :</p>
                    <pre>
                      {"{"}
                      <br />
                      {"  "}name: <i>string</i>, # nom de l'article acheté
                      <br />
                      {"  "}amount: <i>number</i>, # montant
                      <br />
                      {"  "}lastName: <i>number</i>, # nom de la personne, facultatif
                      <br />
                      {"  "}firstName: <i>number</i> # prénom de la personne, facultatif
                      <br />
                      {"}"}
                    </pre>
                    <p>
                      Vous pouvez faire des tests avec l'URL suivante, qui validera n'importe quel
                      numéro de billet, sauf si l'indentifiant du billet vaut "
                      <span style={{fontFamily: "monospace"}}>fail</span>" :
                    </p>
                    <a href={`${URLS.API}/projects/${project._id}/ticketing/test`}>
                      {URLS.API}/projects/{project._id}/ticketing/test
                    </a>
                  </Paragraph>
                </>
              }
              type="info"
            />
            <TextInput
              label="URL de validation des billets"
              name="customTicketing"
              placeholder="https://mon-url-de-validation-de-ticket.com"
              rules={[{type: "url"}]}
            />
          </TicketingCardElement>

          <TicketingCardElement title="Hello Asso" ticketingMode="helloAsso">
            {!project.helloAsso?.selectedEvent &&
              project.helloAsso?.organizationSlug &&
              !isModified && (
                <Alert
                  type="warning"
                  showIcon
                  style={{marginBottom: 26}}
                  className="bounce-in"
                  message="Plus qu'une étape pour finir la configuration !"
                  description="Veillez sélectionner une billetterie Hello Asso avec laquelle lier votre événement, puis Enregistrer les modifications."
                />
              )}
            {project.helloAsso?.organizationSlug && (
              <Alert
                showIcon
                style={{marginBottom: 26}}
                className="bounce-in"
                message="Une dernière étape pour intégrer Hello Asso !"
                description={
                  <>
                    <p>
                      <strong>
                        NOÉ a besoin que Hello Asso l'avertisse lorsqu'un paiement est réalisé.
                      </strong>
                      Cela permettra à NOÉ de rentrer automatiquement les numéros de billet de
                      vos·es lorsqu'iels paient sur NOÉ (pratique !). En plus, cela permet aussi à
                      NOÉ d'envoyer un email d'invitation ou de rappel aux particpant·es avec les
                      informations sur leurs billets.
                    </p>
                    <p>
                      Pour cela,{" "}
                      <strong>
                        accédez à votre espace Hello Asso, puis allez dans l'onglet "Mon Compte" >
                        "Intégrations et API"
                      </strong>
                      .
                    </p>
                    <p>Enfin, copiez-collez l'URL ci-dessous dans la partie "Notifications" :</p>
                    <Paragraph
                      copyable
                      style={{
                        color: "var(--noe-primary)",
                        backgroundColor: "white",
                        width: "fit-content",
                        borderRadius: 8,
                        padding: 8,
                        marginBottom: 0,
                      }}>
                      {`${URLS.API}/projects/${project._id}/ticketing/onHelloAssoOrder`}
                    </Paragraph>
                  </>
                }
              />
            )}
            <div className="container-grid two-per-row">
              <TextInput.Password
                label="Client Id"
                name={["helloAsso", "clientId"]}
                placeholder="client id"
              />
              <TextInput.Password
                label="Client Secret"
                name={["helloAsso", "clientSecret"]}
                placeholder="client secret"
              />
            </div>
            {project.helloAsso?.organizationSlug && (
              <>
                <div style={{marginBottom: 26}}>
                  <strong>Organisation trouvée :</strong> {project.helloAsso?.organizationSlug}
                </div>
                <TableElement.Simple
                  showHeader
                  scroll={{x: (columnsHelloAssoEvents.length - 1) * 160 + 70}}
                  rowSelection={rowSelectionHelloAssoEvent}
                  selectedRowKeys={[{key: project.helloAsso?.selectedEvent}]}
                  setSelectedRowKeys={setHelloAssoEvent}
                  columns={columnsHelloAssoEvents}
                  rowKey="formSlug"
                  dataSource={addKeyToItemsOfList(
                    project.helloAsso?.selectedEventCandidates,
                    "formSlug"
                  )}
                />
              </>
            )}
          </TicketingCardElement>
        </div>
      </CardElement>
    </>
  );
}
