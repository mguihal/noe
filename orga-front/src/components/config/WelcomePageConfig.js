import {Pending} from "../common/layout/Pending";
import React, {lazy} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const WelcomePageEditor = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../common/welcomePageEditor/WelcomePageEditor")
);

export const WelcomePageConfig = ({setIsModified, messageNotYetDisplayed}) => {
  const dispatch = useDispatch();

  const project = useSelector(projectsSelectors.selectEditing);

  const onChangeWelcomePage = (value) => {
    setIsModified(true);
    messageNotYetDisplayed.current = false;
    dispatch(projectsActions.changeEditing({content: value}));
  };

  return (
    <Pending.Suspense>
      <WelcomePageEditor value={project.content} onChange={onChangeWelcomePage} />
      <div style={{minHeight: 400}}></div>
    </Pending.Suspense>
  );
};
