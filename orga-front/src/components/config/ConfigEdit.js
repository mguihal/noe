import React, {useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, message} from "antd";
import {safeValidateFields} from "../common/FormElement";
import {TabsPage} from "../common/TabsPage";
import {ToolOutlined} from "@ant-design/icons";
import {projectsActions} from "../../features/projects.js";
import {MembersConfig} from "./MembersConfig";
import {debounce} from "../../helpers/viewUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {GeneralConfig} from "./GeneralConfig";
import {TicketingConfig} from "./TicketingConfig";
import {FormConfig} from "./FormConfig";
import {WelcomePageConfig} from "./WelcomePageConfig";
import {Pending} from "../common/layout/Pending";
import {useUserTour} from "../../helpers/userTourUtilities";
import configUserTour from "../../helpers/userTours/configUserTour";
import {lazyWithRetry} from "../../helpers/lazyWithRetry";
const AdvancedConfig = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./AdvancedConfig"));

export function ConfigEdit() {
  const dispatch = useDispatch();
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const [isModified, setIsModified] = useState(false);
  const messageNotYetDisplayed = useRef();
  const isUserAdminOnThisProject = currentRegistration.role === "admin";

  useUserTour("config", configUserTour(currentRegistration));

  const [mappingForm] = Form.useForm();
  const formBuilderRef = useRef();
  const saveFormData = () => {
    // Save formIO data
    if (formBuilderRef.current) {
      dispatch(
        projectsActions.changeEditing({
          formComponents: formBuilderRef.current.map((c) => ({...c})),
        })
      );
    }
  };

  const onConfigSaveButtonClick = () => {
    safeValidateFields(mappingForm, undefined, () => {
      message.error("Attention, le mapping des champs de formulaire est incomplet.");
    });
    saveFormData();
    isModified && delaySaveTipMessage();
    dispatch(projectsActions.persist());
    message.destroy("notSaved");
    setIsModified(false);
  };

  const delaySaveTipMessage = debounce((triggerNewMessage) => {
    if (messageNotYetDisplayed.current) {
      message.open({
        key: "notSaved",
        content: (
          <div className="containerH buttons-container">
            <span>N'oublie pas de sauver tes modifications :)</span>
            <Button type="primary" onClick={onConfigSaveButtonClick}>
              Enregistrer
            </Button>
          </div>
        ),
        duration: 10,
      });
      messageNotYetDisplayed.current = false;
    }
  }, 1000 * 60);

  return (
    <TabsPage
      title="Configuration de l'événement"
      icon={<ToolOutlined />}
      fullWidth
      forceModifButtonActivation={isModified}
      onValidation={onConfigSaveButtonClick}
      items={[
        {
          label: "Général",
          key: "main",
          className: "with-margins",
          children: <GeneralConfig setIsModified={setIsModified} />,
        },
        {
          label: "Membres",
          key: "members",
          className: "with-margins",
          children: <MembersConfig isAdmin={isUserAdminOnThisProject} />,
        },
        {
          label: "Page d'accueil",
          key: "welcome-page",
          children: (
            <WelcomePageConfig
              messageNotYetDisplayed={messageNotYetDisplayed}
              setIsModified={setIsModified}
            />
          ),
        },
        {
          label: "Formulaire d'inscription",
          key: "form",
          className: "with-margins",
          children: (
            <FormConfig
              saveFormData={saveFormData}
              formBuilderRef={formBuilderRef}
              mappingForm={mappingForm}
              setIsModified={setIsModified}
              messageNotYetDisplayed={messageNotYetDisplayed}
              delaySaveTipMessage={delaySaveTipMessage}
            />
          ),
        },
        isUserAdminOnThisProject && {
          label: "Billetterie",
          key: "ticketing",
          className: "with-margins",
          children: <TicketingConfig isModified={isModified} setIsModified={setIsModified} />,
        },
        isUserAdminOnThisProject && {
          label: "Avancé",
          key: "advanced",
          className: "with-margins",
          children: (
            <Pending.Suspense animationDelay={"300ms"}>
              <AdvancedConfig setIsModified={setIsModified} />
            </Pending.Suspense>
          ),
        },
      ]}
    />
  );
}
