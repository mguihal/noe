import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, message, Space, Steps} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import dayjs from "dayjs";
import {OpeningState, projectsActions, projectsSelectors} from "../../features/projects.js";
import {dataToFields, fieldToData} from "../../helpers/tableUtilities";
import {CardElement} from "../common/layout/CardElement";
import {listRenderer} from "../../helpers/listUtilities";
import {TimeInput} from "../common/inputs/TimeInput";
import {TextInput} from "../common/inputs/TextInput";
import {SliderInput} from "../common/inputs/SliderInput";
import {SwitchInput} from "../common/inputs/SwitchInput";
import {Availability} from "../utils/Availability";
import {ColorInput} from "../common/inputs/ColorInput";

export function GeneralConfig({setIsModified}) {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const projectIsNotOpened = project.openingState === "notOpened";

  const onValuesChange = (changedFields, allFields) => {
    dispatch(projectsActions.changeEditing(allFields));
    setIsModified(true);
  };

  const onOpeningStateChange = (newStateIndex) => {
    const newState = Object.keys(OpeningState).find((key) => OpeningState[key] === newStateIndex);
    const projectData =
      newStateIndex >= 2 ? {openingState: newState, useAI: false} : {openingState: newState};
    if (newStateIndex >= 2 && project.useAI) {
      message.warning(
        "Vous lancez vos inscriptions : par sécurité, nous désactivons automatiquement l'IA. Vous pouvez la réactiver dans les paramètres avancés.",
        15
      );
    }
    if (newStateIndex >= 3 && !project.isPublic) {
      message.info(
        <span>
          Vous lancez vos inscriptions : voulez-vous rendre l'événement public sur NOÉ ?{" "}
          <Button
            type={"primary"}
            style={{marginLeft: 8}}
            target="_blank"
            onClick={() => {
              dispatch(projectsActions.changeEditing({isPublic: true}));
              message.destroy();
            }}
            rel="noreferrer">
            Je rends public !
          </Button>
        </span>
      );
    }
    dispatch(projectsActions.changeEditing(projectData));
    setIsModified(true);
  };

  return (
    <FormElement
      initialValues={{
        ...project,
        breakfastTime: dayjs(project.breakfastTime),
        lunchTime: dayjs(project.lunchTime),
        dinnerTime: dayjs(project.dinnerTime),
      }}
      onValuesChange={onValuesChange}>
      <div className="container-grid two-per-row">
        <div className="container-grid">
          <CardElement>
            <TextInput label="Nom de l'événement" name="name" placeholder="nom" />
          </CardElement>

          <div className="userTourProjectAvailabilities containerV">
            <Availability
              title="Plages d'ouverture de l'événement"
              setIsModified={setIsModified}
              entity={project}
              actions={projectsActions}
            />
          </div>

          <CardElement title="Gestion de l'événément">
            <div className="container-grid">
              <InputElement.Custom label="Étapes de l'événement">
                <Steps
                  current={OpeningState[project.openingState]}
                  direction={"vertical"}
                  onChange={onOpeningStateChange}
                  items={[
                    {
                      title: "Fermé",
                      key: "0",
                      description: "Ni ouvert aux inscriptions, ni ouvert aux préinscriptions.",
                    },
                    {
                      title: "Pré-inscriptions",
                      key: "1",
                      description:
                        "Les personnes peuvent se pré-inscrire en remplissant leurs dates de présence, le formulaire d'inscription et en payant.",
                    },
                    {
                      title: "Inscriptions des encadrant⋅es",
                      key: 2,
                      description:
                        "Les encadrant⋅es peuvent s'inscrire aux sessions en avance, mais les participant⋅es n'ont encore qu'accès aux pré-inscriptions.",
                    },

                    {
                      title: "Inscriptions pour tous",
                      key: "3",
                      description: "Tout le monde peut s'inscrire aux sessions sur l'événement.",
                    },
                  ]}
                />
              </InputElement.Custom>

              <SwitchInput
                label="L'événement est complet (bloquer les nouvelles inscriptions)"
                tooltip="Permet de bloquer les nouvelles inscription, tout en laissant la possibilité aux personnes déjà inscrites de continuer à accéder à leur inscription."
                name="full"
                disabled={projectIsNotOpened}
              />

              <SwitchInput
                label="Rendre l'événement public"
                tooltip="L'événement sera visible dans la liste de tous les événements publics de la plateforme, côté participant⋅e."
                name="isPublic"
              />

              <SwitchInput
                label="Garder la programmation secrète"
                tooltip="Rendre le planning et la page d'accueil secrètes, c'est à dire accessibles seulement par les participant.es dont l'inscription est totalement validée."
                name="secretSchedule"
              />
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <CardElement title="Gestions des inscriptions">
            <div className="container-grid">
              <SwitchInput
                label="Interdire les inscriptions sur plusieurs sessions qui se chevauchent"
                name="notAllowOverlap"
              />

              <SwitchInput
                label="Bloquer toutes les inscriptions et désinscriptions"
                tooltip="Les participant⋅es pourront consulter leur planning sans le modifier. Les orgas peuvent toujours s'inscrire ou se désinscrire."
                name="blockSubscriptions"
              />

              <SwitchInput
                label="Bloquer la désinscription des sessions de bénévolat lorsqu'elles ont lieu dans moins de 48 heures"
                disabled={project.blockSubscriptions}
                tooltip="Si non bloqué, un message de confirmation avec un avertissement apparaîtra si la personne essaie de se désinscrire."
                name="blockVolunteeringUnsubscribeIfBeginsSoon"
              />

              <SliderInput
                label="Nombre d'heures minimum / maximum de bénévolat par jour conseillé"
                name="minMaxVolunteering"
                range
                inputProps={{tooltip: {formatter: (val) => listRenderer.durationFormat(val, true)}}}
                step={15}
                max={480}
                marks={{
                  0: "0h",
                  60: "1h",
                  120: "2h",
                  180: "3h",
                  240: "4h",
                  300: "5h",
                  360: "6h",
                  420: "7h",
                  480: "8h",
                }}
              />
            </div>
          </CardElement>

          <CardElement title="Heure des repas">
            <div className="container-grid">
              <p>
                Pour calculer les <strong>jours de présence</strong>, NOÉ se base sur les heures de
                repas du midi et du soir, en partant du principe que si une personne est présente
                pour au moins un des deux repas du jour, elle est considérée comme "présente" ce
                jour-là.
              </p>
              <div className="container-grid three-per-row">
                <TimeInput label="Petit déjeuner" name="breakfastTime" />
                <TimeInput label="Déjeuner" name="lunchTime" />
                <TimeInput label="Dîner" name="dinnerTime" />
              </div>
            </div>
          </CardElement>

          <CardElement title="Thème">
            <div className="container-grid three-per-row">
              <ColorInput label="Principale" name={["theme", "primary"]} />
              <ColorInput label="Arrière plan" name={["theme", "bg"]} />
              <InputElement.Custom label={"Accentuation"}>
                <Space>
                  <ColorInput name={["theme", "accent1"]} formItemProps={{noStyle: true}} />
                  <ColorInput name={["theme", "accent2"]} formItemProps={{noStyle: true}} />
                </Space>
              </InputElement.Custom>
            </div>
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
}
