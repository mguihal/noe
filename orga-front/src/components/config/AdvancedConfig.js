import React from "react";
import {Alert, Button, Form, Popconfirm, Tabs} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {NumberInput} from "../common/inputs/NumberInput";
import {FormElement} from "../common/FormElement";
import {ImportExport} from "./advanced/ImportExport";
import {FeaturesFormContent} from "./advanced/FeaturesFormContent";
import ModificationsHistoryTimeline from "../common/layout/ModificationsHistoryTimeline";
import {getLocalStorage} from "../../helpers/localStorageUtilities";

const TestCrashAppButton = () => {
  console.log(
    "test",
    typeof getLocalStorage("testMode", false),
    getLocalStorage("testMode", false)
  );
  return getLocalStorage("testMode", false) ? (
    <Button onClick={(unknown) => (unknown.variable.that.crashes = unknown)}>
      Crash the app (test)
    </Button>
  ) : null;
};

export default function AdvancedConfig({setIsModified}) {
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();
  const [rollbackForm] = Form.useForm();

  const rollback = () => {
    dispatch(projectsActions.rollback(rollbackForm.getFieldValue("rollbackPeriod")));
  };

  const deleteProject = () => {
    dispatch(projectsActions.remove());
  };

  return (
    <Tabs
      items={[
        {label: "Import & Export", key: "import-export", children: <ImportExport />},
        {
          label: "Fonctionnalités",
          key: "features",
          children: (
            <FormElement>
              <FeaturesFormContent setIsModified={setIsModified} />
            </FormElement>
          ),
        },
        {
          label: "Oops!",
          key: "rollback",
          children: (
            <FormElement form={rollbackForm}>
              <Alert
                style={{marginBottom: 10}}
                message="Fonctionnalité expérimentale."
                description="Cela peut faire dysfonctionner votre projet. Le résultat n'est pas garanti."
                showIcon
                type="warning"
              />
              <Alert
                style={{marginBottom: 10}}
                message="Ici, vous pouvez récupérer des données supprimées. Il suffit de mettre jusqu'à combien de minutes en arrière vous souhaitez retourner."
                type="info"
              />
              <NumberInput
                min={0}
                name="rollbackPeriod"
                defaultValue={0}
                placeholder="minute"
                label="Nombre de minutes à revenir en arrière"
              />
              <Popconfirm
                title="Si vous ne savez pas ce que vous faites, ne le faites pas."
                onConfirm={rollback}
                okText="Je suis conscient⋅e des risques"
                okButtonProps={{danger: true}}
                cancelText="Annuler">
                <Button type="primary" danger>
                  Récupérer les éléments supprimés
                </Button>
              </Popconfirm>
            </FormElement>
          ),
        },
        {
          label: "Historique de modification",
          key: "history",
          children: (
            <ModificationsHistoryTimeline
              elementsActions={projectsActions}
              record={project}
              root=".."
            />
          ),
        },
        {
          label: "Suppression du projet",
          key: "delete",
          children: (
            <>
              <Alert
                style={{marginBottom: 26}}
                type={"warning"}
                showIcon
                message={
                  <>
                    Attention, <strong>la suppression du projet est irréversible</strong> et
                    engendrera également la suppression des catégories, espaces, encadrant⋅es,
                    activités et sessions liés au projet.
                  </>
                }
              />
              <Popconfirm
                title={
                  "Ëtes-vous sûr de vouloir supprimer l'entièreté du projet " + project.name + " ?"
                }
                onConfirm={deleteProject}
                okText="Oui, je veux définitivement supprimer le projet"
                cancelText="Non, je veux revenir en arrière">
                <Button type="primary" danger icon={<DeleteOutlined />}>
                  Supprimer le projet
                </Button>
              </Popconfirm>
              <TestCrashAppButton />
            </>
          ),
        },
      ]}></Tabs>
  );
}
