import React from "react";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../features/projects";
import {Form, Tabs} from "antd";
import RegistrationForm from "../../common/RegistrationForm";
import FormIoBuilder from "./FormIoBuilder";
import {BetaTag} from "../../common/layout/BetaTag";
import FormEditorHelp from "./FormEditorHelp";
import {EditOutlined, EyeOutlined} from "@ant-design/icons";
import {ClosableAlert} from "../../common/layout/ClosableAlert";

const FormEditor = ({
  setIsModified,
  formRef,
  saveFormData,
  delaySaveTipMessage,
  messageNotYetDisplayed,
}) => {
  const project = useSelector(projectsSelectors.selectEditing);

  const [previewForm] = Form.useForm();
  return (
    <Tabs
      destroyInactiveTabPane
      onChange={() => saveFormData?.()}
      items={[
        {
          label: (
            <>
              <EditOutlined style={{marginRight: 4}} />
              Éditer
            </>
          ),
          key: "edit",
          children: (
            <>
              <ClosableAlert
                localStorageKey={"formBestPracticesAndWarnings"}
                showIcon
                style={{marginBottom: 26}}
                description={
                  <>
                    <p>
                      Les participant⋅es se créent un compte en renseignant leur{" "}
                      <strong>nom, prénom et email</strong>. Il est donc inutile de leur redemander
                      ces informations dans le formulaire d'inscription.
                    </p>
                    Aussi, NOÉ n'est pas habilité à héberger les données sensibles des
                    participant⋅es (données médicales, notamment). Si pour une raison ou une autre,
                    vous devez en collecter, la responsabilité de NOÉ ne pourra être engagée en cas
                    de problème.
                  </>
                }
              />
              <FormIoBuilder
                setIsModified={setIsModified}
                formRef={formRef}
                delaySaveTipMessage={delaySaveTipMessage}
                messageNotYetDisplayed={messageNotYetDisplayed}
              />
            </>
          ),
        },
        {
          label: (
            <>
              <EyeOutlined style={{marginRight: 4}} /> Prévisualiser
            </>
          ),
          key: "preview",
          children: <RegistrationForm formComponents={project.formComponents} form={previewForm} />,
        },
        {
          label: "Aide",
          key: "help",
          children: <FormEditorHelp />,
        },
      ]}
    />
  );
};

export default FormEditor;
