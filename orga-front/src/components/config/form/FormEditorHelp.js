import {Alert, Form, Tabs} from "antd";
import {CardElement} from "../../common/layout/CardElement";
import RegistrationForm from "../../common/RegistrationForm";
import React from "react";
import {useTranslation} from "react-i18next";
import {useWindowDimensions} from "../../../helpers/viewUtilities";

const FormEditorHelp = () => {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const {isMobileView} = useWindowDimensions();
  const addGenericProps = (fields) =>
    fields.map((field) => ({
      ...field,
      validate: {required: false},
      conditional: {show: null, when: null, eq: ""},
    }));

  return (
    <Tabs
      tabPosition={isMobileView ? "top" : "left"}
      items={[
        {
          label: "Composants disponibles",
          key: "components",
          children: (
            <>
              <Alert
                message={
                  "Vous trouverez ici un aperçu de tous les composants de formulaire disponibles sur NOÉ."
                }
                style={{marginBottom: 26}}
              />
              <CardElement title={t("projects:formBuilder.basics")}>
                <RegistrationForm
                  formComponents={addGenericProps([
                    {
                      label: "Text Field",
                      key: "textField",
                      type: "textfield",
                      description: "Champ texte court",
                    },
                    {
                      label: "Text Area",
                      key: "textArea",
                      type: "textarea",
                      description: "Champ texte long",
                    },
                    {label: "Number", key: "number", type: "number"},
                    {label: "Checkbox", key: "checkbox", type: "checkbox"},
                    {
                      label: "Select Boxes",
                      values: [
                        {label: "Option 1", value: "option1"},
                        {label: "Option 2", value: "option2"},
                        {label: "Option 3", value: "option3"},
                      ],
                      key: "selectBoxes",
                      type: "selectboxes",
                      description: "Champ sélection multiple",
                    },
                    {
                      label: "Select",
                      data: {
                        values: [
                          {label: "Option 1", value: "option1"},
                          {label: "Option 2", value: "option2"},
                          {label: "Option 3", value: "option3"},
                        ],
                      },
                      key: "select",
                      type: "select",
                      description: "Champ sélection simple",
                    },
                    {
                      label: "Radio",
                      values: [
                        {label: "Option 1", value: "option1"},
                        {label: "Option 2", value: "option2"},
                        {label: "Option 3", value: "option3"},
                      ],
                      key: "radio",
                      type: "radio",
                      description: "Champ sélection simple",
                    },
                  ])}
                  form={form}
                />
              </CardElement>
              <CardElement title={t("projects:formBuilder.advanced")}>
                <RegistrationForm
                  formComponents={addGenericProps([
                    {
                      label: "Url",
                      key: "url",
                      type: "url",
                      description: "Champ n'acceptant que des URLs valides",
                    },
                    {
                      label: "Phone Number",
                      key: "phoneNumber",
                      type: "phoneNumber",
                      description: "Champ n'acceptant que des numéros de téléphone valides",
                    },
                    {
                      label: "Date / Time",
                      key: "dateTime",
                      type: "datetime",
                      description: "Champ date + heure",
                    },
                    {
                      label: "Day",
                      key: "day",
                      type: "day",
                      description: "Champ date seulement",
                    },
                  ])}
                  form={form}
                />
              </CardElement>

              <CardElement
                title={t("projects:formBuilder.layout")}
                style={{marginBottom: 0}}
                bodyStyle={{marginBottom: -52}}>
                <RegistrationForm
                  formComponents={addGenericProps([
                    {
                      html: '<h3>Content</h3><p>Utilisez le composant Content pour communiquer des informations importantes à vos participant·es au sein de votre formulaire. C\'est ici que vous pourrez capter le plus l\'attention de vos utilisatuer·ices.</p><p><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);"><strong>Lorem ipsum dolor sit amet</strong>, <i>consectetur adipiscing elit</i>. </span><a href="https://en.wikipedia.org/wiki/Lorem_ipsum"><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);">Sed non risus</span></a><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);">.&nbsp;</span></p><ul><li><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);">Suspendisse lectus tortor,&nbsp;</span></li><li><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);">dignissim sit amet, adipiscing nec, ultricies sed, dolor.&nbsp;</span></li></ul><ol><li><span style="background-color:rgb(255,255,255);color:rgb(32,33,34);">Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</span></li></ol>',
                      label: "Content",
                      key: "content",
                      type: "content",
                    },
                    {
                      key: "Panel",
                      title: "Panel",
                      type: "panel",
                      label: "Panel",
                      components: addGenericProps([
                        {
                          html: "Utilisez le composant Panel pour structurer visuellement votre formulaire, en créant des blocs de questions distincts.",
                          label: "Content",
                          key: "content",
                          type: "content",
                        },
                      ]),
                    },
                  ])}
                  form={form}
                />
              </CardElement>
            </>
          ),
        },
        {
          label: "Champs conditionnels",
          key: "conditionalFields",
          children: (
            <>
              <Alert
                message={"Où trouver les champs conditionnels ?"}
                description={
                  <>
                    Vous pouvez utilisez des champs conditionnels dans l'onglet{" "}
                    <strong>"Conditional"</strong> de la configuration de votre composant.
                  </>
                }
                style={{marginBottom: 26}}
              />
              <p>Dans l'onglet "Conditional", il faut remplir les trois champs visibles :</p>

              <CardElement title={<strong>This component should Display</strong>}>
                <p>Quand afficher votre composant.</p>
                <ul>
                  <li>
                    True si on ne veut pas afficher le composant par défaut et l'afficher seulement
                    quand la condition est remplie.
                  </li>
                  <li>
                    False si on veut afficher le composant par défaut et le cacher seulement quand
                    la condition est remplie.
                  </li>
                </ul>
              </CardElement>
              <CardElement title={<strong>When the form component</strong>}>
                <p>La question dont dépendra votre condition.</p>
              </CardElement>
              <CardElement title={<strong>Has the value</strong>}>
                <p>
                  La ou les valeurs du composant qui activeront votre condition. En fonction du type
                  du composant que vous avez choisi à la question 2, les conditions peuvent être
                  simples ou avancées :
                </p>
                <CardElement size={"small"} title={"Conditions simples"}>
                  <p>
                    Pour ces champs, donnez simplement la valeur choisie pour activer la condition.
                  </p>
                  <ul>
                    <li>
                      Text Field, Text Area : la valeur exacte à obtenir{" "}
                      <i>(ex: "un deux trois !")</i>
                    </li>
                    <li>
                      Checkbox : true pour coché, false pour non-coché <i>(ex: "true")</i>
                    </li>
                    <li>
                      Number : utilisez le point pour rentrer un chiffre décimal{" "}
                      <i>(ex: "4" ou "5.6")</i>
                    </li>
                  </ul>
                </CardElement>
                <CardElement size={"small"} title={"Conditions avancées"}>
                  <p>
                    Pour ces champs, utilisez les valeurs exactes des clés associées aux réponses
                    voulues. Elles se trouvent dans l'onglet "Data" du composant, colonne "Value".
                    <br />
                    <i>ex: "jeSuisMajeur" pour sélectionner la réponse "Je suis majeur"</i>.
                  </p>
                  <p>
                    <strong>Séparateur "ou" :</strong> Vous pouvez utiliser le séparateur ";" pour
                    activer la condition si une ou plusieurs valeurs sont sélectionnées.
                    <br />
                    <i>
                      ex: "jeSuisMajeur;jeSuisAccompagne" pour activer la condition si "Je suis
                      majeur" <strong>ou</strong> "Je suis accompagné" est sélectionné.
                    </i>
                  </p>
                  <p>
                    <strong>Séparateur "et" :</strong> Vous pouvez utiliser le séparateur " AND "
                    pour activer la condition si toutes les valeurs demandées sont sélectionnées.{" "}
                    <br />
                    <i>
                      ex: "dispoLe28 AND dispoLe29" pour activer la condition si "Dispo le 28"{" "}
                      <strong>et</strong> "Dispo le 29" sont tous les deux sléectionnés.
                    </i>
                  </p>
                  <p>
                    <strong>Séparateur "ou" + "et" :</strong> Vous pouvez utiliser les deux
                    séparateurs en même temps.
                    <br />
                    <i>
                      ex: "dispoLe28;dispoLe29 AND dispoLe31" pour activer la condition si "Dispo le
                      28" <strong>ou</strong> "Dispo le 29" est sélectionné <strong>et</strong> si
                      "Dispo le 31" est sélectionné.
                    </i>
                  </p>
                  <ul>
                    <li>Radio, Select : sélecteur "ou" disponible</li>
                    <li>Select Boxes : sélecteurs "ou" et "et" disponibles</li>
                  </ul>
                </CardElement>
              </CardElement>
            </>
          ),
        },
      ]}
    />
  );
};

export default FormEditorHelp;
