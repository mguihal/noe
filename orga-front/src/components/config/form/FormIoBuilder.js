import React from "react";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../features/projects";
import {FormBuilder} from "@formio/react";
import {useTranslation} from "react-i18next";

const FormIoBuilder = React.memo(
  ({setIsModified, formRef, delaySaveTipMessage, messageNotYetDisplayed}) => {
    const {t} = useTranslation();
    const project = useSelector(projectsSelectors.selectEditing);

    const getChildrenIdsAndComponents = (parentCompononents) => {
      return parentCompononents.map((childComp) => {
        if (childComp.components?.length > 0) {
          return [childComp.id, ...getChildrenIdsAndComponents(childComp.components)];
        } else {
          return childComp.id;
        }
      });
    };

    const onFormIOChange = (schema) => {
      const idsProject = getChildrenIdsAndComponents(project.formComponents || []);
      const idsForm = getChildrenIdsAndComponents(schema.components);
      if (JSON.stringify(idsProject) !== JSON.stringify(idsForm)) {
        setIsModified(true);
        formRef.current = schema.components;
        messageNotYetDisplayed.current = true;
        delaySaveTipMessage();
      }
    };

    return (
      <FormBuilder
        form={{
          display: "form",
          components: JSON.parse(JSON.stringify(project.formComponents || [])),
        }}
        options={{
          noDefaultSubmitButton: true,

          // Some components are not yet supported and will be added later
          builder: {
            basic: false,
            advanced: false,
            layout: false,
            data: false,
            premium: false,
            basicComponents: {
              title: t("projects:formBuilder.basics"),
              default: true,
              components: {
                textfield: true,
                textarea: true,
                number: true,
                checkbox: true,
                selectboxes: true,
                select: true,
                radio: true,
              },
            },
            advancedComponents: {
              title: t("projects:formBuilder.advanced"),
              components: {
                phoneNumber: true,
                datetime: true,
                day: true,
                url: true,
                // tags: true,
              },
            },
            layoutComponents: {
              title: t("projects:formBuilder.layout"),
              components: {
                panel: true,
                content: true,
              },
            },
          },
        }}
        onChange={onFormIOChange}
      />
    );
  },
  (pp, np) => true
);

export default FormIoBuilder;
