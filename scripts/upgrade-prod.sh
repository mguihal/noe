SERVER_IP=$1
ENV_FILE_FOLDER_NAME=$2
BRANCH_NAME=$3
DELAY_AFTER_INSCRIPTION_FRONT=$4
DELAY_AFTER_API=$5
DELAY_AFTER_ORGA_FRONT=$6
DEPLOY_WEBSITE=$7
DELAY_AFTER_WEBSITE=$8

echo $SERVER_IP $ENV_FILE_FOLDER_NAME $BRANCH_NAME $DELAY_AFTER_INSCRIPTION_FRONT $DELAY_AFTER_API $DELAY_AFTER_ORGA_FRONT $DEPLOY_WEBSITE $DELAY_AFTER_WEBSITE

SCRIPTS_DIRECTORY="$(dirname -- "$0")"

echo "■■■■■■■■■■■■■■■■■■ CONFIGURATION ■■■■■■■■■■■■■■■■■■■"

echo
echo "SERVER_IP: $SERVER_IP"
echo "ENV_FILE_FOLDER_NAME: $ENV_FILE_FOLDER_NAME"
echo "BRANCH_NAME: $BRANCH_NAME"
echo "DELAY_AFTER_INSCRIPTION_FRONT: $DELAY_AFTER_INSCRIPTION_FRONT"
echo "DELAY_AFTER_API: $DELAY_AFTER_API"
echo "DELAY_AFTER_ORGA_FRONT: $DELAY_AFTER_ORGA_FRONT"
echo "DEPLOY_WEBSITE: $DEPLOY_WEBSITE"
echo "DELAY_AFTER_WEBSITE: $DELAY_AFTER_WEBSITE"

echo
echo "■■■■■■■■■■■■■■■ PREPARING DEPLOYMENT ■■■■■■■■■■■■■■■"

echo
echo "■■■■■■■■■■ Updating production configuration from folder ../$ENV_FILE_FOLDER_NAME."

echo "■■■■■ Pulling last changes..."
cd "$SCRIPTS_DIRECTORY/../../$ENV_FILE_FOLDER_NAME" || exit
git pull
cd ../noe || exit

echo "■■■■■ Copying the .env.prod file into /noe..."
cp "../$ENV_FILE_FOLDER_NAME/.env.prod" ".env.prod"
source ".env.prod"

if [ "$DEPLOY_WEBSITE" = "true" ] && [ -n "$WEBSITE_URI" ]
then
  echo "■■■■■ Website deployment is activated at address $WEBSITE_URI."
  SERVICES='api inscription-front orga-front ia-back website'
else
  SERVICES='api inscription-front orga-front ia-back'
fi

echo
echo "■■■■■■■■■■ Updating the NOÉ repository files."

echo "■■■■■ Fetching last changes for the branch \"$BRANCH_NAME\"..."
git fetch origin "$BRANCH_NAME"

echo "■■■■■ Checking if there are any changes to every service in the project..."
CHANGED_FILES_GLOBAL=$(git diff-tree -r --name-only --no-commit-id "origin/$BRANCH_NAME" "$BRANCH_NAME")
if [ -n "$CHANGED_FILES_GLOBAL" ]
then
  echo $CHANGED_FILES_GLOBAL
else
  echo "No changes."
fi


echo "■■■■■ Checking if there are incoming changes to any package.json file in the project..."
CHANGED_PACKAGE_JSON_FILES=$(git diff-tree -r --name-only --no-commit-id "origin/$BRANCH_NAME" "$BRANCH_NAME" | grep "package.json")
if [ -n "$CHANGED_PACKAGE_JSON_FILES" ]
then
  echo $CHANGED_PACKAGE_JSON_FILES
else
  echo "No changes."
fi

echo "■■■■■ Resetting all files on the remote origin of branch \"$BRANCH_NAME\". Checking out branch..."
git reset --hard "origin/$BRANCH_NAME"
git checkout "$BRANCH_NAME"


echo
echo "■■■■■■■■■■ Updating dependencies when needed..."
for service in $SERVICES
do
  if [ "$(echo "$CHANGED_PACKAGE_JSON_FILES" | grep "$service" | wc -c)" -gt 1 ]
  then
    echo "■■■■■ package.json has changed in container $service. Reinstalling node_modules."
    make install-deps-$service
  fi
done

echo
echo "■■■■■■■■■■■■■■■■ READY FOR LAUNCH ! ■■■■■■■■■■■■■■■■"

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep inscription-front | wc -c)" -gt 1 ]
then
  echo
  echo "■■■■■■■■■■ Source files in inscription-front have changed. Restarting container and waiting $DELAY_AFTER_INSCRIPTION_FRONT..."
  make restart-prod-inscription-front && sleep "$DELAY_AFTER_INSCRIPTION_FRONT"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep api | wc -c)" -gt 1 ]
then
  echo
  echo "■■■■■■■■■■ Source files in api have changed. Restarting container and waiting $DELAY_AFTER_API..."
  make restart-prod-api && sleep "$DELAY_AFTER_API"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep orga-front | wc -c)" -gt 1 ]
then
  echo
  echo "■■■■■■■■■■ Source files in orga-front have changed. Restarting orga-front container and waiting $DELAY_AFTER_ORGA_FRONT..."
  make restart-prod-orga-front && sleep "$DELAY_AFTER_ORGA_FRONT"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep website | wc -c)" -gt 1 ] && [ "$DEPLOY_WEBSITE" = "true" ]
then
  echo
  echo "■■■■■■■■■■ Source files in website have changed. Restarting website container..."
  make restart-prod-website && sleep "$DELAY_AFTER_WEBSITE"
fi

echo
echo "■■■■■■■■■■■■■■■■■ RESTART COMPLETE ■■■■■■■■■■■■■■■■■"